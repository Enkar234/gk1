﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace GK1
{
	public class DirectBitmap : IDisposable
	{
		public Bitmap Bitmap { get; private set; }
		public Int32[] Bits { get; private set; }
		public bool Disposed { get; private set; }
		public int Height { get; private set; }
		public int Width { get; private set; }
		public double[,] bufferZ;
		public PriorityQueue[,] tFaces;
		protected GCHandle BitsHandle { get; private set; }

		public DirectBitmap(int width, int height)
		{
			Width = width;
			Height = height;
			Bits = new Int32[width * height];
			BitsHandle = GCHandle.Alloc(Bits, GCHandleType.Pinned);
			Bitmap = new Bitmap(width, height, width * 4, PixelFormat.Format32bppPArgb, BitsHandle.AddrOfPinnedObject());
		}
		public DirectBitmap(DirectBitmap dbmp)
		{
			Width = dbmp.Width;
			Height = dbmp.Height;
			Bits = new Int32[Width * Height];
			dbmp.Bits.CopyTo(Bits, 0);
			BitsHandle = GCHandle.Alloc(Bits, GCHandleType.Pinned);
			Bitmap = new Bitmap(Width, Height, Width * 4, PixelFormat.Format32bppPArgb, BitsHandle.AddrOfPinnedObject());
		}
		public void SetPixel(int x, int y, Color color)
		{
			int index = x + (y * Width);
			int col = color.ToArgb();

			Bits[index] = col;
		}
		public void SetVPixel(int x, int y, Color c, double v)
		{
			if (v > bufferZ[x, y])
			{
				bufferZ[x, y] = v;
				SetPixel(x, y, c);
			}
		}
		public void SetTPixel(int x, int y, Color c, double v,double transI, bool trans)
		{
			if (tFaces[x, y] == null) tFaces[x,y] = new PriorityQueue();
			tFaces[x, y].Insert(v, trans, c);

			int R = 0;
			int G = 0;
			int B = 0;

			foreach (Face f in tFaces[x,y])
			{
				if(f.t)
				{
					R += (int)(f.c.R * transI);
					G += (int)(f.c.G * transI);
					B += (int)(f.c.B * transI);
				}
				else 
				{
					R += f.c.R;
					G += f.c.G;
					B += f.c.B;
					break;
				}
			}
			R = R > 255 ? 255 : R < 0 ? 0 : R;
			G = G > 255 ? 255 : G < 0 ? 0 : G;
			B = B > 255 ? 255 : B < 0 ? 0 : B;
			SetPixel(x, y, Color.FromArgb(R, G, B));
		}
		public void DrawLine(Point from, Point to, Color c, double v)
		{
			int dx = Math.Abs(to.X - from.X);
			int dy = Math.Abs(to.Y - from.Y);
			int kx = to.X > from.X ? 1 : -1;
			int ky = to.Y > from.Y ? 1 : -1;
			int d = dx > dy ? 2 * dy - dx : 2 * dx - dy;
			int incrE = dx > dy ? 2 * dy : 2 * dx;
			int incrNE = dx > dy ? 2 * (dy - dx) : 2 * (dx - dy);

			int x = from.X;
			int y = from.Y;
			if (!Out(x, y) && v > bufferZ[x, y])
			{
				bufferZ[x, y] = v;
				SetPixel(x, y, c);
			}
			while (x != to.X || y != to.Y)
			{
				if (d < 0)
				{
					d += incrE;
					if (dx > dy) x += kx;
					else y += ky;
				}
				else
				{
					int b = Math.Abs(incrE - incrNE);
					if (!Out(x, y) && v > bufferZ[x, y])
					{
						bufferZ[x, y] = v;
						SetPixel(x, y, c);
					}
					d += incrNE;
					x += kx;
					y += ky;
				}
				if (!Out(x,y) && v > bufferZ[x, y])
				{
					bufferZ[x, y] = v;
					SetPixel(x, y, c);
				}
			}
		}
		private bool Out(int x, int y)
		{
			return x >= Width || y >= Height || x < 0 || y < 0;
		}
		public Color GetPixel(int x, int y)
		{
			int index = x + (y * Width);
			int col = Bits[index];
			Color result = Color.FromArgb(col);

			return result;
		}
		public Bitmap GetRHistogram()
		{
			Bitmap hist = new Bitmap(164, 126);
			int[] counts = CalculateHistogram(4);
			int max = int.MinValue;
			for(int i=0;i<256;i++)
			{
				if (max < counts[i]) max = counts[i];
			}
			Pen pen = new Pen(Color.Red);
			for (int i = 0; i < 256; i++)
			{
				using (Graphics g = Graphics.FromImage(hist))
				{
					g.DrawLine(pen, new Point((int)(i * (164.0 / 256)), 126), new Point((int)(i * (164.0 / 256)), 126 - (int)((126.0 / max) * counts[i])));
				}
			}
			return hist;
		}
		public Bitmap GetGHistogram()
		{
			Bitmap hist = new Bitmap(164, 126);
			int[] counts = CalculateHistogram(2);
			int max = int.MinValue;
			for (int i = 0; i < 256; i++)
			{
				if (max < counts[i]) max = counts[i];
			}
			Pen pen = new Pen(Color.Green);
			for (int i = 0; i < 256; i++)
			{
				using (Graphics g = Graphics.FromImage(hist))
				{
					g.DrawLine(pen, new Point((int)(i * (164.0 / 256)), 126), new Point((int)(i * (164.0 / 256)), 126 - (int)((126.0 / max) * counts[i])));
				}
			}
			return hist;
		}
		public Bitmap GetBHistogram()
		{
			Bitmap hist = new Bitmap(164, 126);
			int[] counts = CalculateHistogram(1);
			int max = int.MinValue;
			for (int i = 0; i < 256; i++)
			{
				if (max < counts[i]) max = counts[i];
			}
			Pen pen = new Pen(Color.Blue);
			for (int i = 0; i < 256; i++)
			{
				using (Graphics g = Graphics.FromImage(hist))
				{
					g.DrawLine(pen, new Point((int)(i*(164.0/256)), 126), new Point((int)(i * (164.0 / 256)), 126 - (int)((126.0 / max) * counts[i])));
				}
			}
			return hist;
		}
		private int[] CalculateHistogram(int x)
		{
			int[] counts = new int[256];
			for (int i = 0; i < Width; i++)
			{
				for (int j = 0; j < Height; j++)
				{
					switch (x)
					{
						case 4: counts[GetPixel(i, j).R]++; break;
						case 2: counts[GetPixel(i, j).G]++; break;
						case 1: counts[GetPixel(i, j).B]++; break;
					}
				}
			}
			return counts;
		}
		public void EqualizeHistogram(bool r, bool g, bool b)
		{
			if(r || g || b)
			{ 
				int[] cdfr = null;
				int[] cdfg = null;
				int[] cdfb = null;
				if (r) cdfr = CDF(CalculateHistogram(4));
				if (g) cdfg = CDF(CalculateHistogram(4));
				if (b) cdfb = CDF(CalculateHistogram(4));
				DirectBitmap temp = new DirectBitmap(this);
				double minR=0;
				double minG=0;
				double minB=0;
				if (r) minR = cdfr.Min();
				if (g) minG = cdfg.Min();
				if (b) minB = cdfb.Min();
				int n = Width * Height;
				for (int i = 0; i < Width; i++)
				{
					for (int j = 0; j < Height; j++)
					{
						int R = temp.GetPixel(i, j).R;
						int G = temp.GetPixel(i, j).G;
						int B = temp.GetPixel(i, j).B;
						if (n == minR || n == minG || n == minB) return;
						if (r) R = (int)(((cdfr[R] - minR) / (Width * Height - minR)) * 255);
						if (g) G = (int)(((cdfg[G] - minG) / (Width * Height - minG)) * 255);
						if (b) B = (int)(((cdfb[B] - minB) / (Width * Height - minB)) * 255);
						SetPixel(i, j, Color.FromArgb(R, G, B));
					}
				}
				temp.Dispose();
			}
		}
		public void Dispose()
		{
			if (Disposed) return;
			Disposed = true;
			Bitmap.Dispose();
			BitsHandle.Free();
		}
		private int[] CDF(int[] values)
		{
			int[] cdf = new int[256];
			cdf[0] = values[0];
			for (int i = 1; i < 256; i++)
			{
				cdf[i] = cdf[i - 1] + values[i];
			}
			return cdf;
		}
	}
}
