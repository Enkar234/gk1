﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace GK1
{
	class PaintingManager
	{
		PictureBox Canvas;
		public Rectangle selectedArea;
		public Color CurrentBackgroundColor;
		public Color CurrentGridColor;
		string gridDensity;
		Painter painter;
		Point firstPolygonPoint;
		Bitmap bmp;
		SolidBrush FillBrush;
		Color ToFillColor;

		public Color CurrentDrawingColor;
		public Color CurrentFillSceneColor;
		public Rectangle rect;
		public Point lastPoint;
		public int startX, startY;
		public Point CenterPoint;
		bool[,] visited;
		public PaintingManager(PictureBox pb)
		{
			Canvas = pb;
		}
		public void Initialize(bool withGrid = false)
		{
			SolidBrush brush = new SolidBrush(CurrentBackgroundColor);
			Bitmap bmp = new Bitmap(Canvas.Width, Canvas.Height);
			using (Graphics graph = Graphics.FromImage(bmp))
			{
				Rectangle ImageSize = new Rectangle(0, 0, Canvas.Width, Canvas.Height);
				graph.FillRectangle(brush, ImageSize);
			}
			Canvas.Image = bmp;
			if (withGrid) DrawGrid();
		}
		public void SetDrawingColor(Color c)
		{
			CurrentDrawingColor = c;
		}
		public void SetFirstPolygonPoint(Point point)
		{
			firstPolygonPoint = point;
		}
		public void InitializePainter(bool a, bool[] objects)
		{
			if (objects[0]) painter = new PointDrawer(startX, startY, startX, startY, CurrentDrawingColor);
			if (objects[1]) painter = new LineDrawer(startX, startY, startX, startY, a, CurrentDrawingColor);
			if (objects[2]) painter = new CircleDrawer(startX, startY, startX, startY, CurrentDrawingColor, a, false);
			if (objects[3]) painter = new CircleDrawer(startX, startY, startX, startY, CurrentDrawingColor, a, true);
		}
		public void InitializePolygonPainter(bool filled)
		{
			if (!filled) painter = new PolygonDrawer(new List<Point>() { new Point(startX, startY) }, CurrentDrawingColor, false);
			else painter = new PolygonDrawer(new List<Point>() { new Point(startX, startY) }, CurrentDrawingColor, true);
		}
		public void InitializeClippingRectanglePainter(Bitmap bmp, int sx, int sy, int width, int height)
		{
			painter = new ClippingRectangle(bmp, sx, sy, width, height);
		}
		public void SetBackgroundColor(Color c)
		{
			CurrentBackgroundColor = c;
		}
		public void SetFillSceneColor(Color c)
		{
			CurrentFillSceneColor = c;
		}
		public void SetGridColor(Color c)
		{
			CurrentGridColor = c;
		}
		public void SetStartCoordinates(int x, int y)
		{
			startX = x;
			startY = y;
		}
		public void SetSelectedArea(Rectangle rectangle)
		{
			selectedArea = rectangle;
		}
		public void SetGridDensity(string gd)
		{
			gridDensity = gd;
		}
		public void UpdateSelectedArea(int dx, int dy)
		{
			selectedArea = new Rectangle(selectedArea.X + dx, selectedArea.Y + dy, selectedArea.Width, selectedArea.Height);
		}
		public void UpdateLastPoint(Point p)
		{
			painter.Drag(startX, startY, p.X, p.Y);
			lastPoint = p;
		}
		public void UpdateRectangle(int x, int y)
		{
			int width = Math.Max(startX, x) - Math.Min(startX, x);
			int height = Math.Max(startY, y) - Math.Min(startY, y);
			int diameter = Math.Min(width, height);
			int xx = startX < x ? startX : startX - diameter;
			int yy = startY < y ? startY : startY - diameter;
			rect = new Rectangle(xx, yy, diameter, diameter);
			painter.Drag(xx, yy, xx + diameter, yy + diameter);
		}
		public void UpdateClippingRectangle(int dx, int dy)
		{
			painter.Move(dx, dy);
		}
		public bool polygonClosed(int x, int y)
		{
			int cx = firstPolygonPoint.X;
			int cy = firstPolygonPoint.Y;
			if (x >= cx + 15 && x <= cx - 15 && y >= cy + 15 && y <= cy - 15) return false;
			return (x - cx) * (x - cx) + (y - cy) * (y - cy) <= 225;
		}
		public void PaintObject(Graphics gr)
		{
			painter.Paint(gr);
		}
		private void DrawGrid()
		{
			Pen pen = new Pen(CurrentGridColor);
			pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
			int d;
			if (!int.TryParse(gridDensity, out d)) return;
			using (Graphics g = Graphics.FromImage(Canvas.Image))
			{
				for (int i = 0; i < Canvas.Width; i += d)
				{
					g.DrawLine(pen, i, 0, i, Canvas.Height);
				}
				for (int i = 0; i < Canvas.Height; i += d)
				{
					g.DrawLine(pen, 0, i, Canvas.Width, i);
				}
			}
		}
		public void DrawSelection(Graphics gr)
		{
			Color oppositeColor = Color.FromArgb(255 - CurrentBackgroundColor.R, 255 - CurrentBackgroundColor.G, 255 - CurrentBackgroundColor.B);
			Pen pen = new Pen(oppositeColor);
			pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
			gr.DrawRectangle(pen, selectedArea);
		}
		public void PrepareToFill(int x, int y)
		{
			visited = new bool[x, y];
			bmp = Canvas.Image as Bitmap;
			FillBrush = new SolidBrush(CurrentFillSceneColor);
			ToFillColor = bmp.GetPixel(startX, startY);
		}
		public void FillSceneWithColor8c(Graphics gr, int x, int y)
		{
			Stack<Point> stack = new Stack<Point>();
			stack.Push(new Point(x, y));
			while (stack.Count > 0)
			{
				Point p = stack.Pop();

				if (p.X >= Canvas.Width || p.Y >= Canvas.Height)
					continue;
				if (p.X < 0 || p.Y < 0)
					continue;
				if (visited[p.X, p.Y])
					continue;
				Color cc = bmp.GetPixel(p.X, p.Y);
				if (cc.A == ToFillColor.A && cc.R == ToFillColor.R && cc.G == ToFillColor.G && cc.B == ToFillColor.B)
				{
					visited[p.X, p.Y] = true;
					gr.FillRectangle(FillBrush, p.X, p.Y, 1, 1);
					stack.Push(new Point(p.X + 1, p.Y));
					stack.Push(new Point(p.X, p.Y + 1));
					stack.Push(new Point(p.X, p.Y - 1));
					stack.Push(new Point(p.X - 1, p.Y));

					stack.Push(new Point(p.X + 1, p.Y+1));
					stack.Push(new Point(p.X-1, p.Y + 1));
					stack.Push(new Point(p.X+1, p.Y - 1));
					stack.Push(new Point(p.X - 1, p.Y-1));
				}
			}
		}
		public void FillSceneWithColor4c(Graphics gr, int x, int y)
		{
			Stack<Point> stack = new Stack<Point>();
			stack.Push(new Point(x, y));
			while (stack.Count > 0)
			{
				Point p = stack.Pop();

				if (p.X >= Canvas.Width || p.Y >= Canvas.Height)
					continue;
				if (p.X < 0 || p.Y < 0)
					continue;
				if (visited[p.X, p.Y])
					continue;
				Color cc = bmp.GetPixel(p.X, p.Y);
				if (cc.A == ToFillColor.A && cc.R == ToFillColor.R && cc.G == ToFillColor.G && cc.B == ToFillColor.B)
				{
					visited[p.X, p.Y] = true;
					gr.FillRectangle(FillBrush, p.X, p.Y, 1, 1);
					stack.Push(new Point(p.X + 1, p.Y));
					stack.Push(new Point(p.X, p.Y + 1));
					stack.Push(new Point(p.X, p.Y - 1));
					stack.Push(new Point(p.X - 1, p.Y));
				}
			}
		}
		public bool MouseInSelectedArea(int x, int y)
		{
			return x >= selectedArea.X && x <= selectedArea.X + selectedArea.Width && y >= selectedArea.Y && y <= selectedArea.Y + selectedArea.Height;
		}
	}
}
