﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GK1
{
	static class Mathematics
	{
		public static My3DPoint CrossProduct(My3DPoint v1, My3DPoint v2)
		{
			double s1 = v1.Y * v2.Z - v1.Z * v2.Y;
			double s2 = v1.Z * v2.X - v1.X * v2.Z;
			double s3 = v1.X * v2.Y - v1.Y * v2.X;
			return new My3DPoint(s1, s2, s3);
		}
		public static double DotProduct(My3DPoint v1, My3DPoint v2)
		{
			return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
		}
		public static My3DPoint Difference(My3DPoint v1, My3DPoint v2)
		{
			return new My3DPoint(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
		}
		public static My3DPoint Normalize(My3DPoint V)
		{
			double normV = Math.Sqrt(V.X * V.X + V.Y * V.Y + V.Z * V.Z);
			V = new My3DPoint(V.X / normV, V.Y / normV, V.Z / normV);
			return V;
		}
		public static My3DPoint RotatePoint(My3DPoint p, double[,] Matrix)
		{
			double x, y, z;
			x = Matrix[0, 0] * p.X + Matrix[0, 1] * p.Y + Matrix[0, 2] * p.Z;
			y = Matrix[1, 0] * p.X + Matrix[1, 1] * p.Y + Matrix[1, 2] * p.Z;
			z = Matrix[2, 0] * p.X + Matrix[2, 1] * p.Y + Matrix[2, 2] * p.Z;
			return new My3DPoint(x, y, z);
		}
		public static double AngleBetweenNormal(My3DPoint v1, My3DPoint v2)
		{
			return Math.Acos(DotProduct(v1,v2));
		}
	}
}
