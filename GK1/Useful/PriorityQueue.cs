﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace GK1
{
	public class PriorityQueue : IEnumerable<Face>
	{
		Face head;
		public PriorityQueue()
		{
			head = null;
		}

		public void Insert(double v, bool t, Color c)
		{
			Face n = new Face(v, t, c);
			//if (!t)
			//{
			//	head = n;
			//	return;
			//}
			//else
		//	{
				if (head == null)
				{
					head = n;
					return;
				}
				if (n.v > head.v)
				{
					n.next = head;
					head = n;
					return;
				}
				Face p = head;
				while (p.next != null && p.next.v > n.v)
				{
				//	if (!p.t) return;
					p = p.next;
				}
				n.next = p.next;
				p.next = n;
		//	}
		}
		IEnumerator<Face> IEnumerable<Face>.GetEnumerator()
		{
			Face p = head;
			while (p != null)
			{
				yield return p;
				p = p.next;
			}
		}
		IEnumerator IEnumerable.GetEnumerator()
		{
			Face p = head;
			while (p != null)
			{
				yield return p;
				p = p.next;
			}
		}
	}
	class Face
	{
		public double v;
		public Color c;
		public bool t;
		public Face next;
		public Face(double vv,bool tt, Color cc)
		{
			v = vv;
			t = tt;
			c = cc;
		}
	}

}
