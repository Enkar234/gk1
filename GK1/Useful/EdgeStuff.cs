﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GK1
{
	class Edge
	{
		public int Ymax;
		public float X;
		public float c;
		public Edge(int y, float x, float cc)
		{
			Ymax = y;
			X = x;
			c = cc;
		}
		public Edge(Edge e)
		{
			Ymax = e.Ymax;
			X = e.X;
			c = e.c;
		}
	}
	class EdgeList
	{
		List<Edge>[] edges;
		public int Count { get; set; }
		public EdgeList(int c)
		{
			edges = new List<Edge>[c];
			Count = 0;
		}
		public EdgeList(EdgeList edgeList)
		{
			Count = edgeList.Count;
			edges = new List<Edge>[edgeList.edges.Length];
			for(int i=0;i<edgeList.edges.Length;i++)
			{
				if(edgeList.edges[i]!=null)
				{
					edges[i] = new List<Edge>();
					for (int j=0;j< edgeList.edges[i].Count;j++)
					{
						edges[i].Add(new Edge(edgeList.edges[i][j]));
					}
				}
			}
		}
		public List<Edge> this[int i]
		{
			get
			{
				return edges[i];
			}
			set
			{
				if (value == null) Count--;
				else Count++;
				edges[i] = value;
			}
		}
	}
}
