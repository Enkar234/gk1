﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GK1
{
	class PhongManager
	{
		double Ka;
		double Kd;
		double Ks;
		double alfa=1;
		public Color phongAmbient;
		public Color phongSpecular;
		public Color phongDiffuse;
		double iar; 
		double iag; 
		double iab; 
		double idr; 
		double idg;
		double idb; 
		double isr;
		double isg; 
		double isb;
		List<Point> Lights;
		int vX, vY;
		public PhongManager(double a, double s, double d, double alf, List<Point> L, int vx, int vy, Color pa, Color ps, Color pd)
		{
			Ka = a;
			Ks = s;
			Kd = d;
			vX = 0;
			vY = 0;
			alfa = alf;
			Lights = L;
			phongAmbient = pa;
			phongSpecular = ps;
			phongDiffuse = pd;
			iar = phongAmbient.R / 255.0;
			iag = phongAmbient.G / 255.0;
			iab = phongAmbient.B / 255.0;
			idr = phongDiffuse.R / 255.0;
			idg = phongDiffuse.G / 255.0;
			idb = phongDiffuse.B / 255.0;
			isr = phongSpecular.R / 255.0;
			isg = phongSpecular.G / 255.0;
			isb = phongSpecular.B / 255.0;
		}
		public (double r, double g, double b) CalculateBrightness(My3DTriangle t, My3DPoint[] points, double d, int factor, double minZ)
		{
			My3DPoint p0 = new My3DPoint(points[t.points[0]].X, points[t.points[0]].Y, points[t.points[0]].Z);
			My3DPoint p1 = new My3DPoint(points[t.points[1]].X, points[t.points[1]].Y, points[t.points[1]].Z);
			My3DPoint p2 = new My3DPoint(points[t.points[2]].X, points[t.points[2]].Y, points[t.points[2]].Z);
			My3DPoint S = new My3DPoint((p0.X+p1.X+p2.X)/3, (p0.Y + p1.Y + p2.Y) / 3, (p0.Z + p1.Z + p2.Z) / 3);

			My3DPoint N = Mathematics.CrossProduct(Mathematics.Difference(p1, p0), Mathematics.Difference(p2, p0));
			N = Mathematics.Normalize(N);

			My3DPoint V = new My3DPoint(vX - S.X * factor, vY - S.Y * factor,-minZ*factor+ S.Z * factor);
			V = Mathematics.Normalize(V);

			double Ir, Ig, Ib;
			Ir = Ka * iar;
			Ig = Ka * iag;
			Ib = Ka * iab;
			foreach (Point l in Lights)
			{
				My3DPoint Lm = new My3DPoint(l.X-S.X*factor,l.Y-S.Y*factor,S.Z*factor);
				Lm = Mathematics.Normalize(Lm);

				double w = 2 * Mathematics.DotProduct(Lm, N);
				My3DPoint Rm = new My3DPoint(w * N.X - Lm.X, w * N.Y - Lm.Y, w * N.Z - Lm.Z);
				Rm = Mathematics.Normalize(Rm);
				double LmN = Mathematics.DotProduct(Lm, N);
				double RmV = Mathematics.DotProduct(Rm, V);

				Ir += Kd * LmN * idr + Ks * Math.Pow(Math.Abs(RmV), alfa) * isr * Math.Sign(RmV);
				Ig += Kd * LmN * idg + Ks * Math.Pow(Math.Abs(RmV), alfa) * isg * Math.Sign(RmV);
				Ib += Kd * LmN * idb + Ks * Math.Pow(Math.Abs(RmV), alfa) * isb * Math.Sign(RmV);
			}
			Ir = Ir > 1 ? 1 : Ir < 0 ? 0 : Ir;
			Ig = Ig > 1 ? 1 : Ig < 0 ? 0 : Ig;
			Ib = Ib > 1 ? 1 : Ib < 0 ? 0 : Ib;

			return (Ir, Ig, Ib);
		}
	}
}
