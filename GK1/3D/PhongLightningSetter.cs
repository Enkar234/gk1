﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms; 

namespace GK1
{
	class PhongLightningSetter
	{
		public List<Point> StandardLights;
		public List<Point> RandomLights;

		public PictureBox SPB;
		public PictureBox RPB;

		int SpointToMove;
		int RpointToMove;
		public bool move = false;
		public PhongLightningSetter(PictureBox standardPB, PictureBox randomPB)
		{
			StandardLights = new List<Point>();
			RandomLights = new List<Point>();

			Point s = new Point(standardPB.Width / 2, standardPB.Height / 2);
			Point r = new Point(randomPB.Width / 2, randomPB.Height / 2);
			StandardLights.Add(s);
			RandomLights.Add(r);

			SPB = standardPB;
			RPB = randomPB;

			SPB.Image = new Bitmap(standardPB.Width, standardPB.Height);
			RPB.Image = new Bitmap(randomPB.Width, randomPB.Height);
		}
		public void DrawSLightSource(Graphics g)
		{
			foreach (Point l in StandardLights)
			{
				g.FillEllipse(new SolidBrush(Color.White), l.X - 10, l.Y - 10, 21, 21);
			}
		}
		public void DrawRLightSource(Graphics g)
		{
			foreach (Point l in RandomLights)
			{
				g.FillEllipse(new SolidBrush(Color.White), l.X - 10, l.Y - 10, 21, 21);
			}
		}
		public void AddRemoveSLight(int x, int y)
		{
			(bool? r, int i) = RemoveS(x, y);
			if (!r.HasValue) return;
			else
			{
				if(r.Value)
				{
					StandardLights.RemoveAt(i);
					return;
				}
				StandardLights.Add(new Point(x, y));
			}
		}
		public void AddRemoveRLight(int x, int y)
		{
			(bool? r, int i) = RemoveR(x, y);
			if (!r.HasValue) return;
			else
			{
				if (r.Value)
				{
					RandomLights.RemoveAt(i);
					return;
				}
				RandomLights.Add(new Point(x, y));
			}
		}
		private (bool? r, int i) RemoveS(int x, int y)
		{
			for(int i=0;i<StandardLights.Count;i++)
			{
				if (x > StandardLights[i].X - 10 && x < StandardLights[i].X + 10 && y > StandardLights[i].Y - 10 && y < StandardLights[i].Y + 10)
				{
					if(StandardLights.Count <= 1) return (null, -1);
					else return (true, i);
				}
			}
			return (false,-1);
		}
		private (bool? r, int i) RemoveR(int x, int y)
		{
			for (int i = 0; i < RandomLights.Count; i++)
			{
				if (x > RandomLights[i].X - 10 && x < RandomLights[i].X + 10 && y > RandomLights[i].Y - 10 && y < RandomLights[i].Y + 10)
				{
					if (RandomLights.Count <= 1) return (null, -1);
					else return (true, i);
				}
			}
			return (false, -1);
		}
		public void SetSPointToMove(int x, int y)
		{
			int i = 0;
			foreach (Point p in StandardLights)
			{
				if (x > p.X - 7 && x < p.X + 7 && y > p.Y - 7 && y < p.Y + 7)
				{
					SpointToMove = i;
					move = true;
					return;
				}
				i++;
			}
			move = false;
		}
		public void SetRPointToMove(int x, int y)
		{
			int i = 0;
			foreach (Point p in RandomLights)
			{
				if (x > p.X - 7 && x < p.X + 7 && y > p.Y - 7 && y < p.Y + 7)
				{
					RpointToMove = i;
					move = true;
					return;
				}
				i++;
			}
			move = false;
		}
		public void MoveSPoint(int x, int y)
		{
			if (x>=0 && x<SPB.Width && y >= 0 && y < SPB.Height)
			{
				StandardLights[SpointToMove] = new Point(x,y);
			}
		}
		public void MoveRPoint(int x, int y)
		{
			if (x >= 0 && x < RPB.Width && y >= 0 && y < RPB.Height)
			{
				RandomLights[RpointToMove] = new Point(x, y);
			}
		}
	}
}
