﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GK1
{
	class _3dObjectManager
	{
		My3Dobejct model;
		Point CenterPoint;
		int frames;
		public Point LastPoint;
		DateTime lastTS;
		public int FPS;
		int wheelSum = 0;
		int width, height;
		public _3DEffectManager EffectManager;
		public bool bfc;

		public _3dObjectManager(string path, int tabI, bool p, bool f, bool tr, bool t, PhongManager phm, double fI, int tC, double tI, Color c, Color cf,bool backfc)
		{
			frames = 0;
			bfc = backfc;
			model = new My3Dobejct(path);
			EffectManager = new _3DEffectManager(tabI, p, f, tr, t, phm, fI, tC, tI, c, cf);
		}
		public void Paint(int width, int height, Graphics g)
		{
			DirectBitmap bmp = model.Paint(EffectManager, width, height, bfc);
			int w = bmp.Width;
			int h = bmp.Height;
			int x = (width - w) / 2;
			int y = (height - h) / 2;
			g.DrawImage(bmp.Bitmap, new Point(x, y));
			bmp.Dispose();
		}
		public void SetCenterPoint(int x, int y)
		{
			CenterPoint = new Point(x, y);
		}
		public void UpdateLastPoint(Point p)
		{
			LastPoint = p;
		}
		public void RotateObject(Point e)
		{
			int X1 = LastPoint.X - CenterPoint.X;
			int Y1 = LastPoint.Y - CenterPoint.Y;
			int X2 = e.X - CenterPoint.X;
			int Y2 = e.Y - CenterPoint.Y;
			double z = model.CenterZ();

			My3DPoint v1 = new My3DPoint(X1, Y1, z);
			My3DPoint v2x = new My3DPoint(X2, Y1, z);
			double dotx = v1.X * v2x.X + v1.Y * v2x.Y + v1.Z * v2x.Z;
			double d1 = Math.Sqrt(v1.X * v1.X + v1.Y * v1.Y + v1.Z * v1.Z);
			double d2x = Math.Sqrt(v2x.X * v2x.X + v2x.Y * v2x.Y + v2x.Z * v2x.Z);
			double tx = dotx / (d1 * d2x);
			tx = tx > 1 ? 1 : tx < -1 ? -1 : tx;
			double fiX = Math.Acos(tx);

			My3DPoint v2y = new My3DPoint(X1, Y2, z);
			double doty = v1.X * v2y.X + v1.Y * v2y.Y + v1.Z * v2y.Z;
			double d2y = Math.Sqrt(v2y.X * v2y.X + v2y.Y * v2y.Y + v2y.Z * v2y.Z);
			double ty = doty / (d1 * d2y);
			ty = ty > 1 ? 1 : ty < -1 ? -1 : ty;
			double fiY = Math.Acos(ty);

			model.YRotate(fiX * -Math.Sign(e.X - LastPoint.X));
			model.XRotate(fiY * Math.Sign(e.Y - LastPoint.Y));
		}
		public int GetLightIntensity()
		{
			return model.li;
		}
		public void SetLightIntensity(int li)
		{
			model.li = li;
		}
		public bool UpdateFPScounter()
		{
			frames++;
			if ((DateTime.Now - lastTS).TotalSeconds >= 1)
			{
				FPS = frames;
				frames = 0;
				lastTS = DateTime.Now;
				return true;
			}
			return false;
		}
		public int WheelScale(int d)
		{
			if (d<0 && wheelSum >= -2640)
			{
				wheelSum += d;
			}
			if (d > 0 && wheelSum <= 2280)
			{
				wheelSum += d;
			}
			float s = (float)(Math.Exp((double)wheelSum / 1200));
			if (wheelSum >= -2760 && wheelSum <= 2400)
			{
				Scale(s);
			}
			return (int)(s * 100);
		}
		public void Scale(float s)
		{
			wheelSum = (int)(Math.Log(s) * 1200);
			model.Scale(s);
		}
		public void LoadTexture(Bitmap bmp)
		{
			EffectManager.LoadTexture(bmp);
		}
		public void SetFog(bool f, Color c, double i)
		{
			EffectManager.fog = f;
			EffectManager.fogC = c;
			EffectManager.fogI = i;
		}
		public void SetCanvasSize(int w, int h)
		{
			width = w;
			height = h;
		}
	}
}
