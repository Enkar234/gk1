﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;		

namespace GK1
{
	class _3DEffectManager
	{
		public int tabIndex;
		public bool phong;
		public bool fog;
		public bool texture;
		public bool transparency;
		public bool textureLoaded;
		public PhongManager pm;
		public double fogI;
		public int transC;
		public double transI;
		public Color color;
		public Color fogC;
		DirectBitmap bitmap;
		public (double r, double g, double b) phongV;
		Color RandomC;
		bool faceVisible;
		public _3DEffectManager(int tabI, bool p, bool f,bool tr, bool t, PhongManager phm, double fI,int tC, double tI, Color c, Color cf)
		{
			tabIndex = tabI;
			phong = p;
			fog = f;
			texture = t;
			transC = tC;
			transI = tI;
			transparency = tr;
			pm = phm;
			fogI = fI;
			color = c;
			fogC = cf;
	}
		public void Paint(DirectBitmap bmp, int k, int j, int i, double V)
		{
			Color c=color;
			(double r, double g, double b) v = (V,V,V);
			if(phong)
			{
				v = phongV;
			}
			if (tabIndex == 2) c = RandomC;
			if (textureLoaded && texture)
			{
				c = k < bitmap.Width && j < bitmap.Height ? bitmap.GetPixel(k, j) : color;
			}
			if(fog)
			{
				int R = (int)(255 * ((c.R / 255.0) * (v.r + 0.5 - fogI) + (0.5 - v.r + fogI) * (fogC.R / 255.0)));
				int G = (int)(255 * ((c.G / 255.0) * (v.g + 0.5 - fogI) + (0.5 - v.g + fogI) * (fogC.G / 255.0)));
				int B = (int)(255 * ((c.B / 255.0) * (v.b + 0.5 - fogI) + (0.5 - v.b + fogI) * (fogC.B / 255.0)));
				R = R > 255 ? 255 : R < 0 ? 0 : R;
				G = G > 255 ? 255 : G < 0 ? 0 : G;
				B = B > 255 ? 255 : B < 0 ? 0 : B;
				c = Color.FromArgb(R, G, B);
			}
			else
			{
				c = Color.FromArgb((int)(c.R * v.r), (int)(c.G * v.g), (int)(c.B * v.b));
			}
			if(transparency)
			{
				bmp.SetTPixel(k, j, c,V, transI, faceVisible);
			}
			else bmp.SetVPixel(k, j, c, V);
		}
		public void GridPaint(DirectBitmap bmp2, MyTriangle triangle, My3DPoint[] points, int bmpMinX, int bmpMinY, double v, int XOffset, int YOffset)
		{
			Color c = color;
			if (fog)
			{
				int R = (int)(255 * ((c.R / 255.0) * (v + 0.5 - fogI) + (0.5 - v + fogI) * (fogC.R / 255.0)));
				int G = (int)(255 * ((c.G / 255.0) * (v + 0.5 - fogI) + (0.5 - v + fogI) * (fogC.G / 255.0)));
				int B = (int)(255 * ((c.B / 255.0) * (v + 0.5 - fogI) + (0.5 - v + fogI) * (fogC.B / 255.0)));
				R = R > 255 ? 255 : R < 0 ? 0 : R;
				G = G > 255 ? 255 : G < 0 ? 0 : G;
				B = B > 255 ? 255 : B < 0 ? 0 : B;
				c = Color.FromArgb(R, G, B);
			}
			else
			{
				c = Color.FromArgb((int)(c.R * v), (int)(c.G * v), (int)(c.B * v));
			}
			bmp2.DrawLine(new Point(triangle.p1.X - bmpMinX-XOffset, triangle.p1.Y - bmpMinY-YOffset), new Point(triangle.p2.X - bmpMinX-XOffset, triangle.p2.Y - bmpMinY - YOffset), c, v);
			bmp2.DrawLine(new Point(triangle.p2.X - bmpMinX-XOffset, triangle.p2.Y - bmpMinY-YOffset), new Point(triangle.p3.X - bmpMinX - XOffset, triangle.p3.Y - bmpMinY - YOffset), c, v);
			bmp2.DrawLine(new Point(triangle.p3.X - bmpMinX-XOffset, triangle.p3.Y - bmpMinY-YOffset), new Point(triangle.p1.X - bmpMinX - XOffset, triangle.p1.Y - bmpMinY - YOffset), c, v);
		}
		public void LoadTexture(Bitmap bmp)
		{
			if (bitmap != null && !bitmap.Disposed) bitmap.Dispose();
			bitmap = new DirectBitmap(bmp.Width, bmp.Height);
			for (int i = 0; i < bmp.Width; i++)
			{
				for (int j = 0; j < bmp.Height; j++)
				{
					bitmap.SetPixel(i, j, bmp.GetPixel(i, j));
				}
			}
			textureLoaded = true;
		}
		public void SetRandomColor(int i)
		{
			Random r = new Random(i);
			RandomC = Color.FromArgb(r.Next(0, 255), r.Next(0, 255), r.Next(0, 255));
		}
		public void DetermineTransparency(int i)
		{
			Random r = new Random(i);
			int a = r.Next(0, 100);
			if (a < transC)
			{
				faceVisible = false;
			}
			else faceVisible = true;
		}
	}
}
