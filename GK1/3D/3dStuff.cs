﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GK1
{
	class My3DPoint
	{
		public double X { get; set; }
		public double Y { get; set; }
		public double Z { get; set; }
		public My3DPoint(double x, double y, double z)
		{
			X = x;
			Y = y;
			Z = z;
		}
	}
	class My3DTriangle
	{
		public int[] points;
		public My3DTriangle(int p1, int p2, int p3)
		{
			points = new int[3];
			points[0] = p1;
			points[1] = p2;
			points[2] = p3;
		}
	}
	class MyTriangle
	{
		public Point p1;
		public Point p2;
		public Point p3;
		public Point ul;
		public Point lr;
		Color c;
		public MyTriangle(List<Point> points)
		{
			p1 = points[0];
			p2 = points[1];
			p3 = points[2];
			ul = UpperLeftCorner();
			lr = LowerRightCorner();
		}
		public void SetColor(int v)
		{
			c = Color.FromArgb(v, v, v);
		}
		private Point UpperLeftCorner()
		{
			int x = Math.Min(Math.Min(p1.X, p2.X), p3.X);
			int y = Math.Min(Math.Min(p1.Y, p2.Y), p3.Y);
			return new Point(x, y);
		}
		private Point LowerRightCorner()
		{
			int x = Math.Max(Math.Max(p1.X, p2.X), p3.X);
			int y = Math.Max(Math.Max(p1.Y, p2.Y), p3.Y);
			return new Point(x, y);
		}
		public int[,] GetLines()
		{
			int height = lr.Y - ul.Y + 1;
			int[,] lines = new int[height, 2];
			for (int i = 0; i < height; i++)
			{
				lines[i, 0] = int.MaxValue;
				lines[i, 1] = int.MinValue;
			}
			List<Point> points = new List<Point>(3);
			points.Add(p1);
			points.Add(p2);
			points.Add(p3);

			for (int i = 0; i < 3; i++)
			{
				int startX = points[i].X;
				int startY = points[i].Y;
				int endX = points[(i + 1) % 3].X;
				int endY = points[(i + 1) % 3].Y;
				int dx = Math.Abs(endX - startX);
				int dy = Math.Abs(endY - startY);
				int kx = endX > startX ? 1 : -1;
				int ky = endY > startY ? 1 : -1;
				int d = dx > dy ? 2 * dy - dx : 2 * dx - dy;
				int incrE = dx > dy ? 2 * dy : 2 * dx;
				int incrNE = dx > dy ? 2 * (dy - dx) : 2 * (dx - dy);

				int x = startX;
				int y = startY;

				lines[y - ul.Y, 0] = lines[y - ul.Y, 0] < x ? lines[y - ul.Y, 0] : x;
				lines[y - ul.Y, 1] = lines[y - ul.Y, 1] > x ? lines[y - ul.Y, 1] : x;

				while (x != endX || y != endY)
				{
					if (d < 0)
					{
						d += incrE;
						if (dx > dy)
						{
							x += kx;
						}
						else
						{
							lines[y - ul.Y, 0] = lines[y - ul.Y, 0] < x ? lines[y - ul.Y, 0] : x;
							lines[y - ul.Y, 1] = lines[y - ul.Y, 1] > x ? lines[y - ul.Y, 1] : x;
							y += ky;
						}
					}
					else
					{
						int b = Math.Abs(incrE - incrNE);
						d += incrNE;
						lines[y - ul.Y, 0] = lines[y - ul.Y, 0] < x ? lines[y - ul.Y, 0] : x;
						lines[y - ul.Y, 1] = lines[y - ul.Y, 1] > x ? lines[y - ul.Y, 1] : x;
						x += kx;
						y += ky;
						lines[y - ul.Y, 0] = lines[y - ul.Y, 0] < x ? lines[y - ul.Y, 0] : x;
						lines[y - ul.Y, 1] = lines[y - ul.Y, 1] > x ? lines[y - ul.Y, 1] : x;
					}
				}
			}
			return lines;
		}
	}
	class My3Dobejct
	{
		My3DTriangle[] triangles;
		My3DPoint[] points;
		double minZ, maxZ, minY, maxY, minX, maxX;
		int bmpMinX, bmpMinY, bmpMaxX, bmpMaxY;
		int scale = 1600;
		double S = 1 / Math.Tan(110 * (Math.PI / 180));
		public int li { get; set; }
		public My3Dobejct(string path)
		{
			string[] lines = System.IO.File.ReadAllLines(path);
			string p = lines[1].Split(' ')[0];
			string f = lines[1].Split(' ')[1];
			int pointsCount;
			int facesCount;
			int.TryParse(p, out pointsCount);
			int.TryParse(f, out facesCount);
			minZ = double.MaxValue;
			maxZ = double.MinValue;
			minY = double.MaxValue;
			maxY = double.MinValue;
			minX = double.MaxValue;
			maxX = double.MinValue;
			points = new My3DPoint[pointsCount];
			//triangles = new My3DTriangle[facesCount];
			List<My3DTriangle> tri = new List<My3DTriangle>();

			for (int i = 0; i < pointsCount; i++)
			{
				string[] words = lines[i + 2].Split(' ');
				double x, y, z;
				if (double.TryParse(words[0], out x) && double.TryParse(words[1], out y) && double.TryParse(words[2], out z))
				{
					points[i] = new My3DPoint(x, y, z);
					if (z < minZ) minZ = z;
					if (z > maxZ) maxZ = z;
					if (y < minY) minY = y;
					if (y > maxY) maxY = y;
					if (x < minX) minX = x;
					if (x > maxX) maxX = x;
				}
			}
			for (int i = 0; i < facesCount; i++)
			{
				string[] words = lines[i + pointsCount + 2].Split(' ');
				int c, p1, p2, p3;
				if (int.TryParse(words[0], out c))
				{
					for (int k = 0; k < c - 2; k++)
					{
						if (int.TryParse(words[2], out p1) && int.TryParse(words[3 + k], out p2) && int.TryParse(words[4 + k], out p3))
						{
							tri.Add(new My3DTriangle(p1, p2, p3));
						}
					}
				}
			}
			double m = maxZ + Math.Abs(minZ) + 1;
			li = (int)(-100 * (minZ + Math.Abs(minZ) + 1 - m) / m);
			triangles = tri.ToArray();
		}
		private List<(MyTriangle t, double d, int ix)> GetPerspectiveView(bool bfc)
		{
			double f = maxZ + Math.Abs(minZ) + 1;
			double n = ((minZ - maxZ) / 100.0) * li + f;
			//double n = 0;
			double c0 = -f / (f - n);
			double c1 = c0 * n;

			int count = triangles.Length;
			bmpMaxX = int.MinValue;
			bmpMaxY = int.MinValue;
			bmpMinX = int.MaxValue;
			bmpMinY = int.MaxValue;

			int factor = (int)(scale / ((maxX - minX) + (maxY - minY)));

			List<(MyTriangle t, double d, int ix)> perspTriangles = new List<(MyTriangle t, double d, int ix)>(count);
			for (int i = 0; i < count; i++)
			{
				List<Point> newPoints = new List<Point>(3);
				List<My3DPoint> new3DPoints = new List<My3DPoint>(3);
				double sZ = 0;
				for (int v = 0; v < 3; v++)
				{
					sZ += points[triangles[i].points[v]].Z + Math.Abs(minZ) + 1;
					double x = S * points[triangles[i].points[v]].X;
					double y = S * points[triangles[i].points[v]].Y;
					new3DPoints.Add(new My3DPoint(x, y, points[triangles[i].points[v]].Z + Math.Abs(minZ) + 1));
					int newX = (int)(factor * x);
					int newY = (int)(factor * y);
					if (bmpMinX > newX) bmpMinX = newX;
					if (bmpMinY > newY) bmpMinY = newY;
					if (bmpMaxX < newX) bmpMaxX = newX;
					if (bmpMaxY < newY) bmpMaxY = newY;
					newPoints.Add(new Point(newX, newY));
				}
				if (bfc && Discard(new3DPoints)) continue;
				double Z = -sZ / 3;
				double d = -c0 - c1 / Z;
				perspTriangles.Add((new MyTriangle(newPoints), d, i));
			}
			return perspTriangles;
		}
		public DirectBitmap Paint(_3DEffectManager E, int w, int h, bool bfc)
		{
			List<(MyTriangle t, double d, int ix)> list = GetPerspectiveView(bfc);
			int width = bmpMaxX - bmpMinX + 1 <= w ? bmpMaxX - bmpMinX + 1 : w;
			int height = bmpMaxY - bmpMinY + 1 <= h ? bmpMaxY - bmpMinY + 1 : h;
			int XOffset = bmpMaxX - bmpMinX + 1 <= w ? 0 : (bmpMaxX - bmpMinX + 1) - w;
			int YOffset = bmpMaxY - bmpMinY + 1 <= h ? 0 : (bmpMaxY - bmpMinY + 1) - h;

			DirectBitmap bmp2 = new DirectBitmap(width, height);
			bmp2.bufferZ = new double[width, height];
			if (E.transparency) bmp2.tFaces = new PriorityQueue[width, height];

			for (int i = 0; i < list.Count; i++)
			{
				MyTriangle triangle = list[i].t;
				int[,] lines = triangle.GetLines();

				int y = triangle.ul.Y - bmpMinY - YOffset / 2;

				int yy = 0;
				if (y < 0) yy = Math.Abs(y);

				double v = list[i].d;
				if (v < 0) continue;
				if (E.tabIndex == 1)
				{
					E.GridPaint(bmp2, triangle, points, bmpMinX, bmpMinY, v, XOffset / 2, YOffset / 2);
					continue;
				}
				if (E.phong) E.phongV = E.pm.CalculateBrightness(triangles[list[i].ix], points, list[i].d, (int)(scale / ((maxX - minX) + (maxY - minY))), minZ);

				if (E.tabIndex == 2) E.SetRandomColor(list[i].ix);
				if (E.transparency) E.DetermineTransparency(list[i].ix);

				for (int j = y + yy; j < y + lines.GetLength(0) && j < height; j++)
				{
					int from = lines[j - y, 0] - bmpMinX - XOffset / 2;
					int to = lines[j - y, 1] - bmpMinX - XOffset / 2;
					if (to < 0) continue;
					from = from < 0 ? 0 : from;
					for (int k = from; k <= to && k < width; k++)
					{
						E.Paint(bmp2, k, j, i, v);
					}
				}
			}
			return bmp2;
		}
		public void XRotate(double fi)
		{
			double[,] X = GetXMatrix(fi);
			Rotate(X);
		}
		public void YRotate(double fi)
		{
			double[,] Y = GetYMatrix(fi);
			Rotate(Y);
		}
		public void ZRotate(double fi)
		{
			double[,] Z = GetZMatrix(fi);
			Rotate(Z);
		}
		private void Rotate(double[,] Matrix)
		{
			minZ = double.MaxValue;
			maxZ = double.MinValue;
			Parallel.For(0, points.Length, i =>
			{
				 double[] newVector = new double[3];
				 double[] oldVector = new double[3];
				 oldVector[0] = points[i].X;
				 oldVector[1] = points[i].Y;
				 oldVector[2] = points[i].Z;

				 for (int j = 0; j < 3; j++)
				 {
					 for (int k = 0; k < 3; k++)
					 {
						 newVector[j] += Matrix[j, k] * oldVector[k];
					 }
				 }
				 double x = newVector[0];
				 double y = newVector[1];
				 double z = newVector[2];
				 if (z < minZ) minZ = z;
				 if (z > maxZ) maxZ = z;
	
				 points[i].X = x;
				 points[i].Y = y;
				 points[i].Z = z;
			});
		}
		public void Scale(float s)
		{
			scale = (int)(s * 1600);
		}
		private bool Discard(List<My3DPoint> points)
		{

			My3DPoint N = Mathematics.CrossProduct(Mathematics.Difference(points[1], points[0]), Mathematics.Difference(points[2], points[0]));
			return N.Z <= 0;
		}
		private double[,] GetRotationMatrix(double fi, My3DPoint u)
		{
			double[,] M = { { Math.Cos(fi)+u.X*u.X*(1-Math.Cos(fi)),u.X*u.Y*(1-Math.Cos(fi))-u.Z*Math.Sin(fi),u.X*u.Z*(1-Math.Cos(fi))+u.Y*Math.Sin(fi)},
							{ u.X*u.Y*(1-Math.Cos(fi))+u.Z*Math.Sin(fi),Math.Cos(fi)+u.Y*u.Y*(1-Math.Cos(fi)),u.Y*u.Z*(1-Math.Cos(fi))-u.X*Math.Sin(fi)},
							{ u.Z*u.X*(1-Math.Cos(fi))-u.Y*Math.Sin(fi),u.Z*u.Y*(1-Math.Cos(fi))+u.X*Math.Sin(fi), Math.Cos(fi)+u.Z*u.Z*(1-Math.Cos(fi))}};
			return M;
		}
		private double[,] GetXMatrix(double fi)
		{
			return new double[,] { { 1, 0, 0 }, { 0, Math.Cos(fi), -Math.Sin(fi) }, { 0, Math.Sin(fi), Math.Cos(fi) } };
		}
		private double[,] GetYMatrix(double fi)
		{
			return new double[,] { { Math.Cos(fi), 0, Math.Sin(fi) }, { 0, 1, 0 }, { -Math.Sin(fi), 0, Math.Cos(fi) } };
		}
		private double[,] GetZMatrix(double fi)
		{
			return new double[,] { { Math.Cos(fi), -Math.Sin(fi), 0 }, { Math.Sin(fi), Math.Cos(fi), 0 }, { 0, 0, 1 } };
		}
		public double CenterZ()
		{
			return ((minZ + 2 * Math.Abs(minZ) + 2) / 2) * 1 / Math.Tan(110 * (Math.PI / 180)) * (int)(scale / ((maxX - minX) + (maxY - minY)));
		}
	}
}
