﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GK1
{
    public abstract class MyObject
    {
        public enum Type { Point, Circle, Line, Polygon}
        public Type type;
        public Color color;
        public string name;
        public bool aa = false;
        public int startX, startY, endX, endY;
        public int layer;
        public MyObject(int sx, int sy, int ex, int ey, Color c, Type t, int l,string n, bool a)
        {
            startX = sx;
            startY = sy;
            endX = ex;
            endY = ey;
            color = c;
            type = t;
            layer = l;
            name = n;
			aa = a;
        }
        public abstract Rectangle Area();
        public abstract bool InRectangle(int x, int y);
        public abstract bool Selected(int x, int y);
        public abstract void Move(int x, int y);
        public abstract void PaintMe(PictureBox Canvas);
		public abstract void UpdateObject(Color c, bool a, string n);
		public abstract List<MyLine> ClipObject(int x, int y, int width, int height);
    }
    class MyPoint : MyObject
    {
		private Painter pointDrawer;
        public MyPoint(int xx, int yy, Color c, int l, string name="point", bool aa=false) : base(xx, yy, xx, yy, c, Type.Point,l, name,aa)
		{
			pointDrawer = new PointDrawer(xx, yy, xx, yy, c);
		}
        public override Rectangle Area()
        {
            return new Rectangle(startX, startY, 1, 1);
        }
        public override bool InRectangle(int x, int y)
        {
            return startX == x && startY == y;
        }
        public override bool Selected(int x, int y)
        {
            return InRectangle(x, y);
        }
        public override void Move(int dx, int dy)
        {
            startX += dx;
            startY += dy;
            endX += dx;
            endY += dy;
			pointDrawer.Move(dx, dy);
        }
        public override void PaintMe(PictureBox Canvas)
        {
			Graphics gr = Graphics.FromImage(Canvas.Image);
			pointDrawer.Paint(gr);
		}
		public override void UpdateObject(Color c, bool a, string n)
		{
			color = c;
			aa = a;
			name = n;
			pointDrawer.Update(c,aa);
		}
		public override List<MyLine> ClipObject(int x, int y, int width, int height)
		{
			List<MyLine> point = new List<MyLine>();
			if (startX >= x && startX <= x + width && startY >= y && startY <= y + height)
			{
				point.Add(new MyLine(startX, startY, startX, startY, color, layer, name, aa));
			}
			return point;
		}
	}
    class MyCircle : MyObject
    {
        int diameter;
        public bool filled;
		private Painter circleDrawer;
		public MyCircle(int xx, int yy, int d, Color c, int l, bool f,string name="circle",bool aa=false) : base(xx - d/2, yy - d/2, xx + d/2, yy + d/2, c,Type.Circle,l,name,aa)
        {
            diameter = d;
            filled = f;
			circleDrawer = new CircleDrawer(xx - (d / 2), yy - (d / 2), xx + (d / 2), yy + (d / 2), c, aa, f);
		}
        public override Rectangle Area()
        {
            return new Rectangle(startX, startY, diameter, diameter);
        }
        public override bool InRectangle(int x, int y)
        {
            Rectangle rect = Area();
            return x >= rect.Left && x <= rect.Right && y >= rect.Top && y <= rect.Bottom;
        }
        public override bool Selected(int x, int y)
        {
            if (!InRectangle(x, y)) return false;
            int cx = startX + diameter / 2;
            int cy = startY + diameter / 2;
            if (filled) return (x - cx) * (x - cx) + (y-cy) * (y-cy) <= diameter * diameter/4;
            else return Math.Abs(1-((double)(x - cx) * (x - cx) + (y - cy) * (y - cy))/(diameter * diameter / 4)) < 0.05;
        }
        public override void Move(int dx, int dy)
        {
            startX += dx;
            startY += dy;
            endX += dx;
            endY += dy;
			circleDrawer.Move(dx, dy);
        }
        public override void PaintMe(PictureBox Canvas)
        {
			Graphics g = Graphics.FromImage(Canvas.Image);
			circleDrawer.Paint(g);
		}
		public override void UpdateObject(Color c, bool a, string n)
		{
			color = c;
			aa = a;
			name = n;
			circleDrawer.Update(c, aa);
		}
		public override List<MyLine> ClipObject(int x, int y, int width, int height)
		{
			return new List<MyLine>();
		}
	}
    public class MyLine : MyObject
    {
		private Painter lineDrawer;
        public MyLine(int sx, int sy, int ex, int ey, Color c, int l, string name = "line", bool aa=false) : base(sx, sy, ex, ey, c,Type.Line,l,name,aa)
		{
			lineDrawer = new LineDrawer(sx, sy, ex, ey, aa, c);
		}
        public override Rectangle Area()
        {
            return new Rectangle(Math.Min(startX, endX), Math.Min(startY, endY), Math.Abs(endX - startX)+1, Math.Abs(endY - startY)+1);
        }
        public override bool InRectangle(int x, int y)
        {
            Rectangle rect = Area();
            return x >= rect.Left && x <= rect.Right && y >= rect.Top && y <= rect.Bottom;
        }
        public override bool Selected(int x, int y)
        {
            return InRectangle(x, y);
            //if (!InRectangle(x, y)) return false;
           // else return Math.Abs(1-(((double)(startY - endY) / (startX - endX)) * x - startY + ((double)(startY - endY) / (startX - endX)) * startX)/y)<0.5;
        }
        public override void Move(int dx, int dy)
        {
            startX += dx;
            startY += dy;
            endX += dx;
            endY += dy;
			lineDrawer.Move(dx, dy);
        }
        public override void PaintMe(PictureBox Canvas)
        {
			Graphics g = Graphics.FromImage(Canvas.Image);
			lineDrawer.Paint(g);
        }
		public override void UpdateObject(Color c, bool a, string n)
		{
			color = c;
			aa = a;
			name = n;
			lineDrawer.Update(c, aa);
		}
		public override List<MyLine> ClipObject(int x, int y, int width, int height)
		{
			List<MyLine> line = new List<MyLine>();
			double[] t = {
				-(double)(startX - x) / (endX - startX), //left
				-(double)(startY - (y + height)) / (endY - startY), //top
				-(double)(startX - (x+width)) / (endX - startX), //right
				-(double)(startY - y) / (endY - startY)}; //bottom
			Point[] Ni = { new Point(-1, 0), new Point(0, 1), new Point(1, 0), new Point(0, -1) };
			Point[] PEi = { new Point(x, y), new Point(x + width, y + height), new Point(x+width, y+height), new Point(x, y) };
			Point D = new Point(endX - startX, endY - startY);
			double te = 0;
			double tl = 1;

			for (int i=0;i<4;i++)
			{
				int NiD = Ni[i].X * D.X + Ni[i].Y * D.Y;
				if (NiD == 0) continue;
				else if(Math.Sign(NiD)<0)
				{
					te = Math.Max(te, t[i]);
				}
				else
				{
					tl = Math.Min(tl, t[i]);
				}
			}
			if (te > tl)
				return null;
			else
			{
				Point EP = new Point(startX + (int)(te * (endX - startX)), startY + (int)(te * (endY - startY)));
				Point LP = new Point(startX + (int)(tl * (endX - startX)), startY + (int)(tl * (endY - startY)));
				line.Add(new MyLine(EP.X, EP.Y, LP.X, LP.Y, color, layer, name, aa));
			}
			return line;
		}
	}
	class MyPolygon : MyObject
	{
		List<Point> points;
		public Painter polygonDrawer;
		public bool filled;
		public MyPolygon(List<Point> ppoints,Color c, int l,bool f, string name = "polygon") : base(ppoints.First().X, ppoints.First().Y, ppoints.First().X, ppoints.First().Y,c,Type.Polygon,l,name,f)
		{
			int count = ppoints.Count;
			filled = f;
			polygonDrawer = new PolygonDrawer(ppoints,c,f);
			points = new List<Point>(count);
			for(int i=0;i< count; i++)
			{
				points.Add(ppoints[i]);
			}
		}
		public override Rectangle Area()
		{
			int minX = points.Min(e => e.X);
			int maxX = points.Max(e => e.X);
			int minY = points.Min(e => e.Y);
			int maxY = points.Max(e => e.Y);
			return new Rectangle(minX, minY, Math.Abs(maxX - minX), Math.Abs(maxY - minY));
		}
		public override bool InRectangle(int x, int y)
		{
			Rectangle rect = Area();
			return x >= rect.Left && x <= rect.Right && y >= rect.Top && y <= rect.Bottom;
		}
		public override bool Selected(int x, int y)
		{
			return InRectangle(x, y);
		}
		public override void Move(int x, int y)
		{
			for(int i=0;i<points.Count;i++)
			{
				points[i] = new Point(points[i].X + x, points[i].Y + y);
			}
			polygonDrawer.Move(x, y);
		}
		public override void PaintMe(PictureBox Canvas)
		{
			Graphics g = Graphics.FromImage(Canvas.Image);
			polygonDrawer.Paint(g);
		}
		public override void UpdateObject(Color c, bool a, string n)
		{
			color = c;
			filled = a;
			name = n;
			polygonDrawer.Update(c, a);
		}
		public override List<MyLine> ClipObject(int x, int y, int width, int height)
		{
			List<MyLine> lines = new List<MyLine>();
			if (filled) return lines;

			for (int i = 0; i < points.Count; i++)
			{
				double[] t = {
					-(double)(points[i].X - x) / (points[(i+1)%points.Count].X - points[i].X), //left
					-(double)(points[i].Y - (y + height)) / (points[(i+1)%points.Count].Y - points[i].Y), //top
					-(double)(points[i].X - (x+width)) / (points[(i+1)%points.Count].X - points[i].X), //right
					-(double)(points[i].Y - y) / (points[(i+1)%points.Count].Y - points[i].Y)}; //bottom
				Point[] Ni = { new Point(-1, 0), new Point(0, 1), new Point(1, 0), new Point(0, -1) };
				Point[] PEi = { new Point(x, y), new Point(x + width, y + height), new Point(x + width, y + height), new Point(x, y) };

				Point D = new Point(points[(i + 1) % points.Count].X - points[i].X, points[(i + 1) % points.Count].Y - points[i].Y);
				double te = 0;
				double tl = 1;

				for (int j = 0; j < 4; j++)
				{
					int NiD = Ni[j].X * D.X + Ni[j].Y * D.Y;
					if (NiD == 0) continue;
					else if (Math.Sign(NiD) < 0)
					{
						te = Math.Max(te, t[j]);
					}
					else
					{
						tl = Math.Min(tl, t[j]);
					}
				}
				if (te <= tl)
				{
					Point EP = new Point(points[i].X + (int)(te * (points[(i + 1) % points.Count].X - points[i].X)), points[i].Y + (int)(te * (points[(i + 1) % points.Count].Y - points[i].Y)));
					Point LP = new Point(points[i].X + (int)(tl * (points[(i + 1) % points.Count].X - points[i].X)), points[i].Y + (int)(tl * (points[(i + 1) % points.Count].Y - points[i].Y)));
					lines.Add(new MyLine(EP.X, EP.Y, LP.X, LP.Y, color, layer, name, aa));
				}
			}
			return lines;
		}
		public void LoadTexture(Bitmap bmp)
		{
			(polygonDrawer as PolygonDrawer).LoadTexture(bmp);
		}
		public void ReduceColors(int k)
		{
			(polygonDrawer as PolygonDrawer).ReduceColors(k);
		}
	}
    class ObjectList
    {
        public List<MyObject> list;
        public int count;
        public ObjectList()
        {
            list = new List<MyObject>();
            count = 0;
        }
        public void Add(MyObject o)
        {
            list.Add(o);
            count++;
        }
        public void Repaint(PictureBox Canvas, bool aa=false)
        {
			for (int i = 0; i < count; i++)
			{
				list[i].PaintMe(Canvas);
			}
        }
        public void Delete(int i)
        {
            list.RemoveAt(i);
            count--;
        }
        public void ChangeOrder(int before, int after)
        {
            MyObject obj = list[before];
            int k = after > before ? 1 : -1;
            for(int i=before;i!=after;i+=k)
            {
                list[i] = list[i+k];
            }
            list[after] = obj;
        }
        public void Clear()
        {
            list.Clear();
            count = 0;
        }
		public Bitmap GetCutScene(int x, int y, int width, int height, bool withgrid=false, int density=0, Color? gridColor=null)
		{
			Bitmap bmp = new Bitmap(width, height);
			if (withgrid)
			{
				Pen pen = new Pen(gridColor.Value);
				pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
				using (Graphics g = Graphics.FromImage(bmp))
				{
					int firstX = density - x % density;
					int firstY = density - y % density;
					for (int i = firstX; i < width; i += density)
					{
						g.DrawLine(pen, i, 0, i, height);
					}
					for (int i = firstY; i < height; i += density)
					{
						g.DrawLine(pen, 0, i, width, i);
					}
				}
			}
			List<MyLine> lines = new List<MyLine>();
			foreach (var obj in list)
			{
				List<MyLine> cutlines = obj.ClipObject(x, y, width, height);
				if(cutlines!=null && cutlines.Count>0)
					lines.AddRange(cutlines);
			}
			using (Graphics g = Graphics.FromImage(bmp))
			{
				foreach (MyLine line in lines)
				{
					if(line.startX==line.endX && line.startY==line.endY)
					{
						g.FillRectangle(new SolidBrush(line.color), line.startX - x, line.startY - y, 1,1);
					}
					else g.DrawLine(new Pen(line.color), line.startX - x, line.startY - y, line.endX - x, line.endY - y);
				}
			}
			return bmp;
		}
	}
}
