﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GK1
{
	public class RGB
	{
		public int R {get; set;}
		public int G { get; set; }
		public int B { get; set; }
		public RGB(int r, int g, int b)
		{
			R = r;
			G = g;
			B = b;
		}
		public HSV ToHSV()
		{
			double rprim = (double)R / 255;
			double gprim = (double)G / 255;
			double bprim = (double)B / 255;
			double Cmax = Math.Max(Math.Max(rprim, gprim), bprim);
			double Cmin = Math.Min(Math.Min(rprim, gprim), bprim);
			double d = Cmax - Cmin;
			double H;
			if (d == 0)
			{
				H = 0;
			}
			else
			{
				if (Cmax == rprim)
				{
					double x = (gprim - bprim) / d;
					if(x<0)
					{
						while (x < 0) x += 6;
						H =60* x;
					}
					else H = 60 * (((gprim - bprim) / d) % 6);
				}
				else if (Cmax == gprim)
					H = 60 * (((bprim - rprim) / d) + 2);
				else H = 60 * (((rprim - gprim) / d) + 4);
			}
			H *= (Math.PI / 180);
			int S = Cmax == 0 ? 0 : (d / Cmax)*100-(int)((d / Cmax) * 100)>=0.5? (int)((d / Cmax) * 100)+1: (int)((d / Cmax) * 100);
			int V= Cmax * 100-(int)(Cmax*100)>=0.5? (int)(Cmax * 100)+1: (int)(Cmax * 100);
			return new HSV(H, S, V);
		}
		public XYZ ToXYZ()
		{
			double[,] M = { {0.4124, 0.3576, 0.1805 },
							{0.2126, 0.7152, 0.0722 },
							{0.0193, 0.1192, 0.9505 }};

			double[] C = { R/255.0, G/255.0, B/255.0 };
			for (int i = 0; i < 3; i++)
			{
				if (C[i] <= 0.04045)
				{
					C[i] = C[i] / 12.92;
				}
				else
				{
					C[i] = Math.Pow(((C[i]+0.055)/1.055),2.4);
				}
			}
			double[] XYZ = new double[3];
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					XYZ[i] += M[i, j] * C[j];
				}
			}
			return new XYZ(XYZ[0], XYZ[1], XYZ[2]);
		}
	}
	public class HSV
	{
		public double H { get; set; }
		public int S { get; set; }
		public int V { get; set; }
		public HSV(double h, int s, int v)
		{
			H = h;
			S = s;
			V = v;
		}
		public RGB ToRGB()
		{
			double c = (double)(S * V) / 10000;
			double h = H* (180.0 / Math.PI);
			double x = c * (1 - Math.Abs((h / 60) % 2 - 1));
			double rprim, gprim, bprim;

			if (h >= 0 && h < 60)
			{
				rprim = c;
				gprim = x;
				bprim = 0;
			}
			else if (h >= 60 && h < 120)
			{
				rprim = x;
				gprim = c;
				bprim = 0;
			}
			else if (h >= 120 && h < 180)
			{
				rprim = 0;
				gprim = c;
				bprim = x;
			}
			else if (h >= 180 && h < 240)
			{
				rprim = 0;
				gprim = x;
				bprim = c;
			}
			else if (h >= 240 && h < 300)
			{
				rprim = x;
				gprim = 0;
				bprim = c;
			}
			else
			{
				rprim = c;
				gprim = 0;
				bprim = x;
			}
			double m = (double)V / 100 - c;
			return new RGB((int)((rprim + m) * 255), (int)((gprim + m) * 255), (int)((bprim + m) * 255));
		}
	}
	public class XYZ
	{
		public bool ColorCut=false;
		public double X { get; set; }
		public double Y { get; set; }
		public double Z { get; set; }
		public XYZ(double x, double y, double z)
		{
			X = x;
			Y = y;
			Z = z;
		}
		public RGB ToRGB()
		{
			double[,] M = { {3.2406, -1.5372, -0.4986},
							{-0.9689, 1.8758, 0.0415},
							{0.0557, -0.2040, 1.0570}};

			double[] XYZ = { X, Y, Z };
			double[] C = new double[3];
			for(int i=0;i<3;i++)
			{
				for(int j=0;j<3;j++)
				{
					C[i] += M[i, j] * XYZ[j];
				}
			}
			for(int i=0;i<3;i++)
			{
				if(C[i]<=0.0031308)
				{
					C[i] = C[i] * 12.92;
				}
				else
				{
					C[i] = 1.055 * Math.Pow(C[i], (1.0 / 2.4)) - 0.055;
				}
				if (C[i] < 0 || C[i] > 1) return null;
			}
			int R = 255 * C[0] - (int)(255 * C[0]) >= 0.5 ? (int)(255 * C[0]) + 1 : (int)(255 * C[0]);
			int G = 255 * C[1] - (int)(255 * C[1]) >= 0.5 ? (int)(255 * C[1]) + 1 : (int)(255 * C[1]);
			int B = 255 * C[2] - (int)(255 * C[2]) >= 0.5 ? (int)(255 * C[2]) + 1 : (int)(255 * C[2]);
			return new RGB(R, G, B);
			
		}
	}
}
