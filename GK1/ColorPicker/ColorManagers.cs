﻿using GK1.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GK1
{
	class RGBManager
	{
		public RGB PicekdColor;
		public RGB ColorUnderCross;
		PictureBox MainPB;
		bool crossApeared=false;
		int crossX;
		int crossY;
		int OldCrossX;
		int OldCrossY;
		Bitmap cross = Resources.cross;
		public RGBManager(PictureBox mainPB, PictureBox x, PictureBox y)
		{
			MainPB = mainPB;
			InitalizeRGBPB(mainPB);
			InitalizeAxesPBs(x, y);
		}
		private void SetBackgroung()
		{
			SolidBrush brush = new SolidBrush(Color.White);
			Bitmap bmp = new Bitmap(256, 256);
			using (Graphics graph = Graphics.FromImage(bmp))
			{
				Rectangle ImageSize = new Rectangle(0, 0, 256, 256);
				graph.FillRectangle(brush, ImageSize);
			}
			MainPB.Image = bmp;
		}
		private void InitalizeRGBPB(PictureBox RGBPictureBox)
		{
			SetBackgroung();
			for (int g = 0; g <= 255; g++)
			{
				for (int r = 0; r <= 255; r++)
				{
					using (Graphics gr = Graphics.FromImage(RGBPictureBox.Image))
					{
						gr.FillRectangle(new SolidBrush(Color.FromArgb(r, g, 0)), g, r, 1, 1);
					}
				}
			}
		}
		private void InitalizeAxesPBs(PictureBox x, PictureBox y)
		{
			Bitmap Xbmp = Resources.RGBXaxis;
			Bitmap Ybmp = Resources.RGBYaxis;
			x.Image = Xbmp;
			y.Image = Ybmp;
		}
		public void RepaintPB(int b)
		{
			for (int g = 0; g <= 255; g++)
			{
				for (int r = 0; r <= 255; r++)
				{
					using (Graphics gr = Graphics.FromImage(MainPB.Image))
					{
						gr.FillRectangle(new SolidBrush(Color.FromArgb(r, g, b)), g, r, 1, 1);
					}
				}
			}
			if(crossApeared)
			{
				using (Graphics gr = Graphics.FromImage(MainPB.Image))
				{
					gr.DrawImage(cross, crossX - 9, crossY - 9, 19, 19);
				}
				ColorUnderCross = CalculateColor(crossX, crossY);
			}
			MainPB.Invalidate();
		}
		public void PickColorFromPB(int x, int y)
		{
			PicekdColor = CalculateColor(x, y);
			crossApeared = true;
			crossX = x;
			crossY = y;
			DrawCross(x, y);
		}
		public void PickSpecificColor(int r, int g, int b)
		{
			(int x, int y) = CalculateXY(r, g, b);
			crossApeared = true;
			crossX = x;
			crossY = y;
			RepaintPB(b);
			RGB color = new RGB(r,g,b);
			PicekdColor = color;
			DrawCross(x, y);
		}
		private void DrawCross(int x, int y)
		{
			if(crossApeared) RepaintPB((int)PicekdColor.B);
		}
		private RGB CalculateColor(int x, int y)
		{
			Bitmap bmp = new Bitmap(MainPB.Image);
			Color c = bmp.GetPixel(x, y);
			RGB color = new RGB(c.R,c.G,c.B);
			return color;
		}
		private (int x, int y) CalculateXY(int r, int g, int b)
		{
			return (g,r);
		}
		public void RemeberCrossPosition()
		{
			OldCrossX = crossX;
			OldCrossY = crossY;
		}
		public void ResetCrossPosition()
		{
			crossX = OldCrossX;
			crossY = OldCrossY;
		}
	}
	class HSVManager
	{
		public HSV PicekdColor;
		public HSV ColorUnderCross;
		PictureBox MainPB;
		bool crossApeared = false;
		int crossX;
		int crossY;
		int OldCrossX;
		int OldCrossY;
		Bitmap cross = Resources.cross;
		public HSVManager(PictureBox mainPB, PictureBox x, PictureBox y)
		{
			MainPB = mainPB;
			InitalizeHSVPB(mainPB);
			InitalizeAxesPBs(x, y);
		}
		private void SetBackgroung()
		{
			SolidBrush brush = new SolidBrush(Color.White);
			Bitmap bmp = new Bitmap(256, 256);
			using (Graphics graph = Graphics.FromImage(bmp))
			{
				Rectangle ImageSize = new Rectangle(0, 0, 256, 256);
				graph.FillRectangle(brush, ImageSize);
			}
			MainPB.Image = bmp;
		}
		private void InitalizeHSVPB(PictureBox HSVPictureBox)
		{
			SetBackgroung();
			for (double h = 0; h <=2*Math.PI; h+=(Math.PI/180))
			{
				for (int s = 0; s <= 100; s++)
				{
					HSV hSV = new HSV(h, s, 100);
					RGB rGB = hSV.ToRGB();
					Color c = Color.FromArgb(rGB.R, rGB.G, rGB.B);
					using (Graphics gr = Graphics.FromImage(HSVPictureBox.Image))
					{
						gr.FillRectangle(new SolidBrush(c), (int)((s * (126.0 / 100)) * Math.Cos(h) + 128),(int)((s * (126.0 / 100)) * Math.Sin(h)+128), 3, 3);
					}
				}
			}
		}
		private void InitalizeAxesPBs(PictureBox x, PictureBox y)
		{
			Bitmap Xbmp = Resources.HSVXaxis; 
			Bitmap Ybmp = Resources.HSVYaxis; 
			x.Image = Xbmp;
			y.Image = Ybmp;
		}
		public void RepaintPB(int v)
		{
			SetBackgroung();
			for (double h = 0; h <= 2 * Math.PI; h += (Math.PI / 180))
			{
				for (int s = 0; s <= 100; s++)
				{
					HSV hSV = new HSV(h, s, v);
					RGB rGB = hSV.ToRGB();
					Color c = Color.FromArgb(rGB.R, rGB.G, rGB.B);
					using (Graphics gr = Graphics.FromImage(MainPB.Image))
					{
						gr.FillRectangle(new SolidBrush(c), (int)((s * (126.0 / 100)) * Math.Cos(h) + 128), (int)((s * (126.0 / 100)) * Math.Sin(h) + 128), 3, 3);
					}
				}
			}
			if (crossApeared)
			{
				using (Graphics gr = Graphics.FromImage(MainPB.Image))
				{
					gr.DrawImage(cross, crossX - 9, crossY - 9, 19, 19);
				}
				ColorUnderCross = CalculateColor(crossX, crossY);
			}
			MainPB.Invalidate();
		}
		public void PickColorFromPB(int x, int y)
		{
			PicekdColor = CalculateColor(x, y);
			crossApeared = true;
			crossX = x;
			crossY = y;
			DrawCross(x, y);
		}
		public void PickSpecificColor(double h, int s, int v)
		{
			(int x, int y) = CalculateXY(h, s, v);
			crossApeared = true;
			crossX = x;
			crossY = y;
			RepaintPB(v);
			HSV color = new HSV(h,s,v);
			PicekdColor = color;
			DrawCross(x, y);
		}
		private void DrawCross(int x, int y)
		{
			if (crossApeared) RepaintPB((int)PicekdColor.V);
		}
		private HSV CalculateColor(int x, int y)
		{
			Bitmap bmp = new Bitmap(MainPB.Image);
			Color c = bmp.GetPixel(x, y);
			RGB rgbC = new RGB(c.R, c.G, c.B);
			HSV color = rgbC.ToHSV();
			return color;
		}
		private (int x, int y) CalculateXY(double h, int s, int v)
		{
			return ((int)((s * (126.0 / 100)) * Math.Cos(h) + 128), (int)((s * (126.0 / 100)) * Math.Sin(h) + 128));
		}
		public void RemeberCrossPosition()
		{
			OldCrossX = crossX;
			OldCrossY = crossY;
		}
		public void ResetCrossPosition()
		{
			crossX = OldCrossX;
			crossY = OldCrossY;
		}
	}
	class XYZManager
	{
		public XYZ PicekdColor;
		public XYZ ColorUnderCross;
		PictureBox MainPB;
		bool crossApeared = false;
		public int crossX;
		public int crossY;
		int OldCrossX;
		int OldCrossY;
		Bitmap cross = Resources.cross;
		public XYZManager(PictureBox mainPB, PictureBox x, PictureBox y)
		{
			MainPB = mainPB;
			InitalizeXYZPB(mainPB);
			InitalizeAxesPBs(x, y);
		}
		private void SetBackgroung()
		{
			SolidBrush brush = new SolidBrush(Color.White);
			Bitmap bmp = new Bitmap(256, 256);
			using (Graphics graph = Graphics.FromImage(bmp))
			{
				Rectangle ImageSize = new Rectangle(0, 0, 256, 256);
				graph.FillRectangle(brush, ImageSize);
			}
			MainPB.Image = bmp;
		}
		private void InitalizeXYZPB(PictureBox XYZPictureBox)
		{
			SetBackgroung();
			for (double x = 0; x <= 1.0; x += 1.0/256)
			{
				for (double y = 0; y <= 1.0; y += 1.0/ 256)
				{
					XYZ xyz = new XYZ(x, y,0.55);
					RGB rGB = xyz.ToRGB();
					if (rGB != null)
					{
						Color c = Color.FromArgb(rGB.R, rGB.G, rGB.B);
						using (Graphics gr = Graphics.FromImage(XYZPictureBox.Image))
						{
							gr.FillRectangle(new SolidBrush(c), (int)(x *256), (int)(y *256), 1, 1);
						}
					}
				}
			}
		}
		private void InitalizeAxesPBs(PictureBox x, PictureBox y)
		{
			Bitmap Xbmp = Resources.XYZXaxis; 
			Bitmap Ybmp = Resources.XYZYaxis;
			x.Image = Xbmp;
			y.Image = Ybmp;
		}
		public void RepaintPB(double z)
		{
			SetBackgroung();
			for (double x = 0; x <= 1.0; x += 1.0 / 256)
			{
				for (double y = 0; y <= 1.0; y += 1.0 / 256)
				{
					XYZ xyz = new XYZ(x, y, z);
					RGB rGB = xyz.ToRGB();
					if (rGB != null)
					{
						Color c = Color.FromArgb(rGB.R, rGB.G, rGB.B);
						using (Graphics gr = Graphics.FromImage(MainPB.Image))
						{
							gr.FillRectangle(new SolidBrush(c), (int)(x * 256), (int)(y * 256), 1, 1);
						}
					}
				}
			}
			if (crossApeared)
			{
				using (Graphics gr = Graphics.FromImage(MainPB.Image))
				{
					gr.DrawImage(cross, crossX - 9, crossY - 9, 19, 19);
				}
				ColorUnderCross = CalculateColor(crossX, crossY);
			}
			MainPB.Invalidate();
		}
		public void PickColorFromPB(int x, int y)
		{
			PicekdColor = CalculateColor(x, y);
			crossApeared = true;
			crossX = x;
			crossY = y;
			DrawCross(x, y);
		}
		public void PickSpecificColor(double xx, double yy, double zz)
		{
			(int x, int y) = CalculateXY(xx, yy, zz);
			crossApeared = true;
			crossX = x;
			crossY = y;
			RepaintPB(zz);
			XYZ color = new XYZ(xx, yy, zz);
			PicekdColor = color;
			DrawCross(x, y);
		}
		private void DrawCross(int x, int y)
		{
			if (crossApeared) RepaintPB(PicekdColor.Z);
		}
		private XYZ CalculateColor(int x, int y)
		{
			Bitmap bmp = new Bitmap(MainPB.Image);
			Color c = bmp.GetPixel(x, y);
			RGB rgbC = new RGB(c.R, c.G, c.B);
			XYZ color = rgbC.ToXYZ();
			return color;
		}
		private (int x, int y) CalculateXY(double x, double y, double z)
		{
			return ((int)(x * 255), (int)(y * 255));
		}
		public void RemeberCrossPosition()
		{
			OldCrossX = crossX;
			OldCrossY = crossY;
		}
		public void ResetCrossPosition()
		{
			crossX = OldCrossX;
			crossY = OldCrossY;
		}
	}
}
