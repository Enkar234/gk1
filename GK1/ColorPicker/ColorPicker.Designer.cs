﻿namespace GK1
{
	partial class ColorPicker
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.MainTabControl = new System.Windows.Forms.TabControl();
			this.RGBtab = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.RGBslider = new System.Windows.Forms.TrackBar();
			this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
			this.RGBBTB = new System.Windows.Forms.TextBox();
			this.RGBGTB = new System.Windows.Forms.TextBox();
			this.Rlabel = new System.Windows.Forms.Label();
			this.RGBRTB = new System.Windows.Forms.TextBox();
			this.Glabel = new System.Windows.Forms.Label();
			this.Blabel = new System.Windows.Forms.Label();
			this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
			this.RGBPickedColor = new System.Windows.Forms.Button();
			this.RGBCurrentColor = new System.Windows.Forms.Button();
			this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
			this.RGBYaxis = new System.Windows.Forms.PictureBox();
			this.RGBPictureBox = new System.Windows.Forms.PictureBox();
			this.RGBXaxis = new System.Windows.Forms.PictureBox();
			this.label4 = new System.Windows.Forms.Label();
			this.Tab0PB = new System.Windows.Forms.PictureBox();
			this.OKbutton1 = new System.Windows.Forms.Button();
			this.HSVtab = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.HSVslider = new System.Windows.Forms.TrackBar();
			this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
			this.HSVVTB = new System.Windows.Forms.TextBox();
			this.HSVSTB = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.HSVHTB = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
			this.HSVPickedColor = new System.Windows.Forms.Button();
			this.HSVCurrentColor = new System.Windows.Forms.Button();
			this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
			this.HSVYaxis = new System.Windows.Forms.PictureBox();
			this.HSVPictureBox = new System.Windows.Forms.PictureBox();
			this.HSVXaxis = new System.Windows.Forms.PictureBox();
			this.label8 = new System.Windows.Forms.Label();
			this.Tab1PB = new System.Windows.Forms.PictureBox();
			this.OKbutton2 = new System.Windows.Forms.Button();
			this.CIEXYZtab = new System.Windows.Forms.TabPage();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.XYZslider = new System.Windows.Forms.TrackBar();
			this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
			this.XYZZTB = new System.Windows.Forms.TextBox();
			this.XYZYTB = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.XYZXTB = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
			this.XYZPickedColor = new System.Windows.Forms.Button();
			this.XYZCurrentColor = new System.Windows.Forms.Button();
			this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
			this.XYZYaxis = new System.Windows.Forms.PictureBox();
			this.XYZPictureBox = new System.Windows.Forms.PictureBox();
			this.XYZXaxis = new System.Windows.Forms.PictureBox();
			this.label12 = new System.Windows.Forms.Label();
			this.Tab2PB = new System.Windows.Forms.PictureBox();
			this.OKbutton3 = new System.Windows.Forms.Button();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.MainTabControl.SuspendLayout();
			this.RGBtab.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.RGBslider)).BeginInit();
			this.tableLayoutPanel4.SuspendLayout();
			this.tableLayoutPanel5.SuspendLayout();
			this.tableLayoutPanel6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.RGBYaxis)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.RGBPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.RGBXaxis)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Tab0PB)).BeginInit();
			this.HSVtab.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.HSVslider)).BeginInit();
			this.tableLayoutPanel7.SuspendLayout();
			this.tableLayoutPanel8.SuspendLayout();
			this.tableLayoutPanel9.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.HSVYaxis)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.HSVPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.HSVXaxis)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Tab1PB)).BeginInit();
			this.CIEXYZtab.SuspendLayout();
			this.tableLayoutPanel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.XYZslider)).BeginInit();
			this.tableLayoutPanel10.SuspendLayout();
			this.tableLayoutPanel11.SuspendLayout();
			this.tableLayoutPanel12.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.XYZYaxis)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.XYZPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.XYZXaxis)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Tab2PB)).BeginInit();
			this.tabControl1.SuspendLayout();
			this.SuspendLayout();
			// 
			// MainTabControl
			// 
			this.MainTabControl.Controls.Add(this.RGBtab);
			this.MainTabControl.Controls.Add(this.HSVtab);
			this.MainTabControl.Controls.Add(this.CIEXYZtab);
			this.MainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.MainTabControl.Location = new System.Drawing.Point(0, 0);
			this.MainTabControl.Name = "MainTabControl";
			this.MainTabControl.SelectedIndex = 0;
			this.MainTabControl.Size = new System.Drawing.Size(424, 421);
			this.MainTabControl.TabIndex = 0;
			this.MainTabControl.SelectedIndexChanged += new System.EventHandler(this.MainTabControl_SelectedIndexChanged);
			// 
			// RGBtab
			// 
			this.RGBtab.Controls.Add(this.tableLayoutPanel1);
			this.RGBtab.Location = new System.Drawing.Point(4, 22);
			this.RGBtab.Name = "RGBtab";
			this.RGBtab.Padding = new System.Windows.Forms.Padding(3);
			this.RGBtab.Size = new System.Drawing.Size(416, 395);
			this.RGBtab.TabIndex = 0;
			this.RGBtab.Text = "RGB";
			this.RGBtab.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 305F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
			this.tableLayoutPanel1.Controls.Add(this.RGBslider, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.label4, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.Tab0PB, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.OKbutton1, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 297F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(410, 389);
			this.tableLayoutPanel1.TabIndex = 4;
			// 
			// RGBslider
			// 
			this.RGBslider.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RGBslider.Location = new System.Drawing.Point(3, 338);
			this.RGBslider.Maximum = 255;
			this.RGBslider.Name = "RGBslider";
			this.RGBslider.Size = new System.Drawing.Size(299, 48);
			this.RGBslider.TabIndex = 1;
			this.RGBslider.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RGBslider_MouseUp);
			// 
			// tableLayoutPanel4
			// 
			this.tableLayoutPanel4.ColumnCount = 1;
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel4.Controls.Add(this.RGBBTB, 0, 5);
			this.tableLayoutPanel4.Controls.Add(this.RGBGTB, 0, 3);
			this.tableLayoutPanel4.Controls.Add(this.Rlabel, 0, 0);
			this.tableLayoutPanel4.Controls.Add(this.RGBRTB, 0, 1);
			this.tableLayoutPanel4.Controls.Add(this.Glabel, 0, 2);
			this.tableLayoutPanel4.Controls.Add(this.Blabel, 0, 4);
			this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 0, 7);
			this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel4.Location = new System.Drawing.Point(308, 41);
			this.tableLayoutPanel4.Name = "tableLayoutPanel4";
			this.tableLayoutPanel4.RowCount = 8;
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel4.Size = new System.Drawing.Size(99, 291);
			this.tableLayoutPanel4.TabIndex = 4;
			// 
			// RGBBTB
			// 
			this.RGBBTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.RGBBTB.Location = new System.Drawing.Point(3, 94);
			this.RGBBTB.Name = "RGBBTB";
			this.RGBBTB.Size = new System.Drawing.Size(93, 20);
			this.RGBBTB.TabIndex = 7;
			this.RGBBTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.RGBBTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BlueTB_KeyDown);
			// 
			// RGBGTB
			// 
			this.RGBGTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.RGBGTB.Location = new System.Drawing.Point(3, 55);
			this.RGBGTB.Name = "RGBGTB";
			this.RGBGTB.Size = new System.Drawing.Size(93, 20);
			this.RGBGTB.TabIndex = 6;
			this.RGBGTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.RGBGTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GreenTB_KeyDown);
			// 
			// Rlabel
			// 
			this.Rlabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.Rlabel.AutoSize = true;
			this.Rlabel.Location = new System.Drawing.Point(3, 0);
			this.Rlabel.Name = "Rlabel";
			this.Rlabel.Size = new System.Drawing.Size(93, 13);
			this.Rlabel.TabIndex = 0;
			this.Rlabel.Text = "Red [0-255]";
			this.Rlabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// RGBRTB
			// 
			this.RGBRTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.RGBRTB.Location = new System.Drawing.Point(3, 16);
			this.RGBRTB.Name = "RGBRTB";
			this.RGBRTB.Size = new System.Drawing.Size(93, 20);
			this.RGBRTB.TabIndex = 1;
			this.RGBRTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.RGBRTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RedTB_KeyDown);
			// 
			// Glabel
			// 
			this.Glabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.Glabel.AutoSize = true;
			this.Glabel.Location = new System.Drawing.Point(3, 39);
			this.Glabel.Name = "Glabel";
			this.Glabel.Size = new System.Drawing.Size(93, 13);
			this.Glabel.TabIndex = 4;
			this.Glabel.Text = "Green [0-255]";
			this.Glabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Blabel
			// 
			this.Blabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.Blabel.AutoSize = true;
			this.Blabel.Location = new System.Drawing.Point(3, 78);
			this.Blabel.Name = "Blabel";
			this.Blabel.Size = new System.Drawing.Size(93, 13);
			this.Blabel.TabIndex = 5;
			this.Blabel.Text = "Blue [0-255]";
			this.Blabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tableLayoutPanel5
			// 
			this.tableLayoutPanel5.ColumnCount = 1;
			this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel5.Controls.Add(this.RGBPickedColor, 0, 0);
			this.tableLayoutPanel5.Controls.Add(this.RGBCurrentColor, 0, 1);
			this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 120);
			this.tableLayoutPanel5.Name = "tableLayoutPanel5";
			this.tableLayoutPanel5.RowCount = 2;
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel5.Size = new System.Drawing.Size(93, 168);
			this.tableLayoutPanel5.TabIndex = 8;
			// 
			// RGBPickedColor
			// 
			this.RGBPickedColor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RGBPickedColor.Enabled = false;
			this.RGBPickedColor.Location = new System.Drawing.Point(3, 3);
			this.RGBPickedColor.Name = "RGBPickedColor";
			this.RGBPickedColor.Size = new System.Drawing.Size(87, 78);
			this.RGBPickedColor.TabIndex = 0;
			this.RGBPickedColor.UseVisualStyleBackColor = true;
			// 
			// RGBCurrentColor
			// 
			this.RGBCurrentColor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RGBCurrentColor.Enabled = false;
			this.RGBCurrentColor.Location = new System.Drawing.Point(3, 87);
			this.RGBCurrentColor.Name = "RGBCurrentColor";
			this.RGBCurrentColor.Size = new System.Drawing.Size(87, 78);
			this.RGBCurrentColor.TabIndex = 1;
			this.RGBCurrentColor.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel6
			// 
			this.tableLayoutPanel6.ColumnCount = 2;
			this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 39F));
			this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel6.Controls.Add(this.RGBYaxis, 0, 1);
			this.tableLayoutPanel6.Controls.Add(this.RGBPictureBox, 1, 1);
			this.tableLayoutPanel6.Controls.Add(this.RGBXaxis, 1, 0);
			this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 41);
			this.tableLayoutPanel6.Name = "tableLayoutPanel6";
			this.tableLayoutPanel6.RowCount = 2;
			this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 262F));
			this.tableLayoutPanel6.Size = new System.Drawing.Size(299, 291);
			this.tableLayoutPanel6.TabIndex = 5;
			// 
			// RGBYaxis
			// 
			this.RGBYaxis.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RGBYaxis.Location = new System.Drawing.Point(3, 32);
			this.RGBYaxis.MaximumSize = new System.Drawing.Size(36, 256);
			this.RGBYaxis.MinimumSize = new System.Drawing.Size(36, 256);
			this.RGBYaxis.Name = "RGBYaxis";
			this.RGBYaxis.Size = new System.Drawing.Size(36, 256);
			this.RGBYaxis.TabIndex = 5;
			this.RGBYaxis.TabStop = false;
			// 
			// RGBPictureBox
			// 
			this.RGBPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.RGBPictureBox.Location = new System.Drawing.Point(42, 32);
			this.RGBPictureBox.MaximumSize = new System.Drawing.Size(256, 256);
			this.RGBPictureBox.MinimumSize = new System.Drawing.Size(256, 256);
			this.RGBPictureBox.Name = "RGBPictureBox";
			this.RGBPictureBox.Size = new System.Drawing.Size(256, 256);
			this.RGBPictureBox.TabIndex = 4;
			this.RGBPictureBox.TabStop = false;
			this.RGBPictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RGBPictureBox_MouseClick);
			// 
			// RGBXaxis
			// 
			this.RGBXaxis.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RGBXaxis.Location = new System.Drawing.Point(42, 3);
			this.RGBXaxis.MaximumSize = new System.Drawing.Size(256, 26);
			this.RGBXaxis.MinimumSize = new System.Drawing.Size(256, 26);
			this.RGBXaxis.Name = "RGBXaxis";
			this.RGBXaxis.Size = new System.Drawing.Size(256, 26);
			this.RGBXaxis.TabIndex = 6;
			this.RGBXaxis.TabStop = false;
			// 
			// label4
			// 
			this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(310, 355);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(94, 13);
			this.label4.TabIndex = 6;
			this.label4.Text = "Slider sets B value";
			// 
			// Tab0PB
			// 
			this.Tab0PB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Tab0PB.Location = new System.Drawing.Point(3, 3);
			this.Tab0PB.Name = "Tab0PB";
			this.Tab0PB.Size = new System.Drawing.Size(299, 32);
			this.Tab0PB.TabIndex = 7;
			this.Tab0PB.TabStop = false;
			// 
			// OKbutton1
			// 
			this.OKbutton1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.OKbutton1.Location = new System.Drawing.Point(308, 3);
			this.OKbutton1.Name = "OKbutton1";
			this.OKbutton1.Size = new System.Drawing.Size(99, 32);
			this.OKbutton1.TabIndex = 8;
			this.OKbutton1.Text = "OK";
			this.OKbutton1.UseVisualStyleBackColor = true;
			this.OKbutton1.Click += new System.EventHandler(this.OKbutton1_Click);
			// 
			// HSVtab
			// 
			this.HSVtab.Controls.Add(this.tableLayoutPanel2);
			this.HSVtab.Location = new System.Drawing.Point(4, 22);
			this.HSVtab.Name = "HSVtab";
			this.HSVtab.Padding = new System.Windows.Forms.Padding(3);
			this.HSVtab.Size = new System.Drawing.Size(416, 395);
			this.HSVtab.TabIndex = 1;
			this.HSVtab.Text = "HSV";
			this.HSVtab.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 2;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 305F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
			this.tableLayoutPanel2.Controls.Add(this.HSVslider, 0, 2);
			this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel7, 1, 1);
			this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel9, 0, 1);
			this.tableLayoutPanel2.Controls.Add(this.label8, 1, 2);
			this.tableLayoutPanel2.Controls.Add(this.Tab1PB, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.OKbutton2, 1, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 3;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 297F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(410, 389);
			this.tableLayoutPanel2.TabIndex = 5;
			// 
			// HSVslider
			// 
			this.HSVslider.Dock = System.Windows.Forms.DockStyle.Fill;
			this.HSVslider.Location = new System.Drawing.Point(3, 338);
			this.HSVslider.Maximum = 100;
			this.HSVslider.Name = "HSVslider";
			this.HSVslider.Size = new System.Drawing.Size(299, 48);
			this.HSVslider.TabIndex = 1;
			this.HSVslider.Value = 100;
			this.HSVslider.MouseUp += new System.Windows.Forms.MouseEventHandler(this.HSVSlider_MouseUp);
			// 
			// tableLayoutPanel7
			// 
			this.tableLayoutPanel7.ColumnCount = 1;
			this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel7.Controls.Add(this.HSVVTB, 0, 5);
			this.tableLayoutPanel7.Controls.Add(this.HSVSTB, 0, 3);
			this.tableLayoutPanel7.Controls.Add(this.label5, 0, 0);
			this.tableLayoutPanel7.Controls.Add(this.HSVHTB, 0, 1);
			this.tableLayoutPanel7.Controls.Add(this.label6, 0, 2);
			this.tableLayoutPanel7.Controls.Add(this.label7, 0, 4);
			this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel8, 0, 7);
			this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel7.Location = new System.Drawing.Point(308, 41);
			this.tableLayoutPanel7.Name = "tableLayoutPanel7";
			this.tableLayoutPanel7.RowCount = 8;
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel7.Size = new System.Drawing.Size(99, 291);
			this.tableLayoutPanel7.TabIndex = 4;
			// 
			// HSVVTB
			// 
			this.HSVVTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.HSVVTB.Location = new System.Drawing.Point(3, 107);
			this.HSVVTB.Name = "HSVVTB";
			this.HSVVTB.Size = new System.Drawing.Size(93, 20);
			this.HSVVTB.TabIndex = 7;
			this.HSVVTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.HSVVTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HSVVTB_KeyDown);
			// 
			// HSVSTB
			// 
			this.HSVSTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.HSVSTB.Location = new System.Drawing.Point(3, 68);
			this.HSVSTB.Name = "HSVSTB";
			this.HSVSTB.Size = new System.Drawing.Size(93, 20);
			this.HSVSTB.TabIndex = 6;
			this.HSVSTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.HSVSTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HSVSTB_KeyDown);
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(3, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(93, 13);
			this.label5.TabIndex = 0;
			this.label5.Text = "Hue [0-360°]";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// HSVHTB
			// 
			this.HSVHTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.HSVHTB.Location = new System.Drawing.Point(3, 16);
			this.HSVHTB.Name = "HSVHTB";
			this.HSVHTB.Size = new System.Drawing.Size(93, 20);
			this.HSVHTB.TabIndex = 1;
			this.HSVHTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.HSVHTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HSVHTB_KeyDown);
			// 
			// label6
			// 
			this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(3, 39);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(93, 26);
			this.label6.TabIndex = 4;
			this.label6.Text = "Saturation [0-100%]";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label7
			// 
			this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(3, 91);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(93, 13);
			this.label7.TabIndex = 5;
			this.label7.Text = "Value [0-100%]";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tableLayoutPanel8
			// 
			this.tableLayoutPanel8.ColumnCount = 1;
			this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel8.Controls.Add(this.HSVPickedColor, 0, 0);
			this.tableLayoutPanel8.Controls.Add(this.HSVCurrentColor, 0, 1);
			this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 133);
			this.tableLayoutPanel8.Name = "tableLayoutPanel8";
			this.tableLayoutPanel8.RowCount = 2;
			this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel8.Size = new System.Drawing.Size(93, 155);
			this.tableLayoutPanel8.TabIndex = 8;
			// 
			// HSVPickedColor
			// 
			this.HSVPickedColor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.HSVPickedColor.Enabled = false;
			this.HSVPickedColor.Location = new System.Drawing.Point(3, 3);
			this.HSVPickedColor.Name = "HSVPickedColor";
			this.HSVPickedColor.Size = new System.Drawing.Size(87, 71);
			this.HSVPickedColor.TabIndex = 0;
			this.HSVPickedColor.UseVisualStyleBackColor = true;
			// 
			// HSVCurrentColor
			// 
			this.HSVCurrentColor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.HSVCurrentColor.Enabled = false;
			this.HSVCurrentColor.Location = new System.Drawing.Point(3, 80);
			this.HSVCurrentColor.Name = "HSVCurrentColor";
			this.HSVCurrentColor.Size = new System.Drawing.Size(87, 72);
			this.HSVCurrentColor.TabIndex = 1;
			this.HSVCurrentColor.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel9
			// 
			this.tableLayoutPanel9.ColumnCount = 2;
			this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 39F));
			this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel9.Controls.Add(this.HSVYaxis, 0, 1);
			this.tableLayoutPanel9.Controls.Add(this.HSVPictureBox, 1, 1);
			this.tableLayoutPanel9.Controls.Add(this.HSVXaxis, 1, 0);
			this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 41);
			this.tableLayoutPanel9.Name = "tableLayoutPanel9";
			this.tableLayoutPanel9.RowCount = 2;
			this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 262F));
			this.tableLayoutPanel9.Size = new System.Drawing.Size(299, 291);
			this.tableLayoutPanel9.TabIndex = 5;
			// 
			// HSVYaxis
			// 
			this.HSVYaxis.Dock = System.Windows.Forms.DockStyle.Fill;
			this.HSVYaxis.Location = new System.Drawing.Point(3, 32);
			this.HSVYaxis.MaximumSize = new System.Drawing.Size(36, 256);
			this.HSVYaxis.MinimumSize = new System.Drawing.Size(36, 256);
			this.HSVYaxis.Name = "HSVYaxis";
			this.HSVYaxis.Size = new System.Drawing.Size(36, 256);
			this.HSVYaxis.TabIndex = 5;
			this.HSVYaxis.TabStop = false;
			// 
			// HSVPictureBox
			// 
			this.HSVPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.HSVPictureBox.Location = new System.Drawing.Point(42, 32);
			this.HSVPictureBox.MaximumSize = new System.Drawing.Size(256, 256);
			this.HSVPictureBox.MinimumSize = new System.Drawing.Size(256, 256);
			this.HSVPictureBox.Name = "HSVPictureBox";
			this.HSVPictureBox.Size = new System.Drawing.Size(256, 256);
			this.HSVPictureBox.TabIndex = 4;
			this.HSVPictureBox.TabStop = false;
			this.HSVPictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.HSVPictureBox_MouseClick);
			// 
			// HSVXaxis
			// 
			this.HSVXaxis.Dock = System.Windows.Forms.DockStyle.Fill;
			this.HSVXaxis.Location = new System.Drawing.Point(42, 3);
			this.HSVXaxis.MaximumSize = new System.Drawing.Size(256, 26);
			this.HSVXaxis.MinimumSize = new System.Drawing.Size(256, 26);
			this.HSVXaxis.Name = "HSVXaxis";
			this.HSVXaxis.Size = new System.Drawing.Size(256, 26);
			this.HSVXaxis.TabIndex = 6;
			this.HSVXaxis.TabStop = false;
			// 
			// label8
			// 
			this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(310, 355);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(94, 13);
			this.label8.TabIndex = 6;
			this.label8.Text = "Slider sets V value";
			// 
			// Tab1PB
			// 
			this.Tab1PB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Tab1PB.Location = new System.Drawing.Point(3, 3);
			this.Tab1PB.Name = "Tab1PB";
			this.Tab1PB.Size = new System.Drawing.Size(299, 32);
			this.Tab1PB.TabIndex = 7;
			this.Tab1PB.TabStop = false;
			// 
			// OKbutton2
			// 
			this.OKbutton2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.OKbutton2.Location = new System.Drawing.Point(308, 3);
			this.OKbutton2.Name = "OKbutton2";
			this.OKbutton2.Size = new System.Drawing.Size(99, 32);
			this.OKbutton2.TabIndex = 8;
			this.OKbutton2.Text = "OK";
			this.OKbutton2.UseVisualStyleBackColor = true;
			this.OKbutton2.Click += new System.EventHandler(this.OKbutton2_Click);
			// 
			// CIEXYZtab
			// 
			this.CIEXYZtab.Controls.Add(this.tableLayoutPanel3);
			this.CIEXYZtab.Location = new System.Drawing.Point(4, 22);
			this.CIEXYZtab.Name = "CIEXYZtab";
			this.CIEXYZtab.Padding = new System.Windows.Forms.Padding(3);
			this.CIEXYZtab.Size = new System.Drawing.Size(416, 395);
			this.CIEXYZtab.TabIndex = 2;
			this.CIEXYZtab.Text = "CIE XYZ";
			this.CIEXYZtab.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.ColumnCount = 2;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 305F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
			this.tableLayoutPanel3.Controls.Add(this.XYZslider, 0, 2);
			this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel10, 1, 1);
			this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel12, 0, 1);
			this.tableLayoutPanel3.Controls.Add(this.label12, 1, 2);
			this.tableLayoutPanel3.Controls.Add(this.Tab2PB, 0, 0);
			this.tableLayoutPanel3.Controls.Add(this.OKbutton3, 1, 0);
			this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 3;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 297F));
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(410, 389);
			this.tableLayoutPanel3.TabIndex = 6;
			// 
			// XYZslider
			// 
			this.XYZslider.Dock = System.Windows.Forms.DockStyle.Fill;
			this.XYZslider.Location = new System.Drawing.Point(3, 338);
			this.XYZslider.Maximum = 1100;
			this.XYZslider.Name = "XYZslider";
			this.XYZslider.Size = new System.Drawing.Size(299, 48);
			this.XYZslider.TabIndex = 1;
			this.XYZslider.Value = 55;
			this.XYZslider.MouseUp += new System.Windows.Forms.MouseEventHandler(this.XYZslider_MouseUp);
			// 
			// tableLayoutPanel10
			// 
			this.tableLayoutPanel10.ColumnCount = 1;
			this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel10.Controls.Add(this.XYZZTB, 0, 5);
			this.tableLayoutPanel10.Controls.Add(this.XYZYTB, 0, 3);
			this.tableLayoutPanel10.Controls.Add(this.label9, 0, 0);
			this.tableLayoutPanel10.Controls.Add(this.XYZXTB, 0, 1);
			this.tableLayoutPanel10.Controls.Add(this.label10, 0, 2);
			this.tableLayoutPanel10.Controls.Add(this.label11, 0, 4);
			this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel11, 0, 7);
			this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel10.Location = new System.Drawing.Point(308, 41);
			this.tableLayoutPanel10.Name = "tableLayoutPanel10";
			this.tableLayoutPanel10.RowCount = 8;
			this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle());
			this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
			this.tableLayoutPanel10.Size = new System.Drawing.Size(99, 291);
			this.tableLayoutPanel10.TabIndex = 4;
			// 
			// XYZZTB
			// 
			this.XYZZTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.XYZZTB.Location = new System.Drawing.Point(3, 94);
			this.XYZZTB.Name = "XYZZTB";
			this.XYZZTB.Size = new System.Drawing.Size(93, 20);
			this.XYZZTB.TabIndex = 7;
			this.XYZZTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.XYZZTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XYZZTB_KeyDown);
			// 
			// XYZYTB
			// 
			this.XYZYTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.XYZYTB.Location = new System.Drawing.Point(3, 55);
			this.XYZYTB.Name = "XYZYTB";
			this.XYZYTB.Size = new System.Drawing.Size(93, 20);
			this.XYZYTB.TabIndex = 6;
			this.XYZYTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.XYZYTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XYZYTB_KeyDown);
			// 
			// label9
			// 
			this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(3, 0);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(93, 13);
			this.label9.TabIndex = 0;
			this.label9.Text = "X [0-1]";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// XYZXTB
			// 
			this.XYZXTB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.XYZXTB.Location = new System.Drawing.Point(3, 16);
			this.XYZXTB.Name = "XYZXTB";
			this.XYZXTB.Size = new System.Drawing.Size(93, 20);
			this.XYZXTB.TabIndex = 1;
			this.XYZXTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.XYZXTB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.XYZXTB_KeyDown);
			// 
			// label10
			// 
			this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(3, 39);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(93, 13);
			this.label10.TabIndex = 4;
			this.label10.Text = "Y [0-1]";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label11
			// 
			this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(3, 78);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(93, 13);
			this.label11.TabIndex = 5;
			this.label11.Text = "Z [0-1.1]";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tableLayoutPanel11
			// 
			this.tableLayoutPanel11.ColumnCount = 1;
			this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel11.Controls.Add(this.XYZPickedColor, 0, 0);
			this.tableLayoutPanel11.Controls.Add(this.XYZCurrentColor, 0, 1);
			this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 120);
			this.tableLayoutPanel11.Name = "tableLayoutPanel11";
			this.tableLayoutPanel11.RowCount = 2;
			this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel11.Size = new System.Drawing.Size(93, 168);
			this.tableLayoutPanel11.TabIndex = 8;
			// 
			// XYZPickedColor
			// 
			this.XYZPickedColor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.XYZPickedColor.Enabled = false;
			this.XYZPickedColor.Location = new System.Drawing.Point(3, 3);
			this.XYZPickedColor.Name = "XYZPickedColor";
			this.XYZPickedColor.Size = new System.Drawing.Size(87, 78);
			this.XYZPickedColor.TabIndex = 0;
			this.XYZPickedColor.UseVisualStyleBackColor = true;
			// 
			// XYZCurrentColor
			// 
			this.XYZCurrentColor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.XYZCurrentColor.Enabled = false;
			this.XYZCurrentColor.Location = new System.Drawing.Point(3, 87);
			this.XYZCurrentColor.Name = "XYZCurrentColor";
			this.XYZCurrentColor.Size = new System.Drawing.Size(87, 78);
			this.XYZCurrentColor.TabIndex = 1;
			this.XYZCurrentColor.UseVisualStyleBackColor = true;
			// 
			// tableLayoutPanel12
			// 
			this.tableLayoutPanel12.ColumnCount = 2;
			this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 39F));
			this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel12.Controls.Add(this.XYZYaxis, 0, 1);
			this.tableLayoutPanel12.Controls.Add(this.XYZPictureBox, 1, 1);
			this.tableLayoutPanel12.Controls.Add(this.XYZXaxis, 1, 0);
			this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 41);
			this.tableLayoutPanel12.Name = "tableLayoutPanel12";
			this.tableLayoutPanel12.RowCount = 2;
			this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 262F));
			this.tableLayoutPanel12.Size = new System.Drawing.Size(299, 291);
			this.tableLayoutPanel12.TabIndex = 5;
			// 
			// XYZYaxis
			// 
			this.XYZYaxis.Dock = System.Windows.Forms.DockStyle.Fill;
			this.XYZYaxis.Location = new System.Drawing.Point(3, 32);
			this.XYZYaxis.MaximumSize = new System.Drawing.Size(36, 256);
			this.XYZYaxis.MinimumSize = new System.Drawing.Size(36, 256);
			this.XYZYaxis.Name = "XYZYaxis";
			this.XYZYaxis.Size = new System.Drawing.Size(36, 256);
			this.XYZYaxis.TabIndex = 5;
			this.XYZYaxis.TabStop = false;
			// 
			// XYZPictureBox
			// 
			this.XYZPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.XYZPictureBox.Location = new System.Drawing.Point(42, 32);
			this.XYZPictureBox.MaximumSize = new System.Drawing.Size(256, 256);
			this.XYZPictureBox.MinimumSize = new System.Drawing.Size(256, 256);
			this.XYZPictureBox.Name = "XYZPictureBox";
			this.XYZPictureBox.Size = new System.Drawing.Size(256, 256);
			this.XYZPictureBox.TabIndex = 4;
			this.XYZPictureBox.TabStop = false;
			this.XYZPictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.XYZPictureBox_MouseClick);
			// 
			// XYZXaxis
			// 
			this.XYZXaxis.Dock = System.Windows.Forms.DockStyle.Fill;
			this.XYZXaxis.Location = new System.Drawing.Point(42, 3);
			this.XYZXaxis.MaximumSize = new System.Drawing.Size(256, 26);
			this.XYZXaxis.MinimumSize = new System.Drawing.Size(256, 26);
			this.XYZXaxis.Name = "XYZXaxis";
			this.XYZXaxis.Size = new System.Drawing.Size(256, 26);
			this.XYZXaxis.TabIndex = 6;
			this.XYZXaxis.TabStop = false;
			// 
			// label12
			// 
			this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(310, 355);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(94, 13);
			this.label12.TabIndex = 6;
			this.label12.Text = "Slider sets Z value";
			// 
			// Tab2PB
			// 
			this.Tab2PB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Tab2PB.Location = new System.Drawing.Point(3, 3);
			this.Tab2PB.Name = "Tab2PB";
			this.Tab2PB.Size = new System.Drawing.Size(299, 32);
			this.Tab2PB.TabIndex = 7;
			this.Tab2PB.TabStop = false;
			// 
			// OKbutton3
			// 
			this.OKbutton3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.OKbutton3.Location = new System.Drawing.Point(308, 3);
			this.OKbutton3.Name = "OKbutton3";
			this.OKbutton3.Size = new System.Drawing.Size(99, 32);
			this.OKbutton3.TabIndex = 8;
			this.OKbutton3.Text = "OK";
			this.OKbutton3.UseVisualStyleBackColor = true;
			this.OKbutton3.Click += new System.EventHandler(this.OKbutton3_Click);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.tabPage4);
			this.tabControl1.Location = new System.Drawing.Point(804, 449);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(200, 100);
			this.tabControl1.TabIndex = 1;
			// 
			// tabPage3
			// 
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(192, 74);
			this.tabPage3.TabIndex = 0;
			this.tabPage3.Text = "tabPage3";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// tabPage4
			// 
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage4.Size = new System.Drawing.Size(192, 74);
			this.tabPage4.TabIndex = 1;
			this.tabPage4.Text = "tabPage4";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// ColorPicker
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(424, 421);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.MainTabControl);
			this.MaximumSize = new System.Drawing.Size(440, 460);
			this.MinimumSize = new System.Drawing.Size(440, 460);
			this.Name = "ColorPicker";
			this.Text = "ColorPicker";
			this.MainTabControl.ResumeLayout(false);
			this.RGBtab.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.RGBslider)).EndInit();
			this.tableLayoutPanel4.ResumeLayout(false);
			this.tableLayoutPanel4.PerformLayout();
			this.tableLayoutPanel5.ResumeLayout(false);
			this.tableLayoutPanel6.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.RGBYaxis)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.RGBPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.RGBXaxis)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Tab0PB)).EndInit();
			this.HSVtab.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.HSVslider)).EndInit();
			this.tableLayoutPanel7.ResumeLayout(false);
			this.tableLayoutPanel7.PerformLayout();
			this.tableLayoutPanel8.ResumeLayout(false);
			this.tableLayoutPanel9.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.HSVYaxis)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.HSVPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.HSVXaxis)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Tab1PB)).EndInit();
			this.CIEXYZtab.ResumeLayout(false);
			this.tableLayoutPanel3.ResumeLayout(false);
			this.tableLayoutPanel3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.XYZslider)).EndInit();
			this.tableLayoutPanel10.ResumeLayout(false);
			this.tableLayoutPanel10.PerformLayout();
			this.tableLayoutPanel11.ResumeLayout(false);
			this.tableLayoutPanel12.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.XYZYaxis)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.XYZPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.XYZXaxis)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Tab2PB)).EndInit();
			this.tabControl1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl MainTabControl;
		private System.Windows.Forms.TabPage RGBtab;
		private System.Windows.Forms.TabPage HSVtab;
		private System.Windows.Forms.TabPage CIEXYZtab;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.TrackBar RGBslider;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
		private System.Windows.Forms.TextBox RGBBTB;
		private System.Windows.Forms.TextBox RGBGTB;
		private System.Windows.Forms.Label Rlabel;
		private System.Windows.Forms.TextBox RGBRTB;
		private System.Windows.Forms.Label Glabel;
		private System.Windows.Forms.Label Blabel;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
		private System.Windows.Forms.Button RGBPickedColor;
		private System.Windows.Forms.Button RGBCurrentColor;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
		private System.Windows.Forms.PictureBox RGBYaxis;
		private System.Windows.Forms.PictureBox RGBPictureBox;
		private System.Windows.Forms.PictureBox RGBXaxis;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.TrackBar HSVslider;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
		private System.Windows.Forms.TextBox HSVVTB;
		private System.Windows.Forms.TextBox HSVSTB;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox HSVHTB;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
		private System.Windows.Forms.Button HSVPickedColor;
		private System.Windows.Forms.Button HSVCurrentColor;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
		private System.Windows.Forms.PictureBox HSVYaxis;
		private System.Windows.Forms.PictureBox HSVPictureBox;
		private System.Windows.Forms.PictureBox HSVXaxis;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.TrackBar XYZslider;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
		private System.Windows.Forms.TextBox XYZZTB;
		private System.Windows.Forms.TextBox XYZYTB;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox XYZXTB;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
		private System.Windows.Forms.Button XYZPickedColor;
		private System.Windows.Forms.Button XYZCurrentColor;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
		private System.Windows.Forms.PictureBox XYZYaxis;
		private System.Windows.Forms.PictureBox XYZPictureBox;
		private System.Windows.Forms.PictureBox XYZXaxis;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.PictureBox Tab0PB;
		private System.Windows.Forms.PictureBox Tab1PB;
		private System.Windows.Forms.PictureBox Tab2PB;
		private System.Windows.Forms.Button OKbutton1;
		private System.Windows.Forms.Button OKbutton2;
		private System.Windows.Forms.Button OKbutton3;
	}
}