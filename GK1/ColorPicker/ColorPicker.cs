﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GK1
{
	public partial class ColorPicker : Form
	{
		public Color PickedColor {get; set;}
		private RGBManager RGBManager;
		private HSVManager HSVManager;
		private XYZManager XYZManager;
		int tab;
		bool colorSelected = false;

		#region - Initialization -
		public ColorPicker()
		{
			InitializeComponent();
			InitializePictureBoxes();
			tab = int.MaxValue;
		}
		private void InitializePictureBoxes()
		{
			SetBackgroungColor(Color.White);
			RGBManager = new RGBManager(RGBPictureBox, RGBXaxis, RGBYaxis);
			HSVManager = new HSVManager(HSVPictureBox, HSVXaxis, HSVYaxis);
			XYZManager = new XYZManager(XYZPictureBox, XYZXaxis, XYZYaxis);
		}
		private void SetBackgroungColor(Color c)
		{
			SolidBrush brush = new SolidBrush(c);
			Bitmap bmp = new Bitmap(256, 256);
			using (Graphics graph = Graphics.FromImage(bmp))
			{
				Rectangle ImageSize = new Rectangle(0, 0, 256, 256);
				graph.FillRectangle(brush, ImageSize);
			}
			RGBPictureBox.Image = bmp;
			HSVPictureBox.Image = bmp;
		}
		#endregion

		#region - KeyDown -
		private void RedTB_KeyDown(object sender, KeyEventArgs e)
		{
			TryToPickRGBColor(e);
		}
		private void GreenTB_KeyDown(object sender, KeyEventArgs e)
		{
			TryToPickRGBColor(e);
		}
		private void BlueTB_KeyDown(object sender, KeyEventArgs e)
		{
			TryToPickRGBColor(e);
		}
		private void HSVHTB_KeyDown(object sender, KeyEventArgs e)
		{
			TryToPickHSVColor(e);
		}
		private void HSVSTB_KeyDown(object sender, KeyEventArgs e)
		{
			TryToPickHSVColor(e);
		}
		private void HSVVTB_KeyDown(object sender, KeyEventArgs e)
		{
			TryToPickHSVColor(e);
		}
		private void XYZXTB_KeyDown(object sender, KeyEventArgs e)
		{
			TryToPickXYZColor(e);
		}
		private void XYZYTB_KeyDown(object sender, KeyEventArgs e)
		{
			TryToPickXYZColor(e);
		}
		private void XYZZTB_KeyDown(object sender, KeyEventArgs e)
		{
			TryToPickXYZColor(e);
		}
		private void TryToPickRGBColor(KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
			{
				int r, g, b;
				if (int.TryParse(RGBRTB.Text, out r) && int.TryParse(RGBGTB.Text, out g) && int.TryParse(RGBBTB.Text, out b))
				{
					if (r >= 0 && r <= 255 && g >= 0 && g <= 255 && b >= 0 && b <= 255)
					{
						RGBManager.PickSpecificColor(r, g, b);
						RGBslider.Value = b;
						RGBPickedColor.BackColor = ToNormalColor(RGBManager.PicekdColor);
						RGBCurrentColor.BackColor = ToNormalColor(RGBManager.PicekdColor);
						PickedColor = ToNormalColor(RGBManager.PicekdColor);
						Tab0PB.BackColor = PickedColor;
						colorSelected = true;
					}
				}
			}
		}
		private void TryToPickHSVColor(KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
			{
				double h;
				int s, v;
				if (double.TryParse(HSVHTB.Text, out h) && int.TryParse(HSVSTB.Text, out s) && int.TryParse(HSVVTB.Text, out v))
				{
					if (h >= 0 && h <= 360 && s >= 0 && s <= 100 && v >= 0 && v <= 100)
					{
						h *= (Math.PI / 180);
						HSVManager.PickSpecificColor(h, s, v);
						HSVslider.Value = v;
						RGB color = HSVManager.PicekdColor.ToRGB();
						HSVPickedColor.BackColor = ToNormalColor(color);
						HSVCurrentColor.BackColor = ToNormalColor(color);
						PickedColor = ToNormalColor(color);
						Tab1PB.BackColor = PickedColor;
						colorSelected = true;
					}
				}
			}
		}
		private void TryToPickXYZColor(KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
			{
				double x, y, z;
				if (double.TryParse(XYZXTB.Text, out x) && double.TryParse(XYZYTB.Text, out y) && double.TryParse(XYZZTB.Text, out z))
				{
					if (x >= 0 && x <= 1 && y >= 0 && y <= 1 && z >= 0 && z <= 1.1)
					{
						XYZManager.PickSpecificColor(x, y, z);
						XYZslider.Value = (int)(z*1000);
						RGB color = XYZManager.PicekdColor.ToRGB();
						if(color==null)
						{
							MessageBox.Show("Not existing");
							return;
						}
						XYZPickedColor.BackColor = ToNormalColor(color);
						XYZCurrentColor.BackColor = ToNormalColor(color);
						PickedColor = ToNormalColor(color);
						Tab2PB.BackColor = PickedColor;
						colorSelected = true;
					}
				}
			}
		}
		#endregion

		#region - PBclick -
		private void RGBPictureBox_MouseClick(object sender, MouseEventArgs e)
		{
			RGBManager.PickColorFromPB(e.X, e.Y);
			RGBRTB.Text = RGBManager.PicekdColor.R.ToString();
			RGBGTB.Text = RGBManager.PicekdColor.G.ToString();
			RGBBTB.Text = RGBManager.PicekdColor.B.ToString();
			RGBPickedColor.BackColor = ToNormalColor(RGBManager.PicekdColor);
			PickedColor = ToNormalColor(RGBManager.PicekdColor);
			Tab0PB.BackColor = PickedColor;
			colorSelected = true;
		}
		private void HSVPictureBox_MouseClick(object sender, MouseEventArgs e)
		{
			Bitmap bmp = new Bitmap(HSVPictureBox.Image);
			Color c = bmp.GetPixel(e.X, e.Y);
			if(!(c.R==255 && c.G==255 && c.B==255) || (e.X==128 && e.Y==128))
			{
				HSVManager.PickColorFromPB(e.X, e.Y);
				HSVPickedColor.BackColor = c; 
				HSVHTB.Text = ((int)(HSVManager.PicekdColor.H*(180.0/Math.PI))).ToString();
				HSVSTB.Text = HSVManager.PicekdColor.S.ToString();
				HSVVTB.Text = HSVManager.PicekdColor.V.ToString();
				PickedColor = c;
				Tab1PB.BackColor = PickedColor;
				colorSelected = true;
			}
		}
		private void XYZPictureBox_MouseClick(object sender, MouseEventArgs e)
		{
			Bitmap bmp = new Bitmap(XYZPictureBox.Image);
			Color c = bmp.GetPixel(e.X, e.Y);
			if (!(c.R == 255 && c.G == 255 && c.B == 255) || (e.X == 256 && e.Y == 256))
			{
				XYZManager.PickColorFromPB(e.X, e.Y);
				XYZPickedColor.BackColor = c;
				XYZXTB.Text = XYZManager.PicekdColor.X.ToString("0.000");
				XYZYTB.Text = XYZManager.PicekdColor.Y.ToString("0.000");
				XYZZTB.Text = XYZManager.PicekdColor.Z.ToString("0.000");
				PickedColor = c;
				Tab2PB.BackColor = PickedColor;
				colorSelected = true;
			}
		}
		#endregion

		#region - Sliders -
		private void RGBslider_MouseUp(object sender, MouseEventArgs e)
		{
			RGBManager.RepaintPB(RGBslider.Value);
			RGBBTB.Text = RGBslider.Value.ToString();
			if (RGBManager.ColorUnderCross != null) RGBCurrentColor.BackColor = ToNormalColor(RGBManager.ColorUnderCross);
			RGBManager.RemeberCrossPosition();
		}
		private void HSVSlider_MouseUp(object sender, MouseEventArgs e)
		{
			HSVManager.RepaintPB(HSVslider.Value);
			HSVVTB.Text = HSVslider.Value.ToString();
			if (HSVManager.ColorUnderCross != null)
			{
				RGB color = HSVManager.ColorUnderCross.ToRGB();
				HSVCurrentColor.BackColor = ToNormalColor(color);
			}
			HSVManager.RemeberCrossPosition();
		}
		private void XYZslider_MouseUp(object sender, MouseEventArgs e)
		{
			XYZManager.RepaintPB(XYZslider.Value/1000.0);
			XYZZTB.Text = (XYZslider.Value/1000.0).ToString();
			if (XYZManager.ColorUnderCross != null)
			{
				Bitmap bmp = new Bitmap(XYZPictureBox.Image);
				Color c = bmp.GetPixel(XYZManager.crossX, XYZManager.crossX);
				XYZCurrentColor.BackColor = c;
			}
			XYZManager.RemeberCrossPosition();
		}
		#endregion

		#region - Other -
		public Color ToNormalColor(RGB weirdColor)
		{
			return Color.FromArgb(weirdColor.R, weirdColor.G, weirdColor.B);
		}
		private void MainTabControl_SelectedIndexChanged(object sender, EventArgs e)
		{
			tab = MainTabControl.SelectedIndex;
			if (PickedColor != null && colorSelected)
			{
				switch (tab)
				{
					case 0:
						RGBManager.PickSpecificColor(PickedColor.R, PickedColor.G, PickedColor.B);
						RGBslider.Value = PickedColor.B;
						RGBRTB.Text = PickedColor.R.ToString();
						RGBGTB.Text = PickedColor.G.ToString();
						RGBBTB.Text = PickedColor.B.ToString();
						RGBPickedColor.BackColor = PickedColor;
						RGBCurrentColor.BackColor = PickedColor;
						Tab0PB.BackColor = PickedColor;
						break;
					case 1:
						RGB rgb = new RGB(PickedColor.R, PickedColor.G, PickedColor.B);
						HSV hsv = rgb.ToHSV();
						HSVManager.PickSpecificColor(hsv.H,hsv.S,hsv.V);
						HSVslider.Value = hsv.V;
						HSVHTB.Text = ((int)(HSVManager.PicekdColor.H * (180.0 / Math.PI))).ToString();
						HSVSTB.Text = HSVManager.PicekdColor.S.ToString();
						HSVVTB.Text = HSVManager.PicekdColor.V.ToString();
						HSVPickedColor.BackColor = PickedColor;
						HSVCurrentColor.BackColor = PickedColor;
						Tab1PB.BackColor = PickedColor;
						break;
					case 2:
						RGB rgb2 = new RGB(PickedColor.R, PickedColor.G, PickedColor.B);
						XYZ xyz = rgb2.ToXYZ();
						XYZManager.PickSpecificColor(xyz.X, xyz.Y, xyz.Z);
						XYZslider.Value = (int)(xyz.Z*1000);
						XYZXTB.Text = XYZManager.PicekdColor.X.ToString("0.000");
						XYZYTB.Text = XYZManager.PicekdColor.Y.ToString("0.000");
						XYZZTB.Text = XYZManager.PicekdColor.Z.ToString("0.000");
						XYZPickedColor.BackColor = PickedColor;
						XYZCurrentColor.BackColor = PickedColor;
						Tab2PB.BackColor = PickedColor;
						break;
				}
			}
		}
		private void OKbutton3_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void OKbutton2_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void OKbutton1_Click(object sender, EventArgs e)
		{
			Close();
		}
		#endregion
	}
}
