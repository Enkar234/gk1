﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GK1
{
	class FilterManager
	{
		DirectBitmap Image;
		double[,] filterMatrix;
		bool mFilter = false;
		bool rFilter = false;
		bool gFilter = false;
		bool bFilter = false;
		bool scale = false;
		bool rotate = false;
		bool gamma = false;
		double Scale;
		double Angle;
		double Gamma;
		int[] RFilter;
		int[] GFilter;
		int[] BFilter;
		int[] Default;
		int width;
		int height;
		bool mirror = false;
		public FilterManager()
		{
			Default = new int[256];
			for (int i = 0; i < 256; i++)
				Default[i] = i;
			RFilter = Default;
			GFilter = Default;
			BFilter = Default;
		}
		public DirectBitmap GetModyfiedBitmap()
		{
			PrepareBitmap();
			return Image;
		}
		public void NewBitmap(DirectBitmap bmp)
		{
			Image = bmp;
			width = Image.Width;
			height = Image.Height;
			if (mirror) Mirror();
		}
		private void Mirror()
		{
			MirrorX();
			MirrorY();
		}
		public void ChangeMirrorState()
		{
			mirror = !mirror;
		}
		public void DisableAllEffects()
		{
			mFilter = false;
			rFilter = false;
			gFilter = false;
			bFilter = false;
			scale = false;
			rotate = false;
			mirror = false;
		}
		public void EnableScaling(double sc)
		{
			scale = true;
			Scale = sc;
		}
		public void DisableScaling()
		{
			scale = false;
		}
		public void EnableRotating(double angle)
		{
			rotate = true;
			Angle = angle;
		}
		public void DisableRotating()
		{
			rotate = false;
		}
		public void EnableRedFunctionFilter(int[] R)
		{
			RFilter = R;
			rFilter = true;
		}
		public void DisableRedFunctionFilter()
		{
			RFilter = Default;
			rFilter = false;
		}
		public void EnableGreenFunctionFilter(int[] G)
		{
			GFilter = G;
			gFilter = true;
		}
		public void DisableGreenFunctionFilter()
		{
			GFilter = Default;
			gFilter = false;
		}
		public void EnableBlueFunctionFilter(int[] B)
		{
			BFilter = B;
			bFilter = true;
		}
		public void DisableBlueFunctionFilter()
		{
			BFilter = Default;
			bFilter = false;
		}
		public void EnableGamma(float g)
		{
			gamma = true;
			Gamma = g;
		}
		public void DisableGamma()
		{
			gamma = false;
		}
		public void EnableMatrixFilter(DataGridView dataGrid)
		{
			mFilter = true;
			int w = dataGrid.ColumnCount;
			filterMatrix = new double[w, w];
			for (int i = 0; i < w; i++)
			{
				for (int j = 0; j < w; j++)
					filterMatrix[i, j] = Convert.ToDouble((dataGrid.Rows[i].Cells[j].Value));
			}
		}
		public void DisableMatrixFilter()
		{
			mFilter = false;
		}
		private void PrepareBitmap()
		{
			ApplyFilters();
			Transform();
		}
		private void ApplyFilters()
		{
			if (mFilter)
			{
				int n = filterMatrix.GetLength(0);
				int l = n / 2;
				int k = n % 2 == 0 ? l : l + 1;
				DirectBitmap temp = new DirectBitmap(Image);
				bool lowPass = false;
				for (int i = 0; i < n; i++)
				{
					for (int j = 0; j < n; j++)
						if (filterMatrix[i, j] < 0)
							lowPass = true;
				}
				for (int i = l; i <= Image.Width - k; i++)
				{
					for (int j = l; j <= Image.Height - k; j++)
					{
						double sumR = 0;
						double sumG = 0;
						double sumB = 0;
						double sum = 0;
						for (int x = i - l; x < i + k; x++)
						{
							for (int y = j - l; y < j + k; y++)
							{
								sumR += temp.GetPixel(x, y).R * filterMatrix[x - i + l, y - j + l];
								sumG += temp.GetPixel(x, y).G * filterMatrix[x - i + l, y - j + l];
								sumB += temp.GetPixel(x, y).B * filterMatrix[x - i + l, y - j + l];
								sum += Math.Abs(filterMatrix[x - i + l, y - j + l]);
							}
						}
						int R, G, B;
						if (lowPass)
						{
							sumR = sumR - (int)sumR > 0.5 ? (int)sumR + 1 : (int)sumR;
							sumG = sumG - (int)sumG > 0.5 ? (int)sumG + 1 : (int)sumG;
							sumB = sumB - (int)sumB > 0.5 ? (int)sumB + 1 : (int)sumB;

							sumR = (sumR > 255 ? 255 : (sumR < 0 ? 0 : sumR));
							sumG = (sumG > 255 ? 255 : (sumG < 0 ? 0 : sumG));
							sumB = (sumB > 255 ? 255 : (sumB < 0 ? 0 : sumB));

							R = (int)sumR;
							G = (int)sumG;
							B = (int)sumB;
							R = (R > 255 ? 255 : (R < 0 ? 0 : R));
							G = (G > 255 ? 255 : (G < 0 ? 0 : G));
							B = (B > 255 ? 255 : (B < 0 ? 0 : B));
						}
						else
						{
							sumR /= sum;
							sumG /= sum;
							sumB /= sum;
							R = sumR - (int)(sumR) > 0.5 ? (int)(sumR) + 1 : (int)(sumR);
							G = sumG - (int)(sumG) > 0.5 ? (int)(sumG) + 1 : (int)(sumG);
							B = sumB - (int)(sumB) > 0.5 ? (int)(sumB) + 1 : (int)(sumB);
						}
						Image.SetPixel(i, j, Color.FromArgb(RFilter[R], GFilter[G], BFilter[B]));
					}
				}
				temp.Dispose();
			}
			else if (rFilter || gFilter || bFilter)
			{
				for (int i = 0; i < Image.Width; i++)
				{
					for (int j = 0; j < Image.Height; j++)
					{
						Color c = Image.GetPixel(i, j);
						Image.SetPixel(i, j, Color.FromArgb(RFilter[c.R], GFilter[c.G], BFilter[c.B]));
					}
				}
			}
			if (gamma)
			{
				for (int i = 0; i < Image.Width; i++)
				{
					for (int j = 0; j < Image.Height; j++)
					{
						Color c = Image.GetPixel(i, j);
						double R = Math.Pow(((double)c.R) / 255, Gamma);
						double G = Math.Pow(((double)c.G / 255), Gamma);
						double B = Math.Pow(((double)c.B / 255), Gamma);

						Color newC = Color.FromArgb((int)(R * 255), (int)(G * 255), (int)(B * 255));
						Image.SetPixel(i, j, newC);
					}
				}
			}
		}
		private void Transform()
		{
			if (scale) ScaleImage();
			if (rotate) RotateImage();
		}
		private void ScaleImage()
		{
			Random rand = new Random();

			int h = Image.Height;
			int H = (int)(h * Scale);
			int w = Image.Width;
			int W = (int)(w * Scale);

			DirectBitmap temp = new DirectBitmap(w, h);
			using (Graphics g = Graphics.FromImage(temp.Bitmap))
			{
				g.FillRectangle(new SolidBrush(Color.Black), 0, 0, w, h);
			}

			int rz = rand.Next(0, h);
			int ty = 0;
			for (int y = 0; y < h; y++)
			{
				rz += H;
				while (rz >= H)
				{
					int cz = rand.Next(0, w);
					int tx = 0;
					for (int x = 0; x < w; x++)
					{
						cz += W;
						while (cz >= w)
						{
							if (Scale < 1)
							{
								if (tx >= w || ty >= h) break;
								temp.SetPixel(tx + (w - W) / 2, ty + (h - H) / 2, Image.GetPixel(x, y));
							}
							else if (tx + (w - W) / 2 >= 0 && ty + (h - H) / 2 >= 0)
							{
								if (tx + (w - W) / 2 >= w || ty + (h - H) / 2 >= h) break;
								temp.SetPixel(tx + (w - W) / 2, ty + (h - H) / 2, Image.GetPixel(x, y));
							}
							tx++;
							cz -= w;
						}
					}
					ty++;
					rz -= h;
				}
			}
			Image.Dispose();
			Image = temp;
		}
		private void RotateImage()
		{
			DirectBitmap temp = new DirectBitmap(Image);
			Image.Dispose();
			temp = Xshear(Angle, temp);
			temp = Yshear(Angle, temp);
			temp = Xshear(Angle, temp);
			Image = Cut(temp);
		}
		private DirectBitmap Xshear(double a, DirectBitmap bmp)
		{
			double shear = -Math.Tan(a/2);
			int shift = (int)(Math.Tan(a/2) * bmp.Height);
			DirectBitmap temp = new DirectBitmap(bmp.Width+Math.Abs(shift)+1,bmp.Height);
			shift = shift > 0 ? shift : 0;
			for (int y = 0; y < bmp.Height; y++)
			{
				double skew = shear * y;           
				int skewi = (int)skew;        
				double skewf = Math.Abs(skew - skewi);       
				Color oleft = Color.Black;

				if (shear > 0)
				{
					for (int x = bmp.Width - 1; x >= 0; x--)
					{
						Color pixel = bmp.GetPixel(bmp.Width - x - 1, y);
						Color left = Color.FromArgb((int)(skewf * pixel.R), (int)(skewf * pixel.G), (int)(skewf * pixel.B));

						pixel = Color.FromArgb(pixel.R - left.R + oleft.R, pixel.G - left.G + oleft.G, pixel.B - left.B + oleft.B);
						oleft = left;
						temp.SetPixel(bmp.Width - x + skewi + shift, y, pixel);
					}
				}
				else
				{
					for (int x = 0; x < bmp.Width; x++)
					{
						Color pixel = bmp.GetPixel(bmp.Width - x - 1, y);
						Color left = Color.FromArgb((int)(skewf * pixel.R), (int)(skewf * pixel.G), (int)(skewf * pixel.B));

						pixel = Color.FromArgb(pixel.R - left.R + oleft.R, pixel.G - left.G + oleft.G, pixel.B - left.B + oleft.B);
						oleft = left;
						temp.SetPixel(bmp.Width - x + skewi + shift, y, pixel);
					}
				}
				temp.SetPixel(skewi + shift, y, oleft);
			}
			bmp.Dispose();
			return temp;
		}
		private DirectBitmap Yshear(double a, DirectBitmap bmp)
		{
			double shear = Math.Sin(a);
			int shift = (int)(Math.Sin(a)*bmp.Width);
			DirectBitmap temp = new DirectBitmap(bmp.Width, bmp.Height + Math.Abs(shift)+1);
			shift = shift < 0 ? Math.Abs(shift) : 0;

			for (int x = 0; x < bmp.Width; x++)
			{
				double skew = shear * x;
				int skewi = (int)skew;
				double skewf = Math.Abs(skew - skewi);
				Color oleft = Color.Black;

				if (shear < 0)
				{
					for (int y = 0; y < bmp.Height; y++)
					{
						Color pixel = bmp.GetPixel(x, bmp.Height - y - 1);
						Color left = Color.FromArgb((int)(skewf * pixel.R), (int)(skewf * pixel.G), (int)(skewf * pixel.B));
						pixel = Color.FromArgb(pixel.R - left.R + oleft.R, pixel.G - left.G + oleft.G, pixel.B - left.B + oleft.B);
						oleft = left;
						temp.SetPixel(x, bmp.Height - y + skewi + shift, pixel);
					}
				}
				else
				{
					for (int y = bmp.Height-1; y >=0; y--)
					{
						Color pixel = bmp.GetPixel(x, bmp.Height - y - 1);
						Color left = Color.FromArgb((int)(skewf * pixel.R), (int)(skewf * pixel.G), (int)(skewf * pixel.B));
						pixel = Color.FromArgb(pixel.R - left.R + oleft.R, pixel.G - left.G + oleft.G, pixel.B - left.B + oleft.B);
						oleft = left;
						temp.SetPixel(x, bmp.Height - y + skewi + shift, pixel);
					}
				}
				temp.SetPixel(x, skewi+shift, oleft);
			}
			bmp.Dispose();
			return temp;
		}
		private DirectBitmap Cut(DirectBitmap bmp)
		{
			int w = bmp.Width;
			int h = bmp.Height;

			DirectBitmap temp = new DirectBitmap(width, height);
			for (int i=0;i<width;i++)
			{
				for(int j=0;j<height;j++)
				{
					temp.SetPixel(i, j, bmp.GetPixel(i+(w-width)/2, j+(h-height)/2));
				}
			}
			bmp.Dispose();
			return temp;
		}
		private void MirrorX()
		{
			for(int i=0;i<width;i++)
			{
				for(int j=0;j<height/2;j++)
				{
					Color temp = Image.GetPixel(i, height - j-1);
					Image.SetPixel(i, height - j-1, Image.GetPixel(i, j));
					Image.SetPixel(i, j, temp);
				}
			}
		}
		private void MirrorY()
		{
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width / 2; j++)
				{
					Color temp = Image.GetPixel(width-j-1, i);
					Image.SetPixel(width-j-1,i, Image.GetPixel(j, i));
					Image.SetPixel(j, i, temp);
				}
			}
		}
	}
}
