﻿namespace GK1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.MainTable = new System.Windows.Forms.TableLayoutPanel();
			this.WorkAreaTable = new System.Windows.Forms.TableLayoutPanel();
			this.Canvas = new System.Windows.Forms.PictureBox();
			this.SplittingLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.RightPanelTable = new System.Windows.Forms.TableLayoutPanel();
			this.InfoPanelTable = new System.Windows.Forms.TableLayoutPanel();
			this.NameAndIconTable = new System.Windows.Forms.TableLayoutPanel();
			this.ObjectIcon = new System.Windows.Forms.PictureBox();
			this.ObjectName = new System.Windows.Forms.TextBox();
			this.ColorTable = new System.Windows.Forms.TableLayoutPanel();
			this.ColorLabel = new System.Windows.Forms.Label();
			this.ObjectColor = new System.Windows.Forms.PictureBox();
			this.LayerLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.IncreaseLayer = new System.Windows.Forms.Button();
			this.DecreaseLayer = new System.Windows.Forms.Button();
			this.LayerTextBox = new System.Windows.Forms.TextBox();
			this.LayerUpDownTable = new System.Windows.Forms.TableLayoutPanel();
			this.Layer = new System.Windows.Forms.Label();
			this.UpDownTable = new System.Windows.Forms.TableLayoutPanel();
			this.DownButton = new System.Windows.Forms.Button();
			this.UpButton = new System.Windows.Forms.Button();
			this.EnableAACheckBox = new System.Windows.Forms.CheckBox();
			this.SubmitButton = new System.Windows.Forms.Button();
			this.ObjectList = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.FiltersLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.MatrixFiltersTabControl = new System.Windows.Forms.TabControl();
			this.Tab3x3 = new System.Windows.Forms.TabPage();
			this.Main3x3LayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.GridView3x3 = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Buttons3x3LayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.CheckBox3x3 = new System.Windows.Forms.CheckBox();
			this.Gaussian3x3Button = new System.Windows.Forms.Button();
			this.Laplacian3x3Button = new System.Windows.Forms.Button();
			this.Tab5x5 = new System.Windows.Forms.TabPage();
			this.Main5x5LayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.GridView5x5 = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Buttons5x5LayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.CheckBox5x5 = new System.Windows.Forms.CheckBox();
			this.Gaussian5x5Button = new System.Windows.Forms.Button();
			this.Laplacian5x5Button = new System.Windows.Forms.Button();
			this.Tab7x7 = new System.Windows.Forms.TabPage();
			this.Main7x7LayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.Buttons7x7LayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.Gaussian7x7Button = new System.Windows.Forms.Button();
			this.Laplacian7x7Button = new System.Windows.Forms.Button();
			this.CheckBox7x7 = new System.Windows.Forms.CheckBox();
			this.GridView7x7 = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.HistogramTabControl = new System.Windows.Forms.TabControl();
			this.RTab = new System.Windows.Forms.TabPage();
			this.RTabLayotPanel = new System.Windows.Forms.TableLayoutPanel();
			this.RShowFilterWindowButton = new System.Windows.Forms.Button();
			this.RHistogramLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.RHistogramVisualizingLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.RHistogramPictureBox = new System.Windows.Forms.PictureBox();
			this.RHistogramXAxisPB = new System.Windows.Forms.PictureBox();
			this.RHistogramYAxisPB = new System.Windows.Forms.PictureBox();
			this.EqualizeHistogramRed = new System.Windows.Forms.CheckBox();
			this.RFunctonFilterLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.RFinctionFilterVisualizingLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.RFilterVisualizingMainPB = new System.Windows.Forms.PictureBox();
			this.RYAxisForFilterVisPB = new System.Windows.Forms.PictureBox();
			this.RXAxisForFilterVisPB = new System.Windows.Forms.PictureBox();
			this.RApplyFilterCheckBox = new System.Windows.Forms.CheckBox();
			this.GTab = new System.Windows.Forms.TabPage();
			this.GTabLayotPanel = new System.Windows.Forms.TableLayoutPanel();
			this.GShowFilterWindowButton = new System.Windows.Forms.Button();
			this.GHistogramLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.GHistogramVisualizingLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.GHistogramPictureBox = new System.Windows.Forms.PictureBox();
			this.GHistogramXAxisPB = new System.Windows.Forms.PictureBox();
			this.GHistogramYAxisPB = new System.Windows.Forms.PictureBox();
			this.EqualizeHistogramGreen = new System.Windows.Forms.CheckBox();
			this.GFunctonFilterLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.GFinctionFilterVisualizingLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.GFilterVisualizingMainPB = new System.Windows.Forms.PictureBox();
			this.GYAxisForFilterVisPB = new System.Windows.Forms.PictureBox();
			this.GXAxisForFilterVisPB = new System.Windows.Forms.PictureBox();
			this.GApplyFilterCheckBox = new System.Windows.Forms.CheckBox();
			this.Btab = new System.Windows.Forms.TabPage();
			this.BTabLayotPanel = new System.Windows.Forms.TableLayoutPanel();
			this.BShowFilterWindowButton = new System.Windows.Forms.Button();
			this.BHistogramLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.BHistogramVisualizingLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.BHistogramPictureBox = new System.Windows.Forms.PictureBox();
			this.BHistogramXAxisPB = new System.Windows.Forms.PictureBox();
			this.BHistogramYAxisPB = new System.Windows.Forms.PictureBox();
			this.EqualizeHistogramBlue = new System.Windows.Forms.CheckBox();
			this.BFunctonFilterLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.BFinctionFilterVisualizingLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.BFilterVisualizingMainPB = new System.Windows.Forms.PictureBox();
			this.BYAxisForFilterVisPB = new System.Windows.Forms.PictureBox();
			this.BXAxisForFilterVisPB = new System.Windows.Forms.PictureBox();
			this.BApplyFilterCheckBox = new System.Windows.Forms.CheckBox();
			this.GammaTab = new System.Windows.Forms.TabPage();
			this.GammaEnableCheckBox = new System.Windows.Forms.CheckBox();
			this.GammaTextBox = new System.Windows.Forms.TextBox();
			this.Main3DLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.Import3DModelButton = new System.Windows.Forms.Button();
			this.BackTo2DButton = new System.Windows.Forms.Button();
			this.Viewing3DLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.ViewOptionsLabel = new System.Windows.Forms.Label();
			this.Viewing3DTabControl = new System.Windows.Forms.TabControl();
			this.StandardViewTab = new System.Windows.Forms.TabPage();
			this.StandardViewSplittingPanel = new System.Windows.Forms.TableLayoutPanel();
			this.StandardViewMainLP = new System.Windows.Forms.TableLayoutPanel();
			this.LIFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.LITextBox = new System.Windows.Forms.TextBox();
			this.PercentLabel2 = new System.Windows.Forms.Label();
			this.FileLabel = new System.Windows.Forms.Label();
			this.FileNameLabel = new System.Windows.Forms.Label();
			this.ScaleLabel = new System.Windows.Forms.Label();
			this.ScaleFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.ScaleTextBox = new System.Windows.Forms.TextBox();
			this.PercentLabel1 = new System.Windows.Forms.Label();
			this.LightIntensityLabel = new System.Windows.Forms.Label();
			this.StandardPhongCheckBox = new System.Windows.Forms.CheckBox();
			this.EditStandardPhongButton = new System.Windows.Forms.Button();
			this.StandardFPSlabel = new System.Windows.Forms.Label();
			this.StandardFPScountLabel = new System.Windows.Forms.Label();
			this.StandardColorLabel = new System.Windows.Forms.Label();
			this.StandardColorButton = new System.Windows.Forms.Button();
			this.StandardPhonglayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.StandardApplyPhongLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.StandardSpecularExponentLabel = new System.Windows.Forms.Label();
			this.StandardSpecularExponentTextBox = new System.Windows.Forms.TextBox();
			this.StandardPhongApplyButton = new System.Windows.Forms.Button();
			this.StandardPhongEditionLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.StandardDiffuseLabel = new System.Windows.Forms.Label();
			this.StandardDiffuseTextBox = new System.Windows.Forms.TextBox();
			this.StandardAmbienttextBox = new System.Windows.Forms.TextBox();
			this.StandardAmbientLabel = new System.Windows.Forms.Label();
			this.StandardSpecularTextBox = new System.Windows.Forms.TextBox();
			this.StandardSpecularLabel = new System.Windows.Forms.Label();
			this.StandardDiffuseColorButton = new System.Windows.Forms.Button();
			this.StandardSpecularColorButton = new System.Windows.Forms.Button();
			this.StandardAmbientColorButton = new System.Windows.Forms.Button();
			this.StandardPhongPictureBox = new System.Windows.Forms.PictureBox();
			this.GridViewTab = new System.Windows.Forms.TabPage();
			this.GridLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.Grid3DColorButton = new System.Windows.Forms.Button();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.GridLITextBox = new System.Windows.Forms.TextBox();
			this.GridPercent2 = new System.Windows.Forms.Label();
			this.GridFileLabel = new System.Windows.Forms.Label();
			this.GridFileNameLabel = new System.Windows.Forms.Label();
			this.GridScaleLabel = new System.Windows.Forms.Label();
			this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
			this.GridScaleTextBox = new System.Windows.Forms.TextBox();
			this.GridPercent1 = new System.Windows.Forms.Label();
			this.GridLightIntensityLabel = new System.Windows.Forms.Label();
			this.GridFPSLabel = new System.Windows.Forms.Label();
			this.GridFPSCountLabel = new System.Windows.Forms.Label();
			this.GridColorLabel = new System.Windows.Forms.Label();
			this.RandomColorsTab = new System.Windows.Forms.TabPage();
			this.RandomViewSplittingPanel = new System.Windows.Forms.TableLayoutPanel();
			this.RandomPhongLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.RandomPhongPictureBox = new System.Windows.Forms.PictureBox();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.RandomDLabel = new System.Windows.Forms.Label();
			this.RandomATextBox = new System.Windows.Forms.TextBox();
			this.RandomALabel = new System.Windows.Forms.Label();
			this.RandomSTextBox = new System.Windows.Forms.TextBox();
			this.RandomSLabel = new System.Windows.Forms.Label();
			this.RandomDTextBox = new System.Windows.Forms.TextBox();
			this.RandomDiffuseColorButton = new System.Windows.Forms.Button();
			this.RandomSpecularColorButton = new System.Windows.Forms.Button();
			this.RandomAmbientColorButton = new System.Windows.Forms.Button();
			this.RandomApplyPhongLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.RandomSpecularExponentLabel = new System.Windows.Forms.Label();
			this.RandomSpecularExponentTextBox = new System.Windows.Forms.TextBox();
			this.RandomPhongApplyButton = new System.Windows.Forms.Button();
			this.RandomLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
			this.RandomLITextBox = new System.Windows.Forms.TextBox();
			this.RandomPercent2 = new System.Windows.Forms.Label();
			this.RandomFileLabel = new System.Windows.Forms.Label();
			this.RandomFileNameLabel = new System.Windows.Forms.Label();
			this.RandomScaleLabel = new System.Windows.Forms.Label();
			this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
			this.RandomScaleTextBox = new System.Windows.Forms.TextBox();
			this.RandomPercent1 = new System.Windows.Forms.Label();
			this.RandomLILabel = new System.Windows.Forms.Label();
			this.RandomPhongCheckBox = new System.Windows.Forms.CheckBox();
			this.RandomEditPhongButton = new System.Windows.Forms.Button();
			this.RandomFPSLabel = new System.Windows.Forms.Label();
			this.RandomFPSCountLabel = new System.Windows.Forms.Label();
			this.Effects3DLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.EffectsLabel = new System.Windows.Forms.Label();
			this.LoadTextureLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.LoadTextureButton = new System.Windows.Forms.Button();
			this.LoadTextureCheckBox = new System.Windows.Forms.CheckBox();
			this.LoadedTextureLabel = new System.Windows.Forms.Label();
			this.FogLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.FogLabel = new System.Windows.Forms.Label();
			this.EnableFogCheckBox = new System.Windows.Forms.CheckBox();
			this.FogColorLabel = new System.Windows.Forms.Label();
			this.FogIntensityLabel = new System.Windows.Forms.Label();
			this.FogColorButton = new System.Windows.Forms.Button();
			this.FogIntensitySlider = new System.Windows.Forms.TrackBar();
			this.TransparencyLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.EnableTransparencyCheckBox = new System.Windows.Forms.CheckBox();
			this.TransparencyLabel = new System.Windows.Forms.Label();
			this.TransparencyIntensityLabel = new System.Windows.Forms.Label();
			this.TransparencyCountLabel = new System.Windows.Forms.Label();
			this.TransparencyLevelSlider = new System.Windows.Forms.TrackBar();
			this.TransparentCountSlider = new System.Windows.Forms.TrackBar();
			this.BackfaceCullingLeyoutPanel = new System.Windows.Forms.TableLayoutPanel();
			this.BackfaceCullingLabel = new System.Windows.Forms.Label();
			this.BackfaceCullingCheckBox = new System.Windows.Forms.CheckBox();
			this.ToolTable = new System.Windows.Forms.TableLayoutPanel();
			this.ToolBar = new System.Windows.Forms.ToolStrip();
			this.BackgroundColorButton = new System.Windows.Forms.ToolStripButton();
			this.PointButton = new System.Windows.Forms.ToolStripButton();
			this.LineButton = new System.Windows.Forms.ToolStripButton();
			this.CircleButton = new System.Windows.Forms.ToolStripButton();
			this.FilledCircleButton = new System.Windows.Forms.ToolStripButton();
			this.PolygonButton = new System.Windows.Forms.ToolStripButton();
			this.FilledPolygonButton = new System.Windows.Forms.ToolStripButton();
			this.CombButton = new System.Windows.Forms.ToolStripButton();
			this.ToolStripColorLabel = new System.Windows.Forms.ToolStripLabel();
			this.ColorButton = new System.Windows.Forms.ToolStripButton();
			this.Separator1 = new System.Windows.Forms.ToolStripSeparator();
			this.SelectToolButton = new System.Windows.Forms.ToolStripButton();
			this.MoveToolButton = new System.Windows.Forms.ToolStripButton();
			this.ClearSceneButton = new System.Windows.Forms.ToolStripButton();
			this.Separator2 = new System.Windows.Forms.ToolStripSeparator();
			this.GridLabel = new System.Windows.Forms.ToolStripLabel();
			this.GridButton = new System.Windows.Forms.ToolStripButton();
			this.GridDensityTextBox = new System.Windows.Forms.ToolStripTextBox();
			this.GridColorButton = new System.Windows.Forms.ToolStripButton();
			this.Separator3 = new System.Windows.Forms.ToolStripSeparator();
			this.FillSceneButton = new System.Windows.Forms.ToolStripDropDownButton();
			this.connectedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.connectedToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.FillSceneColorButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.ClippingToolButton = new System.Windows.Forms.ToolStripButton();
			this.ClippingRectangleWidth = new System.Windows.Forms.ToolStripTextBox();
			this.Xlabel = new System.Windows.Forms.ToolStripLabel();
			this.ClippingRectangleHeight = new System.Windows.Forms.ToolStripTextBox();
			this.FiltersButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.AdvancedColorsButton = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.Button3D = new System.Windows.Forms.ToolStripButton();
			this.AACheckBox = new System.Windows.Forms.CheckBox();
			this.MainTable.SuspendLayout();
			this.WorkAreaTable.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Canvas)).BeginInit();
			this.SplittingLayoutPanel.SuspendLayout();
			this.RightPanelTable.SuspendLayout();
			this.InfoPanelTable.SuspendLayout();
			this.NameAndIconTable.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ObjectIcon)).BeginInit();
			this.ColorTable.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ObjectColor)).BeginInit();
			this.LayerLayoutPanel.SuspendLayout();
			this.LayerUpDownTable.SuspendLayout();
			this.UpDownTable.SuspendLayout();
			this.FiltersLayoutPanel.SuspendLayout();
			this.MatrixFiltersTabControl.SuspendLayout();
			this.Tab3x3.SuspendLayout();
			this.Main3x3LayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridView3x3)).BeginInit();
			this.Buttons3x3LayoutPanel.SuspendLayout();
			this.Tab5x5.SuspendLayout();
			this.Main5x5LayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridView5x5)).BeginInit();
			this.Buttons5x5LayoutPanel.SuspendLayout();
			this.Tab7x7.SuspendLayout();
			this.Main7x7LayoutPanel.SuspendLayout();
			this.Buttons7x7LayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridView7x7)).BeginInit();
			this.HistogramTabControl.SuspendLayout();
			this.RTab.SuspendLayout();
			this.RTabLayotPanel.SuspendLayout();
			this.RHistogramLayoutPanel.SuspendLayout();
			this.RHistogramVisualizingLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.RHistogramPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.RHistogramXAxisPB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.RHistogramYAxisPB)).BeginInit();
			this.RFunctonFilterLayoutPanel.SuspendLayout();
			this.RFinctionFilterVisualizingLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.RFilterVisualizingMainPB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.RYAxisForFilterVisPB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.RXAxisForFilterVisPB)).BeginInit();
			this.GTab.SuspendLayout();
			this.GTabLayotPanel.SuspendLayout();
			this.GHistogramLayoutPanel.SuspendLayout();
			this.GHistogramVisualizingLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GHistogramPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GHistogramXAxisPB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GHistogramYAxisPB)).BeginInit();
			this.GFunctonFilterLayoutPanel.SuspendLayout();
			this.GFinctionFilterVisualizingLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GFilterVisualizingMainPB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GYAxisForFilterVisPB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GXAxisForFilterVisPB)).BeginInit();
			this.Btab.SuspendLayout();
			this.BTabLayotPanel.SuspendLayout();
			this.BHistogramLayoutPanel.SuspendLayout();
			this.BHistogramVisualizingLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.BHistogramPictureBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.BHistogramXAxisPB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.BHistogramYAxisPB)).BeginInit();
			this.BFunctonFilterLayoutPanel.SuspendLayout();
			this.BFinctionFilterVisualizingLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.BFilterVisualizingMainPB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.BYAxisForFilterVisPB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.BXAxisForFilterVisPB)).BeginInit();
			this.GammaTab.SuspendLayout();
			this.Main3DLayoutPanel.SuspendLayout();
			this.Viewing3DLayoutPanel.SuspendLayout();
			this.Viewing3DTabControl.SuspendLayout();
			this.StandardViewTab.SuspendLayout();
			this.StandardViewSplittingPanel.SuspendLayout();
			this.StandardViewMainLP.SuspendLayout();
			this.LIFlowLayoutPanel.SuspendLayout();
			this.ScaleFlowLayoutPanel.SuspendLayout();
			this.StandardPhonglayoutPanel.SuspendLayout();
			this.StandardApplyPhongLayoutPanel.SuspendLayout();
			this.StandardPhongEditionLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.StandardPhongPictureBox)).BeginInit();
			this.GridViewTab.SuspendLayout();
			this.GridLayoutPanel.SuspendLayout();
			this.flowLayoutPanel1.SuspendLayout();
			this.flowLayoutPanel2.SuspendLayout();
			this.RandomColorsTab.SuspendLayout();
			this.RandomViewSplittingPanel.SuspendLayout();
			this.RandomPhongLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.RandomPhongPictureBox)).BeginInit();
			this.tableLayoutPanel2.SuspendLayout();
			this.RandomApplyPhongLayoutPanel.SuspendLayout();
			this.RandomLayoutPanel.SuspendLayout();
			this.flowLayoutPanel3.SuspendLayout();
			this.flowLayoutPanel4.SuspendLayout();
			this.Effects3DLayoutPanel.SuspendLayout();
			this.LoadTextureLayoutPanel.SuspendLayout();
			this.FogLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.FogIntensitySlider)).BeginInit();
			this.TransparencyLayoutPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.TransparencyLevelSlider)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.TransparentCountSlider)).BeginInit();
			this.BackfaceCullingLeyoutPanel.SuspendLayout();
			this.ToolTable.SuspendLayout();
			this.ToolBar.SuspendLayout();
			this.SuspendLayout();
			// 
			// MainTable
			// 
			this.MainTable.ColumnCount = 1;
			this.MainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.MainTable.Controls.Add(this.WorkAreaTable, 0, 1);
			this.MainTable.Controls.Add(this.ToolTable, 0, 0);
			this.MainTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.MainTable.Location = new System.Drawing.Point(0, 0);
			this.MainTable.Name = "MainTable";
			this.MainTable.RowCount = 2;
			this.MainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.MainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.MainTable.Size = new System.Drawing.Size(856, 441);
			this.MainTable.TabIndex = 3;
			// 
			// WorkAreaTable
			// 
			this.WorkAreaTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.WorkAreaTable.ColumnCount = 2;
			this.WorkAreaTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.WorkAreaTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 254F));
			this.WorkAreaTable.Controls.Add(this.Canvas, 0, 0);
			this.WorkAreaTable.Controls.Add(this.SplittingLayoutPanel, 1, 0);
			this.WorkAreaTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.WorkAreaTable.Location = new System.Drawing.Point(0, 30);
			this.WorkAreaTable.Margin = new System.Windows.Forms.Padding(0);
			this.WorkAreaTable.Name = "WorkAreaTable";
			this.WorkAreaTable.RowCount = 1;
			this.WorkAreaTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.WorkAreaTable.Size = new System.Drawing.Size(856, 411);
			this.WorkAreaTable.TabIndex = 3;
			// 
			// Canvas
			// 
			this.Canvas.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Canvas.Location = new System.Drawing.Point(4, 4);
			this.Canvas.Name = "Canvas";
			this.Canvas.Size = new System.Drawing.Size(593, 403);
			this.Canvas.TabIndex = 0;
			this.Canvas.TabStop = false;
			this.Canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.Canvas_Paint);
			this.Canvas.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Canvas_MouseClick);
			this.Canvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Canvas_MouseDown);
			this.Canvas.MouseHover += new System.EventHandler(this.Canvas_MouseHover);
			this.Canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Canvas_MouseMove);
			this.Canvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Canvas_MouseUp);
			// 
			// SplittingLayoutPanel
			// 
			this.SplittingLayoutPanel.ColumnCount = 3;
			this.SplittingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.SplittingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 0F));
			this.SplittingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 0F));
			this.SplittingLayoutPanel.Controls.Add(this.RightPanelTable, 0, 0);
			this.SplittingLayoutPanel.Controls.Add(this.FiltersLayoutPanel, 1, 0);
			this.SplittingLayoutPanel.Controls.Add(this.Main3DLayoutPanel, 2, 0);
			this.SplittingLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.SplittingLayoutPanel.Location = new System.Drawing.Point(604, 4);
			this.SplittingLayoutPanel.Name = "SplittingLayoutPanel";
			this.SplittingLayoutPanel.RowCount = 1;
			this.SplittingLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.SplittingLayoutPanel.Size = new System.Drawing.Size(248, 403);
			this.SplittingLayoutPanel.TabIndex = 2;
			// 
			// RightPanelTable
			// 
			this.RightPanelTable.BackColor = System.Drawing.SystemColors.Control;
			this.RightPanelTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.RightPanelTable.ColumnCount = 1;
			this.RightPanelTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.RightPanelTable.Controls.Add(this.InfoPanelTable, 0, 1);
			this.RightPanelTable.Controls.Add(this.ObjectList, 0, 0);
			this.RightPanelTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RightPanelTable.Location = new System.Drawing.Point(3, 3);
			this.RightPanelTable.Name = "RightPanelTable";
			this.RightPanelTable.RowCount = 2;
			this.RightPanelTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.RightPanelTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
			this.RightPanelTable.Size = new System.Drawing.Size(242, 397);
			this.RightPanelTable.TabIndex = 1;
			// 
			// InfoPanelTable
			// 
			this.InfoPanelTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.InfoPanelTable.ColumnCount = 1;
			this.InfoPanelTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.InfoPanelTable.Controls.Add(this.NameAndIconTable, 0, 0);
			this.InfoPanelTable.Controls.Add(this.ColorTable, 0, 1);
			this.InfoPanelTable.Controls.Add(this.LayerLayoutPanel, 0, 2);
			this.InfoPanelTable.Controls.Add(this.EnableAACheckBox, 0, 3);
			this.InfoPanelTable.Controls.Add(this.SubmitButton, 0, 4);
			this.InfoPanelTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.InfoPanelTable.Location = new System.Drawing.Point(4, 199);
			this.InfoPanelTable.Name = "InfoPanelTable";
			this.InfoPanelTable.RowCount = 5;
			this.InfoPanelTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.09491F));
			this.InfoPanelTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.42411F));
			this.InfoPanelTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.11885F));
			this.InfoPanelTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.36213F));
			this.InfoPanelTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
			this.InfoPanelTable.Size = new System.Drawing.Size(234, 194);
			this.InfoPanelTable.TabIndex = 4;
			this.InfoPanelTable.Visible = false;
			// 
			// NameAndIconTable
			// 
			this.NameAndIconTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.NameAndIconTable.ColumnCount = 2;
			this.NameAndIconTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 43F));
			this.NameAndIconTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.NameAndIconTable.Controls.Add(this.ObjectIcon, 0, 0);
			this.NameAndIconTable.Controls.Add(this.ObjectName, 1, 0);
			this.NameAndIconTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NameAndIconTable.Location = new System.Drawing.Point(4, 4);
			this.NameAndIconTable.Name = "NameAndIconTable";
			this.NameAndIconTable.RowCount = 1;
			this.NameAndIconTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.NameAndIconTable.Size = new System.Drawing.Size(226, 34);
			this.NameAndIconTable.TabIndex = 0;
			// 
			// ObjectIcon
			// 
			this.ObjectIcon.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.ObjectIcon.InitialImage = null;
			this.ObjectIcon.Location = new System.Drawing.Point(7, 2);
			this.ObjectIcon.Margin = new System.Windows.Forms.Padding(0);
			this.ObjectIcon.Name = "ObjectIcon";
			this.ObjectIcon.Size = new System.Drawing.Size(30, 30);
			this.ObjectIcon.TabIndex = 0;
			this.ObjectIcon.TabStop = false;
			this.ObjectIcon.WaitOnLoad = true;
			// 
			// ObjectName
			// 
			this.ObjectName.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ObjectName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.ObjectName.Location = new System.Drawing.Point(48, 4);
			this.ObjectName.Name = "ObjectName";
			this.ObjectName.Size = new System.Drawing.Size(174, 26);
			this.ObjectName.TabIndex = 1;
			// 
			// ColorTable
			// 
			this.ColorTable.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.ColorTable.ColumnCount = 2;
			this.ColorTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.ColorTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.ColorTable.Controls.Add(this.ColorLabel, 0, 0);
			this.ColorTable.Controls.Add(this.ObjectColor, 1, 0);
			this.ColorTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ColorTable.Location = new System.Drawing.Point(4, 45);
			this.ColorTable.Name = "ColorTable";
			this.ColorTable.RowCount = 1;
			this.ColorTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.ColorTable.Size = new System.Drawing.Size(226, 23);
			this.ColorTable.TabIndex = 1;
			// 
			// ColorLabel
			// 
			this.ColorLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
			this.ColorLabel.AutoSize = true;
			this.ColorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.ColorLabel.Location = new System.Drawing.Point(33, 1);
			this.ColorLabel.Name = "ColorLabel";
			this.ColorLabel.Size = new System.Drawing.Size(46, 21);
			this.ColorLabel.TabIndex = 0;
			this.ColorLabel.Text = "Color";
			// 
			// ObjectColor
			// 
			this.ObjectColor.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ObjectColor.Location = new System.Drawing.Point(113, 1);
			this.ObjectColor.Margin = new System.Windows.Forms.Padding(0);
			this.ObjectColor.Name = "ObjectColor";
			this.ObjectColor.Size = new System.Drawing.Size(112, 21);
			this.ObjectColor.TabIndex = 1;
			this.ObjectColor.TabStop = false;
			this.ObjectColor.Click += new System.EventHandler(this.ObjectColor_Click);
			// 
			// LayerLayoutPanel
			// 
			this.LayerLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.LayerLayoutPanel.ColumnCount = 4;
			this.LayerLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.LayerLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.LayerLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
			this.LayerLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61F));
			this.LayerLayoutPanel.Controls.Add(this.IncreaseLayer, 2, 0);
			this.LayerLayoutPanel.Controls.Add(this.DecreaseLayer, 3, 0);
			this.LayerLayoutPanel.Controls.Add(this.LayerTextBox, 1, 0);
			this.LayerLayoutPanel.Controls.Add(this.LayerUpDownTable, 0, 0);
			this.LayerLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LayerLayoutPanel.Location = new System.Drawing.Point(4, 75);
			this.LayerLayoutPanel.Name = "LayerLayoutPanel";
			this.LayerLayoutPanel.RowCount = 1;
			this.LayerLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.LayerLayoutPanel.Size = new System.Drawing.Size(226, 55);
			this.LayerLayoutPanel.TabIndex = 2;
			// 
			// IncreaseLayer
			// 
			this.IncreaseLayer.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.IncreaseLayer.BackgroundImage = global::GK1.Properties.Resources.plus_512;
			this.IncreaseLayer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.IncreaseLayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.IncreaseLayer.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.IncreaseLayer.Location = new System.Drawing.Point(135, 17);
			this.IncreaseLayer.Name = "IncreaseLayer";
			this.IncreaseLayer.Size = new System.Drawing.Size(19, 20);
			this.IncreaseLayer.TabIndex = 2;
			this.IncreaseLayer.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.IncreaseLayer.UseVisualStyleBackColor = true;
			this.IncreaseLayer.Click += new System.EventHandler(this.IncreaseLayer_Click);
			// 
			// DecreaseLayer
			// 
			this.DecreaseLayer.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.DecreaseLayer.BackgroundImage = global::GK1.Properties.Resources.minus_512;
			this.DecreaseLayer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.DecreaseLayer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.DecreaseLayer.Location = new System.Drawing.Point(184, 17);
			this.DecreaseLayer.Name = "DecreaseLayer";
			this.DecreaseLayer.Size = new System.Drawing.Size(20, 20);
			this.DecreaseLayer.TabIndex = 3;
			this.DecreaseLayer.UseVisualStyleBackColor = true;
			this.DecreaseLayer.Click += new System.EventHandler(this.DecreaseLayer_Click);
			// 
			// LayerTextBox
			// 
			this.LayerTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.LayerTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.LayerTextBox.Location = new System.Drawing.Point(80, 16);
			this.LayerTextBox.MinimumSize = new System.Drawing.Size(30, 22);
			this.LayerTextBox.Name = "LayerTextBox";
			this.LayerTextBox.Size = new System.Drawing.Size(30, 22);
			this.LayerTextBox.TabIndex = 4;
			// 
			// LayerUpDownTable
			// 
			this.LayerUpDownTable.ColumnCount = 1;
			this.LayerUpDownTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.LayerUpDownTable.Controls.Add(this.Layer, 0, 0);
			this.LayerUpDownTable.Controls.Add(this.UpDownTable, 0, 1);
			this.LayerUpDownTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LayerUpDownTable.Location = new System.Drawing.Point(1, 1);
			this.LayerUpDownTable.Margin = new System.Windows.Forms.Padding(0);
			this.LayerUpDownTable.Name = "LayerUpDownTable";
			this.LayerUpDownTable.RowCount = 2;
			this.LayerUpDownTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 42.22222F));
			this.LayerUpDownTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 57.77778F));
			this.LayerUpDownTable.Size = new System.Drawing.Size(62, 53);
			this.LayerUpDownTable.TabIndex = 5;
			// 
			// Layer
			// 
			this.Layer.AutoSize = true;
			this.Layer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Layer.Location = new System.Drawing.Point(0, 0);
			this.Layer.Margin = new System.Windows.Forms.Padding(0);
			this.Layer.Name = "Layer";
			this.Layer.Size = new System.Drawing.Size(62, 22);
			this.Layer.TabIndex = 0;
			this.Layer.Text = "Layer";
			this.Layer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// UpDownTable
			// 
			this.UpDownTable.ColumnCount = 2;
			this.UpDownTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.UpDownTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.UpDownTable.Controls.Add(this.DownButton, 1, 0);
			this.UpDownTable.Controls.Add(this.UpButton, 0, 0);
			this.UpDownTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.UpDownTable.Location = new System.Drawing.Point(0, 22);
			this.UpDownTable.Margin = new System.Windows.Forms.Padding(0);
			this.UpDownTable.Name = "UpDownTable";
			this.UpDownTable.RowCount = 1;
			this.UpDownTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.UpDownTable.Size = new System.Drawing.Size(62, 31);
			this.UpDownTable.TabIndex = 1;
			// 
			// DownButton
			// 
			this.DownButton.BackgroundImage = global::GK1.Properties.Resources.tobottom;
			this.DownButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.DownButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.DownButton.Location = new System.Drawing.Point(34, 3);
			this.DownButton.Name = "DownButton";
			this.DownButton.Size = new System.Drawing.Size(25, 25);
			this.DownButton.TabIndex = 1;
			this.DownButton.UseVisualStyleBackColor = true;
			this.DownButton.Click += new System.EventHandler(this.DownButton_Click);
			// 
			// UpButton
			// 
			this.UpButton.BackgroundImage = global::GK1.Properties.Resources.totop;
			this.UpButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.UpButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.UpButton.Location = new System.Drawing.Point(3, 3);
			this.UpButton.Name = "UpButton";
			this.UpButton.Size = new System.Drawing.Size(25, 25);
			this.UpButton.TabIndex = 0;
			this.UpButton.UseVisualStyleBackColor = true;
			this.UpButton.Click += new System.EventHandler(this.UpButton_Click);
			// 
			// EnableAACheckBox
			// 
			this.EnableAACheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.EnableAACheckBox.AutoSize = true;
			this.EnableAACheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.EnableAACheckBox.Location = new System.Drawing.Point(73, 138);
			this.EnableAACheckBox.Name = "EnableAACheckBox";
			this.EnableAACheckBox.Size = new System.Drawing.Size(88, 20);
			this.EnableAACheckBox.TabIndex = 3;
			this.EnableAACheckBox.Text = "AA / Filled";
			this.EnableAACheckBox.UseVisualStyleBackColor = true;
			// 
			// SubmitButton
			// 
			this.SubmitButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.SubmitButton.Location = new System.Drawing.Point(2, 165);
			this.SubmitButton.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.SubmitButton.Name = "SubmitButton";
			this.SubmitButton.Size = new System.Drawing.Size(230, 27);
			this.SubmitButton.TabIndex = 4;
			this.SubmitButton.Text = "Submit Changes";
			this.SubmitButton.UseVisualStyleBackColor = true;
			this.SubmitButton.Click += new System.EventHandler(this.SubmitButton_Click);
			// 
			// ObjectList
			// 
			this.ObjectList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
			this.ObjectList.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ObjectList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.ObjectList.HideSelection = false;
			this.ObjectList.Location = new System.Drawing.Point(4, 4);
			this.ObjectList.Name = "ObjectList";
			this.ObjectList.Size = new System.Drawing.Size(234, 188);
			this.ObjectList.TabIndex = 3;
			this.ObjectList.UseCompatibleStateImageBehavior = false;
			this.ObjectList.View = System.Windows.Forms.View.List;
			this.ObjectList.SelectedIndexChanged += new System.EventHandler(this.ObjectList_SelectedIndexChanged);
			// 
			// FiltersLayoutPanel
			// 
			this.FiltersLayoutPanel.ColumnCount = 1;
			this.FiltersLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.FiltersLayoutPanel.Controls.Add(this.MatrixFiltersTabControl, 0, 1);
			this.FiltersLayoutPanel.Controls.Add(this.HistogramTabControl, 0, 0);
			this.FiltersLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.FiltersLayoutPanel.Location = new System.Drawing.Point(251, 3);
			this.FiltersLayoutPanel.Name = "FiltersLayoutPanel";
			this.FiltersLayoutPanel.RowCount = 2;
			this.FiltersLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220F));
			this.FiltersLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.FiltersLayoutPanel.Size = new System.Drawing.Size(1, 397);
			this.FiltersLayoutPanel.TabIndex = 2;
			// 
			// MatrixFiltersTabControl
			// 
			this.MatrixFiltersTabControl.Controls.Add(this.Tab3x3);
			this.MatrixFiltersTabControl.Controls.Add(this.Tab5x5);
			this.MatrixFiltersTabControl.Controls.Add(this.Tab7x7);
			this.MatrixFiltersTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.MatrixFiltersTabControl.Location = new System.Drawing.Point(1, 221);
			this.MatrixFiltersTabControl.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.MatrixFiltersTabControl.Name = "MatrixFiltersTabControl";
			this.MatrixFiltersTabControl.SelectedIndex = 0;
			this.MatrixFiltersTabControl.Size = new System.Drawing.Size(1, 175);
			this.MatrixFiltersTabControl.TabIndex = 2;
			this.MatrixFiltersTabControl.SelectedIndexChanged += new System.EventHandler(this.MatrixFiltersTabControl_SelectedIndexChanged);
			// 
			// Tab3x3
			// 
			this.Tab3x3.Controls.Add(this.Main3x3LayoutPanel);
			this.Tab3x3.Location = new System.Drawing.Point(4, 22);
			this.Tab3x3.Name = "Tab3x3";
			this.Tab3x3.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
			this.Tab3x3.Size = new System.Drawing.Size(0, 149);
			this.Tab3x3.TabIndex = 0;
			this.Tab3x3.Text = "3x3";
			this.Tab3x3.UseVisualStyleBackColor = true;
			// 
			// Main3x3LayoutPanel
			// 
			this.Main3x3LayoutPanel.ColumnCount = 2;
			this.Main3x3LayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Main3x3LayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.Main3x3LayoutPanel.Controls.Add(this.GridView3x3, 0, 0);
			this.Main3x3LayoutPanel.Controls.Add(this.Buttons3x3LayoutPanel, 1, 0);
			this.Main3x3LayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Main3x3LayoutPanel.Location = new System.Drawing.Point(3, 3);
			this.Main3x3LayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.Main3x3LayoutPanel.Name = "Main3x3LayoutPanel";
			this.Main3x3LayoutPanel.RowCount = 1;
			this.Main3x3LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Main3x3LayoutPanel.Size = new System.Drawing.Size(0, 143);
			this.Main3x3LayoutPanel.TabIndex = 0;
			// 
			// GridView3x3
			// 
			this.GridView3x3.AllowUserToAddRows = false;
			this.GridView3x3.AllowUserToDeleteRows = false;
			this.GridView3x3.AllowUserToResizeColumns = false;
			this.GridView3x3.AllowUserToResizeRows = false;
			this.GridView3x3.BackgroundColor = System.Drawing.Color.White;
			this.GridView3x3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridView3x3.ColumnHeadersVisible = false;
			this.GridView3x3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle1.Format = "N0";
			dataGridViewCellStyle1.NullValue = null;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.GridView3x3.DefaultCellStyle = dataGridViewCellStyle1;
			this.GridView3x3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GridView3x3.EnableHeadersVisualStyles = false;
			this.GridView3x3.Location = new System.Drawing.Point(0, 0);
			this.GridView3x3.Margin = new System.Windows.Forms.Padding(0);
			this.GridView3x3.MaximumSize = new System.Drawing.Size(153, 143);
			this.GridView3x3.MinimumSize = new System.Drawing.Size(153, 143);
			this.GridView3x3.MultiSelect = false;
			this.GridView3x3.Name = "GridView3x3";
			this.GridView3x3.RowHeadersVisible = false;
			this.GridView3x3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			this.GridView3x3.RowsDefaultCellStyle = dataGridViewCellStyle2;
			this.GridView3x3.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.GridView3x3.RowTemplate.DefaultCellStyle.Format = "N0";
			this.GridView3x3.RowTemplate.DefaultCellStyle.NullValue = null;
			this.GridView3x3.RowTemplate.Height = 46;
			this.GridView3x3.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.GridView3x3.ScrollBars = System.Windows.Forms.ScrollBars.None;
			this.GridView3x3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
			this.GridView3x3.Size = new System.Drawing.Size(153, 143);
			this.GridView3x3.TabIndex = 3;
			// 
			// dataGridViewTextBoxColumn6
			// 
			this.dataGridViewTextBoxColumn6.HeaderText = "h1";
			this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
			this.dataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn6.Width = 50;
			// 
			// dataGridViewTextBoxColumn7
			// 
			this.dataGridViewTextBoxColumn7.HeaderText = "h2";
			this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
			this.dataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn7.Width = 50;
			// 
			// dataGridViewTextBoxColumn8
			// 
			this.dataGridViewTextBoxColumn8.HeaderText = "h3";
			this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
			this.dataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn8.Width = 50;
			// 
			// Buttons3x3LayoutPanel
			// 
			this.Buttons3x3LayoutPanel.ColumnCount = 1;
			this.Buttons3x3LayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Buttons3x3LayoutPanel.Controls.Add(this.CheckBox3x3, 0, 2);
			this.Buttons3x3LayoutPanel.Controls.Add(this.Gaussian3x3Button, 0, 0);
			this.Buttons3x3LayoutPanel.Controls.Add(this.Laplacian3x3Button, 0, 1);
			this.Buttons3x3LayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Buttons3x3LayoutPanel.Location = new System.Drawing.Point(-31, 0);
			this.Buttons3x3LayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.Buttons3x3LayoutPanel.Name = "Buttons3x3LayoutPanel";
			this.Buttons3x3LayoutPanel.RowCount = 3;
			this.Buttons3x3LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.Buttons3x3LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.Buttons3x3LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.Buttons3x3LayoutPanel.Size = new System.Drawing.Size(32, 143);
			this.Buttons3x3LayoutPanel.TabIndex = 0;
			// 
			// CheckBox3x3
			// 
			this.CheckBox3x3.AutoSize = true;
			this.CheckBox3x3.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
			this.CheckBox3x3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.CheckBox3x3.Location = new System.Drawing.Point(5, 75);
			this.CheckBox3x3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
			this.CheckBox3x3.Name = "CheckBox3x3";
			this.CheckBox3x3.Size = new System.Drawing.Size(22, 63);
			this.CheckBox3x3.TabIndex = 3;
			this.CheckBox3x3.Text = "ON";
			this.CheckBox3x3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.CheckBox3x3.UseVisualStyleBackColor = true;
			this.CheckBox3x3.CheckedChanged += new System.EventHandler(this.CheckBox3x3_CheckedChanged);
			// 
			// Gaussian3x3Button
			// 
			this.Gaussian3x3Button.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Gaussian3x3Button.Location = new System.Drawing.Point(2, 3);
			this.Gaussian3x3Button.Margin = new System.Windows.Forms.Padding(2, 3, 0, 3);
			this.Gaussian3x3Button.Name = "Gaussian3x3Button";
			this.Gaussian3x3Button.Size = new System.Drawing.Size(30, 29);
			this.Gaussian3x3Button.TabIndex = 0;
			this.Gaussian3x3Button.Text = "G";
			this.Gaussian3x3Button.UseVisualStyleBackColor = true;
			this.Gaussian3x3Button.Click += new System.EventHandler(this.Gaussian3x3Button_Click);
			// 
			// Laplacian3x3Button
			// 
			this.Laplacian3x3Button.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Laplacian3x3Button.Location = new System.Drawing.Point(2, 38);
			this.Laplacian3x3Button.Margin = new System.Windows.Forms.Padding(2, 3, 0, 3);
			this.Laplacian3x3Button.Name = "Laplacian3x3Button";
			this.Laplacian3x3Button.Size = new System.Drawing.Size(30, 29);
			this.Laplacian3x3Button.TabIndex = 1;
			this.Laplacian3x3Button.Text = "L";
			this.Laplacian3x3Button.UseVisualStyleBackColor = true;
			this.Laplacian3x3Button.Click += new System.EventHandler(this.Laplacian3x3Button_Click);
			// 
			// Tab5x5
			// 
			this.Tab5x5.Controls.Add(this.Main5x5LayoutPanel);
			this.Tab5x5.Location = new System.Drawing.Point(4, 22);
			this.Tab5x5.Name = "Tab5x5";
			this.Tab5x5.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
			this.Tab5x5.Size = new System.Drawing.Size(0, 148);
			this.Tab5x5.TabIndex = 1;
			this.Tab5x5.Text = "5x5";
			this.Tab5x5.UseVisualStyleBackColor = true;
			// 
			// Main5x5LayoutPanel
			// 
			this.Main5x5LayoutPanel.ColumnCount = 2;
			this.Main5x5LayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Main5x5LayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.Main5x5LayoutPanel.Controls.Add(this.GridView5x5, 0, 0);
			this.Main5x5LayoutPanel.Controls.Add(this.Buttons5x5LayoutPanel, 1, 0);
			this.Main5x5LayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Main5x5LayoutPanel.Location = new System.Drawing.Point(3, 3);
			this.Main5x5LayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.Main5x5LayoutPanel.Name = "Main5x5LayoutPanel";
			this.Main5x5LayoutPanel.RowCount = 1;
			this.Main5x5LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Main5x5LayoutPanel.Size = new System.Drawing.Size(0, 142);
			this.Main5x5LayoutPanel.TabIndex = 0;
			// 
			// GridView5x5
			// 
			this.GridView5x5.AllowUserToAddRows = false;
			this.GridView5x5.AllowUserToDeleteRows = false;
			this.GridView5x5.AllowUserToResizeColumns = false;
			this.GridView5x5.AllowUserToResizeRows = false;
			this.GridView5x5.BackgroundColor = System.Drawing.Color.White;
			this.GridView5x5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridView5x5.ColumnHeadersVisible = false;
			this.GridView5x5.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle3.Format = "N0";
			dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.GridView5x5.DefaultCellStyle = dataGridViewCellStyle3;
			this.GridView5x5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GridView5x5.EnableHeadersVisualStyles = false;
			this.GridView5x5.Location = new System.Drawing.Point(0, 0);
			this.GridView5x5.Margin = new System.Windows.Forms.Padding(0);
			this.GridView5x5.MaximumSize = new System.Drawing.Size(153, 143);
			this.GridView5x5.MinimumSize = new System.Drawing.Size(153, 143);
			this.GridView5x5.MultiSelect = false;
			this.GridView5x5.Name = "GridView5x5";
			this.GridView5x5.RowHeadersVisible = false;
			this.GridView5x5.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			this.GridView5x5.RowsDefaultCellStyle = dataGridViewCellStyle4;
			this.GridView5x5.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.GridView5x5.RowTemplate.DefaultCellStyle.Format = "N0";
			this.GridView5x5.RowTemplate.DefaultCellStyle.NullValue = null;
			this.GridView5x5.RowTemplate.Height = 28;
			this.GridView5x5.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.GridView5x5.ScrollBars = System.Windows.Forms.ScrollBars.None;
			this.GridView5x5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
			this.GridView5x5.Size = new System.Drawing.Size(153, 143);
			this.GridView5x5.TabIndex = 2;
			// 
			// dataGridViewTextBoxColumn1
			// 
			this.dataGridViewTextBoxColumn1.HeaderText = "h1";
			this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
			this.dataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn1.Width = 30;
			// 
			// dataGridViewTextBoxColumn2
			// 
			this.dataGridViewTextBoxColumn2.HeaderText = "h2";
			this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
			this.dataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn2.Width = 30;
			// 
			// dataGridViewTextBoxColumn3
			// 
			this.dataGridViewTextBoxColumn3.HeaderText = "h3";
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn3.Width = 30;
			// 
			// dataGridViewTextBoxColumn4
			// 
			this.dataGridViewTextBoxColumn4.HeaderText = "h4";
			this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
			this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn4.Width = 30;
			// 
			// dataGridViewTextBoxColumn5
			// 
			this.dataGridViewTextBoxColumn5.HeaderText = "h5";
			this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
			this.dataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.dataGridViewTextBoxColumn5.Width = 30;
			// 
			// Buttons5x5LayoutPanel
			// 
			this.Buttons5x5LayoutPanel.ColumnCount = 1;
			this.Buttons5x5LayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Buttons5x5LayoutPanel.Controls.Add(this.CheckBox5x5, 0, 2);
			this.Buttons5x5LayoutPanel.Controls.Add(this.Gaussian5x5Button, 0, 0);
			this.Buttons5x5LayoutPanel.Controls.Add(this.Laplacian5x5Button, 0, 1);
			this.Buttons5x5LayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Buttons5x5LayoutPanel.Location = new System.Drawing.Point(-31, 0);
			this.Buttons5x5LayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.Buttons5x5LayoutPanel.Name = "Buttons5x5LayoutPanel";
			this.Buttons5x5LayoutPanel.RowCount = 3;
			this.Buttons5x5LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.Buttons5x5LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.Buttons5x5LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.Buttons5x5LayoutPanel.Size = new System.Drawing.Size(32, 142);
			this.Buttons5x5LayoutPanel.TabIndex = 0;
			// 
			// CheckBox5x5
			// 
			this.CheckBox5x5.AutoSize = true;
			this.CheckBox5x5.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
			this.CheckBox5x5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.CheckBox5x5.Location = new System.Drawing.Point(5, 75);
			this.CheckBox5x5.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
			this.CheckBox5x5.Name = "CheckBox5x5";
			this.CheckBox5x5.Size = new System.Drawing.Size(22, 62);
			this.CheckBox5x5.TabIndex = 3;
			this.CheckBox5x5.Text = "ON";
			this.CheckBox5x5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.CheckBox5x5.UseVisualStyleBackColor = true;
			this.CheckBox5x5.CheckedChanged += new System.EventHandler(this.CheckBox5x5_CheckedChanged);
			// 
			// Gaussian5x5Button
			// 
			this.Gaussian5x5Button.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Gaussian5x5Button.Location = new System.Drawing.Point(2, 3);
			this.Gaussian5x5Button.Margin = new System.Windows.Forms.Padding(2, 3, 0, 3);
			this.Gaussian5x5Button.Name = "Gaussian5x5Button";
			this.Gaussian5x5Button.Size = new System.Drawing.Size(30, 29);
			this.Gaussian5x5Button.TabIndex = 0;
			this.Gaussian5x5Button.Text = "G";
			this.Gaussian5x5Button.UseVisualStyleBackColor = true;
			this.Gaussian5x5Button.Click += new System.EventHandler(this.Gaussian5x5Button_Click);
			// 
			// Laplacian5x5Button
			// 
			this.Laplacian5x5Button.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Laplacian5x5Button.Location = new System.Drawing.Point(2, 38);
			this.Laplacian5x5Button.Margin = new System.Windows.Forms.Padding(2, 3, 0, 3);
			this.Laplacian5x5Button.Name = "Laplacian5x5Button";
			this.Laplacian5x5Button.Size = new System.Drawing.Size(30, 29);
			this.Laplacian5x5Button.TabIndex = 1;
			this.Laplacian5x5Button.Text = "L";
			this.Laplacian5x5Button.UseVisualStyleBackColor = true;
			this.Laplacian5x5Button.Click += new System.EventHandler(this.Laplacian5x5Button_Click);
			// 
			// Tab7x7
			// 
			this.Tab7x7.Controls.Add(this.Main7x7LayoutPanel);
			this.Tab7x7.Location = new System.Drawing.Point(4, 22);
			this.Tab7x7.Margin = new System.Windows.Forms.Padding(0);
			this.Tab7x7.Name = "Tab7x7";
			this.Tab7x7.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
			this.Tab7x7.Size = new System.Drawing.Size(0, 148);
			this.Tab7x7.TabIndex = 2;
			this.Tab7x7.Text = "7x7";
			this.Tab7x7.UseVisualStyleBackColor = true;
			// 
			// Main7x7LayoutPanel
			// 
			this.Main7x7LayoutPanel.ColumnCount = 2;
			this.Main7x7LayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Main7x7LayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.Main7x7LayoutPanel.Controls.Add(this.Buttons7x7LayoutPanel, 1, 0);
			this.Main7x7LayoutPanel.Controls.Add(this.GridView7x7, 0, 0);
			this.Main7x7LayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Main7x7LayoutPanel.Location = new System.Drawing.Point(3, 3);
			this.Main7x7LayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.Main7x7LayoutPanel.Name = "Main7x7LayoutPanel";
			this.Main7x7LayoutPanel.RowCount = 1;
			this.Main7x7LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Main7x7LayoutPanel.Size = new System.Drawing.Size(0, 142);
			this.Main7x7LayoutPanel.TabIndex = 0;
			// 
			// Buttons7x7LayoutPanel
			// 
			this.Buttons7x7LayoutPanel.ColumnCount = 1;
			this.Buttons7x7LayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Buttons7x7LayoutPanel.Controls.Add(this.Gaussian7x7Button, 0, 0);
			this.Buttons7x7LayoutPanel.Controls.Add(this.Laplacian7x7Button, 0, 1);
			this.Buttons7x7LayoutPanel.Controls.Add(this.CheckBox7x7, 0, 2);
			this.Buttons7x7LayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Buttons7x7LayoutPanel.Location = new System.Drawing.Point(-31, 0);
			this.Buttons7x7LayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.Buttons7x7LayoutPanel.Name = "Buttons7x7LayoutPanel";
			this.Buttons7x7LayoutPanel.RowCount = 3;
			this.Buttons7x7LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.Buttons7x7LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.Buttons7x7LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.Buttons7x7LayoutPanel.Size = new System.Drawing.Size(32, 142);
			this.Buttons7x7LayoutPanel.TabIndex = 0;
			// 
			// Gaussian7x7Button
			// 
			this.Gaussian7x7Button.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Gaussian7x7Button.Location = new System.Drawing.Point(2, 3);
			this.Gaussian7x7Button.Margin = new System.Windows.Forms.Padding(2, 3, 0, 3);
			this.Gaussian7x7Button.Name = "Gaussian7x7Button";
			this.Gaussian7x7Button.Size = new System.Drawing.Size(30, 29);
			this.Gaussian7x7Button.TabIndex = 0;
			this.Gaussian7x7Button.Text = "G";
			this.Gaussian7x7Button.UseVisualStyleBackColor = true;
			this.Gaussian7x7Button.Click += new System.EventHandler(this.Gaussian7x7Button_Click);
			// 
			// Laplacian7x7Button
			// 
			this.Laplacian7x7Button.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Laplacian7x7Button.Location = new System.Drawing.Point(2, 38);
			this.Laplacian7x7Button.Margin = new System.Windows.Forms.Padding(2, 3, 0, 3);
			this.Laplacian7x7Button.Name = "Laplacian7x7Button";
			this.Laplacian7x7Button.Size = new System.Drawing.Size(30, 29);
			this.Laplacian7x7Button.TabIndex = 1;
			this.Laplacian7x7Button.Text = "L";
			this.Laplacian7x7Button.UseVisualStyleBackColor = true;
			this.Laplacian7x7Button.Click += new System.EventHandler(this.Laplacian7x7Button_Click);
			// 
			// CheckBox7x7
			// 
			this.CheckBox7x7.AutoSize = true;
			this.CheckBox7x7.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
			this.CheckBox7x7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.CheckBox7x7.Location = new System.Drawing.Point(5, 75);
			this.CheckBox7x7.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
			this.CheckBox7x7.Name = "CheckBox7x7";
			this.CheckBox7x7.Size = new System.Drawing.Size(22, 62);
			this.CheckBox7x7.TabIndex = 2;
			this.CheckBox7x7.Text = "ON";
			this.CheckBox7x7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.CheckBox7x7.UseVisualStyleBackColor = true;
			this.CheckBox7x7.CheckedChanged += new System.EventHandler(this.CheckBox7x7_CheckedChanged);
			// 
			// GridView7x7
			// 
			this.GridView7x7.AllowUserToAddRows = false;
			this.GridView7x7.AllowUserToDeleteRows = false;
			this.GridView7x7.AllowUserToResizeColumns = false;
			this.GridView7x7.AllowUserToResizeRows = false;
			this.GridView7x7.BackgroundColor = System.Drawing.Color.White;
			this.GridView7x7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridView7x7.ColumnHeadersVisible = false;
			this.GridView7x7.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7});
			dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
			dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
			dataGridViewCellStyle5.Format = "N0";
			dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
			this.GridView7x7.DefaultCellStyle = dataGridViewCellStyle5;
			this.GridView7x7.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GridView7x7.EnableHeadersVisualStyles = false;
			this.GridView7x7.Location = new System.Drawing.Point(0, 0);
			this.GridView7x7.Margin = new System.Windows.Forms.Padding(0);
			this.GridView7x7.MaximumSize = new System.Drawing.Size(153, 143);
			this.GridView7x7.MinimumSize = new System.Drawing.Size(153, 143);
			this.GridView7x7.MultiSelect = false;
			this.GridView7x7.Name = "GridView7x7";
			this.GridView7x7.RowHeadersVisible = false;
			this.GridView7x7.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
			dataGridViewCellStyle6.Format = "N0";
			this.GridView7x7.RowsDefaultCellStyle = dataGridViewCellStyle6;
			this.GridView7x7.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.GridView7x7.RowTemplate.DefaultCellStyle.Format = "N0";
			this.GridView7x7.RowTemplate.DefaultCellStyle.NullValue = null;
			this.GridView7x7.RowTemplate.Height = 20;
			this.GridView7x7.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.GridView7x7.ScrollBars = System.Windows.Forms.ScrollBars.None;
			this.GridView7x7.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
			this.GridView7x7.Size = new System.Drawing.Size(153, 143);
			this.GridView7x7.TabIndex = 1;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "h1";
			this.Column1.Name = "Column1";
			this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.Column1.Width = 22;
			// 
			// Column2
			// 
			this.Column2.HeaderText = "h2";
			this.Column2.Name = "Column2";
			this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.Column2.Width = 21;
			// 
			// Column3
			// 
			this.Column3.HeaderText = "h3";
			this.Column3.Name = "Column3";
			this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.Column3.Width = 21;
			// 
			// Column4
			// 
			this.Column4.HeaderText = "h4";
			this.Column4.Name = "Column4";
			this.Column4.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.Column4.Width = 21;
			// 
			// Column5
			// 
			this.Column5.HeaderText = "h5";
			this.Column5.Name = "Column5";
			this.Column5.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.Column5.Width = 21;
			// 
			// Column6
			// 
			this.Column6.HeaderText = "h6";
			this.Column6.Name = "Column6";
			this.Column6.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.Column6.Width = 22;
			// 
			// Column7
			// 
			this.Column7.HeaderText = "h7";
			this.Column7.Name = "Column7";
			this.Column7.Resizable = System.Windows.Forms.DataGridViewTriState.False;
			this.Column7.Width = 22;
			// 
			// HistogramTabControl
			// 
			this.HistogramTabControl.Controls.Add(this.RTab);
			this.HistogramTabControl.Controls.Add(this.GTab);
			this.HistogramTabControl.Controls.Add(this.Btab);
			this.HistogramTabControl.Controls.Add(this.GammaTab);
			this.HistogramTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.HistogramTabControl.Location = new System.Drawing.Point(1, 1);
			this.HistogramTabControl.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.HistogramTabControl.Name = "HistogramTabControl";
			this.HistogramTabControl.SelectedIndex = 0;
			this.HistogramTabControl.Size = new System.Drawing.Size(1, 218);
			this.HistogramTabControl.TabIndex = 0;
			// 
			// RTab
			// 
			this.RTab.Controls.Add(this.RTabLayotPanel);
			this.RTab.Location = new System.Drawing.Point(4, 22);
			this.RTab.Name = "RTab";
			this.RTab.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
			this.RTab.Size = new System.Drawing.Size(0, 192);
			this.RTab.TabIndex = 0;
			this.RTab.Text = "R";
			this.RTab.UseVisualStyleBackColor = true;
			// 
			// RTabLayotPanel
			// 
			this.RTabLayotPanel.ColumnCount = 1;
			this.RTabLayotPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.RTabLayotPanel.Controls.Add(this.RShowFilterWindowButton, 0, 2);
			this.RTabLayotPanel.Controls.Add(this.RHistogramLayoutPanel, 0, 0);
			this.RTabLayotPanel.Controls.Add(this.RFunctonFilterLayoutPanel, 0, 1);
			this.RTabLayotPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RTabLayotPanel.Location = new System.Drawing.Point(3, 3);
			this.RTabLayotPanel.Margin = new System.Windows.Forms.Padding(0);
			this.RTabLayotPanel.Name = "RTabLayotPanel";
			this.RTabLayotPanel.RowCount = 3;
			this.RTabLayotPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.RTabLayotPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 0F));
			this.RTabLayotPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.RTabLayotPanel.Size = new System.Drawing.Size(0, 186);
			this.RTabLayotPanel.TabIndex = 0;
			// 
			// RShowFilterWindowButton
			// 
			this.RShowFilterWindowButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RShowFilterWindowButton.Location = new System.Drawing.Point(0, 166);
			this.RShowFilterWindowButton.Margin = new System.Windows.Forms.Padding(0);
			this.RShowFilterWindowButton.Name = "RShowFilterWindowButton";
			this.RShowFilterWindowButton.Size = new System.Drawing.Size(1, 20);
			this.RShowFilterWindowButton.TabIndex = 1;
			this.RShowFilterWindowButton.Text = "Show function filter editor";
			this.RShowFilterWindowButton.UseVisualStyleBackColor = true;
			this.RShowFilterWindowButton.Click += new System.EventHandler(this.RShowFilterWindowButton_Click);
			// 
			// RHistogramLayoutPanel
			// 
			this.RHistogramLayoutPanel.ColumnCount = 1;
			this.RHistogramLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.RHistogramLayoutPanel.Controls.Add(this.RHistogramVisualizingLayoutPanel, 0, 0);
			this.RHistogramLayoutPanel.Controls.Add(this.EqualizeHistogramRed, 0, 1);
			this.RHistogramLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RHistogramLayoutPanel.Location = new System.Drawing.Point(0, 0);
			this.RHistogramLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.RHistogramLayoutPanel.Name = "RHistogramLayoutPanel";
			this.RHistogramLayoutPanel.RowCount = 2;
			this.RHistogramLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.RHistogramLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.RHistogramLayoutPanel.Size = new System.Drawing.Size(1, 166);
			this.RHistogramLayoutPanel.TabIndex = 2;
			// 
			// RHistogramVisualizingLayoutPanel
			// 
			this.RHistogramVisualizingLayoutPanel.ColumnCount = 2;
			this.RHistogramVisualizingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.RHistogramVisualizingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.RHistogramVisualizingLayoutPanel.Controls.Add(this.RHistogramPictureBox, 1, 0);
			this.RHistogramVisualizingLayoutPanel.Controls.Add(this.RHistogramXAxisPB, 1, 1);
			this.RHistogramVisualizingLayoutPanel.Controls.Add(this.RHistogramYAxisPB, 0, 0);
			this.RHistogramVisualizingLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RHistogramVisualizingLayoutPanel.Location = new System.Drawing.Point(0, 0);
			this.RHistogramVisualizingLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.RHistogramVisualizingLayoutPanel.Name = "RHistogramVisualizingLayoutPanel";
			this.RHistogramVisualizingLayoutPanel.RowCount = 2;
			this.RHistogramVisualizingLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.RHistogramVisualizingLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.RHistogramVisualizingLayoutPanel.Size = new System.Drawing.Size(1, 146);
			this.RHistogramVisualizingLayoutPanel.TabIndex = 1;
			// 
			// RHistogramPictureBox
			// 
			this.RHistogramPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RHistogramPictureBox.Location = new System.Drawing.Point(20, 0);
			this.RHistogramPictureBox.Margin = new System.Windows.Forms.Padding(0);
			this.RHistogramPictureBox.MaximumSize = new System.Drawing.Size(164, 126);
			this.RHistogramPictureBox.MinimumSize = new System.Drawing.Size(164, 126);
			this.RHistogramPictureBox.Name = "RHistogramPictureBox";
			this.RHistogramPictureBox.Size = new System.Drawing.Size(164, 126);
			this.RHistogramPictureBox.TabIndex = 0;
			this.RHistogramPictureBox.TabStop = false;
			// 
			// RHistogramXAxisPB
			// 
			this.RHistogramXAxisPB.BackgroundImage = global::GK1.Properties.Resources.Axis;
			this.RHistogramXAxisPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RHistogramXAxisPB.Location = new System.Drawing.Point(20, 129);
			this.RHistogramXAxisPB.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
			this.RHistogramXAxisPB.MaximumSize = new System.Drawing.Size(164, 17);
			this.RHistogramXAxisPB.MinimumSize = new System.Drawing.Size(164, 17);
			this.RHistogramXAxisPB.Name = "RHistogramXAxisPB";
			this.RHistogramXAxisPB.Size = new System.Drawing.Size(164, 17);
			this.RHistogramXAxisPB.TabIndex = 1;
			this.RHistogramXAxisPB.TabStop = false;
			// 
			// RHistogramYAxisPB
			// 
			this.RHistogramYAxisPB.BackgroundImage = global::GK1.Properties.Resources.AxisMax;
			this.RHistogramYAxisPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RHistogramYAxisPB.Location = new System.Drawing.Point(0, 0);
			this.RHistogramYAxisPB.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
			this.RHistogramYAxisPB.MaximumSize = new System.Drawing.Size(17, 126);
			this.RHistogramYAxisPB.MinimumSize = new System.Drawing.Size(17, 126);
			this.RHistogramYAxisPB.Name = "RHistogramYAxisPB";
			this.RHistogramYAxisPB.Size = new System.Drawing.Size(17, 126);
			this.RHistogramYAxisPB.TabIndex = 2;
			this.RHistogramYAxisPB.TabStop = false;
			// 
			// EqualizeHistogramRed
			// 
			this.EqualizeHistogramRed.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.EqualizeHistogramRed.AutoSize = true;
			this.EqualizeHistogramRed.Location = new System.Drawing.Point(0, 147);
			this.EqualizeHistogramRed.Margin = new System.Windows.Forms.Padding(0);
			this.EqualizeHistogramRed.Name = "EqualizeHistogramRed";
			this.EqualizeHistogramRed.Size = new System.Drawing.Size(1, 17);
			this.EqualizeHistogramRed.TabIndex = 2;
			this.EqualizeHistogramRed.Text = "Equalize Histogram";
			this.EqualizeHistogramRed.UseVisualStyleBackColor = true;
			// 
			// RFunctonFilterLayoutPanel
			// 
			this.RFunctonFilterLayoutPanel.ColumnCount = 1;
			this.RFunctonFilterLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 199F));
			this.RFunctonFilterLayoutPanel.Controls.Add(this.RFinctionFilterVisualizingLayoutPanel, 0, 0);
			this.RFunctonFilterLayoutPanel.Controls.Add(this.RApplyFilterCheckBox, 0, 1);
			this.RFunctonFilterLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RFunctonFilterLayoutPanel.Location = new System.Drawing.Point(0, 166);
			this.RFunctonFilterLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.RFunctonFilterLayoutPanel.Name = "RFunctonFilterLayoutPanel";
			this.RFunctonFilterLayoutPanel.RowCount = 2;
			this.RFunctonFilterLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.RFunctonFilterLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.RFunctonFilterLayoutPanel.Size = new System.Drawing.Size(1, 1);
			this.RFunctonFilterLayoutPanel.TabIndex = 3;
			// 
			// RFinctionFilterVisualizingLayoutPanel
			// 
			this.RFinctionFilterVisualizingLayoutPanel.ColumnCount = 2;
			this.RFinctionFilterVisualizingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.RFinctionFilterVisualizingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.RFinctionFilterVisualizingLayoutPanel.Controls.Add(this.RFilterVisualizingMainPB, 1, 0);
			this.RFinctionFilterVisualizingLayoutPanel.Controls.Add(this.RYAxisForFilterVisPB, 0, 0);
			this.RFinctionFilterVisualizingLayoutPanel.Controls.Add(this.RXAxisForFilterVisPB, 1, 1);
			this.RFinctionFilterVisualizingLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RFinctionFilterVisualizingLayoutPanel.Location = new System.Drawing.Point(0, 0);
			this.RFinctionFilterVisualizingLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.RFinctionFilterVisualizingLayoutPanel.Name = "RFinctionFilterVisualizingLayoutPanel";
			this.RFinctionFilterVisualizingLayoutPanel.RowCount = 2;
			this.RFinctionFilterVisualizingLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.RFinctionFilterVisualizingLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.RFinctionFilterVisualizingLayoutPanel.Size = new System.Drawing.Size(199, 1);
			this.RFinctionFilterVisualizingLayoutPanel.TabIndex = 0;
			// 
			// RFilterVisualizingMainPB
			// 
			this.RFilterVisualizingMainPB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.RFilterVisualizingMainPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RFilterVisualizingMainPB.Location = new System.Drawing.Point(20, 0);
			this.RFilterVisualizingMainPB.Margin = new System.Windows.Forms.Padding(0);
			this.RFilterVisualizingMainPB.MaximumSize = new System.Drawing.Size(164, 126);
			this.RFilterVisualizingMainPB.MinimumSize = new System.Drawing.Size(164, 126);
			this.RFilterVisualizingMainPB.Name = "RFilterVisualizingMainPB";
			this.RFilterVisualizingMainPB.Size = new System.Drawing.Size(164, 126);
			this.RFilterVisualizingMainPB.TabIndex = 0;
			this.RFilterVisualizingMainPB.TabStop = false;
			this.RFilterVisualizingMainPB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RFilterVisualizingMainPB_MouseClick);
			this.RFilterVisualizingMainPB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.RFilterVisualizingMainPB_MouseDown);
			this.RFilterVisualizingMainPB.MouseMove += new System.Windows.Forms.MouseEventHandler(this.RFilterVisualizingMainPB_MouseMove);
			this.RFilterVisualizingMainPB.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RFilterVisualizingMainPB_MouseUp);
			// 
			// RYAxisForFilterVisPB
			// 
			this.RYAxisForFilterVisPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RYAxisForFilterVisPB.Location = new System.Drawing.Point(0, 0);
			this.RYAxisForFilterVisPB.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
			this.RYAxisForFilterVisPB.MaximumSize = new System.Drawing.Size(17, 126);
			this.RYAxisForFilterVisPB.MinimumSize = new System.Drawing.Size(17, 126);
			this.RYAxisForFilterVisPB.Name = "RYAxisForFilterVisPB";
			this.RYAxisForFilterVisPB.Size = new System.Drawing.Size(17, 126);
			this.RYAxisForFilterVisPB.TabIndex = 1;
			this.RYAxisForFilterVisPB.TabStop = false;
			// 
			// RXAxisForFilterVisPB
			// 
			this.RXAxisForFilterVisPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RXAxisForFilterVisPB.Location = new System.Drawing.Point(20, -16);
			this.RXAxisForFilterVisPB.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
			this.RXAxisForFilterVisPB.MaximumSize = new System.Drawing.Size(164, 17);
			this.RXAxisForFilterVisPB.MinimumSize = new System.Drawing.Size(164, 17);
			this.RXAxisForFilterVisPB.Name = "RXAxisForFilterVisPB";
			this.RXAxisForFilterVisPB.Size = new System.Drawing.Size(164, 17);
			this.RXAxisForFilterVisPB.TabIndex = 2;
			this.RXAxisForFilterVisPB.TabStop = false;
			// 
			// RApplyFilterCheckBox
			// 
			this.RApplyFilterCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RApplyFilterCheckBox.AutoSize = true;
			this.RApplyFilterCheckBox.Location = new System.Drawing.Point(73, -19);
			this.RApplyFilterCheckBox.Margin = new System.Windows.Forms.Padding(0);
			this.RApplyFilterCheckBox.Name = "RApplyFilterCheckBox";
			this.RApplyFilterCheckBox.Padding = new System.Windows.Forms.Padding(0, 2, 0, 0);
			this.RApplyFilterCheckBox.Size = new System.Drawing.Size(52, 19);
			this.RApplyFilterCheckBox.TabIndex = 1;
			this.RApplyFilterCheckBox.Text = "Apply";
			this.RApplyFilterCheckBox.UseVisualStyleBackColor = true;
			this.RApplyFilterCheckBox.CheckedChanged += new System.EventHandler(this.RApplyFilterCheckBox_CheckedChanged);
			// 
			// GTab
			// 
			this.GTab.Controls.Add(this.GTabLayotPanel);
			this.GTab.Location = new System.Drawing.Point(4, 22);
			this.GTab.Name = "GTab";
			this.GTab.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
			this.GTab.Size = new System.Drawing.Size(0, 191);
			this.GTab.TabIndex = 1;
			this.GTab.Text = "G";
			this.GTab.UseVisualStyleBackColor = true;
			// 
			// GTabLayotPanel
			// 
			this.GTabLayotPanel.ColumnCount = 1;
			this.GTabLayotPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.GTabLayotPanel.Controls.Add(this.GShowFilterWindowButton, 0, 2);
			this.GTabLayotPanel.Controls.Add(this.GHistogramLayoutPanel, 0, 0);
			this.GTabLayotPanel.Controls.Add(this.GFunctonFilterLayoutPanel, 0, 1);
			this.GTabLayotPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GTabLayotPanel.Location = new System.Drawing.Point(3, 3);
			this.GTabLayotPanel.Margin = new System.Windows.Forms.Padding(0);
			this.GTabLayotPanel.Name = "GTabLayotPanel";
			this.GTabLayotPanel.RowCount = 3;
			this.GTabLayotPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.GTabLayotPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 0F));
			this.GTabLayotPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.GTabLayotPanel.Size = new System.Drawing.Size(0, 185);
			this.GTabLayotPanel.TabIndex = 1;
			// 
			// GShowFilterWindowButton
			// 
			this.GShowFilterWindowButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GShowFilterWindowButton.Location = new System.Drawing.Point(0, 165);
			this.GShowFilterWindowButton.Margin = new System.Windows.Forms.Padding(0);
			this.GShowFilterWindowButton.Name = "GShowFilterWindowButton";
			this.GShowFilterWindowButton.Size = new System.Drawing.Size(1, 20);
			this.GShowFilterWindowButton.TabIndex = 1;
			this.GShowFilterWindowButton.Text = "Show function filter editor";
			this.GShowFilterWindowButton.UseVisualStyleBackColor = true;
			this.GShowFilterWindowButton.Click += new System.EventHandler(this.GShowFilterWindowButton_Click);
			// 
			// GHistogramLayoutPanel
			// 
			this.GHistogramLayoutPanel.ColumnCount = 1;
			this.GHistogramLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.GHistogramLayoutPanel.Controls.Add(this.GHistogramVisualizingLayoutPanel, 0, 0);
			this.GHistogramLayoutPanel.Controls.Add(this.EqualizeHistogramGreen, 0, 1);
			this.GHistogramLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GHistogramLayoutPanel.Location = new System.Drawing.Point(0, 0);
			this.GHistogramLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.GHistogramLayoutPanel.Name = "GHistogramLayoutPanel";
			this.GHistogramLayoutPanel.RowCount = 2;
			this.GHistogramLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.GHistogramLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.GHistogramLayoutPanel.Size = new System.Drawing.Size(1, 165);
			this.GHistogramLayoutPanel.TabIndex = 2;
			// 
			// GHistogramVisualizingLayoutPanel
			// 
			this.GHistogramVisualizingLayoutPanel.ColumnCount = 2;
			this.GHistogramVisualizingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.GHistogramVisualizingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.GHistogramVisualizingLayoutPanel.Controls.Add(this.GHistogramPictureBox, 1, 0);
			this.GHistogramVisualizingLayoutPanel.Controls.Add(this.GHistogramXAxisPB, 1, 1);
			this.GHistogramVisualizingLayoutPanel.Controls.Add(this.GHistogramYAxisPB, 0, 0);
			this.GHistogramVisualizingLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GHistogramVisualizingLayoutPanel.Location = new System.Drawing.Point(0, 0);
			this.GHistogramVisualizingLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.GHistogramVisualizingLayoutPanel.Name = "GHistogramVisualizingLayoutPanel";
			this.GHistogramVisualizingLayoutPanel.RowCount = 2;
			this.GHistogramVisualizingLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.GHistogramVisualizingLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.GHistogramVisualizingLayoutPanel.Size = new System.Drawing.Size(1, 145);
			this.GHistogramVisualizingLayoutPanel.TabIndex = 1;
			// 
			// GHistogramPictureBox
			// 
			this.GHistogramPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GHistogramPictureBox.Location = new System.Drawing.Point(20, 0);
			this.GHistogramPictureBox.Margin = new System.Windows.Forms.Padding(0);
			this.GHistogramPictureBox.MaximumSize = new System.Drawing.Size(164, 126);
			this.GHistogramPictureBox.MinimumSize = new System.Drawing.Size(164, 126);
			this.GHistogramPictureBox.Name = "GHistogramPictureBox";
			this.GHistogramPictureBox.Size = new System.Drawing.Size(164, 126);
			this.GHistogramPictureBox.TabIndex = 0;
			this.GHistogramPictureBox.TabStop = false;
			// 
			// GHistogramXAxisPB
			// 
			this.GHistogramXAxisPB.BackgroundImage = global::GK1.Properties.Resources.Axis;
			this.GHistogramXAxisPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GHistogramXAxisPB.Location = new System.Drawing.Point(20, 128);
			this.GHistogramXAxisPB.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
			this.GHistogramXAxisPB.MaximumSize = new System.Drawing.Size(164, 17);
			this.GHistogramXAxisPB.MinimumSize = new System.Drawing.Size(164, 17);
			this.GHistogramXAxisPB.Name = "GHistogramXAxisPB";
			this.GHistogramXAxisPB.Size = new System.Drawing.Size(164, 17);
			this.GHistogramXAxisPB.TabIndex = 1;
			this.GHistogramXAxisPB.TabStop = false;
			// 
			// GHistogramYAxisPB
			// 
			this.GHistogramYAxisPB.BackgroundImage = global::GK1.Properties.Resources.AxisMax;
			this.GHistogramYAxisPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GHistogramYAxisPB.Location = new System.Drawing.Point(0, 0);
			this.GHistogramYAxisPB.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
			this.GHistogramYAxisPB.MaximumSize = new System.Drawing.Size(17, 126);
			this.GHistogramYAxisPB.MinimumSize = new System.Drawing.Size(17, 126);
			this.GHistogramYAxisPB.Name = "GHistogramYAxisPB";
			this.GHistogramYAxisPB.Size = new System.Drawing.Size(17, 126);
			this.GHistogramYAxisPB.TabIndex = 2;
			this.GHistogramYAxisPB.TabStop = false;
			// 
			// EqualizeHistogramGreen
			// 
			this.EqualizeHistogramGreen.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.EqualizeHistogramGreen.AutoSize = true;
			this.EqualizeHistogramGreen.Location = new System.Drawing.Point(0, 146);
			this.EqualizeHistogramGreen.Margin = new System.Windows.Forms.Padding(0);
			this.EqualizeHistogramGreen.Name = "EqualizeHistogramGreen";
			this.EqualizeHistogramGreen.Size = new System.Drawing.Size(1, 17);
			this.EqualizeHistogramGreen.TabIndex = 2;
			this.EqualizeHistogramGreen.Text = "Equalize Histogram";
			this.EqualizeHistogramGreen.UseVisualStyleBackColor = true;
			// 
			// GFunctonFilterLayoutPanel
			// 
			this.GFunctonFilterLayoutPanel.ColumnCount = 1;
			this.GFunctonFilterLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 199F));
			this.GFunctonFilterLayoutPanel.Controls.Add(this.GFinctionFilterVisualizingLayoutPanel, 0, 0);
			this.GFunctonFilterLayoutPanel.Controls.Add(this.GApplyFilterCheckBox, 0, 1);
			this.GFunctonFilterLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GFunctonFilterLayoutPanel.Location = new System.Drawing.Point(0, 165);
			this.GFunctonFilterLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.GFunctonFilterLayoutPanel.Name = "GFunctonFilterLayoutPanel";
			this.GFunctonFilterLayoutPanel.RowCount = 2;
			this.GFunctonFilterLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.GFunctonFilterLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.GFunctonFilterLayoutPanel.Size = new System.Drawing.Size(1, 1);
			this.GFunctonFilterLayoutPanel.TabIndex = 3;
			// 
			// GFinctionFilterVisualizingLayoutPanel
			// 
			this.GFinctionFilterVisualizingLayoutPanel.ColumnCount = 2;
			this.GFinctionFilterVisualizingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.GFinctionFilterVisualizingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.GFinctionFilterVisualizingLayoutPanel.Controls.Add(this.GFilterVisualizingMainPB, 1, 0);
			this.GFinctionFilterVisualizingLayoutPanel.Controls.Add(this.GYAxisForFilterVisPB, 0, 0);
			this.GFinctionFilterVisualizingLayoutPanel.Controls.Add(this.GXAxisForFilterVisPB, 1, 1);
			this.GFinctionFilterVisualizingLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GFinctionFilterVisualizingLayoutPanel.Location = new System.Drawing.Point(0, 0);
			this.GFinctionFilterVisualizingLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.GFinctionFilterVisualizingLayoutPanel.Name = "GFinctionFilterVisualizingLayoutPanel";
			this.GFinctionFilterVisualizingLayoutPanel.RowCount = 2;
			this.GFinctionFilterVisualizingLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.GFinctionFilterVisualizingLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.GFinctionFilterVisualizingLayoutPanel.Size = new System.Drawing.Size(199, 1);
			this.GFinctionFilterVisualizingLayoutPanel.TabIndex = 0;
			// 
			// GFilterVisualizingMainPB
			// 
			this.GFilterVisualizingMainPB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.GFilterVisualizingMainPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GFilterVisualizingMainPB.Location = new System.Drawing.Point(20, 0);
			this.GFilterVisualizingMainPB.Margin = new System.Windows.Forms.Padding(0);
			this.GFilterVisualizingMainPB.MaximumSize = new System.Drawing.Size(164, 126);
			this.GFilterVisualizingMainPB.MinimumSize = new System.Drawing.Size(164, 126);
			this.GFilterVisualizingMainPB.Name = "GFilterVisualizingMainPB";
			this.GFilterVisualizingMainPB.Size = new System.Drawing.Size(164, 126);
			this.GFilterVisualizingMainPB.TabIndex = 0;
			this.GFilterVisualizingMainPB.TabStop = false;
			this.GFilterVisualizingMainPB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.GFilterVisualizingMainPB_MouseClick);
			this.GFilterVisualizingMainPB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GFilterVisualizingMainPB_MouseDown);
			this.GFilterVisualizingMainPB.MouseMove += new System.Windows.Forms.MouseEventHandler(this.GFilterVisualizingMainPB_MouseMove);
			this.GFilterVisualizingMainPB.MouseUp += new System.Windows.Forms.MouseEventHandler(this.GFilterVisualizingMainPB_MouseUp);
			// 
			// GYAxisForFilterVisPB
			// 
			this.GYAxisForFilterVisPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GYAxisForFilterVisPB.Location = new System.Drawing.Point(0, 0);
			this.GYAxisForFilterVisPB.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
			this.GYAxisForFilterVisPB.MaximumSize = new System.Drawing.Size(17, 126);
			this.GYAxisForFilterVisPB.MinimumSize = new System.Drawing.Size(17, 126);
			this.GYAxisForFilterVisPB.Name = "GYAxisForFilterVisPB";
			this.GYAxisForFilterVisPB.Size = new System.Drawing.Size(17, 126);
			this.GYAxisForFilterVisPB.TabIndex = 1;
			this.GYAxisForFilterVisPB.TabStop = false;
			// 
			// GXAxisForFilterVisPB
			// 
			this.GXAxisForFilterVisPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GXAxisForFilterVisPB.Location = new System.Drawing.Point(20, -16);
			this.GXAxisForFilterVisPB.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
			this.GXAxisForFilterVisPB.MaximumSize = new System.Drawing.Size(164, 17);
			this.GXAxisForFilterVisPB.MinimumSize = new System.Drawing.Size(164, 17);
			this.GXAxisForFilterVisPB.Name = "GXAxisForFilterVisPB";
			this.GXAxisForFilterVisPB.Size = new System.Drawing.Size(164, 17);
			this.GXAxisForFilterVisPB.TabIndex = 2;
			this.GXAxisForFilterVisPB.TabStop = false;
			// 
			// GApplyFilterCheckBox
			// 
			this.GApplyFilterCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.GApplyFilterCheckBox.AutoSize = true;
			this.GApplyFilterCheckBox.Location = new System.Drawing.Point(73, -17);
			this.GApplyFilterCheckBox.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
			this.GApplyFilterCheckBox.Name = "GApplyFilterCheckBox";
			this.GApplyFilterCheckBox.Size = new System.Drawing.Size(52, 17);
			this.GApplyFilterCheckBox.TabIndex = 1;
			this.GApplyFilterCheckBox.Text = "Apply";
			this.GApplyFilterCheckBox.UseVisualStyleBackColor = true;
			this.GApplyFilterCheckBox.CheckedChanged += new System.EventHandler(this.GApplyFilterCheckBox_CheckedChanged);
			// 
			// Btab
			// 
			this.Btab.Controls.Add(this.BTabLayotPanel);
			this.Btab.Location = new System.Drawing.Point(4, 22);
			this.Btab.Name = "Btab";
			this.Btab.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
			this.Btab.Size = new System.Drawing.Size(0, 191);
			this.Btab.TabIndex = 2;
			this.Btab.Text = "B";
			this.Btab.UseVisualStyleBackColor = true;
			// 
			// BTabLayotPanel
			// 
			this.BTabLayotPanel.ColumnCount = 1;
			this.BTabLayotPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.BTabLayotPanel.Controls.Add(this.BShowFilterWindowButton, 0, 2);
			this.BTabLayotPanel.Controls.Add(this.BHistogramLayoutPanel, 0, 0);
			this.BTabLayotPanel.Controls.Add(this.BFunctonFilterLayoutPanel, 0, 1);
			this.BTabLayotPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BTabLayotPanel.Location = new System.Drawing.Point(3, 3);
			this.BTabLayotPanel.Margin = new System.Windows.Forms.Padding(0);
			this.BTabLayotPanel.Name = "BTabLayotPanel";
			this.BTabLayotPanel.RowCount = 3;
			this.BTabLayotPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.BTabLayotPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 0F));
			this.BTabLayotPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.BTabLayotPanel.Size = new System.Drawing.Size(0, 185);
			this.BTabLayotPanel.TabIndex = 2;
			// 
			// BShowFilterWindowButton
			// 
			this.BShowFilterWindowButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BShowFilterWindowButton.Location = new System.Drawing.Point(0, 165);
			this.BShowFilterWindowButton.Margin = new System.Windows.Forms.Padding(0);
			this.BShowFilterWindowButton.Name = "BShowFilterWindowButton";
			this.BShowFilterWindowButton.Size = new System.Drawing.Size(1, 20);
			this.BShowFilterWindowButton.TabIndex = 1;
			this.BShowFilterWindowButton.Text = "Show function filter editor";
			this.BShowFilterWindowButton.UseVisualStyleBackColor = true;
			this.BShowFilterWindowButton.Click += new System.EventHandler(this.BShowFilterWindowButton_Click);
			// 
			// BHistogramLayoutPanel
			// 
			this.BHistogramLayoutPanel.ColumnCount = 1;
			this.BHistogramLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.BHistogramLayoutPanel.Controls.Add(this.BHistogramVisualizingLayoutPanel, 0, 0);
			this.BHistogramLayoutPanel.Controls.Add(this.EqualizeHistogramBlue, 0, 1);
			this.BHistogramLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BHistogramLayoutPanel.Location = new System.Drawing.Point(0, 0);
			this.BHistogramLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.BHistogramLayoutPanel.Name = "BHistogramLayoutPanel";
			this.BHistogramLayoutPanel.RowCount = 2;
			this.BHistogramLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.BHistogramLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.BHistogramLayoutPanel.Size = new System.Drawing.Size(1, 165);
			this.BHistogramLayoutPanel.TabIndex = 2;
			// 
			// BHistogramVisualizingLayoutPanel
			// 
			this.BHistogramVisualizingLayoutPanel.ColumnCount = 2;
			this.BHistogramVisualizingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.BHistogramVisualizingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.BHistogramVisualizingLayoutPanel.Controls.Add(this.BHistogramPictureBox, 1, 0);
			this.BHistogramVisualizingLayoutPanel.Controls.Add(this.BHistogramXAxisPB, 1, 1);
			this.BHistogramVisualizingLayoutPanel.Controls.Add(this.BHistogramYAxisPB, 0, 0);
			this.BHistogramVisualizingLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BHistogramVisualizingLayoutPanel.Location = new System.Drawing.Point(0, 0);
			this.BHistogramVisualizingLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.BHistogramVisualizingLayoutPanel.Name = "BHistogramVisualizingLayoutPanel";
			this.BHistogramVisualizingLayoutPanel.RowCount = 2;
			this.BHistogramVisualizingLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.BHistogramVisualizingLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.BHistogramVisualizingLayoutPanel.Size = new System.Drawing.Size(1, 145);
			this.BHistogramVisualizingLayoutPanel.TabIndex = 1;
			// 
			// BHistogramPictureBox
			// 
			this.BHistogramPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BHistogramPictureBox.Location = new System.Drawing.Point(20, 0);
			this.BHistogramPictureBox.Margin = new System.Windows.Forms.Padding(0);
			this.BHistogramPictureBox.MaximumSize = new System.Drawing.Size(164, 126);
			this.BHistogramPictureBox.MinimumSize = new System.Drawing.Size(164, 126);
			this.BHistogramPictureBox.Name = "BHistogramPictureBox";
			this.BHistogramPictureBox.Size = new System.Drawing.Size(164, 126);
			this.BHistogramPictureBox.TabIndex = 0;
			this.BHistogramPictureBox.TabStop = false;
			// 
			// BHistogramXAxisPB
			// 
			this.BHistogramXAxisPB.BackgroundImage = global::GK1.Properties.Resources.Axis;
			this.BHistogramXAxisPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BHistogramXAxisPB.Location = new System.Drawing.Point(20, 128);
			this.BHistogramXAxisPB.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
			this.BHistogramXAxisPB.MaximumSize = new System.Drawing.Size(164, 17);
			this.BHistogramXAxisPB.MinimumSize = new System.Drawing.Size(164, 17);
			this.BHistogramXAxisPB.Name = "BHistogramXAxisPB";
			this.BHistogramXAxisPB.Size = new System.Drawing.Size(164, 17);
			this.BHistogramXAxisPB.TabIndex = 1;
			this.BHistogramXAxisPB.TabStop = false;
			// 
			// BHistogramYAxisPB
			// 
			this.BHistogramYAxisPB.BackgroundImage = global::GK1.Properties.Resources.AxisMax;
			this.BHistogramYAxisPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BHistogramYAxisPB.Location = new System.Drawing.Point(0, 0);
			this.BHistogramYAxisPB.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
			this.BHistogramYAxisPB.MaximumSize = new System.Drawing.Size(17, 126);
			this.BHistogramYAxisPB.MinimumSize = new System.Drawing.Size(17, 126);
			this.BHistogramYAxisPB.Name = "BHistogramYAxisPB";
			this.BHistogramYAxisPB.Size = new System.Drawing.Size(17, 126);
			this.BHistogramYAxisPB.TabIndex = 2;
			this.BHistogramYAxisPB.TabStop = false;
			// 
			// EqualizeHistogramBlue
			// 
			this.EqualizeHistogramBlue.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.EqualizeHistogramBlue.AutoSize = true;
			this.EqualizeHistogramBlue.Location = new System.Drawing.Point(0, 146);
			this.EqualizeHistogramBlue.Margin = new System.Windows.Forms.Padding(0);
			this.EqualizeHistogramBlue.Name = "EqualizeHistogramBlue";
			this.EqualizeHistogramBlue.Size = new System.Drawing.Size(1, 17);
			this.EqualizeHistogramBlue.TabIndex = 2;
			this.EqualizeHistogramBlue.Text = "Equalize Histogram";
			this.EqualizeHistogramBlue.UseVisualStyleBackColor = true;
			// 
			// BFunctonFilterLayoutPanel
			// 
			this.BFunctonFilterLayoutPanel.ColumnCount = 1;
			this.BFunctonFilterLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 199F));
			this.BFunctonFilterLayoutPanel.Controls.Add(this.BFinctionFilterVisualizingLayoutPanel, 0, 0);
			this.BFunctonFilterLayoutPanel.Controls.Add(this.BApplyFilterCheckBox, 0, 1);
			this.BFunctonFilterLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BFunctonFilterLayoutPanel.Location = new System.Drawing.Point(0, 165);
			this.BFunctonFilterLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.BFunctonFilterLayoutPanel.Name = "BFunctonFilterLayoutPanel";
			this.BFunctonFilterLayoutPanel.RowCount = 2;
			this.BFunctonFilterLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.BFunctonFilterLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.BFunctonFilterLayoutPanel.Size = new System.Drawing.Size(1, 1);
			this.BFunctonFilterLayoutPanel.TabIndex = 3;
			// 
			// BFinctionFilterVisualizingLayoutPanel
			// 
			this.BFinctionFilterVisualizingLayoutPanel.ColumnCount = 2;
			this.BFinctionFilterVisualizingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.BFinctionFilterVisualizingLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.BFinctionFilterVisualizingLayoutPanel.Controls.Add(this.BFilterVisualizingMainPB, 1, 0);
			this.BFinctionFilterVisualizingLayoutPanel.Controls.Add(this.BYAxisForFilterVisPB, 0, 0);
			this.BFinctionFilterVisualizingLayoutPanel.Controls.Add(this.BXAxisForFilterVisPB, 1, 1);
			this.BFinctionFilterVisualizingLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BFinctionFilterVisualizingLayoutPanel.Location = new System.Drawing.Point(0, 0);
			this.BFinctionFilterVisualizingLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.BFinctionFilterVisualizingLayoutPanel.Name = "BFinctionFilterVisualizingLayoutPanel";
			this.BFinctionFilterVisualizingLayoutPanel.RowCount = 2;
			this.BFinctionFilterVisualizingLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.BFinctionFilterVisualizingLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.BFinctionFilterVisualizingLayoutPanel.Size = new System.Drawing.Size(199, 1);
			this.BFinctionFilterVisualizingLayoutPanel.TabIndex = 0;
			// 
			// BFilterVisualizingMainPB
			// 
			this.BFilterVisualizingMainPB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.BFilterVisualizingMainPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BFilterVisualizingMainPB.Location = new System.Drawing.Point(20, 0);
			this.BFilterVisualizingMainPB.Margin = new System.Windows.Forms.Padding(0);
			this.BFilterVisualizingMainPB.MaximumSize = new System.Drawing.Size(164, 126);
			this.BFilterVisualizingMainPB.MinimumSize = new System.Drawing.Size(164, 126);
			this.BFilterVisualizingMainPB.Name = "BFilterVisualizingMainPB";
			this.BFilterVisualizingMainPB.Size = new System.Drawing.Size(164, 126);
			this.BFilterVisualizingMainPB.TabIndex = 0;
			this.BFilterVisualizingMainPB.TabStop = false;
			this.BFilterVisualizingMainPB.MouseClick += new System.Windows.Forms.MouseEventHandler(this.BFilterVisualizingMainPB_MouseClick);
			this.BFilterVisualizingMainPB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BFilterVisualizingMainPB_MouseDown);
			this.BFilterVisualizingMainPB.MouseMove += new System.Windows.Forms.MouseEventHandler(this.BFilterVisualizingMainPB_MouseMove);
			this.BFilterVisualizingMainPB.MouseUp += new System.Windows.Forms.MouseEventHandler(this.BFilterVisualizingMainPB_MouseUp);
			// 
			// BYAxisForFilterVisPB
			// 
			this.BYAxisForFilterVisPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BYAxisForFilterVisPB.Location = new System.Drawing.Point(0, 0);
			this.BYAxisForFilterVisPB.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
			this.BYAxisForFilterVisPB.MaximumSize = new System.Drawing.Size(17, 126);
			this.BYAxisForFilterVisPB.MinimumSize = new System.Drawing.Size(17, 126);
			this.BYAxisForFilterVisPB.Name = "BYAxisForFilterVisPB";
			this.BYAxisForFilterVisPB.Size = new System.Drawing.Size(17, 126);
			this.BYAxisForFilterVisPB.TabIndex = 1;
			this.BYAxisForFilterVisPB.TabStop = false;
			// 
			// BXAxisForFilterVisPB
			// 
			this.BXAxisForFilterVisPB.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BXAxisForFilterVisPB.Location = new System.Drawing.Point(20, -16);
			this.BXAxisForFilterVisPB.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
			this.BXAxisForFilterVisPB.MaximumSize = new System.Drawing.Size(164, 17);
			this.BXAxisForFilterVisPB.MinimumSize = new System.Drawing.Size(164, 17);
			this.BXAxisForFilterVisPB.Name = "BXAxisForFilterVisPB";
			this.BXAxisForFilterVisPB.Size = new System.Drawing.Size(164, 17);
			this.BXAxisForFilterVisPB.TabIndex = 2;
			this.BXAxisForFilterVisPB.TabStop = false;
			// 
			// BApplyFilterCheckBox
			// 
			this.BApplyFilterCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.BApplyFilterCheckBox.AutoSize = true;
			this.BApplyFilterCheckBox.Location = new System.Drawing.Point(73, -17);
			this.BApplyFilterCheckBox.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
			this.BApplyFilterCheckBox.Name = "BApplyFilterCheckBox";
			this.BApplyFilterCheckBox.Size = new System.Drawing.Size(52, 17);
			this.BApplyFilterCheckBox.TabIndex = 1;
			this.BApplyFilterCheckBox.Text = "Apply";
			this.BApplyFilterCheckBox.UseVisualStyleBackColor = true;
			this.BApplyFilterCheckBox.CheckedChanged += new System.EventHandler(this.BApplyFilterCheckBox_CheckedChanged);
			// 
			// GammaTab
			// 
			this.GammaTab.Controls.Add(this.GammaEnableCheckBox);
			this.GammaTab.Controls.Add(this.GammaTextBox);
			this.GammaTab.Location = new System.Drawing.Point(4, 22);
			this.GammaTab.Name = "GammaTab";
			this.GammaTab.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
			this.GammaTab.Size = new System.Drawing.Size(0, 191);
			this.GammaTab.TabIndex = 3;
			this.GammaTab.Text = "Gamma";
			this.GammaTab.UseVisualStyleBackColor = true;
			// 
			// GammaEnableCheckBox
			// 
			this.GammaEnableCheckBox.AutoSize = true;
			this.GammaEnableCheckBox.Location = new System.Drawing.Point(74, 66);
			this.GammaEnableCheckBox.Name = "GammaEnableCheckBox";
			this.GammaEnableCheckBox.Size = new System.Drawing.Size(52, 17);
			this.GammaEnableCheckBox.TabIndex = 1;
			this.GammaEnableCheckBox.Text = "Apply";
			this.GammaEnableCheckBox.UseVisualStyleBackColor = true;
			this.GammaEnableCheckBox.CheckedChanged += new System.EventHandler(this.GammaEnableCheckBox_CheckedChanged);
			// 
			// GammaTextBox
			// 
			this.GammaTextBox.Location = new System.Drawing.Point(74, 40);
			this.GammaTextBox.Name = "GammaTextBox";
			this.GammaTextBox.Size = new System.Drawing.Size(52, 20);
			this.GammaTextBox.TabIndex = 0;
			// 
			// Main3DLayoutPanel
			// 
			this.Main3DLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.Main3DLayoutPanel.ColumnCount = 1;
			this.Main3DLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Main3DLayoutPanel.Controls.Add(this.Import3DModelButton, 0, 0);
			this.Main3DLayoutPanel.Controls.Add(this.BackTo2DButton, 0, 3);
			this.Main3DLayoutPanel.Controls.Add(this.Viewing3DLayoutPanel, 0, 1);
			this.Main3DLayoutPanel.Controls.Add(this.Effects3DLayoutPanel, 0, 2);
			this.Main3DLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Main3DLayoutPanel.Location = new System.Drawing.Point(248, 0);
			this.Main3DLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.Main3DLayoutPanel.Name = "Main3DLayoutPanel";
			this.Main3DLayoutPanel.RowCount = 4;
			this.Main3DLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.Main3DLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55F));
			this.Main3DLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45F));
			this.Main3DLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
			this.Main3DLayoutPanel.Size = new System.Drawing.Size(1, 403);
			this.Main3DLayoutPanel.TabIndex = 3;
			// 
			// Import3DModelButton
			// 
			this.Import3DModelButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Import3DModelButton.Location = new System.Drawing.Point(4, 4);
			this.Import3DModelButton.Name = "Import3DModelButton";
			this.Import3DModelButton.Size = new System.Drawing.Size(1, 24);
			this.Import3DModelButton.TabIndex = 0;
			this.Import3DModelButton.Text = "Import Model";
			this.Import3DModelButton.UseVisualStyleBackColor = true;
			this.Import3DModelButton.Click += new System.EventHandler(this.Import3DModelButton_Click);
			// 
			// BackTo2DButton
			// 
			this.BackTo2DButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BackTo2DButton.Location = new System.Drawing.Point(4, 374);
			this.BackTo2DButton.Name = "BackTo2DButton";
			this.BackTo2DButton.Size = new System.Drawing.Size(1, 25);
			this.BackTo2DButton.TabIndex = 1;
			this.BackTo2DButton.Text = "Back to 2D";
			this.BackTo2DButton.UseVisualStyleBackColor = true;
			this.BackTo2DButton.Click += new System.EventHandler(this.BackTo2DButton_Click);
			// 
			// Viewing3DLayoutPanel
			// 
			this.Viewing3DLayoutPanel.ColumnCount = 1;
			this.Viewing3DLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Viewing3DLayoutPanel.Controls.Add(this.ViewOptionsLabel, 0, 0);
			this.Viewing3DLayoutPanel.Controls.Add(this.Viewing3DTabControl, 0, 1);
			this.Viewing3DLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Viewing3DLayoutPanel.Location = new System.Drawing.Point(4, 35);
			this.Viewing3DLayoutPanel.Name = "Viewing3DLayoutPanel";
			this.Viewing3DLayoutPanel.RowCount = 2;
			this.Viewing3DLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
			this.Viewing3DLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Viewing3DLayoutPanel.Size = new System.Drawing.Size(1, 179);
			this.Viewing3DLayoutPanel.TabIndex = 2;
			// 
			// ViewOptionsLabel
			// 
			this.ViewOptionsLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.ViewOptionsLabel.AutoSize = true;
			this.ViewOptionsLabel.Location = new System.Drawing.Point(3, 2);
			this.ViewOptionsLabel.Name = "ViewOptionsLabel";
			this.ViewOptionsLabel.Size = new System.Drawing.Size(1, 13);
			this.ViewOptionsLabel.TabIndex = 0;
			this.ViewOptionsLabel.Text = "View Options";
			// 
			// Viewing3DTabControl
			// 
			this.Viewing3DTabControl.Controls.Add(this.StandardViewTab);
			this.Viewing3DTabControl.Controls.Add(this.GridViewTab);
			this.Viewing3DTabControl.Controls.Add(this.RandomColorsTab);
			this.Viewing3DTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Viewing3DTabControl.Location = new System.Drawing.Point(0, 18);
			this.Viewing3DTabControl.Margin = new System.Windows.Forms.Padding(0);
			this.Viewing3DTabControl.Name = "Viewing3DTabControl";
			this.Viewing3DTabControl.SelectedIndex = 0;
			this.Viewing3DTabControl.Size = new System.Drawing.Size(1, 161);
			this.Viewing3DTabControl.TabIndex = 1;
			this.Viewing3DTabControl.SelectedIndexChanged += new System.EventHandler(this.Viewing3DTabControl_SelectedIndexChanged);
			// 
			// StandardViewTab
			// 
			this.StandardViewTab.Controls.Add(this.StandardViewSplittingPanel);
			this.StandardViewTab.Location = new System.Drawing.Point(4, 22);
			this.StandardViewTab.Margin = new System.Windows.Forms.Padding(0);
			this.StandardViewTab.Name = "StandardViewTab";
			this.StandardViewTab.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
			this.StandardViewTab.Size = new System.Drawing.Size(0, 135);
			this.StandardViewTab.TabIndex = 0;
			this.StandardViewTab.Text = "Standard";
			this.StandardViewTab.UseVisualStyleBackColor = true;
			// 
			// StandardViewSplittingPanel
			// 
			this.StandardViewSplittingPanel.ColumnCount = 2;
			this.StandardViewSplittingPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.StandardViewSplittingPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 0F));
			this.StandardViewSplittingPanel.Controls.Add(this.StandardViewMainLP, 0, 0);
			this.StandardViewSplittingPanel.Controls.Add(this.StandardPhonglayoutPanel, 1, 0);
			this.StandardViewSplittingPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StandardViewSplittingPanel.Location = new System.Drawing.Point(3, 3);
			this.StandardViewSplittingPanel.Margin = new System.Windows.Forms.Padding(0);
			this.StandardViewSplittingPanel.Name = "StandardViewSplittingPanel";
			this.StandardViewSplittingPanel.RowCount = 1;
			this.StandardViewSplittingPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 146F));
			this.StandardViewSplittingPanel.Size = new System.Drawing.Size(0, 129);
			this.StandardViewSplittingPanel.TabIndex = 0;
			// 
			// StandardViewMainLP
			// 
			this.StandardViewMainLP.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.StandardViewMainLP.ColumnCount = 2;
			this.StandardViewMainLP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64F));
			this.StandardViewMainLP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36F));
			this.StandardViewMainLP.Controls.Add(this.LIFlowLayoutPanel, 1, 2);
			this.StandardViewMainLP.Controls.Add(this.FileLabel, 0, 0);
			this.StandardViewMainLP.Controls.Add(this.FileNameLabel, 1, 0);
			this.StandardViewMainLP.Controls.Add(this.ScaleLabel, 0, 1);
			this.StandardViewMainLP.Controls.Add(this.ScaleFlowLayoutPanel, 1, 1);
			this.StandardViewMainLP.Controls.Add(this.LightIntensityLabel, 0, 2);
			this.StandardViewMainLP.Controls.Add(this.StandardPhongCheckBox, 0, 3);
			this.StandardViewMainLP.Controls.Add(this.EditStandardPhongButton, 1, 3);
			this.StandardViewMainLP.Controls.Add(this.StandardFPSlabel, 0, 5);
			this.StandardViewMainLP.Controls.Add(this.StandardFPScountLabel, 1, 5);
			this.StandardViewMainLP.Controls.Add(this.StandardColorLabel, 0, 4);
			this.StandardViewMainLP.Controls.Add(this.StandardColorButton, 1, 4);
			this.StandardViewMainLP.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StandardViewMainLP.Location = new System.Drawing.Point(0, 0);
			this.StandardViewMainLP.Margin = new System.Windows.Forms.Padding(0);
			this.StandardViewMainLP.Name = "StandardViewMainLP";
			this.StandardViewMainLP.RowCount = 6;
			this.StandardViewMainLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.00039F));
			this.StandardViewMainLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0004F));
			this.StandardViewMainLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.00041F));
			this.StandardViewMainLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.00041F));
			this.StandardViewMainLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.9984F));
			this.StandardViewMainLP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
			this.StandardViewMainLP.Size = new System.Drawing.Size(1, 146);
			this.StandardViewMainLP.TabIndex = 0;
			// 
			// LIFlowLayoutPanel
			// 
			this.LIFlowLayoutPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.LIFlowLayoutPanel.Controls.Add(this.LITextBox);
			this.LIFlowLayoutPanel.Controls.Add(this.PercentLabel2);
			this.LIFlowLayoutPanel.Location = new System.Drawing.Point(2, 52);
			this.LIFlowLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.LIFlowLayoutPanel.MaximumSize = new System.Drawing.Size(55, 22);
			this.LIFlowLayoutPanel.MinimumSize = new System.Drawing.Size(55, 22);
			this.LIFlowLayoutPanel.Name = "LIFlowLayoutPanel";
			this.LIFlowLayoutPanel.Size = new System.Drawing.Size(55, 22);
			this.LIFlowLayoutPanel.TabIndex = 5;
			// 
			// LITextBox
			// 
			this.LITextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.LITextBox.Location = new System.Drawing.Point(0, 1);
			this.LITextBox.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
			this.LITextBox.Name = "LITextBox";
			this.LITextBox.Size = new System.Drawing.Size(32, 20);
			this.LITextBox.TabIndex = 0;
			this.LITextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.LITextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LITextBox_KeyDown);
			// 
			// PercentLabel2
			// 
			this.PercentLabel2.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.PercentLabel2.AutoSize = true;
			this.PercentLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.PercentLabel2.Location = new System.Drawing.Point(32, 2);
			this.PercentLabel2.Margin = new System.Windows.Forms.Padding(0);
			this.PercentLabel2.Name = "PercentLabel2";
			this.PercentLabel2.Size = new System.Drawing.Size(20, 17);
			this.PercentLabel2.TabIndex = 1;
			this.PercentLabel2.Text = "%";
			// 
			// FileLabel
			// 
			this.FileLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.FileLabel.AutoSize = true;
			this.FileLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.FileLabel.Location = new System.Drawing.Point(4, 4);
			this.FileLabel.Name = "FileLabel";
			this.FileLabel.Size = new System.Drawing.Size(1, 17);
			this.FileLabel.TabIndex = 0;
			this.FileLabel.Text = "File";
			// 
			// FileNameLabel
			// 
			this.FileNameLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.FileNameLabel.AutoSize = true;
			this.FileNameLabel.Location = new System.Drawing.Point(2, 6);
			this.FileNameLabel.Margin = new System.Windows.Forms.Padding(0);
			this.FileNameLabel.Name = "FileNameLabel";
			this.FileNameLabel.Size = new System.Drawing.Size(0, 13);
			this.FileNameLabel.TabIndex = 1;
			// 
			// ScaleLabel
			// 
			this.ScaleLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.ScaleLabel.AutoSize = true;
			this.ScaleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.ScaleLabel.Location = new System.Drawing.Point(4, 29);
			this.ScaleLabel.Name = "ScaleLabel";
			this.ScaleLabel.Size = new System.Drawing.Size(1, 17);
			this.ScaleLabel.TabIndex = 2;
			this.ScaleLabel.Text = "Scale";
			// 
			// ScaleFlowLayoutPanel
			// 
			this.ScaleFlowLayoutPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.ScaleFlowLayoutPanel.Controls.Add(this.ScaleTextBox);
			this.ScaleFlowLayoutPanel.Controls.Add(this.PercentLabel1);
			this.ScaleFlowLayoutPanel.Location = new System.Drawing.Point(2, 27);
			this.ScaleFlowLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.ScaleFlowLayoutPanel.MaximumSize = new System.Drawing.Size(55, 22);
			this.ScaleFlowLayoutPanel.MinimumSize = new System.Drawing.Size(55, 22);
			this.ScaleFlowLayoutPanel.Name = "ScaleFlowLayoutPanel";
			this.ScaleFlowLayoutPanel.Size = new System.Drawing.Size(55, 22);
			this.ScaleFlowLayoutPanel.TabIndex = 3;
			// 
			// ScaleTextBox
			// 
			this.ScaleTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.ScaleTextBox.Location = new System.Drawing.Point(0, 1);
			this.ScaleTextBox.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
			this.ScaleTextBox.Name = "ScaleTextBox";
			this.ScaleTextBox.Size = new System.Drawing.Size(32, 20);
			this.ScaleTextBox.TabIndex = 0;
			this.ScaleTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.ScaleTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ScaleTextBox_KeyDown);
			// 
			// PercentLabel1
			// 
			this.PercentLabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.PercentLabel1.AutoSize = true;
			this.PercentLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.PercentLabel1.Location = new System.Drawing.Point(32, 2);
			this.PercentLabel1.Margin = new System.Windows.Forms.Padding(0);
			this.PercentLabel1.Name = "PercentLabel1";
			this.PercentLabel1.Size = new System.Drawing.Size(20, 17);
			this.PercentLabel1.TabIndex = 1;
			this.PercentLabel1.Text = "%";
			// 
			// LightIntensityLabel
			// 
			this.LightIntensityLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.LightIntensityLabel.AutoSize = true;
			this.LightIntensityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.LightIntensityLabel.Location = new System.Drawing.Point(4, 54);
			this.LightIntensityLabel.Name = "LightIntensityLabel";
			this.LightIntensityLabel.Size = new System.Drawing.Size(1, 17);
			this.LightIntensityLabel.TabIndex = 4;
			this.LightIntensityLabel.Text = "Light intensity";
			// 
			// StandardPhongCheckBox
			// 
			this.StandardPhongCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.StandardPhongCheckBox.AutoSize = true;
			this.StandardPhongCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.StandardPhongCheckBox.Location = new System.Drawing.Point(1, 77);
			this.StandardPhongCheckBox.Margin = new System.Windows.Forms.Padding(0);
			this.StandardPhongCheckBox.Name = "StandardPhongCheckBox";
			this.StandardPhongCheckBox.Size = new System.Drawing.Size(1, 21);
			this.StandardPhongCheckBox.TabIndex = 6;
			this.StandardPhongCheckBox.Text = "Phong lighting";
			this.StandardPhongCheckBox.UseVisualStyleBackColor = true;
			this.StandardPhongCheckBox.CheckedChanged += new System.EventHandler(this.StandardPhongCheckBox_CheckedChanged);
			// 
			// EditStandardPhongButton
			// 
			this.EditStandardPhongButton.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.EditStandardPhongButton.Location = new System.Drawing.Point(2, 78);
			this.EditStandardPhongButton.Margin = new System.Windows.Forms.Padding(0);
			this.EditStandardPhongButton.MaximumSize = new System.Drawing.Size(38, 19);
			this.EditStandardPhongButton.MinimumSize = new System.Drawing.Size(38, 19);
			this.EditStandardPhongButton.Name = "EditStandardPhongButton";
			this.EditStandardPhongButton.Size = new System.Drawing.Size(38, 19);
			this.EditStandardPhongButton.TabIndex = 7;
			this.EditStandardPhongButton.Text = "Edit";
			this.EditStandardPhongButton.UseVisualStyleBackColor = true;
			this.EditStandardPhongButton.Click += new System.EventHandler(this.EditStandardPhongButton_Click);
			// 
			// StandardFPSlabel
			// 
			this.StandardFPSlabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.StandardFPSlabel.AutoSize = true;
			this.StandardFPSlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.StandardFPSlabel.Location = new System.Drawing.Point(1, 127);
			this.StandardFPSlabel.Margin = new System.Windows.Forms.Padding(0);
			this.StandardFPSlabel.Name = "StandardFPSlabel";
			this.StandardFPSlabel.Size = new System.Drawing.Size(1, 17);
			this.StandardFPSlabel.TabIndex = 8;
			this.StandardFPSlabel.Text = "FPS";
			// 
			// StandardFPScountLabel
			// 
			this.StandardFPScountLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.StandardFPScountLabel.AutoSize = true;
			this.StandardFPScountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.StandardFPScountLabel.Location = new System.Drawing.Point(5, 127);
			this.StandardFPScountLabel.Name = "StandardFPScountLabel";
			this.StandardFPScountLabel.Size = new System.Drawing.Size(0, 17);
			this.StandardFPScountLabel.TabIndex = 9;
			// 
			// StandardColorLabel
			// 
			this.StandardColorLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.StandardColorLabel.AutoSize = true;
			this.StandardColorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.StandardColorLabel.Location = new System.Drawing.Point(4, 104);
			this.StandardColorLabel.Name = "StandardColorLabel";
			this.StandardColorLabel.Size = new System.Drawing.Size(1, 17);
			this.StandardColorLabel.TabIndex = 10;
			this.StandardColorLabel.Text = "Color";
			// 
			// StandardColorButton
			// 
			this.StandardColorButton.BackColor = System.Drawing.Color.White;
			this.StandardColorButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StandardColorButton.Location = new System.Drawing.Point(3, 102);
			this.StandardColorButton.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.StandardColorButton.Name = "StandardColorButton";
			this.StandardColorButton.Size = new System.Drawing.Size(1, 22);
			this.StandardColorButton.TabIndex = 11;
			this.StandardColorButton.UseVisualStyleBackColor = false;
			this.StandardColorButton.Click += new System.EventHandler(this.StandardColorButton_Click);
			// 
			// StandardPhonglayoutPanel
			// 
			this.StandardPhonglayoutPanel.ColumnCount = 1;
			this.StandardPhonglayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.StandardPhonglayoutPanel.Controls.Add(this.StandardApplyPhongLayoutPanel, 0, 2);
			this.StandardPhonglayoutPanel.Controls.Add(this.StandardPhongEditionLayoutPanel, 0, 1);
			this.StandardPhonglayoutPanel.Controls.Add(this.StandardPhongPictureBox, 0, 0);
			this.StandardPhonglayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StandardPhonglayoutPanel.Location = new System.Drawing.Point(1, 0);
			this.StandardPhonglayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.StandardPhonglayoutPanel.Name = "StandardPhonglayoutPanel";
			this.StandardPhonglayoutPanel.RowCount = 3;
			this.StandardPhonglayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 76.71233F));
			this.StandardPhonglayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.28767F));
			this.StandardPhonglayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.StandardPhonglayoutPanel.Size = new System.Drawing.Size(1, 146);
			this.StandardPhonglayoutPanel.TabIndex = 1;
			// 
			// StandardApplyPhongLayoutPanel
			// 
			this.StandardApplyPhongLayoutPanel.ColumnCount = 3;
			this.StandardApplyPhongLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.StandardApplyPhongLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
			this.StandardApplyPhongLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 89F));
			this.StandardApplyPhongLayoutPanel.Controls.Add(this.StandardSpecularExponentLabel, 0, 0);
			this.StandardApplyPhongLayoutPanel.Controls.Add(this.StandardSpecularExponentTextBox, 1, 0);
			this.StandardApplyPhongLayoutPanel.Controls.Add(this.StandardPhongApplyButton, 2, 0);
			this.StandardApplyPhongLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StandardApplyPhongLayoutPanel.Location = new System.Drawing.Point(0, 125);
			this.StandardApplyPhongLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.StandardApplyPhongLayoutPanel.Name = "StandardApplyPhongLayoutPanel";
			this.StandardApplyPhongLayoutPanel.RowCount = 1;
			this.StandardApplyPhongLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.StandardApplyPhongLayoutPanel.Size = new System.Drawing.Size(1, 21);
			this.StandardApplyPhongLayoutPanel.TabIndex = 3;
			// 
			// StandardSpecularExponentLabel
			// 
			this.StandardSpecularExponentLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.StandardSpecularExponentLabel.AutoSize = true;
			this.StandardSpecularExponentLabel.Location = new System.Drawing.Point(0, 4);
			this.StandardSpecularExponentLabel.Margin = new System.Windows.Forms.Padding(0);
			this.StandardSpecularExponentLabel.Name = "StandardSpecularExponentLabel";
			this.StandardSpecularExponentLabel.Size = new System.Drawing.Size(1, 13);
			this.StandardSpecularExponentLabel.TabIndex = 0;
			this.StandardSpecularExponentLabel.Text = "Specular Exponent";
			// 
			// StandardSpecularExponentTextBox
			// 
			this.StandardSpecularExponentTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StandardSpecularExponentTextBox.Location = new System.Drawing.Point(-122, 1);
			this.StandardSpecularExponentTextBox.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.StandardSpecularExponentTextBox.Name = "StandardSpecularExponentTextBox";
			this.StandardSpecularExponentTextBox.Size = new System.Drawing.Size(33, 20);
			this.StandardSpecularExponentTextBox.TabIndex = 1;
			this.StandardSpecularExponentTextBox.Text = "1";
			this.StandardSpecularExponentTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// StandardPhongApplyButton
			// 
			this.StandardPhongApplyButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StandardPhongApplyButton.Location = new System.Drawing.Point(-88, 0);
			this.StandardPhongApplyButton.Margin = new System.Windows.Forms.Padding(0);
			this.StandardPhongApplyButton.Name = "StandardPhongApplyButton";
			this.StandardPhongApplyButton.Size = new System.Drawing.Size(89, 21);
			this.StandardPhongApplyButton.TabIndex = 2;
			this.StandardPhongApplyButton.Text = "Apply";
			this.StandardPhongApplyButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.StandardPhongApplyButton.UseVisualStyleBackColor = true;
			this.StandardPhongApplyButton.Click += new System.EventHandler(this.StandardPhongApplyButton_Click);
			// 
			// StandardPhongEditionLayoutPanel
			// 
			this.StandardPhongEditionLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.StandardPhongEditionLayoutPanel.ColumnCount = 9;
			this.StandardPhongEditionLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 16F));
			this.StandardPhongEditionLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.66627F));
			this.StandardPhongEditionLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.66707F));
			this.StandardPhongEditionLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 16F));
			this.StandardPhongEditionLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.66627F));
			this.StandardPhongEditionLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.66707F));
			this.StandardPhongEditionLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 16F));
			this.StandardPhongEditionLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.66627F));
			this.StandardPhongEditionLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.66707F));
			this.StandardPhongEditionLayoutPanel.Controls.Add(this.StandardDiffuseLabel, 0, 0);
			this.StandardPhongEditionLayoutPanel.Controls.Add(this.StandardDiffuseTextBox, 1, 0);
			this.StandardPhongEditionLayoutPanel.Controls.Add(this.StandardAmbienttextBox, 7, 0);
			this.StandardPhongEditionLayoutPanel.Controls.Add(this.StandardAmbientLabel, 6, 0);
			this.StandardPhongEditionLayoutPanel.Controls.Add(this.StandardSpecularTextBox, 4, 0);
			this.StandardPhongEditionLayoutPanel.Controls.Add(this.StandardSpecularLabel, 3, 0);
			this.StandardPhongEditionLayoutPanel.Controls.Add(this.StandardDiffuseColorButton, 2, 0);
			this.StandardPhongEditionLayoutPanel.Controls.Add(this.StandardSpecularColorButton, 5, 0);
			this.StandardPhongEditionLayoutPanel.Controls.Add(this.StandardAmbientColorButton, 8, 0);
			this.StandardPhongEditionLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StandardPhongEditionLayoutPanel.Location = new System.Drawing.Point(0, 96);
			this.StandardPhongEditionLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.StandardPhongEditionLayoutPanel.Name = "StandardPhongEditionLayoutPanel";
			this.StandardPhongEditionLayoutPanel.RowCount = 1;
			this.StandardPhongEditionLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.StandardPhongEditionLayoutPanel.Size = new System.Drawing.Size(1, 29);
			this.StandardPhongEditionLayoutPanel.TabIndex = 1;
			// 
			// StandardDiffuseLabel
			// 
			this.StandardDiffuseLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.StandardDiffuseLabel.AutoSize = true;
			this.StandardDiffuseLabel.Location = new System.Drawing.Point(1, 8);
			this.StandardDiffuseLabel.Margin = new System.Windows.Forms.Padding(0);
			this.StandardDiffuseLabel.Name = "StandardDiffuseLabel";
			this.StandardDiffuseLabel.Size = new System.Drawing.Size(15, 13);
			this.StandardDiffuseLabel.TabIndex = 0;
			this.StandardDiffuseLabel.Text = "D";
			// 
			// StandardDiffuseTextBox
			// 
			this.StandardDiffuseTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.StandardDiffuseTextBox.Location = new System.Drawing.Point(18, 4);
			this.StandardDiffuseTextBox.Margin = new System.Windows.Forms.Padding(0);
			this.StandardDiffuseTextBox.MaximumSize = new System.Drawing.Size(26, 20);
			this.StandardDiffuseTextBox.MinimumSize = new System.Drawing.Size(26, 20);
			this.StandardDiffuseTextBox.Name = "StandardDiffuseTextBox";
			this.StandardDiffuseTextBox.Size = new System.Drawing.Size(26, 20);
			this.StandardDiffuseTextBox.TabIndex = 1;
			this.StandardDiffuseTextBox.Text = "0.6";
			// 
			// StandardAmbienttextBox
			// 
			this.StandardAmbienttextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.StandardAmbienttextBox.Location = new System.Drawing.Point(20, 4);
			this.StandardAmbienttextBox.Margin = new System.Windows.Forms.Padding(0);
			this.StandardAmbienttextBox.MaximumSize = new System.Drawing.Size(26, 20);
			this.StandardAmbienttextBox.MinimumSize = new System.Drawing.Size(26, 20);
			this.StandardAmbienttextBox.Name = "StandardAmbienttextBox";
			this.StandardAmbienttextBox.Size = new System.Drawing.Size(26, 20);
			this.StandardAmbienttextBox.TabIndex = 6;
			this.StandardAmbienttextBox.Text = "0.7";
			// 
			// StandardAmbientLabel
			// 
			this.StandardAmbientLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.StandardAmbientLabel.AutoSize = true;
			this.StandardAmbientLabel.Location = new System.Drawing.Point(6, 8);
			this.StandardAmbientLabel.Name = "StandardAmbientLabel";
			this.StandardAmbientLabel.Size = new System.Drawing.Size(10, 13);
			this.StandardAmbientLabel.TabIndex = 5;
			this.StandardAmbientLabel.Text = "A";
			// 
			// StandardSpecularTextBox
			// 
			this.StandardSpecularTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.StandardSpecularTextBox.Location = new System.Drawing.Point(19, 4);
			this.StandardSpecularTextBox.Margin = new System.Windows.Forms.Padding(0);
			this.StandardSpecularTextBox.MaximumSize = new System.Drawing.Size(26, 20);
			this.StandardSpecularTextBox.MinimumSize = new System.Drawing.Size(26, 20);
			this.StandardSpecularTextBox.Name = "StandardSpecularTextBox";
			this.StandardSpecularTextBox.Size = new System.Drawing.Size(26, 20);
			this.StandardSpecularTextBox.TabIndex = 4;
			this.StandardSpecularTextBox.Text = "0.5";
			// 
			// StandardSpecularLabel
			// 
			this.StandardSpecularLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.StandardSpecularLabel.AutoSize = true;
			this.StandardSpecularLabel.Location = new System.Drawing.Point(3, 8);
			this.StandardSpecularLabel.Margin = new System.Windows.Forms.Padding(0);
			this.StandardSpecularLabel.Name = "StandardSpecularLabel";
			this.StandardSpecularLabel.Size = new System.Drawing.Size(14, 13);
			this.StandardSpecularLabel.TabIndex = 3;
			this.StandardSpecularLabel.Text = "S";
			// 
			// StandardDiffuseColorButton
			// 
			this.StandardDiffuseColorButton.BackColor = System.Drawing.Color.White;
			this.StandardDiffuseColorButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StandardDiffuseColorButton.Location = new System.Drawing.Point(10, 2);
			this.StandardDiffuseColorButton.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.StandardDiffuseColorButton.Name = "StandardDiffuseColorButton";
			this.StandardDiffuseColorButton.Size = new System.Drawing.Size(1, 25);
			this.StandardDiffuseColorButton.TabIndex = 7;
			this.StandardDiffuseColorButton.UseVisualStyleBackColor = false;
			this.StandardDiffuseColorButton.Click += new System.EventHandler(this.StandardDiffuseColorButton_Click);
			// 
			// StandardSpecularColorButton
			// 
			this.StandardSpecularColorButton.BackColor = System.Drawing.Color.White;
			this.StandardSpecularColorButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StandardSpecularColorButton.Location = new System.Drawing.Point(11, 2);
			this.StandardSpecularColorButton.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.StandardSpecularColorButton.Name = "StandardSpecularColorButton";
			this.StandardSpecularColorButton.Size = new System.Drawing.Size(1, 25);
			this.StandardSpecularColorButton.TabIndex = 8;
			this.StandardSpecularColorButton.UseVisualStyleBackColor = false;
			this.StandardSpecularColorButton.Click += new System.EventHandler(this.StandardSpecularColorButton_Click);
			// 
			// StandardAmbientColorButton
			// 
			this.StandardAmbientColorButton.BackColor = System.Drawing.Color.White;
			this.StandardAmbientColorButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StandardAmbientColorButton.Location = new System.Drawing.Point(12, 2);
			this.StandardAmbientColorButton.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.StandardAmbientColorButton.Name = "StandardAmbientColorButton";
			this.StandardAmbientColorButton.Size = new System.Drawing.Size(1, 25);
			this.StandardAmbientColorButton.TabIndex = 9;
			this.StandardAmbientColorButton.UseVisualStyleBackColor = false;
			this.StandardAmbientColorButton.Click += new System.EventHandler(this.StandardAmbientColorButton_Click);
			// 
			// StandardPhongPictureBox
			// 
			this.StandardPhongPictureBox.BackColor = System.Drawing.Color.Black;
			this.StandardPhongPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StandardPhongPictureBox.Location = new System.Drawing.Point(1, 1);
			this.StandardPhongPictureBox.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.StandardPhongPictureBox.Name = "StandardPhongPictureBox";
			this.StandardPhongPictureBox.Size = new System.Drawing.Size(1, 94);
			this.StandardPhongPictureBox.TabIndex = 0;
			this.StandardPhongPictureBox.TabStop = false;
			this.StandardPhongPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.StandardPhongPictureBox_Paint);
			this.StandardPhongPictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.StandardPhongPictureBox_MouseClick);
			this.StandardPhongPictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.StandardPhongPictureBox_MouseDown);
			this.StandardPhongPictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.StandardPhongPictureBox_MouseMove);
			this.StandardPhongPictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.StandardPhongPictureBox_MouseUp);
			// 
			// GridViewTab
			// 
			this.GridViewTab.Controls.Add(this.GridLayoutPanel);
			this.GridViewTab.Location = new System.Drawing.Point(4, 22);
			this.GridViewTab.Name = "GridViewTab";
			this.GridViewTab.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
			this.GridViewTab.Size = new System.Drawing.Size(0, 136);
			this.GridViewTab.TabIndex = 1;
			this.GridViewTab.Text = "Grid";
			this.GridViewTab.UseVisualStyleBackColor = true;
			// 
			// GridLayoutPanel
			// 
			this.GridLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.GridLayoutPanel.ColumnCount = 2;
			this.GridLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64F));
			this.GridLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36F));
			this.GridLayoutPanel.Controls.Add(this.Grid3DColorButton, 1, 3);
			this.GridLayoutPanel.Controls.Add(this.flowLayoutPanel1, 1, 2);
			this.GridLayoutPanel.Controls.Add(this.GridFileLabel, 0, 0);
			this.GridLayoutPanel.Controls.Add(this.GridFileNameLabel, 1, 0);
			this.GridLayoutPanel.Controls.Add(this.GridScaleLabel, 0, 1);
			this.GridLayoutPanel.Controls.Add(this.flowLayoutPanel2, 1, 1);
			this.GridLayoutPanel.Controls.Add(this.GridLightIntensityLabel, 0, 2);
			this.GridLayoutPanel.Controls.Add(this.GridFPSLabel, 0, 4);
			this.GridLayoutPanel.Controls.Add(this.GridFPSCountLabel, 1, 4);
			this.GridLayoutPanel.Controls.Add(this.GridColorLabel, 0, 3);
			this.GridLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GridLayoutPanel.Location = new System.Drawing.Point(3, 3);
			this.GridLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.GridLayoutPanel.Name = "GridLayoutPanel";
			this.GridLayoutPanel.RowCount = 5;
			this.GridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.99998F));
			this.GridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.GridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00001F));
			this.GridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00001F));
			this.GridLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.GridLayoutPanel.Size = new System.Drawing.Size(0, 130);
			this.GridLayoutPanel.TabIndex = 1;
			// 
			// Grid3DColorButton
			// 
			this.Grid3DColorButton.BackColor = System.Drawing.Color.White;
			this.Grid3DColorButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Grid3DColorButton.Location = new System.Drawing.Point(3, 82);
			this.Grid3DColorButton.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.Grid3DColorButton.Name = "Grid3DColorButton";
			this.Grid3DColorButton.Size = new System.Drawing.Size(1, 24);
			this.Grid3DColorButton.TabIndex = 12;
			this.Grid3DColorButton.UseVisualStyleBackColor = false;
			this.Grid3DColorButton.Click += new System.EventHandler(this.Grid3DColorButton_Click);
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.flowLayoutPanel1.Controls.Add(this.GridLITextBox);
			this.flowLayoutPanel1.Controls.Add(this.GridPercent2);
			this.flowLayoutPanel1.Location = new System.Drawing.Point(2, 56);
			this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
			this.flowLayoutPanel1.MaximumSize = new System.Drawing.Size(55, 22);
			this.flowLayoutPanel1.MinimumSize = new System.Drawing.Size(55, 22);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(55, 22);
			this.flowLayoutPanel1.TabIndex = 5;
			// 
			// GridLITextBox
			// 
			this.GridLITextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.GridLITextBox.Location = new System.Drawing.Point(0, 1);
			this.GridLITextBox.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
			this.GridLITextBox.Name = "GridLITextBox";
			this.GridLITextBox.Size = new System.Drawing.Size(32, 20);
			this.GridLITextBox.TabIndex = 0;
			this.GridLITextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.GridLITextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridLITextBox_KeyDown);
			// 
			// GridPercent2
			// 
			this.GridPercent2.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.GridPercent2.AutoSize = true;
			this.GridPercent2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.GridPercent2.Location = new System.Drawing.Point(32, 2);
			this.GridPercent2.Margin = new System.Windows.Forms.Padding(0);
			this.GridPercent2.Name = "GridPercent2";
			this.GridPercent2.Size = new System.Drawing.Size(20, 17);
			this.GridPercent2.TabIndex = 1;
			this.GridPercent2.Text = "%";
			// 
			// GridFileLabel
			// 
			this.GridFileLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.GridFileLabel.AutoSize = true;
			this.GridFileLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.GridFileLabel.Location = new System.Drawing.Point(4, 5);
			this.GridFileLabel.Name = "GridFileLabel";
			this.GridFileLabel.Size = new System.Drawing.Size(1, 17);
			this.GridFileLabel.TabIndex = 0;
			this.GridFileLabel.Text = "File";
			// 
			// GridFileNameLabel
			// 
			this.GridFileNameLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.GridFileNameLabel.AutoSize = true;
			this.GridFileNameLabel.Location = new System.Drawing.Point(2, 7);
			this.GridFileNameLabel.Margin = new System.Windows.Forms.Padding(0);
			this.GridFileNameLabel.Name = "GridFileNameLabel";
			this.GridFileNameLabel.Size = new System.Drawing.Size(0, 13);
			this.GridFileNameLabel.TabIndex = 1;
			// 
			// GridScaleLabel
			// 
			this.GridScaleLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.GridScaleLabel.AutoSize = true;
			this.GridScaleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.GridScaleLabel.Location = new System.Drawing.Point(4, 31);
			this.GridScaleLabel.Name = "GridScaleLabel";
			this.GridScaleLabel.Size = new System.Drawing.Size(1, 17);
			this.GridScaleLabel.TabIndex = 2;
			this.GridScaleLabel.Text = "Scale";
			// 
			// flowLayoutPanel2
			// 
			this.flowLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.flowLayoutPanel2.Controls.Add(this.GridScaleTextBox);
			this.flowLayoutPanel2.Controls.Add(this.GridPercent1);
			this.flowLayoutPanel2.Location = new System.Drawing.Point(2, 29);
			this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
			this.flowLayoutPanel2.MaximumSize = new System.Drawing.Size(55, 22);
			this.flowLayoutPanel2.MinimumSize = new System.Drawing.Size(55, 22);
			this.flowLayoutPanel2.Name = "flowLayoutPanel2";
			this.flowLayoutPanel2.Size = new System.Drawing.Size(55, 22);
			this.flowLayoutPanel2.TabIndex = 3;
			// 
			// GridScaleTextBox
			// 
			this.GridScaleTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.GridScaleTextBox.Location = new System.Drawing.Point(0, 1);
			this.GridScaleTextBox.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
			this.GridScaleTextBox.Name = "GridScaleTextBox";
			this.GridScaleTextBox.Size = new System.Drawing.Size(32, 20);
			this.GridScaleTextBox.TabIndex = 0;
			this.GridScaleTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.GridScaleTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridScaleTextBox_KeyDown);
			// 
			// GridPercent1
			// 
			this.GridPercent1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.GridPercent1.AutoSize = true;
			this.GridPercent1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.GridPercent1.Location = new System.Drawing.Point(32, 2);
			this.GridPercent1.Margin = new System.Windows.Forms.Padding(0);
			this.GridPercent1.Name = "GridPercent1";
			this.GridPercent1.Size = new System.Drawing.Size(20, 17);
			this.GridPercent1.TabIndex = 1;
			this.GridPercent1.Text = "%";
			// 
			// GridLightIntensityLabel
			// 
			this.GridLightIntensityLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.GridLightIntensityLabel.AutoSize = true;
			this.GridLightIntensityLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.GridLightIntensityLabel.Location = new System.Drawing.Point(4, 58);
			this.GridLightIntensityLabel.Name = "GridLightIntensityLabel";
			this.GridLightIntensityLabel.Size = new System.Drawing.Size(1, 17);
			this.GridLightIntensityLabel.TabIndex = 4;
			this.GridLightIntensityLabel.Text = "Light intensity";
			// 
			// GridFPSLabel
			// 
			this.GridFPSLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.GridFPSLabel.AutoSize = true;
			this.GridFPSLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.GridFPSLabel.Location = new System.Drawing.Point(4, 108);
			this.GridFPSLabel.Name = "GridFPSLabel";
			this.GridFPSLabel.Size = new System.Drawing.Size(1, 20);
			this.GridFPSLabel.TabIndex = 8;
			this.GridFPSLabel.Text = "FPS";
			// 
			// GridFPSCountLabel
			// 
			this.GridFPSCountLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.GridFPSCountLabel.AutoSize = true;
			this.GridFPSCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.GridFPSCountLabel.Location = new System.Drawing.Point(5, 108);
			this.GridFPSCountLabel.Name = "GridFPSCountLabel";
			this.GridFPSCountLabel.Size = new System.Drawing.Size(0, 21);
			this.GridFPSCountLabel.TabIndex = 9;
			// 
			// GridColorLabel
			// 
			this.GridColorLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.GridColorLabel.AutoSize = true;
			this.GridColorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.GridColorLabel.Location = new System.Drawing.Point(4, 85);
			this.GridColorLabel.Name = "GridColorLabel";
			this.GridColorLabel.Size = new System.Drawing.Size(1, 17);
			this.GridColorLabel.TabIndex = 10;
			this.GridColorLabel.Text = "Color";
			// 
			// RandomColorsTab
			// 
			this.RandomColorsTab.Controls.Add(this.RandomViewSplittingPanel);
			this.RandomColorsTab.Location = new System.Drawing.Point(4, 22);
			this.RandomColorsTab.Name = "RandomColorsTab";
			this.RandomColorsTab.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
			this.RandomColorsTab.Size = new System.Drawing.Size(0, 136);
			this.RandomColorsTab.TabIndex = 2;
			this.RandomColorsTab.Text = "Random Colors";
			this.RandomColorsTab.UseVisualStyleBackColor = true;
			// 
			// RandomViewSplittingPanel
			// 
			this.RandomViewSplittingPanel.ColumnCount = 2;
			this.RandomViewSplittingPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.RandomViewSplittingPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 0F));
			this.RandomViewSplittingPanel.Controls.Add(this.RandomPhongLayoutPanel, 0, 0);
			this.RandomViewSplittingPanel.Controls.Add(this.RandomLayoutPanel, 0, 0);
			this.RandomViewSplittingPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RandomViewSplittingPanel.Location = new System.Drawing.Point(3, 3);
			this.RandomViewSplittingPanel.Margin = new System.Windows.Forms.Padding(0);
			this.RandomViewSplittingPanel.Name = "RandomViewSplittingPanel";
			this.RandomViewSplittingPanel.RowCount = 1;
			this.RandomViewSplittingPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 146F));
			this.RandomViewSplittingPanel.Size = new System.Drawing.Size(0, 130);
			this.RandomViewSplittingPanel.TabIndex = 1;
			// 
			// RandomPhongLayoutPanel
			// 
			this.RandomPhongLayoutPanel.ColumnCount = 1;
			this.RandomPhongLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.RandomPhongLayoutPanel.Controls.Add(this.RandomPhongPictureBox, 0, 0);
			this.RandomPhongLayoutPanel.Controls.Add(this.tableLayoutPanel2, 0, 1);
			this.RandomPhongLayoutPanel.Controls.Add(this.RandomApplyPhongLayoutPanel, 0, 2);
			this.RandomPhongLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RandomPhongLayoutPanel.Location = new System.Drawing.Point(1, 0);
			this.RandomPhongLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.RandomPhongLayoutPanel.Name = "RandomPhongLayoutPanel";
			this.RandomPhongLayoutPanel.RowCount = 3;
			this.RandomPhongLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 76.71233F));
			this.RandomPhongLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.28767F));
			this.RandomPhongLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.RandomPhongLayoutPanel.Size = new System.Drawing.Size(1, 146);
			this.RandomPhongLayoutPanel.TabIndex = 2;
			// 
			// RandomPhongPictureBox
			// 
			this.RandomPhongPictureBox.BackColor = System.Drawing.Color.Black;
			this.RandomPhongPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RandomPhongPictureBox.Location = new System.Drawing.Point(1, 1);
			this.RandomPhongPictureBox.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.RandomPhongPictureBox.Name = "RandomPhongPictureBox";
			this.RandomPhongPictureBox.Size = new System.Drawing.Size(1, 94);
			this.RandomPhongPictureBox.TabIndex = 0;
			this.RandomPhongPictureBox.TabStop = false;
			this.RandomPhongPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.RandomPhongPictureBox_Paint);
			this.RandomPhongPictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.RandomPhongPictureBox_MouseClick);
			this.RandomPhongPictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.RandomPhongPictureBox_MouseDown);
			this.RandomPhongPictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.RandomPhongPictureBox_MouseMove);
			this.RandomPhongPictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RandomPhongPictureBox_MouseUp);
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.tableLayoutPanel2.ColumnCount = 9;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 16F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.66627F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.66707F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 16F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.66627F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.66707F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 16F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.66627F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.66707F));
			this.tableLayoutPanel2.Controls.Add(this.RandomDLabel, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.RandomATextBox, 7, 0);
			this.tableLayoutPanel2.Controls.Add(this.RandomALabel, 6, 0);
			this.tableLayoutPanel2.Controls.Add(this.RandomSTextBox, 4, 0);
			this.tableLayoutPanel2.Controls.Add(this.RandomSLabel, 3, 0);
			this.tableLayoutPanel2.Controls.Add(this.RandomDTextBox, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.RandomDiffuseColorButton, 2, 0);
			this.tableLayoutPanel2.Controls.Add(this.RandomSpecularColorButton, 5, 0);
			this.tableLayoutPanel2.Controls.Add(this.RandomAmbientColorButton, 8, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 96);
			this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 1;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(1, 29);
			this.tableLayoutPanel2.TabIndex = 1;
			// 
			// RandomDLabel
			// 
			this.RandomDLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomDLabel.AutoSize = true;
			this.RandomDLabel.Location = new System.Drawing.Point(1, 8);
			this.RandomDLabel.Margin = new System.Windows.Forms.Padding(0);
			this.RandomDLabel.Name = "RandomDLabel";
			this.RandomDLabel.Size = new System.Drawing.Size(15, 13);
			this.RandomDLabel.TabIndex = 0;
			this.RandomDLabel.Text = "D";
			// 
			// RandomATextBox
			// 
			this.RandomATextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomATextBox.Location = new System.Drawing.Point(20, 4);
			this.RandomATextBox.Margin = new System.Windows.Forms.Padding(0);
			this.RandomATextBox.MaximumSize = new System.Drawing.Size(26, 20);
			this.RandomATextBox.MinimumSize = new System.Drawing.Size(26, 20);
			this.RandomATextBox.Name = "RandomATextBox";
			this.RandomATextBox.Size = new System.Drawing.Size(26, 20);
			this.RandomATextBox.TabIndex = 6;
			this.RandomATextBox.Text = "0.7";
			// 
			// RandomALabel
			// 
			this.RandomALabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomALabel.AutoSize = true;
			this.RandomALabel.Location = new System.Drawing.Point(6, 8);
			this.RandomALabel.Name = "RandomALabel";
			this.RandomALabel.Size = new System.Drawing.Size(10, 13);
			this.RandomALabel.TabIndex = 5;
			this.RandomALabel.Text = "A";
			// 
			// RandomSTextBox
			// 
			this.RandomSTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomSTextBox.Location = new System.Drawing.Point(19, 4);
			this.RandomSTextBox.Margin = new System.Windows.Forms.Padding(0);
			this.RandomSTextBox.MaximumSize = new System.Drawing.Size(26, 20);
			this.RandomSTextBox.MinimumSize = new System.Drawing.Size(26, 20);
			this.RandomSTextBox.Name = "RandomSTextBox";
			this.RandomSTextBox.Size = new System.Drawing.Size(26, 20);
			this.RandomSTextBox.TabIndex = 4;
			this.RandomSTextBox.Text = "0.5";
			// 
			// RandomSLabel
			// 
			this.RandomSLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomSLabel.AutoSize = true;
			this.RandomSLabel.Location = new System.Drawing.Point(3, 8);
			this.RandomSLabel.Margin = new System.Windows.Forms.Padding(0);
			this.RandomSLabel.Name = "RandomSLabel";
			this.RandomSLabel.Size = new System.Drawing.Size(14, 13);
			this.RandomSLabel.TabIndex = 3;
			this.RandomSLabel.Text = "S";
			// 
			// RandomDTextBox
			// 
			this.RandomDTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomDTextBox.Location = new System.Drawing.Point(18, 4);
			this.RandomDTextBox.Margin = new System.Windows.Forms.Padding(0);
			this.RandomDTextBox.MaximumSize = new System.Drawing.Size(26, 20);
			this.RandomDTextBox.MinimumSize = new System.Drawing.Size(26, 20);
			this.RandomDTextBox.Name = "RandomDTextBox";
			this.RandomDTextBox.Size = new System.Drawing.Size(26, 20);
			this.RandomDTextBox.TabIndex = 1;
			this.RandomDTextBox.Text = "0.6";
			// 
			// RandomDiffuseColorButton
			// 
			this.RandomDiffuseColorButton.BackColor = System.Drawing.Color.White;
			this.RandomDiffuseColorButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RandomDiffuseColorButton.Location = new System.Drawing.Point(10, 2);
			this.RandomDiffuseColorButton.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.RandomDiffuseColorButton.Name = "RandomDiffuseColorButton";
			this.RandomDiffuseColorButton.Size = new System.Drawing.Size(1, 25);
			this.RandomDiffuseColorButton.TabIndex = 7;
			this.RandomDiffuseColorButton.UseVisualStyleBackColor = false;
			this.RandomDiffuseColorButton.Click += new System.EventHandler(this.RandomDiffuseColorButton_Click);
			// 
			// RandomSpecularColorButton
			// 
			this.RandomSpecularColorButton.BackColor = System.Drawing.Color.White;
			this.RandomSpecularColorButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RandomSpecularColorButton.Location = new System.Drawing.Point(11, 2);
			this.RandomSpecularColorButton.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.RandomSpecularColorButton.Name = "RandomSpecularColorButton";
			this.RandomSpecularColorButton.Size = new System.Drawing.Size(1, 25);
			this.RandomSpecularColorButton.TabIndex = 8;
			this.RandomSpecularColorButton.UseVisualStyleBackColor = false;
			this.RandomSpecularColorButton.Click += new System.EventHandler(this.RandomSpecularColorButton_Click);
			// 
			// RandomAmbientColorButton
			// 
			this.RandomAmbientColorButton.BackColor = System.Drawing.Color.White;
			this.RandomAmbientColorButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RandomAmbientColorButton.Location = new System.Drawing.Point(12, 2);
			this.RandomAmbientColorButton.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.RandomAmbientColorButton.Name = "RandomAmbientColorButton";
			this.RandomAmbientColorButton.Size = new System.Drawing.Size(1, 25);
			this.RandomAmbientColorButton.TabIndex = 9;
			this.RandomAmbientColorButton.UseVisualStyleBackColor = false;
			this.RandomAmbientColorButton.Click += new System.EventHandler(this.RandomAmbientColorButton_Click);
			// 
			// RandomApplyPhongLayoutPanel
			// 
			this.RandomApplyPhongLayoutPanel.ColumnCount = 3;
			this.RandomApplyPhongLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.RandomApplyPhongLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
			this.RandomApplyPhongLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 89F));
			this.RandomApplyPhongLayoutPanel.Controls.Add(this.RandomSpecularExponentLabel, 0, 0);
			this.RandomApplyPhongLayoutPanel.Controls.Add(this.RandomSpecularExponentTextBox, 1, 0);
			this.RandomApplyPhongLayoutPanel.Controls.Add(this.RandomPhongApplyButton, 2, 0);
			this.RandomApplyPhongLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RandomApplyPhongLayoutPanel.Location = new System.Drawing.Point(0, 125);
			this.RandomApplyPhongLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.RandomApplyPhongLayoutPanel.Name = "RandomApplyPhongLayoutPanel";
			this.RandomApplyPhongLayoutPanel.RowCount = 1;
			this.RandomApplyPhongLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.RandomApplyPhongLayoutPanel.Size = new System.Drawing.Size(1, 21);
			this.RandomApplyPhongLayoutPanel.TabIndex = 2;
			// 
			// RandomSpecularExponentLabel
			// 
			this.RandomSpecularExponentLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomSpecularExponentLabel.AutoSize = true;
			this.RandomSpecularExponentLabel.Location = new System.Drawing.Point(0, 4);
			this.RandomSpecularExponentLabel.Margin = new System.Windows.Forms.Padding(0);
			this.RandomSpecularExponentLabel.Name = "RandomSpecularExponentLabel";
			this.RandomSpecularExponentLabel.Size = new System.Drawing.Size(1, 13);
			this.RandomSpecularExponentLabel.TabIndex = 0;
			this.RandomSpecularExponentLabel.Text = "Specular Exponent";
			// 
			// RandomSpecularExponentTextBox
			// 
			this.RandomSpecularExponentTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RandomSpecularExponentTextBox.Location = new System.Drawing.Point(-122, 1);
			this.RandomSpecularExponentTextBox.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
			this.RandomSpecularExponentTextBox.Name = "RandomSpecularExponentTextBox";
			this.RandomSpecularExponentTextBox.Size = new System.Drawing.Size(33, 20);
			this.RandomSpecularExponentTextBox.TabIndex = 1;
			this.RandomSpecularExponentTextBox.Text = "1";
			this.RandomSpecularExponentTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// RandomPhongApplyButton
			// 
			this.RandomPhongApplyButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RandomPhongApplyButton.Location = new System.Drawing.Point(-88, 0);
			this.RandomPhongApplyButton.Margin = new System.Windows.Forms.Padding(0);
			this.RandomPhongApplyButton.Name = "RandomPhongApplyButton";
			this.RandomPhongApplyButton.Size = new System.Drawing.Size(89, 21);
			this.RandomPhongApplyButton.TabIndex = 2;
			this.RandomPhongApplyButton.Text = "Apply";
			this.RandomPhongApplyButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.RandomPhongApplyButton.UseVisualStyleBackColor = true;
			this.RandomPhongApplyButton.Click += new System.EventHandler(this.RandomPhongApplyButton_Click);
			// 
			// RandomLayoutPanel
			// 
			this.RandomLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.RandomLayoutPanel.ColumnCount = 2;
			this.RandomLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64F));
			this.RandomLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36F));
			this.RandomLayoutPanel.Controls.Add(this.flowLayoutPanel3, 1, 2);
			this.RandomLayoutPanel.Controls.Add(this.RandomFileLabel, 0, 0);
			this.RandomLayoutPanel.Controls.Add(this.RandomFileNameLabel, 1, 0);
			this.RandomLayoutPanel.Controls.Add(this.RandomScaleLabel, 0, 1);
			this.RandomLayoutPanel.Controls.Add(this.flowLayoutPanel4, 1, 1);
			this.RandomLayoutPanel.Controls.Add(this.RandomLILabel, 0, 2);
			this.RandomLayoutPanel.Controls.Add(this.RandomPhongCheckBox, 0, 3);
			this.RandomLayoutPanel.Controls.Add(this.RandomEditPhongButton, 1, 3);
			this.RandomLayoutPanel.Controls.Add(this.RandomFPSLabel, 0, 4);
			this.RandomLayoutPanel.Controls.Add(this.RandomFPSCountLabel, 1, 4);
			this.RandomLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.RandomLayoutPanel.Location = new System.Drawing.Point(0, 0);
			this.RandomLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.RandomLayoutPanel.Name = "RandomLayoutPanel";
			this.RandomLayoutPanel.RowCount = 5;
			this.RandomLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.99998F));
			this.RandomLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.RandomLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00001F));
			this.RandomLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.00001F));
			this.RandomLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.RandomLayoutPanel.Size = new System.Drawing.Size(1, 146);
			this.RandomLayoutPanel.TabIndex = 0;
			// 
			// flowLayoutPanel3
			// 
			this.flowLayoutPanel3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.flowLayoutPanel3.Controls.Add(this.RandomLITextBox);
			this.flowLayoutPanel3.Controls.Add(this.RandomPercent2);
			this.flowLayoutPanel3.Location = new System.Drawing.Point(2, 66);
			this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
			this.flowLayoutPanel3.MaximumSize = new System.Drawing.Size(55, 22);
			this.flowLayoutPanel3.MinimumSize = new System.Drawing.Size(55, 22);
			this.flowLayoutPanel3.Name = "flowLayoutPanel3";
			this.flowLayoutPanel3.Size = new System.Drawing.Size(55, 22);
			this.flowLayoutPanel3.TabIndex = 5;
			// 
			// RandomLITextBox
			// 
			this.RandomLITextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomLITextBox.Location = new System.Drawing.Point(0, 1);
			this.RandomLITextBox.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
			this.RandomLITextBox.Name = "RandomLITextBox";
			this.RandomLITextBox.Size = new System.Drawing.Size(32, 20);
			this.RandomLITextBox.TabIndex = 0;
			this.RandomLITextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.RandomLITextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RandomLITextBox_KeyDown);
			// 
			// RandomPercent2
			// 
			this.RandomPercent2.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomPercent2.AutoSize = true;
			this.RandomPercent2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.RandomPercent2.Location = new System.Drawing.Point(32, 2);
			this.RandomPercent2.Margin = new System.Windows.Forms.Padding(0);
			this.RandomPercent2.Name = "RandomPercent2";
			this.RandomPercent2.Size = new System.Drawing.Size(20, 17);
			this.RandomPercent2.TabIndex = 1;
			this.RandomPercent2.Text = "%";
			// 
			// RandomFileLabel
			// 
			this.RandomFileLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomFileLabel.AutoSize = true;
			this.RandomFileLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.RandomFileLabel.Location = new System.Drawing.Point(4, 7);
			this.RandomFileLabel.Name = "RandomFileLabel";
			this.RandomFileLabel.Size = new System.Drawing.Size(1, 17);
			this.RandomFileLabel.TabIndex = 0;
			this.RandomFileLabel.Text = "File";
			// 
			// RandomFileNameLabel
			// 
			this.RandomFileNameLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomFileNameLabel.AutoSize = true;
			this.RandomFileNameLabel.Location = new System.Drawing.Point(2, 9);
			this.RandomFileNameLabel.Margin = new System.Windows.Forms.Padding(0);
			this.RandomFileNameLabel.Name = "RandomFileNameLabel";
			this.RandomFileNameLabel.Size = new System.Drawing.Size(0, 13);
			this.RandomFileNameLabel.TabIndex = 1;
			// 
			// RandomScaleLabel
			// 
			this.RandomScaleLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomScaleLabel.AutoSize = true;
			this.RandomScaleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.RandomScaleLabel.Location = new System.Drawing.Point(4, 37);
			this.RandomScaleLabel.Name = "RandomScaleLabel";
			this.RandomScaleLabel.Size = new System.Drawing.Size(1, 17);
			this.RandomScaleLabel.TabIndex = 2;
			this.RandomScaleLabel.Text = "Scale";
			// 
			// flowLayoutPanel4
			// 
			this.flowLayoutPanel4.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.flowLayoutPanel4.Controls.Add(this.RandomScaleTextBox);
			this.flowLayoutPanel4.Controls.Add(this.RandomPercent1);
			this.flowLayoutPanel4.Location = new System.Drawing.Point(2, 35);
			this.flowLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
			this.flowLayoutPanel4.MaximumSize = new System.Drawing.Size(55, 22);
			this.flowLayoutPanel4.MinimumSize = new System.Drawing.Size(55, 22);
			this.flowLayoutPanel4.Name = "flowLayoutPanel4";
			this.flowLayoutPanel4.Size = new System.Drawing.Size(55, 22);
			this.flowLayoutPanel4.TabIndex = 3;
			// 
			// RandomScaleTextBox
			// 
			this.RandomScaleTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomScaleTextBox.Location = new System.Drawing.Point(0, 1);
			this.RandomScaleTextBox.Margin = new System.Windows.Forms.Padding(0, 1, 0, 0);
			this.RandomScaleTextBox.Name = "RandomScaleTextBox";
			this.RandomScaleTextBox.Size = new System.Drawing.Size(32, 20);
			this.RandomScaleTextBox.TabIndex = 0;
			this.RandomScaleTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.RandomScaleTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RandomScaleTextBox_KeyDown);
			// 
			// RandomPercent1
			// 
			this.RandomPercent1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomPercent1.AutoSize = true;
			this.RandomPercent1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.RandomPercent1.Location = new System.Drawing.Point(32, 2);
			this.RandomPercent1.Margin = new System.Windows.Forms.Padding(0);
			this.RandomPercent1.Name = "RandomPercent1";
			this.RandomPercent1.Size = new System.Drawing.Size(20, 17);
			this.RandomPercent1.TabIndex = 1;
			this.RandomPercent1.Text = "%";
			// 
			// RandomLILabel
			// 
			this.RandomLILabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomLILabel.AutoSize = true;
			this.RandomLILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.RandomLILabel.Location = new System.Drawing.Point(4, 68);
			this.RandomLILabel.Name = "RandomLILabel";
			this.RandomLILabel.Size = new System.Drawing.Size(1, 17);
			this.RandomLILabel.TabIndex = 4;
			this.RandomLILabel.Text = "Light intensity";
			// 
			// RandomPhongCheckBox
			// 
			this.RandomPhongCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomPhongCheckBox.AutoSize = true;
			this.RandomPhongCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.RandomPhongCheckBox.Location = new System.Drawing.Point(1, 97);
			this.RandomPhongCheckBox.Margin = new System.Windows.Forms.Padding(0);
			this.RandomPhongCheckBox.Name = "RandomPhongCheckBox";
			this.RandomPhongCheckBox.Size = new System.Drawing.Size(1, 21);
			this.RandomPhongCheckBox.TabIndex = 6;
			this.RandomPhongCheckBox.Text = "Phong lighting";
			this.RandomPhongCheckBox.UseVisualStyleBackColor = true;
			this.RandomPhongCheckBox.CheckedChanged += new System.EventHandler(this.RandomPhongCheckBox_CheckedChanged);
			// 
			// RandomEditPhongButton
			// 
			this.RandomEditPhongButton.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomEditPhongButton.Location = new System.Drawing.Point(2, 98);
			this.RandomEditPhongButton.Margin = new System.Windows.Forms.Padding(0);
			this.RandomEditPhongButton.MaximumSize = new System.Drawing.Size(38, 19);
			this.RandomEditPhongButton.MinimumSize = new System.Drawing.Size(38, 19);
			this.RandomEditPhongButton.Name = "RandomEditPhongButton";
			this.RandomEditPhongButton.Size = new System.Drawing.Size(38, 19);
			this.RandomEditPhongButton.TabIndex = 7;
			this.RandomEditPhongButton.Text = "Edit";
			this.RandomEditPhongButton.UseVisualStyleBackColor = true;
			this.RandomEditPhongButton.Click += new System.EventHandler(this.RandomEditPhongButton_Click);
			// 
			// RandomFPSLabel
			// 
			this.RandomFPSLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomFPSLabel.AutoSize = true;
			this.RandomFPSLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.RandomFPSLabel.Location = new System.Drawing.Point(4, 124);
			this.RandomFPSLabel.Name = "RandomFPSLabel";
			this.RandomFPSLabel.Size = new System.Drawing.Size(1, 20);
			this.RandomFPSLabel.TabIndex = 8;
			this.RandomFPSLabel.Text = "FPS";
			// 
			// RandomFPSCountLabel
			// 
			this.RandomFPSCountLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.RandomFPSCountLabel.AutoSize = true;
			this.RandomFPSCountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.RandomFPSCountLabel.Location = new System.Drawing.Point(5, 124);
			this.RandomFPSCountLabel.Name = "RandomFPSCountLabel";
			this.RandomFPSCountLabel.Size = new System.Drawing.Size(0, 21);
			this.RandomFPSCountLabel.TabIndex = 9;
			// 
			// Effects3DLayoutPanel
			// 
			this.Effects3DLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
			this.Effects3DLayoutPanel.ColumnCount = 1;
			this.Effects3DLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.Effects3DLayoutPanel.Controls.Add(this.EffectsLabel, 0, 0);
			this.Effects3DLayoutPanel.Controls.Add(this.LoadTextureLayoutPanel, 0, 1);
			this.Effects3DLayoutPanel.Controls.Add(this.FogLayoutPanel, 0, 2);
			this.Effects3DLayoutPanel.Controls.Add(this.TransparencyLayoutPanel, 0, 3);
			this.Effects3DLayoutPanel.Controls.Add(this.BackfaceCullingLeyoutPanel, 0, 4);
			this.Effects3DLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Effects3DLayoutPanel.Location = new System.Drawing.Point(1, 218);
			this.Effects3DLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.Effects3DLayoutPanel.Name = "Effects3DLayoutPanel";
			this.Effects3DLayoutPanel.RowCount = 5;
			this.Effects3DLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.Effects3DLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.Effects3DLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.Effects3DLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.Effects3DLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.Effects3DLayoutPanel.Size = new System.Drawing.Size(1, 152);
			this.Effects3DLayoutPanel.TabIndex = 3;
			// 
			// EffectsLabel
			// 
			this.EffectsLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.EffectsLabel.AutoSize = true;
			this.EffectsLabel.Location = new System.Drawing.Point(4, 4);
			this.EffectsLabel.Name = "EffectsLabel";
			this.EffectsLabel.Size = new System.Drawing.Size(1, 13);
			this.EffectsLabel.TabIndex = 1;
			this.EffectsLabel.Text = "Effects";
			// 
			// LoadTextureLayoutPanel
			// 
			this.LoadTextureLayoutPanel.ColumnCount = 3;
			this.LoadTextureLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 82F));
			this.LoadTextureLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 72F));
			this.LoadTextureLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.LoadTextureLayoutPanel.Controls.Add(this.LoadTextureButton, 2, 0);
			this.LoadTextureLayoutPanel.Controls.Add(this.LoadTextureCheckBox, 0, 0);
			this.LoadTextureLayoutPanel.Controls.Add(this.LoadedTextureLabel, 1, 0);
			this.LoadTextureLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LoadTextureLayoutPanel.Location = new System.Drawing.Point(1, 22);
			this.LoadTextureLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.LoadTextureLayoutPanel.Name = "LoadTextureLayoutPanel";
			this.LoadTextureLayoutPanel.RowCount = 1;
			this.LoadTextureLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.LoadTextureLayoutPanel.Size = new System.Drawing.Size(1, 20);
			this.LoadTextureLayoutPanel.TabIndex = 2;
			// 
			// LoadTextureButton
			// 
			this.LoadTextureButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LoadTextureButton.Location = new System.Drawing.Point(154, 0);
			this.LoadTextureButton.Margin = new System.Windows.Forms.Padding(0);
			this.LoadTextureButton.Name = "LoadTextureButton";
			this.LoadTextureButton.Size = new System.Drawing.Size(1, 20);
			this.LoadTextureButton.TabIndex = 3;
			this.LoadTextureButton.Text = "Load Texture";
			this.LoadTextureButton.UseVisualStyleBackColor = true;
			this.LoadTextureButton.Click += new System.EventHandler(this.LoadTextureButton_Click);
			// 
			// LoadTextureCheckBox
			// 
			this.LoadTextureCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.LoadTextureCheckBox.AutoSize = true;
			this.LoadTextureCheckBox.Location = new System.Drawing.Point(11, 1);
			this.LoadTextureCheckBox.Margin = new System.Windows.Forms.Padding(0);
			this.LoadTextureCheckBox.Name = "LoadTextureCheckBox";
			this.LoadTextureCheckBox.Size = new System.Drawing.Size(59, 17);
			this.LoadTextureCheckBox.TabIndex = 0;
			this.LoadTextureCheckBox.Text = "Enable";
			this.LoadTextureCheckBox.UseVisualStyleBackColor = true;
			this.LoadTextureCheckBox.CheckedChanged += new System.EventHandler(this.LoadTextureCheckBox_CheckedChanged);
			// 
			// LoadedTextureLabel
			// 
			this.LoadedTextureLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.LoadedTextureLabel.AutoSize = true;
			this.LoadedTextureLabel.Location = new System.Drawing.Point(118, 3);
			this.LoadedTextureLabel.Margin = new System.Windows.Forms.Padding(0);
			this.LoadedTextureLabel.Name = "LoadedTextureLabel";
			this.LoadedTextureLabel.Size = new System.Drawing.Size(0, 13);
			this.LoadedTextureLabel.TabIndex = 2;
			// 
			// FogLayoutPanel
			// 
			this.FogLayoutPanel.ColumnCount = 3;
			this.FogLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84F));
			this.FogLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
			this.FogLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.FogLayoutPanel.Controls.Add(this.FogLabel, 0, 1);
			this.FogLayoutPanel.Controls.Add(this.EnableFogCheckBox, 0, 0);
			this.FogLayoutPanel.Controls.Add(this.FogColorLabel, 1, 0);
			this.FogLayoutPanel.Controls.Add(this.FogIntensityLabel, 1, 1);
			this.FogLayoutPanel.Controls.Add(this.FogColorButton, 2, 0);
			this.FogLayoutPanel.Controls.Add(this.FogIntensitySlider, 2, 1);
			this.FogLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.FogLayoutPanel.Location = new System.Drawing.Point(1, 43);
			this.FogLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.FogLayoutPanel.Name = "FogLayoutPanel";
			this.FogLayoutPanel.RowCount = 2;
			this.FogLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.FogLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.FogLayoutPanel.Size = new System.Drawing.Size(1, 43);
			this.FogLayoutPanel.TabIndex = 3;
			// 
			// FogLabel
			// 
			this.FogLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.FogLabel.AutoSize = true;
			this.FogLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.FogLabel.Location = new System.Drawing.Point(28, 25);
			this.FogLabel.Margin = new System.Windows.Forms.Padding(0);
			this.FogLabel.Name = "FogLabel";
			this.FogLabel.Size = new System.Drawing.Size(28, 13);
			this.FogLabel.TabIndex = 0;
			this.FogLabel.Text = "Fog";
			this.FogLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// EnableFogCheckBox
			// 
			this.EnableFogCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.EnableFogCheckBox.AutoSize = true;
			this.EnableFogCheckBox.Location = new System.Drawing.Point(12, 2);
			this.EnableFogCheckBox.Margin = new System.Windows.Forms.Padding(0);
			this.EnableFogCheckBox.Name = "EnableFogCheckBox";
			this.EnableFogCheckBox.Size = new System.Drawing.Size(59, 17);
			this.EnableFogCheckBox.TabIndex = 1;
			this.EnableFogCheckBox.Text = "Enable";
			this.EnableFogCheckBox.UseVisualStyleBackColor = true;
			this.EnableFogCheckBox.CheckedChanged += new System.EventHandler(this.EnableFogCheckBox_CheckedChanged);
			// 
			// FogColorLabel
			// 
			this.FogColorLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.FogColorLabel.AutoSize = true;
			this.FogColorLabel.Location = new System.Drawing.Point(94, 4);
			this.FogColorLabel.Name = "FogColorLabel";
			this.FogColorLabel.Size = new System.Drawing.Size(31, 13);
			this.FogColorLabel.TabIndex = 2;
			this.FogColorLabel.Text = "Color";
			// 
			// FogIntensityLabel
			// 
			this.FogIntensityLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.FogIntensityLabel.AutoSize = true;
			this.FogIntensityLabel.Location = new System.Drawing.Point(87, 25);
			this.FogIntensityLabel.Name = "FogIntensityLabel";
			this.FogIntensityLabel.Size = new System.Drawing.Size(46, 13);
			this.FogIntensityLabel.TabIndex = 3;
			this.FogIntensityLabel.Text = "Intensity";
			// 
			// FogColorButton
			// 
			this.FogColorButton.BackColor = System.Drawing.Color.White;
			this.FogColorButton.Dock = System.Windows.Forms.DockStyle.Fill;
			this.FogColorButton.Location = new System.Drawing.Point(136, 0);
			this.FogColorButton.Margin = new System.Windows.Forms.Padding(0);
			this.FogColorButton.Name = "FogColorButton";
			this.FogColorButton.Size = new System.Drawing.Size(1, 21);
			this.FogColorButton.TabIndex = 4;
			this.FogColorButton.UseVisualStyleBackColor = false;
			this.FogColorButton.Click += new System.EventHandler(this.FogColorButton_Click);
			// 
			// FogIntensitySlider
			// 
			this.FogIntensitySlider.Dock = System.Windows.Forms.DockStyle.Fill;
			this.FogIntensitySlider.Location = new System.Drawing.Point(139, 24);
			this.FogIntensitySlider.Name = "FogIntensitySlider";
			this.FogIntensitySlider.Size = new System.Drawing.Size(1, 16);
			this.FogIntensitySlider.TabIndex = 5;
			this.FogIntensitySlider.Value = 5;
			this.FogIntensitySlider.ValueChanged += new System.EventHandler(this.FogIntensitySlider_ValueChanged);
			// 
			// TransparencyLayoutPanel
			// 
			this.TransparencyLayoutPanel.ColumnCount = 3;
			this.TransparencyLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84F));
			this.TransparencyLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
			this.TransparencyLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.TransparencyLayoutPanel.Controls.Add(this.EnableTransparencyCheckBox, 0, 0);
			this.TransparencyLayoutPanel.Controls.Add(this.TransparencyLabel, 0, 1);
			this.TransparencyLayoutPanel.Controls.Add(this.TransparencyIntensityLabel, 1, 0);
			this.TransparencyLayoutPanel.Controls.Add(this.TransparencyCountLabel, 1, 1);
			this.TransparencyLayoutPanel.Controls.Add(this.TransparencyLevelSlider, 2, 0);
			this.TransparencyLayoutPanel.Controls.Add(this.TransparentCountSlider, 2, 1);
			this.TransparencyLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TransparencyLayoutPanel.Location = new System.Drawing.Point(1, 87);
			this.TransparencyLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.TransparencyLayoutPanel.Name = "TransparencyLayoutPanel";
			this.TransparencyLayoutPanel.RowCount = 2;
			this.TransparencyLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.TransparencyLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.TransparencyLayoutPanel.Size = new System.Drawing.Size(1, 43);
			this.TransparencyLayoutPanel.TabIndex = 4;
			// 
			// EnableTransparencyCheckBox
			// 
			this.EnableTransparencyCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.EnableTransparencyCheckBox.AutoSize = true;
			this.EnableTransparencyCheckBox.Location = new System.Drawing.Point(12, 2);
			this.EnableTransparencyCheckBox.Margin = new System.Windows.Forms.Padding(0);
			this.EnableTransparencyCheckBox.Name = "EnableTransparencyCheckBox";
			this.EnableTransparencyCheckBox.Size = new System.Drawing.Size(59, 17);
			this.EnableTransparencyCheckBox.TabIndex = 0;
			this.EnableTransparencyCheckBox.Text = "Enable";
			this.EnableTransparencyCheckBox.UseVisualStyleBackColor = true;
			this.EnableTransparencyCheckBox.CheckedChanged += new System.EventHandler(this.EnableTransparencyCheckBox_CheckedChanged);
			// 
			// TransparencyLabel
			// 
			this.TransparencyLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.TransparencyLabel.AutoSize = true;
			this.TransparencyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.TransparencyLabel.Location = new System.Drawing.Point(0, 25);
			this.TransparencyLabel.Margin = new System.Windows.Forms.Padding(0);
			this.TransparencyLabel.Name = "TransparencyLabel";
			this.TransparencyLabel.Size = new System.Drawing.Size(84, 13);
			this.TransparencyLabel.TabIndex = 1;
			this.TransparencyLabel.Text = "Transparency";
			// 
			// TransparencyIntensityLabel
			// 
			this.TransparencyIntensityLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.TransparencyIntensityLabel.AutoSize = true;
			this.TransparencyIntensityLabel.Location = new System.Drawing.Point(93, 4);
			this.TransparencyIntensityLabel.Name = "TransparencyIntensityLabel";
			this.TransparencyIntensityLabel.Size = new System.Drawing.Size(33, 13);
			this.TransparencyIntensityLabel.TabIndex = 2;
			this.TransparencyIntensityLabel.Text = "Level";
			// 
			// TransparencyCountLabel
			// 
			this.TransparencyCountLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.TransparencyCountLabel.AutoSize = true;
			this.TransparencyCountLabel.Location = new System.Drawing.Point(87, 25);
			this.TransparencyCountLabel.Name = "TransparencyCountLabel";
			this.TransparencyCountLabel.Size = new System.Drawing.Size(46, 13);
			this.TransparencyCountLabel.TabIndex = 3;
			this.TransparencyCountLabel.Text = "Count %";
			// 
			// TransparencyLevelSlider
			// 
			this.TransparencyLevelSlider.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TransparencyLevelSlider.Location = new System.Drawing.Point(139, 3);
			this.TransparencyLevelSlider.Name = "TransparencyLevelSlider";
			this.TransparencyLevelSlider.Size = new System.Drawing.Size(1, 15);
			this.TransparencyLevelSlider.TabIndex = 4;
			this.TransparencyLevelSlider.Value = 5;
			this.TransparencyLevelSlider.ValueChanged += new System.EventHandler(this.TransparencyLevelSlider_ValueChanged);
			// 
			// TransparentCountSlider
			// 
			this.TransparentCountSlider.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TransparentCountSlider.Location = new System.Drawing.Point(139, 24);
			this.TransparentCountSlider.Name = "TransparentCountSlider";
			this.TransparentCountSlider.Size = new System.Drawing.Size(1, 16);
			this.TransparentCountSlider.TabIndex = 5;
			this.TransparentCountSlider.Value = 5;
			this.TransparentCountSlider.ValueChanged += new System.EventHandler(this.TransparentCountSlider_ValueChanged);
			// 
			// BackfaceCullingLeyoutPanel
			// 
			this.BackfaceCullingLeyoutPanel.ColumnCount = 2;
			this.BackfaceCullingLeyoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
			this.BackfaceCullingLeyoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.BackfaceCullingLeyoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.BackfaceCullingLeyoutPanel.Controls.Add(this.BackfaceCullingLabel, 0, 0);
			this.BackfaceCullingLeyoutPanel.Controls.Add(this.BackfaceCullingCheckBox, 1, 0);
			this.BackfaceCullingLeyoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BackfaceCullingLeyoutPanel.Location = new System.Drawing.Point(1, 131);
			this.BackfaceCullingLeyoutPanel.Margin = new System.Windows.Forms.Padding(0);
			this.BackfaceCullingLeyoutPanel.Name = "BackfaceCullingLeyoutPanel";
			this.BackfaceCullingLeyoutPanel.RowCount = 1;
			this.BackfaceCullingLeyoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.BackfaceCullingLeyoutPanel.Size = new System.Drawing.Size(1, 20);
			this.BackfaceCullingLeyoutPanel.TabIndex = 5;
			// 
			// BackfaceCullingLabel
			// 
			this.BackfaceCullingLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.BackfaceCullingLabel.AutoSize = true;
			this.BackfaceCullingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.BackfaceCullingLabel.Location = new System.Drawing.Point(3, 3);
			this.BackfaceCullingLabel.Margin = new System.Windows.Forms.Padding(0);
			this.BackfaceCullingLabel.Name = "BackfaceCullingLabel";
			this.BackfaceCullingLabel.Size = new System.Drawing.Size(103, 13);
			this.BackfaceCullingLabel.TabIndex = 2;
			this.BackfaceCullingLabel.Text = "Backface Culling";
			// 
			// BackfaceCullingCheckBox
			// 
			this.BackfaceCullingCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.BackfaceCullingCheckBox.AutoSize = true;
			this.BackfaceCullingCheckBox.Location = new System.Drawing.Point(110, 1);
			this.BackfaceCullingCheckBox.Margin = new System.Windows.Forms.Padding(0);
			this.BackfaceCullingCheckBox.Name = "BackfaceCullingCheckBox";
			this.BackfaceCullingCheckBox.Size = new System.Drawing.Size(1, 17);
			this.BackfaceCullingCheckBox.TabIndex = 1;
			this.BackfaceCullingCheckBox.Text = "Enable";
			this.BackfaceCullingCheckBox.UseVisualStyleBackColor = true;
			this.BackfaceCullingCheckBox.CheckedChanged += new System.EventHandler(this.BackfaceCullingCheckBox_CheckedChanged);
			// 
			// ToolTable
			// 
			this.ToolTable.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ToolTable.ColumnCount = 2;
			this.ToolTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 46F));
			this.ToolTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.ToolTable.Controls.Add(this.ToolBar, 1, 0);
			this.ToolTable.Controls.Add(this.AACheckBox, 0, 0);
			this.ToolTable.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ToolTable.Location = new System.Drawing.Point(0, 0);
			this.ToolTable.Margin = new System.Windows.Forms.Padding(0);
			this.ToolTable.Name = "ToolTable";
			this.ToolTable.RowCount = 1;
			this.ToolTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.ToolTable.Size = new System.Drawing.Size(856, 30);
			this.ToolTable.TabIndex = 5;
			// 
			// ToolBar
			// 
			this.ToolBar.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ToolBar.GripMargin = new System.Windows.Forms.Padding(0);
			this.ToolBar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
			this.ToolBar.ImageScalingSize = new System.Drawing.Size(24, 24);
			this.ToolBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BackgroundColorButton,
            this.PointButton,
            this.LineButton,
            this.CircleButton,
            this.FilledCircleButton,
            this.PolygonButton,
            this.FilledPolygonButton,
            this.CombButton,
            this.ToolStripColorLabel,
            this.ColorButton,
            this.Separator1,
            this.SelectToolButton,
            this.MoveToolButton,
            this.ClearSceneButton,
            this.Separator2,
            this.GridLabel,
            this.GridButton,
            this.GridDensityTextBox,
            this.GridColorButton,
            this.Separator3,
            this.FillSceneButton,
            this.FillSceneColorButton,
            this.toolStripSeparator1,
            this.ClippingToolButton,
            this.ClippingRectangleWidth,
            this.Xlabel,
            this.ClippingRectangleHeight,
            this.FiltersButton,
            this.toolStripSeparator2,
            this.AdvancedColorsButton,
            this.toolStripSeparator3,
            this.Button3D});
			this.ToolBar.Location = new System.Drawing.Point(46, 0);
			this.ToolBar.Name = "ToolBar";
			this.ToolBar.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
			this.ToolBar.Size = new System.Drawing.Size(810, 30);
			this.ToolBar.TabIndex = 5;
			this.ToolBar.Text = "ToolBar";
			// 
			// BackgroundColorButton
			// 
			this.BackgroundColorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.BackgroundColorButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.BackgroundColorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.BackgroundColorButton.Name = "BackgroundColorButton";
			this.BackgroundColorButton.Size = new System.Drawing.Size(23, 27);
			this.BackgroundColorButton.Text = "Background color";
			this.BackgroundColorButton.Click += new System.EventHandler(this.BackgroundColorButton_Click);
			// 
			// PointButton
			// 
			this.PointButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.PointButton.Image = global::GK1.Properties.Resources.point;
			this.PointButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.PointButton.Name = "PointButton";
			this.PointButton.Size = new System.Drawing.Size(28, 27);
			this.PointButton.Text = "Draw a point";
			this.PointButton.Click += new System.EventHandler(this.PointButton_Click);
			// 
			// LineButton
			// 
			this.LineButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.LineButton.Image = global::GK1.Properties.Resources.line;
			this.LineButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.LineButton.Name = "LineButton";
			this.LineButton.Size = new System.Drawing.Size(28, 27);
			this.LineButton.Text = "Draw a line";
			this.LineButton.Click += new System.EventHandler(this.LineButton_Click);
			// 
			// CircleButton
			// 
			this.CircleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.CircleButton.Image = global::GK1.Properties.Resources.circle;
			this.CircleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.CircleButton.Name = "CircleButton";
			this.CircleButton.Size = new System.Drawing.Size(28, 27);
			this.CircleButton.Text = "Draw a cycle";
			this.CircleButton.Click += new System.EventHandler(this.CircleButton_Click);
			// 
			// FilledCircleButton
			// 
			this.FilledCircleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.FilledCircleButton.Image = global::GK1.Properties.Resources.filled_circle;
			this.FilledCircleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.FilledCircleButton.Name = "FilledCircleButton";
			this.FilledCircleButton.Size = new System.Drawing.Size(28, 27);
			this.FilledCircleButton.Text = "Filled circle";
			this.FilledCircleButton.Click += new System.EventHandler(this.FilledCircleButton_Click);
			// 
			// PolygonButton
			// 
			this.PolygonButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.PolygonButton.Image = global::GK1.Properties.Resources.polygon;
			this.PolygonButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.PolygonButton.Name = "PolygonButton";
			this.PolygonButton.Size = new System.Drawing.Size(28, 27);
			this.PolygonButton.Text = "Draw Polygon";
			this.PolygonButton.Click += new System.EventHandler(this.PolygonButton_Click);
			// 
			// FilledPolygonButton
			// 
			this.FilledPolygonButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.FilledPolygonButton.Image = ((System.Drawing.Image)(resources.GetObject("FilledPolygonButton.Image")));
			this.FilledPolygonButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.FilledPolygonButton.Name = "FilledPolygonButton";
			this.FilledPolygonButton.Size = new System.Drawing.Size(28, 27);
			this.FilledPolygonButton.Text = "Draw Filled Polygon";
			this.FilledPolygonButton.Click += new System.EventHandler(this.FilledPolygonButton_Click);
			// 
			// CombButton
			// 
			this.CombButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.CombButton.Image = global::GK1.Properties.Resources.comb;
			this.CombButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.CombButton.Name = "CombButton";
			this.CombButton.Size = new System.Drawing.Size(28, 27);
			this.CombButton.Text = "Draw comb";
			this.CombButton.Click += new System.EventHandler(this.CombButton_Click);
			// 
			// ToolStripColorLabel
			// 
			this.ToolStripColorLabel.Name = "ToolStripColorLabel";
			this.ToolStripColorLabel.Size = new System.Drawing.Size(36, 27);
			this.ToolStripColorLabel.Text = "Color";
			// 
			// ColorButton
			// 
			this.ColorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ColorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ColorButton.Name = "ColorButton";
			this.ColorButton.Size = new System.Drawing.Size(23, 27);
			this.ColorButton.Text = "Drawing color";
			this.ColorButton.Click += new System.EventHandler(this.ColorButton_Click);
			// 
			// Separator1
			// 
			this.Separator1.Name = "Separator1";
			this.Separator1.Size = new System.Drawing.Size(6, 30);
			// 
			// SelectToolButton
			// 
			this.SelectToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.SelectToolButton.Image = global::GK1.Properties.Resources.select;
			this.SelectToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.SelectToolButton.Name = "SelectToolButton";
			this.SelectToolButton.Size = new System.Drawing.Size(28, 27);
			this.SelectToolButton.Text = "Select obejct";
			this.SelectToolButton.Click += new System.EventHandler(this.SelectToolButton_Click);
			// 
			// MoveToolButton
			// 
			this.MoveToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.MoveToolButton.Image = global::GK1.Properties.Resources.move;
			this.MoveToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.MoveToolButton.Name = "MoveToolButton";
			this.MoveToolButton.Size = new System.Drawing.Size(28, 27);
			this.MoveToolButton.Text = "Move object";
			this.MoveToolButton.Click += new System.EventHandler(this.MoveToolButton_Click);
			// 
			// ClearSceneButton
			// 
			this.ClearSceneButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ClearSceneButton.Image = global::GK1.Properties.Resources.clear;
			this.ClearSceneButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ClearSceneButton.Name = "ClearSceneButton";
			this.ClearSceneButton.Size = new System.Drawing.Size(28, 27);
			this.ClearSceneButton.Text = "Clear scene";
			this.ClearSceneButton.Click += new System.EventHandler(this.ClearSceneButton_Click);
			// 
			// Separator2
			// 
			this.Separator2.Name = "Separator2";
			this.Separator2.Size = new System.Drawing.Size(6, 30);
			// 
			// GridLabel
			// 
			this.GridLabel.Name = "GridLabel";
			this.GridLabel.Size = new System.Drawing.Size(29, 27);
			this.GridLabel.Text = "Grid";
			// 
			// GridButton
			// 
			this.GridButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.GridButton.Image = global::GK1.Properties.Resources.grid;
			this.GridButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.GridButton.Name = "GridButton";
			this.GridButton.Size = new System.Drawing.Size(28, 27);
			this.GridButton.Text = "Add grid";
			this.GridButton.Click += new System.EventHandler(this.GridButton_Click);
			// 
			// GridDensityTextBox
			// 
			this.GridDensityTextBox.Name = "GridDensityTextBox";
			this.GridDensityTextBox.Size = new System.Drawing.Size(30, 30);
			this.GridDensityTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GridDensityTextBox_KeyDown);
			// 
			// GridColorButton
			// 
			this.GridColorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.GridColorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.GridColorButton.Name = "GridColorButton";
			this.GridColorButton.Size = new System.Drawing.Size(23, 27);
			this.GridColorButton.Text = "Grid color";
			this.GridColorButton.Click += new System.EventHandler(this.GridColorButton_Click);
			// 
			// Separator3
			// 
			this.Separator3.Name = "Separator3";
			this.Separator3.Size = new System.Drawing.Size(6, 30);
			// 
			// FillSceneButton
			// 
			this.FillSceneButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.FillSceneButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectedToolStripMenuItem,
            this.connectedToolStripMenuItem1});
			this.FillSceneButton.Image = global::GK1.Properties.Resources.empty_bucket;
			this.FillSceneButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.FillSceneButton.Name = "FillSceneButton";
			this.FillSceneButton.Size = new System.Drawing.Size(37, 27);
			this.FillSceneButton.Text = "Fill scene";
			this.FillSceneButton.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.FillSceneButton_DropDownItemClicked);
			// 
			// connectedToolStripMenuItem
			// 
			this.connectedToolStripMenuItem.Name = "connectedToolStripMenuItem";
			this.connectedToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
			this.connectedToolStripMenuItem.Text = "4-connected";
			// 
			// connectedToolStripMenuItem1
			// 
			this.connectedToolStripMenuItem1.Name = "connectedToolStripMenuItem1";
			this.connectedToolStripMenuItem1.Size = new System.Drawing.Size(141, 22);
			this.connectedToolStripMenuItem1.Text = "8-connected";
			// 
			// FillSceneColorButton
			// 
			this.FillSceneColorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.FillSceneColorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.FillSceneColorButton.Name = "FillSceneColorButton";
			this.FillSceneColorButton.Size = new System.Drawing.Size(23, 27);
			this.FillSceneColorButton.Text = "FillSceneColor";
			this.FillSceneColorButton.Click += new System.EventHandler(this.FillSceneColorButton_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 30);
			// 
			// ClippingToolButton
			// 
			this.ClippingToolButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ClippingToolButton.Image = ((System.Drawing.Image)(resources.GetObject("ClippingToolButton.Image")));
			this.ClippingToolButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ClippingToolButton.Name = "ClippingToolButton";
			this.ClippingToolButton.Size = new System.Drawing.Size(28, 27);
			this.ClippingToolButton.Text = "Clipping Tool";
			this.ClippingToolButton.Click += new System.EventHandler(this.ClippingToolButton_Click);
			// 
			// ClippingRectangleWidth
			// 
			this.ClippingRectangleWidth.Name = "ClippingRectangleWidth";
			this.ClippingRectangleWidth.Size = new System.Drawing.Size(24, 30);
			this.ClippingRectangleWidth.Text = "240";
			this.ClippingRectangleWidth.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ClippingRectangleXSize_KeyDown);
			// 
			// Xlabel
			// 
			this.Xlabel.Name = "Xlabel";
			this.Xlabel.Size = new System.Drawing.Size(12, 27);
			this.Xlabel.Text = "x";
			// 
			// ClippingRectangleHeight
			// 
			this.ClippingRectangleHeight.Name = "ClippingRectangleHeight";
			this.ClippingRectangleHeight.Size = new System.Drawing.Size(24, 30);
			this.ClippingRectangleHeight.Text = "160";
			this.ClippingRectangleHeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ClippingRectangleYSize_KeyDown);
			// 
			// FiltersButton
			// 
			this.FiltersButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.FiltersButton.Image = ((System.Drawing.Image)(resources.GetObject("FiltersButton.Image")));
			this.FiltersButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.FiltersButton.Name = "FiltersButton";
			this.FiltersButton.Size = new System.Drawing.Size(28, 27);
			this.FiltersButton.Text = "Filters";
			this.FiltersButton.Click += new System.EventHandler(this.FiltersButton_Click);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 30);
			// 
			// AdvancedColorsButton
			// 
			this.AdvancedColorsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.AdvancedColorsButton.Image = ((System.Drawing.Image)(resources.GetObject("AdvancedColorsButton.Image")));
			this.AdvancedColorsButton.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.AdvancedColorsButton.Name = "AdvancedColorsButton";
			this.AdvancedColorsButton.Size = new System.Drawing.Size(28, 27);
			this.AdvancedColorsButton.Text = "AdvancedColorPicking";
			this.AdvancedColorsButton.Click += new System.EventHandler(this.AdvancedColorsButton_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(6, 30);
			// 
			// Button3D
			// 
			this.Button3D.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.Button3D.Image = ((System.Drawing.Image)(resources.GetObject("Button3D.Image")));
			this.Button3D.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.Button3D.Name = "Button3D";
			this.Button3D.Size = new System.Drawing.Size(28, 27);
			this.Button3D.Click += new System.EventHandler(this.Button3D_Click);
			// 
			// AACheckBox
			// 
			this.AACheckBox.AutoSize = true;
			this.AACheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.AACheckBox.Dock = System.Windows.Forms.DockStyle.Left;
			this.AACheckBox.Location = new System.Drawing.Point(3, 3);
			this.AACheckBox.Name = "AACheckBox";
			this.AACheckBox.Size = new System.Drawing.Size(40, 24);
			this.AACheckBox.TabIndex = 6;
			this.AACheckBox.Text = "AA";
			this.AACheckBox.UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.ClientSize = new System.Drawing.Size(856, 441);
			this.Controls.Add(this.MainTable);
			this.Name = "Form1";
			this.Text = "MainWindow";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
			this.Resize += new System.EventHandler(this.Form1_Resize);
			this.MainTable.ResumeLayout(false);
			this.WorkAreaTable.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Canvas)).EndInit();
			this.SplittingLayoutPanel.ResumeLayout(false);
			this.RightPanelTable.ResumeLayout(false);
			this.InfoPanelTable.ResumeLayout(false);
			this.InfoPanelTable.PerformLayout();
			this.NameAndIconTable.ResumeLayout(false);
			this.NameAndIconTable.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.ObjectIcon)).EndInit();
			this.ColorTable.ResumeLayout(false);
			this.ColorTable.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.ObjectColor)).EndInit();
			this.LayerLayoutPanel.ResumeLayout(false);
			this.LayerLayoutPanel.PerformLayout();
			this.LayerUpDownTable.ResumeLayout(false);
			this.LayerUpDownTable.PerformLayout();
			this.UpDownTable.ResumeLayout(false);
			this.FiltersLayoutPanel.ResumeLayout(false);
			this.MatrixFiltersTabControl.ResumeLayout(false);
			this.Tab3x3.ResumeLayout(false);
			this.Main3x3LayoutPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridView3x3)).EndInit();
			this.Buttons3x3LayoutPanel.ResumeLayout(false);
			this.Buttons3x3LayoutPanel.PerformLayout();
			this.Tab5x5.ResumeLayout(false);
			this.Main5x5LayoutPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridView5x5)).EndInit();
			this.Buttons5x5LayoutPanel.ResumeLayout(false);
			this.Buttons5x5LayoutPanel.PerformLayout();
			this.Tab7x7.ResumeLayout(false);
			this.Main7x7LayoutPanel.ResumeLayout(false);
			this.Buttons7x7LayoutPanel.ResumeLayout(false);
			this.Buttons7x7LayoutPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridView7x7)).EndInit();
			this.HistogramTabControl.ResumeLayout(false);
			this.RTab.ResumeLayout(false);
			this.RTabLayotPanel.ResumeLayout(false);
			this.RHistogramLayoutPanel.ResumeLayout(false);
			this.RHistogramLayoutPanel.PerformLayout();
			this.RHistogramVisualizingLayoutPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.RHistogramPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.RHistogramXAxisPB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.RHistogramYAxisPB)).EndInit();
			this.RFunctonFilterLayoutPanel.ResumeLayout(false);
			this.RFunctonFilterLayoutPanel.PerformLayout();
			this.RFinctionFilterVisualizingLayoutPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.RFilterVisualizingMainPB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.RYAxisForFilterVisPB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.RXAxisForFilterVisPB)).EndInit();
			this.GTab.ResumeLayout(false);
			this.GTabLayotPanel.ResumeLayout(false);
			this.GHistogramLayoutPanel.ResumeLayout(false);
			this.GHistogramLayoutPanel.PerformLayout();
			this.GHistogramVisualizingLayoutPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GHistogramPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GHistogramXAxisPB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GHistogramYAxisPB)).EndInit();
			this.GFunctonFilterLayoutPanel.ResumeLayout(false);
			this.GFunctonFilterLayoutPanel.PerformLayout();
			this.GFinctionFilterVisualizingLayoutPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GFilterVisualizingMainPB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GYAxisForFilterVisPB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GXAxisForFilterVisPB)).EndInit();
			this.Btab.ResumeLayout(false);
			this.BTabLayotPanel.ResumeLayout(false);
			this.BHistogramLayoutPanel.ResumeLayout(false);
			this.BHistogramLayoutPanel.PerformLayout();
			this.BHistogramVisualizingLayoutPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.BHistogramPictureBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.BHistogramXAxisPB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.BHistogramYAxisPB)).EndInit();
			this.BFunctonFilterLayoutPanel.ResumeLayout(false);
			this.BFunctonFilterLayoutPanel.PerformLayout();
			this.BFinctionFilterVisualizingLayoutPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.BFilterVisualizingMainPB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.BYAxisForFilterVisPB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.BXAxisForFilterVisPB)).EndInit();
			this.GammaTab.ResumeLayout(false);
			this.GammaTab.PerformLayout();
			this.Main3DLayoutPanel.ResumeLayout(false);
			this.Viewing3DLayoutPanel.ResumeLayout(false);
			this.Viewing3DLayoutPanel.PerformLayout();
			this.Viewing3DTabControl.ResumeLayout(false);
			this.StandardViewTab.ResumeLayout(false);
			this.StandardViewSplittingPanel.ResumeLayout(false);
			this.StandardViewMainLP.ResumeLayout(false);
			this.StandardViewMainLP.PerformLayout();
			this.LIFlowLayoutPanel.ResumeLayout(false);
			this.LIFlowLayoutPanel.PerformLayout();
			this.ScaleFlowLayoutPanel.ResumeLayout(false);
			this.ScaleFlowLayoutPanel.PerformLayout();
			this.StandardPhonglayoutPanel.ResumeLayout(false);
			this.StandardApplyPhongLayoutPanel.ResumeLayout(false);
			this.StandardApplyPhongLayoutPanel.PerformLayout();
			this.StandardPhongEditionLayoutPanel.ResumeLayout(false);
			this.StandardPhongEditionLayoutPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.StandardPhongPictureBox)).EndInit();
			this.GridViewTab.ResumeLayout(false);
			this.GridLayoutPanel.ResumeLayout(false);
			this.GridLayoutPanel.PerformLayout();
			this.flowLayoutPanel1.ResumeLayout(false);
			this.flowLayoutPanel1.PerformLayout();
			this.flowLayoutPanel2.ResumeLayout(false);
			this.flowLayoutPanel2.PerformLayout();
			this.RandomColorsTab.ResumeLayout(false);
			this.RandomViewSplittingPanel.ResumeLayout(false);
			this.RandomPhongLayoutPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.RandomPhongPictureBox)).EndInit();
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.RandomApplyPhongLayoutPanel.ResumeLayout(false);
			this.RandomApplyPhongLayoutPanel.PerformLayout();
			this.RandomLayoutPanel.ResumeLayout(false);
			this.RandomLayoutPanel.PerformLayout();
			this.flowLayoutPanel3.ResumeLayout(false);
			this.flowLayoutPanel3.PerformLayout();
			this.flowLayoutPanel4.ResumeLayout(false);
			this.flowLayoutPanel4.PerformLayout();
			this.Effects3DLayoutPanel.ResumeLayout(false);
			this.Effects3DLayoutPanel.PerformLayout();
			this.LoadTextureLayoutPanel.ResumeLayout(false);
			this.LoadTextureLayoutPanel.PerformLayout();
			this.FogLayoutPanel.ResumeLayout(false);
			this.FogLayoutPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.FogIntensitySlider)).EndInit();
			this.TransparencyLayoutPanel.ResumeLayout(false);
			this.TransparencyLayoutPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.TransparencyLevelSlider)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.TransparentCountSlider)).EndInit();
			this.BackfaceCullingLeyoutPanel.ResumeLayout(false);
			this.BackfaceCullingLeyoutPanel.PerformLayout();
			this.ToolTable.ResumeLayout(false);
			this.ToolTable.PerformLayout();
			this.ToolBar.ResumeLayout(false);
			this.ToolBar.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel MainTable;
        private System.Windows.Forms.TableLayoutPanel WorkAreaTable;
        private System.Windows.Forms.PictureBox Canvas;
        private System.Windows.Forms.TableLayoutPanel RightPanelTable;
        private System.Windows.Forms.ListView ObjectList;
        public System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.TableLayoutPanel InfoPanelTable;
        private System.Windows.Forms.TableLayoutPanel NameAndIconTable;
        private System.Windows.Forms.PictureBox ObjectIcon;
        private System.Windows.Forms.TextBox ObjectName;
        private System.Windows.Forms.TableLayoutPanel ColorTable;
        private System.Windows.Forms.Label ColorLabel;
        private System.Windows.Forms.PictureBox ObjectColor;
        private System.Windows.Forms.TableLayoutPanel LayerLayoutPanel;
        private System.Windows.Forms.Button IncreaseLayer;
        private System.Windows.Forms.Button DecreaseLayer;
        private System.Windows.Forms.TextBox LayerTextBox;
        private System.Windows.Forms.TableLayoutPanel LayerUpDownTable;
        private System.Windows.Forms.Label Layer;
        private System.Windows.Forms.TableLayoutPanel UpDownTable;
        private System.Windows.Forms.Button DownButton;
        private System.Windows.Forms.Button UpButton;
        private System.Windows.Forms.CheckBox EnableAACheckBox;
		private System.Windows.Forms.TableLayoutPanel ToolTable;
		private System.Windows.Forms.ToolStrip ToolBar;
		private System.Windows.Forms.ToolStripButton PointButton;
		private System.Windows.Forms.ToolStripButton LineButton;
		private System.Windows.Forms.ToolStripButton CircleButton;
		private System.Windows.Forms.ToolStripButton FilledCircleButton;
		private System.Windows.Forms.ToolStripSeparator Separator1;
		private System.Windows.Forms.ToolStripButton SelectToolButton;
		private System.Windows.Forms.ToolStripButton MoveToolButton;
		private System.Windows.Forms.ToolStripButton ClearSceneButton;
		private System.Windows.Forms.ToolStripSeparator Separator2;
		private System.Windows.Forms.ToolStripLabel GridLabel;
		private System.Windows.Forms.ToolStripButton GridButton;
		private System.Windows.Forms.ToolStripTextBox GridDensityTextBox;
		private System.Windows.Forms.ToolStripButton GridColorButton;
		private System.Windows.Forms.ToolStripSeparator Separator3;
		private System.Windows.Forms.ToolStripLabel ToolStripColorLabel;
		private System.Windows.Forms.ToolStripButton ColorButton;
		private System.Windows.Forms.ToolStripButton BackgroundColorButton;
		private System.Windows.Forms.CheckBox AACheckBox;
		private System.Windows.Forms.ToolStripButton PolygonButton;
		private System.Windows.Forms.ToolStripDropDownButton FillSceneButton;
		private System.Windows.Forms.ToolStripMenuItem connectedToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem connectedToolStripMenuItem1;
		private System.Windows.Forms.ToolStripButton FillSceneColorButton;
		private System.Windows.Forms.ToolStripButton FilledPolygonButton;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripButton ClippingToolButton;
		private System.Windows.Forms.ToolStripTextBox ClippingRectangleWidth;
		private System.Windows.Forms.ToolStripLabel Xlabel;
		private System.Windows.Forms.ToolStripTextBox ClippingRectangleHeight;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripButton AdvancedColorsButton;
		private System.Windows.Forms.ToolStripButton CombButton;
		private System.Windows.Forms.ToolStripButton FiltersButton;
		private System.Windows.Forms.TableLayoutPanel SplittingLayoutPanel;
		private System.Windows.Forms.TableLayoutPanel FiltersLayoutPanel;
		private System.Windows.Forms.TabControl HistogramTabControl;
		private System.Windows.Forms.TabPage RTab;
		private System.Windows.Forms.TableLayoutPanel RTabLayotPanel;
		private System.Windows.Forms.Button RShowFilterWindowButton;
		private System.Windows.Forms.TableLayoutPanel RHistogramLayoutPanel;
		private System.Windows.Forms.TableLayoutPanel RHistogramVisualizingLayoutPanel;
		private System.Windows.Forms.PictureBox RHistogramPictureBox;
		private System.Windows.Forms.PictureBox RHistogramXAxisPB;
		private System.Windows.Forms.PictureBox RHistogramYAxisPB;
		private System.Windows.Forms.TableLayoutPanel RFunctonFilterLayoutPanel;
		private System.Windows.Forms.TabPage GTab;
		private System.Windows.Forms.TabPage Btab;
		private System.Windows.Forms.TableLayoutPanel RFinctionFilterVisualizingLayoutPanel;
		private System.Windows.Forms.PictureBox RFilterVisualizingMainPB;
		private System.Windows.Forms.PictureBox RYAxisForFilterVisPB;
		private System.Windows.Forms.PictureBox RXAxisForFilterVisPB;
		private System.Windows.Forms.Button SubmitButton;
		private System.Windows.Forms.TableLayoutPanel GTabLayotPanel;
		private System.Windows.Forms.Button GShowFilterWindowButton;
		private System.Windows.Forms.TableLayoutPanel GHistogramLayoutPanel;
		private System.Windows.Forms.TableLayoutPanel GHistogramVisualizingLayoutPanel;
		private System.Windows.Forms.PictureBox GHistogramPictureBox;
		private System.Windows.Forms.PictureBox GHistogramXAxisPB;
		private System.Windows.Forms.PictureBox GHistogramYAxisPB;
		private System.Windows.Forms.TableLayoutPanel GFunctonFilterLayoutPanel;
		private System.Windows.Forms.TableLayoutPanel GFinctionFilterVisualizingLayoutPanel;
		private System.Windows.Forms.PictureBox GFilterVisualizingMainPB;
		private System.Windows.Forms.PictureBox GYAxisForFilterVisPB;
		private System.Windows.Forms.PictureBox GXAxisForFilterVisPB;
		private System.Windows.Forms.TableLayoutPanel BTabLayotPanel;
		private System.Windows.Forms.Button BShowFilterWindowButton;
		private System.Windows.Forms.TableLayoutPanel BHistogramLayoutPanel;
		private System.Windows.Forms.TableLayoutPanel BHistogramVisualizingLayoutPanel;
		private System.Windows.Forms.PictureBox BHistogramXAxisPB;
		private System.Windows.Forms.PictureBox BHistogramYAxisPB;
		private System.Windows.Forms.TableLayoutPanel BFunctonFilterLayoutPanel;
		private System.Windows.Forms.TableLayoutPanel BFinctionFilterVisualizingLayoutPanel;
		private System.Windows.Forms.PictureBox BFilterVisualizingMainPB;
		private System.Windows.Forms.PictureBox BYAxisForFilterVisPB;
		private System.Windows.Forms.PictureBox BXAxisForFilterVisPB;
		private System.Windows.Forms.TabControl MatrixFiltersTabControl;
		private System.Windows.Forms.TabPage Tab3x3;
		private System.Windows.Forms.TabPage Tab5x5;
		private System.Windows.Forms.TabPage Tab7x7;
		private System.Windows.Forms.TableLayoutPanel Main3x3LayoutPanel;
		private System.Windows.Forms.TableLayoutPanel Buttons3x3LayoutPanel;
		private System.Windows.Forms.TableLayoutPanel Main5x5LayoutPanel;
		private System.Windows.Forms.TableLayoutPanel Buttons5x5LayoutPanel;
		private System.Windows.Forms.TableLayoutPanel Main7x7LayoutPanel;
		private System.Windows.Forms.TableLayoutPanel Buttons7x7LayoutPanel;
		private System.Windows.Forms.Button Gaussian3x3Button;
		private System.Windows.Forms.Button Laplacian3x3Button;
		private System.Windows.Forms.Button Gaussian5x5Button;
		private System.Windows.Forms.Button Laplacian5x5Button;
		private System.Windows.Forms.Button Gaussian7x7Button;
		private System.Windows.Forms.Button Laplacian7x7Button;
		private System.Windows.Forms.DataGridView GridView7x7;
		private System.Windows.Forms.DataGridView GridView5x5;
		private System.Windows.Forms.DataGridView GridView3x3;
		private System.Windows.Forms.CheckBox CheckBox7x7;
		private System.Windows.Forms.CheckBox CheckBox3x3;
		private System.Windows.Forms.CheckBox CheckBox5x5;
		private System.Windows.Forms.PictureBox BHistogramPictureBox;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
		private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
		private System.Windows.Forms.CheckBox EqualizeHistogramRed;
		private System.Windows.Forms.CheckBox EqualizeHistogramGreen;
		private System.Windows.Forms.CheckBox EqualizeHistogramBlue;
		private System.Windows.Forms.CheckBox RApplyFilterCheckBox;
		private System.Windows.Forms.CheckBox GApplyFilterCheckBox;
		private System.Windows.Forms.CheckBox BApplyFilterCheckBox;
		private System.Windows.Forms.TabPage GammaTab;
		private System.Windows.Forms.CheckBox GammaEnableCheckBox;
		private System.Windows.Forms.TextBox GammaTextBox;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripButton Button3D;
		private System.Windows.Forms.TableLayoutPanel Main3DLayoutPanel;
		private System.Windows.Forms.Button Import3DModelButton;
		private System.Windows.Forms.Button BackTo2DButton;
		private System.Windows.Forms.TableLayoutPanel Viewing3DLayoutPanel;
		private System.Windows.Forms.Label ViewOptionsLabel;
		private System.Windows.Forms.TabControl Viewing3DTabControl;
		private System.Windows.Forms.TabPage StandardViewTab;
		private System.Windows.Forms.TabPage GridViewTab;
		private System.Windows.Forms.TabPage RandomColorsTab;
		private System.Windows.Forms.TableLayoutPanel Effects3DLayoutPanel;
		private System.Windows.Forms.Label EffectsLabel;
		private System.Windows.Forms.TableLayoutPanel LoadTextureLayoutPanel;
		private System.Windows.Forms.CheckBox LoadTextureCheckBox;
		private System.Windows.Forms.TableLayoutPanel FogLayoutPanel;
		private System.Windows.Forms.Label FogLabel;
		private System.Windows.Forms.CheckBox EnableFogCheckBox;
		private System.Windows.Forms.Label FogColorLabel;
		private System.Windows.Forms.Label FogIntensityLabel;
		private System.Windows.Forms.Button FogColorButton;
		private System.Windows.Forms.TrackBar FogIntensitySlider;
		private System.Windows.Forms.TableLayoutPanel TransparencyLayoutPanel;
		private System.Windows.Forms.CheckBox EnableTransparencyCheckBox;
		private System.Windows.Forms.Label TransparencyLabel;
		private System.Windows.Forms.Label TransparencyIntensityLabel;
		private System.Windows.Forms.Label TransparencyCountLabel;
		private System.Windows.Forms.TrackBar TransparencyLevelSlider;
		private System.Windows.Forms.TrackBar TransparentCountSlider;
		private System.Windows.Forms.TableLayoutPanel StandardViewSplittingPanel;
		private System.Windows.Forms.TableLayoutPanel StandardViewMainLP;
		private System.Windows.Forms.Label FileLabel;
		private System.Windows.Forms.Label FileNameLabel;
		private System.Windows.Forms.Label ScaleLabel;
		private System.Windows.Forms.FlowLayoutPanel ScaleFlowLayoutPanel;
		private System.Windows.Forms.TextBox ScaleTextBox;
		private System.Windows.Forms.Label PercentLabel1;
		private System.Windows.Forms.FlowLayoutPanel LIFlowLayoutPanel;
		private System.Windows.Forms.TextBox LITextBox;
		private System.Windows.Forms.Label PercentLabel2;
		private System.Windows.Forms.Label LightIntensityLabel;
		private System.Windows.Forms.CheckBox StandardPhongCheckBox;
		private System.Windows.Forms.Button EditStandardPhongButton;
		private System.Windows.Forms.Label StandardFPSlabel;
		private System.Windows.Forms.Label StandardFPScountLabel;
		private System.Windows.Forms.Label StandardColorLabel;
		private System.Windows.Forms.Button StandardColorButton;
		private System.Windows.Forms.TableLayoutPanel StandardPhonglayoutPanel;
		private System.Windows.Forms.PictureBox StandardPhongPictureBox;
		private System.Windows.Forms.TableLayoutPanel StandardPhongEditionLayoutPanel;
		private System.Windows.Forms.Label StandardSpecularLabel;
		private System.Windows.Forms.Label StandardDiffuseLabel;
		private System.Windows.Forms.TextBox StandardDiffuseTextBox;
		private System.Windows.Forms.TextBox StandardSpecularTextBox;
		private System.Windows.Forms.TextBox StandardAmbienttextBox;
		private System.Windows.Forms.Label StandardAmbientLabel;
		private System.Windows.Forms.TableLayoutPanel GridLayoutPanel;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.TextBox GridLITextBox;
		private System.Windows.Forms.Label GridPercent2;
		private System.Windows.Forms.Label GridFileLabel;
		private System.Windows.Forms.Label GridFileNameLabel;
		private System.Windows.Forms.Label GridScaleLabel;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
		private System.Windows.Forms.TextBox GridScaleTextBox;
		private System.Windows.Forms.Label GridPercent1;
		private System.Windows.Forms.Label GridLightIntensityLabel;
		private System.Windows.Forms.Label GridFPSLabel;
		private System.Windows.Forms.Label GridFPSCountLabel;
		private System.Windows.Forms.Button Grid3DColorButton;
		private System.Windows.Forms.Label GridColorLabel;
		private System.Windows.Forms.TableLayoutPanel RandomViewSplittingPanel;
		private System.Windows.Forms.TableLayoutPanel RandomLayoutPanel;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
		private System.Windows.Forms.TextBox RandomLITextBox;
		private System.Windows.Forms.Label RandomPercent2;
		private System.Windows.Forms.Label RandomFileLabel;
		private System.Windows.Forms.Label RandomFileNameLabel;
		private System.Windows.Forms.Label RandomScaleLabel;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
		private System.Windows.Forms.TextBox RandomScaleTextBox;
		private System.Windows.Forms.Label RandomPercent1;
		private System.Windows.Forms.Label RandomLILabel;
		private System.Windows.Forms.CheckBox RandomPhongCheckBox;
		private System.Windows.Forms.Button RandomEditPhongButton;
		private System.Windows.Forms.Label RandomFPSLabel;
		private System.Windows.Forms.Label RandomFPSCountLabel;
		private System.Windows.Forms.TableLayoutPanel RandomPhongLayoutPanel;
		private System.Windows.Forms.PictureBox RandomPhongPictureBox;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.TextBox RandomATextBox;
		private System.Windows.Forms.Label RandomSLabel;
		private System.Windows.Forms.Label RandomDLabel;
		private System.Windows.Forms.TextBox RandomDTextBox;
		private System.Windows.Forms.TextBox RandomSTextBox;
		private System.Windows.Forms.Label RandomALabel;
		private System.Windows.Forms.Label LoadedTextureLabel;
		private System.Windows.Forms.Button LoadTextureButton;
		private System.Windows.Forms.TableLayoutPanel BackfaceCullingLeyoutPanel;
		private System.Windows.Forms.CheckBox BackfaceCullingCheckBox;
		private System.Windows.Forms.Label BackfaceCullingLabel;
		private System.Windows.Forms.Button StandardDiffuseColorButton;
		private System.Windows.Forms.Button StandardSpecularColorButton;
		private System.Windows.Forms.Button StandardAmbientColorButton;
		private System.Windows.Forms.Button RandomDiffuseColorButton;
		private System.Windows.Forms.Button RandomSpecularColorButton;
		private System.Windows.Forms.Button RandomAmbientColorButton;
		private System.Windows.Forms.TableLayoutPanel RandomApplyPhongLayoutPanel;
		private System.Windows.Forms.Label RandomSpecularExponentLabel;
		private System.Windows.Forms.TextBox RandomSpecularExponentTextBox;
		private System.Windows.Forms.Button RandomPhongApplyButton;
		private System.Windows.Forms.TableLayoutPanel StandardApplyPhongLayoutPanel;
		private System.Windows.Forms.Label StandardSpecularExponentLabel;
		private System.Windows.Forms.TextBox StandardSpecularExponentTextBox;
		private System.Windows.Forms.Button StandardPhongApplyButton;
	}
}

