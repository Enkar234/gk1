﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Drawing.Imaging;

namespace GK1
{
	public partial class Form1 : Form
	{
		bool[] buttons = new bool[11];
		// 0 - point
		// 1 - line
		// 2 - circle
		// 3 - filled circle
		// 4 - selecton
		// 5 - moving
		// 6 - polygon
		// 7 - filled polygon
		// 8 - fill scene
		// 9 - clipping
		// 10 - filters
		List<Point> polygonPoints;
		ColorPicker cp = new ColorPicker();
		ToolStripButton[] buttonIcons = new ToolStripButton[9];
		int cbi = 0; //clicked button index
		int soi = 0; //selected object index;
		_3dObjectManager _3DManager;

		private bool isMouseDown = false;
		private bool selected = false;
		private bool gridOn = false;
		private bool move = false;
		private bool clippingOn = false;
		private bool filtersOn = false;
		private bool rotate = false;
		private bool scale = false;
		bool mode3dON = false;
		int lines = 0;
		int circles = 0;
		int points = 0;
		int polygons = 0;
		int layers = 1;
		private string fillMethod;
		private Bitmap originalImage;
		DirectBitmap magicalRectangle = null;

		PaintingManager paintingManager;
		FilterManager filterManager;
		PhongManager phongManager;
		FilterFunctionSetter filterFunctionSetter;
		PhongLightningSetter phongLightningSetter;
		ObjectList objects = new ObjectList();

		#region - Zad 1,2 GUI stuff -

		#region - Initialisation -
		public Form1()
		{
			InitializeComponent();
			StartPosition = FormStartPosition.CenterScreen;
			paintingManager = new PaintingManager(Canvas);
			filterManager = new FilterManager();
			paintingManager.SetBackgroundColor(Color.Black);
			paintingManager.SetDrawingColor(Color.White);
			paintingManager.SetFillSceneColor(Color.White);
			paintingManager.SetGridColor(Color.White);
			paintingManager.Initialize();
			filterFunctionSetter = new FilterFunctionSetter(RFilterVisualizingMainPB, GFilterVisualizingMainPB, BFilterVisualizingMainPB);
			phongLightningSetter = new PhongLightningSetter(StandardPhongPictureBox, RandomPhongPictureBox);
			List<Point> lights = new List<Point>();
			lights.Add(new Point(Canvas.Width / 2, Canvas.Height / 2));
			phongManager = new PhongManager(0.7, 0.5, 0.6,1, lights, Canvas.Width / 2, Canvas.Height / 2, Color.White,Color.White,Color.White);
			SetButtonColor(ColorButton, Color.White);
			SetButtonColor(BackgroundColorButton, Color.Black);
			SetButtonColor(GridColorButton, Color.White);
			SetButtonColor(FillSceneColorButton, Color.White);
			SetButtons();
			KeyPreview = true;
			polygonPoints = new List<Point>();
			InitializeDataGrids();
			SetXYAxes();
			Canvas.MouseWheel += Canvas_MouseWheel;
		}
		private void SetButtons()
		{
			buttonIcons[0] = PointButton;
			buttonIcons[1] = LineButton;
			buttonIcons[2] = CircleButton;
			buttonIcons[3] = FilledCircleButton;
			buttonIcons[4] = SelectToolButton;
			buttonIcons[5] = MoveToolButton;
			buttonIcons[6] = PolygonButton;
			buttonIcons[7] = FilledPolygonButton;
		}
		private void SetButtonColor(ToolStripButton b, Color c)
		{
			SolidBrush brush = new SolidBrush(c);
			Bitmap bmp = new Bitmap(5, 5);
			using (Graphics graph = Graphics.FromImage(bmp))
			{
				Rectangle ImageSize = new Rectangle(0, 0, 5, 5);
				graph.FillRectangle(brush, ImageSize);
			}
			b.Image = bmp;
		}
		private void InitializeDataGrids()
		{
			GridView7x7.ColumnCount = 7;
			GridView5x5.ColumnCount = 5;
			GridView3x3.ColumnCount = 3;
			int[] row7 = new int[] { 0, 0, 0, 0, 0, 0, 0 };
			int[] row5 = new int[] { 0, 0, 0, 0, 0 };
			int[] row3 = new int[] { 0, 0, 0 };
			for (int i = 0; i < 7; i++)
				GridView7x7.Rows.Add(row7);
			for (int i = 0; i < 5; i++)
				GridView5x5.Rows.Add(row5);
			for (int i = 0; i < 3; i++)
				GridView3x3.Rows.Add(row3);
			GridView3x3.Rows[0].Height = 47;
			GridView3x3.Rows[2].Height = 47;

			SetGaussian3x3();
			SetGaussian5x5();
			SetGaussian7x7();
		}
		#endregion

		#region - Drawing Objects -
		private void Canvas_MouseDown(object sender, MouseEventArgs e)
		{
			isMouseDown = true;
			paintingManager.SetStartCoordinates(e.X, e.Y);
			if (!buttons[6] && !buttons[7]) paintingManager.InitializePainter(AACheckBox.Checked, buttons);

			if (selected && buttons[5])
			{
				paintingManager.UpdateLastPoint(new Point(e.X, e.Y));
				if (MouseWithinSelection(e.X, e.Y))
					move = true;
			}
			if ((clippingOn || filtersOn) && !rotate && !scale)
			{
				paintingManager.UpdateLastPoint(e.Location);
				if (paintingManager.MouseInSelectedArea(e.X, e.Y))
					move = true;
			}
			if (filtersOn && (scale || rotate))
			{
				Rectangle rect = paintingManager.selectedArea;
				int x = rect.X + rect.Width / 2;
				int y = rect.Y + rect.Height / 2;
				paintingManager.CenterPoint = new Point(x, y);
				paintingManager.startX = e.X;
				paintingManager.startY = e.Y;
				paintingManager.UpdateLastPoint(e.Location);
			}
			if (mode3dON)
			{
				paintingManager.Initialize();
				_3DManager.SetCenterPoint(Canvas.Width / 2, Canvas.Height / 2);
				_3DManager.UpdateLastPoint(e.Location);
			}
		}
		private void Canvas_MouseClick(object sender, MouseEventArgs e)
		{
			if (buttons[4])
			{
				int count = objects.count;
				int i = count - 1;
				ObjectList.SelectedIndices.Clear();
				for (; i >= 0; i--)
				{
					if (objects.list[i].Selected(e.X, e.Y))
					{
						Rectangle area = objects.list[i].Area();
						paintingManager.SetSelectedArea(new Rectangle(area.X - 10, area.Y - 10, area.Width + 20, area.Height + 20));
						selected = true;
						soi = i;
						Canvas.Invalidate();
						ObjectList.Items[i].Selected = true;
						ObjectList.Select();
						SetObjectInfo(i);
						break;
					}
				}
				if (i < 0)
				{
					selected = false;
					InfoPanelTable.Visible = false;
				}
			}
			if ((buttons[6] || buttons[7]) && e.Button == MouseButtons.Left)
			{
				int pc = polygonPoints.Count;
				int x = paintingManager.startX;
				int y = paintingManager.startY;
				if (pc == 0)
				{
					paintingManager.InitializePolygonPainter(buttons[7]);
					paintingManager.SetFirstPolygonPoint(new Point(x, y));
				}
				if (paintingManager.polygonClosed(x, y) && pc > 2)
				{
					objects.Add(
					new MyPolygon(
						polygonPoints,
						paintingManager.CurrentDrawingColor,
						layers,
						buttons[7],
						"Polygon" + polygons.ToString()));
					paintingManager.Initialize(gridOn);
					objects.Repaint(Canvas);
					//objects.list.Last().PaintMe(Canvas);
					ObjectList.Items.Add("Polygon" + polygons.ToString());
					polygons++;
					layers++;
					polygonPoints.Clear();
				}
				else
				{
					polygonPoints.Add(new Point(x, y));
					if (pc > 0)
					{
						using (Graphics g = Graphics.FromImage(Canvas.Image))
						{
							g.DrawLine(new Pen(paintingManager.CurrentDrawingColor), polygonPoints[pc - 1], polygonPoints[pc]);
						}
					}
				}
			}
			if (buttons[8])
			{
				paintingManager.PrepareToFill(Canvas.Width, Canvas.Height);
				Canvas.Invalidate();
			}
			if (e.Button == MouseButtons.Right && selected && paintingManager.MouseInSelectedArea(e.X, e.Y) && objects.list[soi].type == MyObject.Type.Polygon && (objects.list[soi] as MyPolygon).filled == true)
			{
				ContextMenu cm = new ContextMenu();
				cm.MenuItems.Add("Add texture", new EventHandler(MenuItem_Click));
				cm.MenuItems.Add("Color reduction", new EventHandler(MenuItem_Click));
				cm.Show(Canvas, new Point(e.X, e.Y));
			}
			if (e.Button == MouseButtons.Right && filtersOn && paintingManager.MouseInSelectedArea(e.X, e.Y))
			{
				ContextMenu cm = new ContextMenu();
				cm.MenuItems.Add("Rotate", new EventHandler(MenuItem_Click));
				cm.MenuItems.Add("Scale", new EventHandler(MenuItem_Click));
				cm.MenuItems.Add("Move", new EventHandler(MenuItem_Click));
				cm.MenuItems.Add("Reset", new EventHandler(MenuItem_Click));
				cm.Show(Canvas, new Point(e.X, e.Y));
			}
		}
		private void Canvas_MouseMove(object sender, MouseEventArgs e)
		{
			if (isMouseDown)
			{
				if (buttons[1])
				{
					paintingManager.UpdateLastPoint(e.Location);
				}
				if (buttons[2] || buttons[3])
				{
					paintingManager.UpdateRectangle(e.X, e.Y);
				}
				if (selected && buttons[5] && move)
				{
					int dx = e.X - paintingManager.lastPoint.X;
					int dy = e.Y - paintingManager.lastPoint.Y;
					paintingManager.UpdateLastPoint(e.Location);
					paintingManager.Initialize(gridOn);
					objects.list[soi].Move(dx, dy);
					MoveSelectedArea(dx, dy);
					objects.Repaint(Canvas);
				}
				if (clippingOn && move)
				{
					int dx = e.X - paintingManager.lastPoint.X;
					int dy = e.Y - paintingManager.lastPoint.Y;
					paintingManager.UpdateLastPoint(e.Location);
					MoveSelectedArea(dx, dy);
					PrepareClippedBitmap();
				}
				if (filtersOn && move)
				{
					int dx = e.X - paintingManager.lastPoint.X;
					int dy = e.Y - paintingManager.lastPoint.Y;
					paintingManager.UpdateLastPoint(e.Location);
					MoveSelectedArea(dx, dy);
					PrepareMagicalRectangle(originalImage, paintingManager.selectedArea);
				}
				if (filtersOn && scale)
				{
					int cdx = e.X - paintingManager.CenterPoint.X;
					int cdy = e.Y - paintingManager.CenterPoint.Y;
					double cd = Math.Sqrt(cdx * cdx + cdy * cdy);

					int pdx = paintingManager.startX - paintingManager.CenterPoint.X;
					int pdy = paintingManager.startY - paintingManager.CenterPoint.Y;
					double pd = Math.Sqrt(pdx * pdx + pdy * pdy);

					double sc = cd / pd;
					filterManager.EnableScaling(sc);
					PrepareMagicalRectangle(originalImage, paintingManager.selectedArea);
				}
				if (filtersOn && rotate)
				{
					int pdx = paintingManager.startX - paintingManager.CenterPoint.X;
					int pdy = paintingManager.startY - paintingManager.CenterPoint.Y;
					double pfi = Math.Atan((double)pdy / pdx);

					int cdx = e.X - paintingManager.CenterPoint.X;
					int cdy = e.Y - paintingManager.CenterPoint.Y;
					double cfi = Math.Atan((double)cdy / cdx);

					double angle = cfi - pfi;

					bool mirror = paintingManager.lastPoint.X > paintingManager.CenterPoint.X && e.X < paintingManager.CenterPoint.X;
					mirror = mirror || paintingManager.lastPoint.X < paintingManager.CenterPoint.X && e.X > paintingManager.CenterPoint.X;

					filterManager.EnableRotating(angle);
					PrepareMagicalRectangle(originalImage, paintingManager.selectedArea, mirror);

					paintingManager.UpdateLastPoint(e.Location);
				}
				if (mode3dON)
				{
					_3DManager.RotateObject(e.Location);
					_3DManager.UpdateLastPoint(e.Location);
				}
				Canvas.Invalidate();
			}
			if (selected && buttons[5])
			{
				Canvas.Cursor = MouseWithinSelection(e.X, e.Y) ? Cursors.SizeAll : Cursors.Default;
			}
			if (clippingOn || filtersOn)
			{
				Canvas.Cursor = paintingManager.MouseInSelectedArea(e.X, e.Y) ? Cursors.SizeAll : Cursors.Default;
			}
		}
		private void Canvas_Paint(object sender, PaintEventArgs e)
		{
			if ((buttons[1] || buttons[2] || buttons[3]) && isMouseDown)
			{
				paintingManager.PaintObject(e.Graphics);
			}
			if (selected)
			{
				paintingManager.DrawSelection(e.Graphics);
			}
			if (buttons[8])
			{
				if (fillMethod == "4-connected")
					paintingManager.FillSceneWithColor4c(e.Graphics, paintingManager.startX, paintingManager.startY);
				else if (fillMethod == "8-connected")
					paintingManager.FillSceneWithColor8c(e.Graphics, paintingManager.startX, paintingManager.startY);
			}
			if (clippingOn || filtersOn)
			{
				paintingManager.PaintObject(e.Graphics);
			}
			if (mode3dON)
			{
				_3DManager.Paint(Canvas.Width, Canvas.Height, e.Graphics);
				if (_3DManager.UpdateFPScounter())
				{
					StandardFPScountLabel.Text = _3DManager.FPS.ToString();
					GridFPSCountLabel.Text = _3DManager.FPS.ToString();
					RandomFPSCountLabel.Text = _3DManager.FPS.ToString();
				}
			}
		}
		private void Canvas_MouseUp(object sender, MouseEventArgs e)
		{
			if (buttons[0])
			{
				objects.Add(
					new MyPoint(
						paintingManager.startX,
						paintingManager.startY,
						paintingManager.CurrentDrawingColor,
						layers,
						"Point" + points.ToString(),
						AACheckBox.Checked));
				objects.list.Last().PaintMe(Canvas);
				ObjectList.Items.Add("Point" + points.ToString());
				points++;
				layers++;
			}
			if (buttons[1])
			{
				if (paintingManager.startX != e.X || paintingManager.startY != e.Y)
				{
					objects.Add(
						new MyLine(
							paintingManager.startX,
							paintingManager.startY,
							e.X, e.Y,
							paintingManager.CurrentDrawingColor,
							layers,
							"Line" + lines.ToString(),
							AACheckBox.Checked));
					objects.list.Last().PaintMe(Canvas);
					ObjectList.Items.Add("Line" + lines.ToString());
					lines++;
					layers++;
				}
			}
			if (buttons[2])
			{
				if (paintingManager.startX != e.X || paintingManager.startY != e.Y)
				{
					objects.Add(
						new MyCircle(
							paintingManager.rect.X + paintingManager.rect.Height / 2,
							paintingManager.rect.Y + paintingManager.rect.Height / 2,
							paintingManager.rect.Height,
							paintingManager.CurrentDrawingColor,
							layers,
							false,
							"Circle" + circles.ToString(),
							AACheckBox.Checked));
					objects.list.Last().PaintMe(Canvas);
					ObjectList.Items.Add("Circle" + circles.ToString());
					circles++;
					layers++;
				}
			}
			if (buttons[3])
			{
				if (paintingManager.startX != e.X || paintingManager.startY != e.Y)
				{
					objects.Add(
						new MyCircle(
							paintingManager.rect.X + paintingManager.rect.Height / 2,
							paintingManager.rect.Y + paintingManager.rect.Height / 2,
							paintingManager.rect.Height,
							paintingManager.CurrentDrawingColor,
							layers,
							true,
							"Circle" + circles.ToString(),
							AACheckBox.Checked));
					objects.list.Last().PaintMe(Canvas);
					ObjectList.Items.Add("Circle" + circles.ToString());
					circles++;
					layers++;
				}
			}
			Canvas.Invalidate();
			isMouseDown = false;
			move = false;
		}
		#endregion

		#region - Clicking Buttons -
		private void SelectToolButton_Click(object sender, EventArgs e)
		{
			ButtonClicked(4);
		}
		private void MoveToolButton_Click(object sender, EventArgs e)
		{
			ButtonClicked(5);
		}
		private void ClearSceneButton_Click(object sender, EventArgs e)
		{
			DialogResult result = MessageBox.Show("Do you want to clear the scene?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
			if (result == DialogResult.OK)
			{
				selected = false;
				paintingManager.Initialize(true);
				soi = 0;
				layers = 1;
				points = 0;
				circles = 0;
				lines = 0;
				objects.Clear();
				ObjectList.Clear();
				InfoPanelTable.Visible = false;
				polygonPoints.Clear();
			}
		}
		private void GridButton_Click(object sender, EventArgs e)
		{
			if (gridOn)
			{
				gridOn = false;
				GridButton.Checked = false;
				paintingManager.Initialize();
			}
			else
			{
				gridOn = true;
				GridButton.Checked = true;
				paintingManager.SetGridDensity(GridDensityTextBox.Text);
				paintingManager.Initialize(true);
			}
			objects.Repaint(Canvas);
		}
		private void GridColorButton_Click(object sender, EventArgs e)
		{
			ColorDialog colorDialog = new ColorDialog();
			if (colorDialog.ShowDialog() == DialogResult.OK)
			{
				SetButtonColor(GridColorButton, colorDialog.Color);
				paintingManager.SetGridColor(colorDialog.Color);
				if (gridOn)
				{
					paintingManager.SetGridDensity(GridDensityTextBox.Text);
					paintingManager.Initialize(true);
					objects.Repaint(Canvas);
				}
			}
		}
		private void ColorButton_Click(object sender, EventArgs e)
		{
			ColorDialog colorDialog = new ColorDialog();
			if (colorDialog.ShowDialog() == DialogResult.OK)
			{
				SetButtonColor(ColorButton, colorDialog.Color);
				paintingManager.SetDrawingColor(colorDialog.Color);
			}
		}
		private void BackgroundColorButton_Click(object sender, EventArgs e)
		{
			ColorDialog colorDialog = new ColorDialog();
			if (colorDialog.ShowDialog() == DialogResult.OK)
			{
				SetButtonColor(BackgroundColorButton, colorDialog.Color);
				paintingManager.SetBackgroundColor(colorDialog.Color);
				paintingManager.Initialize(true);
				objects.Repaint(Canvas);
			}
		}
		private void FillSceneColorButton_Click(object sender, EventArgs e)
		{
			ColorDialog colorDialog = new ColorDialog();
			if (colorDialog.ShowDialog() == DialogResult.OK)
			{
				SetButtonColor(FillSceneColorButton, colorDialog.Color);
				paintingManager.SetFillSceneColor(colorDialog.Color);
			}
		}
		private void FillSceneButton_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
		{
			buttons[cbi] = false;
			if (cbi < 8) buttonIcons[cbi].Checked = false;
			cbi = 8;
			buttons[cbi] = true;
			fillMethod = e.ClickedItem.Text;
		}
		private void ClippingToolButton_Click(object sender, EventArgs e)
		{
			buttons[cbi] = false;
			if (cbi < 8) buttonIcons[cbi].Checked = false;
			cbi = 9;
			buttons[cbi] = true;
			if (!clippingOn)
			{
				DisableButtons();
				selected = false;
				ClippingToolButton.Checked = true;
				clippingOn = true;
				originalImage = new Bitmap(Canvas.Width, Canvas.Height);

				using (Graphics g = Graphics.FromImage(originalImage))
				{
					g.DrawImage(Canvas.Image, 0, 0);
				}
				using (Graphics g = Graphics.FromImage(Canvas.Image))
				{
					g.FillRectangle(new SolidBrush(paintingManager.CurrentBackgroundColor), 0, 0, Canvas.Width, Canvas.Height);
				}
				int width, height;
				if (int.TryParse(ClippingRectangleWidth.Text, out width) && int.TryParse(ClippingRectangleHeight.Text, out height))
				{
					if (width > 0 && height > 0)
					{
						paintingManager.SetStartCoordinates(Canvas.Width / 2 - width / 2, Canvas.Height / 2 - height / 2);
						paintingManager.SetSelectedArea(new Rectangle(Canvas.Width / 2 - width / 2, Canvas.Height / 2 - height / 2, width, height));
					}
				}
				PrepareClippedBitmap();
			}
			else
			{
				EnableButtons();
				ClippingToolButton.Checked = false;
				clippingOn = false;
				using (Graphics g = Graphics.FromImage(Canvas.Image))
				{
					g.DrawImage(originalImage, 0, 0);
				}
			}
			Canvas.Invalidate();
		}
		private void AdvancedColorsButton_Click(object sender, EventArgs e)
		{
			cp.ShowDialog();
			Color c = cp.PickedColor;
			SetButtonColor(ColorButton, c);
			paintingManager.SetDrawingColor(c);
		}
		private void CombButton_Click(object sender, EventArgs e)
		{
			string ww = Interaction.InputBox("\n\nWidth", "Width", "", Left + Width / 2 - 180, Top + Height / 2 - 80);
			string hh = Interaction.InputBox("\n\nHeight", "Height", "", Left + Width / 2 - 180, Top + Height / 2 - 80);
			string bb = Interaction.InputBox("\n\nBase", "Base", "", Left + Width / 2 - 180, Top + Height / 2 - 80);
			string nn = Interaction.InputBox("\n\nN", "N", "", Left + Width / 2 - 180, Top + Height / 2 - 80);
			int w, h, b, n;
			List<Point> points;
			if (int.TryParse(hh, out h) && int.TryParse(ww, out w) && int.TryParse(bb, out b) && int.TryParse(nn, out n))
			{
				paintingManager.InitializePolygonPainter(false);
				paintingManager.SetFirstPolygonPoint(new Point(Canvas.Width / 2 - w / 2, Canvas.Height / 2 + h / 2));
				points = new List<Point>(n + 2);
				points.Add(new Point(Canvas.Width / 2 - w / 2, Canvas.Height / 2 + h / 2));
				points.Add(new Point(Canvas.Width / 2 + w / 2, Canvas.Height / 2 + h / 2));
				double next = Canvas.Width / 2 + w / 2;
				for (int i = 0; i < (n + 1) * 2; i++)
				{
					int x = next - (int)next >= 0.5 ? (int)next + 1 : (int)next;
					if (i % 2 == 0)
						points.Add(new Point(x, Canvas.Height / 2 - h / 2));
					else
						points.Add(new Point(x, Canvas.Height / 2 - h / 2 + (h - b)));
					next -= ((double)w / (2 * n));
				}
				points.RemoveAt(points.Count - 1);
				string name = "Polygon" + polygons.ToString();
				objects.Add(new MyPolygon(points, paintingManager.CurrentDrawingColor, layers, false, name));
				objects.list.Last().PaintMe(Canvas);
				ObjectList.Items.Add("Polygon" + polygons.ToString());
				polygons++;
				layers++;
				Canvas.Invalidate();
			}
		}
		private void PointButton_Click(object sender, EventArgs e)
		{
			ButtonClicked(0);
		}
		private void LineButton_Click(object sender, EventArgs e)
		{
			ButtonClicked(1);
		}
		private void CircleButton_Click(object sender, EventArgs e)
		{
			ButtonClicked(2);
		}
		private void FilledCircleButton_Click(object sender, EventArgs e)
		{
			ButtonClicked(3);
		}
		private void PolygonButton_Click(object sender, EventArgs e)
		{
			ButtonClicked(6);
		}
		private void FilledPolygonButton_Click(object sender, EventArgs e)
		{
			ButtonClicked(7);
		}
		#endregion

		#region - Object list stuff -
		private void ObjectList_SelectedIndexChanged(object sender, EventArgs e)
		{

			if (ObjectList.SelectedItems.Count == 0) return;
			int index = ObjectList.SelectedItems[0].Index;
			Rectangle area = objects.list[index].Area();
			paintingManager.SetSelectedArea(new Rectangle(area.X - 10, area.Y - 10, area.Width + 20, area.Height + 20));
			selected = true;
			soi = index;
			Canvas.Invalidate();
			SetObjectInfo(index);
		}
		private void SetObjectInfo(int i)
		{
			InfoPanelTable.Visible = true;
			MyObject obj = objects.list[i];
			SetObjectIcon(i);
			ObjectName.Text = obj.name;
			ObjectColor.BackColor = obj.color;
			LayerTextBox.Text = (i + 1).ToString();
		}
		private void SetObjectIcon(int i)
		{
			MyObject obj = objects.list[i];
			Bitmap bmp = new Bitmap(30, 30);
			Pen pen = new Pen(obj.color);
			switch (obj.type)
			{
				case MyObject.Type.Point:
					using (Graphics graph = Graphics.FromImage(bmp))
					{
						Rectangle ImageSize = new Rectangle(15, 15, 1, 1);
						graph.DrawRectangle(pen, ImageSize);
					}
					break;
				case MyObject.Type.Line:
					using (Graphics graph = Graphics.FromImage(bmp))
					{
						graph.DrawLine(pen, 1, 1, 29, 29);
					}
					break;
				case MyObject.Type.Circle:
					if ((obj as MyCircle).filled)
					{
						using (Graphics graph = Graphics.FromImage(bmp))
						{
							SolidBrush brush = new SolidBrush(obj.color);
							graph.FillEllipse(brush, 1, 1, 28, 28);
						}
					}
					else
					{
						using (Graphics graph = Graphics.FromImage(bmp))
						{
							graph.DrawEllipse(pen, 1, 1, 28, 28);
						}
					}
					break;
			}
			ObjectIcon.Image = bmp;
		}
		#endregion

		#region - Interacting with obejcts -
		private void ObjectColor_Click(object sender, EventArgs e)
		{
			ColorDialog colorDialog = new ColorDialog();
			if (colorDialog.ShowDialog() == DialogResult.OK)
			{
				ObjectColor.BackColor = colorDialog.Color;
			}
		}
		private void IncreaseLayer_Click(object sender, EventArgs e)
		{
			int i = int.Parse(LayerTextBox.Text);
			if (i >= layers - 1) return;
			else LayerTextBox.Text = (i + 1).ToString();
		}
		private void DecreaseLayer_Click(object sender, EventArgs e)
		{
			int i = int.Parse(LayerTextBox.Text);
			if (i <= 1) return;
			else LayerTextBox.Text = (i - 1).ToString();
		}
		private void UpButton_Click(object sender, EventArgs e)
		{
			LayerTextBox.Text = (layers - 1).ToString();
		}
		private void DownButton_Click(object sender, EventArgs e)
		{
			LayerTextBox.Text = "1";
		}
		private void SubmitButton_Click(object sender, EventArgs e)
		{
			objects.list[soi].UpdateObject(ObjectColor.BackColor, EnableAACheckBox.Checked, ObjectName.Text);
			SetObjectIcon(soi);
			ObjectList.Items[soi].Text = ObjectName.Text;
			int ni = int.Parse(LayerTextBox.Text) - 1;
			ChangeOrder(soi, ni);
			if (ni <= 0)
			{
				ni = 0;
				LayerTextBox.Text = "1";
			}
			else if (ni > layers - 1)
			{
				ni = layers - 2;
				LayerTextBox.Text = (ni + 1).ToString();
			}
			paintingManager.Initialize(gridOn);
			objects.Repaint(Canvas);
			Canvas.Invalidate();
			soi = ni;
		}
		private void ClippingRectangleSize_KeyDown(KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
			{
				if (clippingOn || filtersOn)
				{
					int width, height;
					if (int.TryParse(ClippingRectangleWidth.Text, out width) && int.TryParse(ClippingRectangleHeight.Text, out height))
					{
						if (width > 0 && height > 0)
						{
							int newX = paintingManager.selectedArea.X + (paintingManager.selectedArea.Width - width) / 2;
							int newY = paintingManager.selectedArea.Y + (paintingManager.selectedArea.Height - height) / 2;
							paintingManager.SetSelectedArea(new Rectangle(newX, newY, width, height));
						}
					}
					if (clippingOn) PrepareClippedBitmap();
					else PrepareMagicalRectangle(originalImage, paintingManager.selectedArea);
					Canvas.Invalidate();
				}
			}
		}
		private void ClippingRectangleXSize_KeyDown(object sender, KeyEventArgs e)
		{
			ClippingRectangleSize_KeyDown(e);
		}
		private void ClippingRectangleYSize_KeyDown(object sender, KeyEventArgs e)
		{
			ClippingRectangleSize_KeyDown(e);
		}
		private void Form1_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Delete)
			{
				if (selected)
				{
					objects.Delete(soi);
					ObjectList.Items.RemoveAt(soi);
					InfoPanelTable.Visible = false;
					paintingManager.Initialize(gridOn);
					if (objects.count > 0)
						objects.Repaint(Canvas);
					soi = 0;
					selected = false;
					layers--;
				}
				Canvas.Cursor = Cursors.Default;
			}
		}
		#endregion

		#region - Other -
		private void ChangeOrder(int before, int after)
		{
			objects.ChangeOrder(before, after);
			ChangeListViewOrder(before, after);
		}
		private void ChangeListViewOrder(int before, int after)
		{
			string item = ObjectList.Items[before].Text;
			int k = after > before ? 1 : -1;
			for (int i = before; i != after; i += k)
			{
				ObjectList.Items[i].Text = ObjectList.Items[i + k].Text;
			}
			ObjectList.Items[after].Text = item;
		}
		private bool MouseWithinSelection(int x, int y)
		{
			Rectangle rect = objects.list[soi].Area();
			return x >= rect.Left - 10 && x <= rect.Right + 10 && y >= rect.Top - 10 && y <= rect.Bottom + 10;
		}
		private void MoveSelectedArea(int dx, int dy)
		{
			paintingManager.UpdateSelectedArea(dx, dy);
		}
		private void PrepareClippedBitmap()
		{
			int width, height, gridDensity;
			Bitmap bitmap = null;
			if (int.TryParse(ClippingRectangleWidth.Text, out width) && int.TryParse(ClippingRectangleHeight.Text, out height))
			{
				if (width > 0 && height > 0)
				{
					if (gridOn && int.TryParse(GridDensityTextBox.Text, out gridDensity))
						bitmap = objects.GetCutScene(paintingManager.selectedArea.X, paintingManager.selectedArea.Y, width, height, gridOn, gridDensity, paintingManager.CurrentGridColor);
					else bitmap = objects.GetCutScene(paintingManager.selectedArea.X, paintingManager.selectedArea.Y, width, height);
					using (Graphics g = Graphics.FromImage(bitmap))
					{
						g.DrawRectangle(new Pen(Color.Red), 0, 0, width - 1, height - 1);
					}
					paintingManager.InitializeClippingRectanglePainter(bitmap, paintingManager.selectedArea.X, paintingManager.selectedArea.Y, paintingManager.selectedArea.Width, paintingManager.selectedArea.Height);
				}
			}
		}
		private void ButtonClicked(int i)
		{
			buttons[cbi] = false;
			if (cbi < 8) buttonIcons[cbi].Checked = false;
			cbi = i;
			buttons[cbi] = true;
			buttonIcons[cbi].Checked = true;
		}
		private void GridDensityTextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter && gridOn)
			{
				paintingManager.SetGridDensity(GridDensityTextBox.Text);
				paintingManager.Initialize(true);
				objects.Repaint(Canvas);
			}
		}
		private void Form1_Resize(object sender, EventArgs e)
		{
			paintingManager.Initialize(gridOn);
			objects.Repaint(Canvas);
			if(mode3dON)
			{
				_3DManager.SetCanvasSize(Canvas.Width, Canvas.Height);
			}
		}
		private void DisableButtons()
		{
			for (int i = 0; i < 8; i++)
				buttonIcons[i].Enabled = false;
			BackgroundColorButton.Enabled = false;
			ColorButton.Enabled = false;
			ClearSceneButton.Enabled = false;
			GridButton.Enabled = false;
			GridColorButton.Enabled = false;
			GridDensityTextBox.Enabled = false;
			FillSceneButton.Enabled = false;
			InfoPanelTable.Visible = false;
			ObjectList.Visible = false;
			AdvancedColorsButton.Enabled = false;
			CombButton.Enabled = false;
		}
		private void EnableButtons()
		{
			for (int i = 0; i < 8; i++)
				buttonIcons[i].Enabled = true;
			BackgroundColorButton.Enabled = true;
			ColorButton.Enabled = true;
			ClearSceneButton.Enabled = true;
			GridButton.Enabled = true;
			GridColorButton.Enabled = true;
			GridDensityTextBox.Enabled = true;
			FillSceneButton.Enabled = true;
			InfoPanelTable.Visible = true;
			ObjectList.Visible = true;
			AdvancedColorsButton.Enabled = true;
			CombButton.Enabled = true;
		}
		private void MenuItem_Click(object sender, EventArgs e)
		{
			string item = ((MenuItem)sender).Text;
			if (item == "Add texture")
			{
				OpenFileDialog openFileDialog = new OpenFileDialog();

				openFileDialog.Filter = "All supported graphics |*.jpg; *.jpeg; *.jpe; *.jfif; *.png; *.bmp; *.gif; *.tif | JPEG (*.jpg;*.jpeg) | *.jpg;*.jpeg |Portable Network Graphics (*.png) | *.png";
				openFileDialog.InitialDirectory = @"C:\Users\MarcinZ\Documents\Visual Studio 2017\Projects\GK1\GK1\Images";

				if (openFileDialog.ShowDialog() == DialogResult.OK)
				{
					string path = openFileDialog.FileName;
					Bitmap bmp = new Bitmap(path);
					(objects.list[soi] as MyPolygon).LoadTexture(bmp);
					objects.list[soi].PaintMe(Canvas);
				}
				Canvas.Invalidate();
			}
			else if (item == "Color reduction")
			{
				int k;
				string input = null;
				while (!int.TryParse(input, out k) || k <= 2)
				{
					input = Interaction.InputBox("\n\nNumber should be greater than 2", "Reduce colors", "4", Left + Width / 2 - 180, Top + Height / 2 - 80);
					if (input == "") break;
				}
				if (k > 2)
				{
					(objects.list[soi] as MyPolygon).ReduceColors(k - 1);
					objects.list[soi].PaintMe(Canvas);
					Canvas.Invalidate();
				}
			}
			else if (item == "Rotate")
			{
				move = false;
				rotate = true;
				scale = false;
			}
			else if (item == "Scale")
			{
				move = false;
				scale = true;
				rotate = false;
			}
			else if (item == "Move")
			{
				move = true;
				scale = false;
				rotate = false;
			}
			else
			{
				filterManager.DisableAllEffects();
				PrepareMagicalRectangle(originalImage, paintingManager.selectedArea);
			}

		}

		#endregion

		#endregion

		#region - Zad 3 GUI stuff -

		#region - Initialization - 
		private void FiltersButton_Click(object sender, EventArgs e)
		{
			buttons[cbi] = false;
			if (cbi < 9) buttonIcons[cbi].Checked = false;
			cbi = 10;
			buttons[cbi] = true;
			if (!filtersOn)
			{
				filtersOn = true;
				DisableButtons();
				ClippingToolButton.Enabled = false;
				selected = false;
				ClippingRectangleWidth.Enabled = false;
				ClippingRectangleHeight.Enabled = false;
				originalImage = new Bitmap(Canvas.Width, Canvas.Height);

				using (Graphics g = Graphics.FromImage(originalImage))
				{
					g.DrawImage(Canvas.Image, 0, 0);
				}
				int width, height;
				if (int.TryParse(ClippingRectangleWidth.Text, out width) && int.TryParse(ClippingRectangleHeight.Text, out height))
				{
					if (width > 0 && height > 0)
					{
						Rectangle rect = new Rectangle(Canvas.Width / 2 - width / 2, Canvas.Height / 2 - height / 2, width, height);
						paintingManager.SetStartCoordinates(Canvas.Width / 2 - width / 2, Canvas.Height / 2 - height / 2);
						paintingManager.SetSelectedArea(rect);
						magicalRectangle = new DirectBitmap(width, height);
						PrepareMagicalRectangle(originalImage, rect);
					}
				}
			}
			else
			{
				filtersOn = false;
				EnableButtons();
				ClippingToolButton.Enabled = true;
				ClippingRectangleWidth.Enabled = true;
				ClippingRectangleHeight.Enabled = true;
				magicalRectangle.Dispose();
				using (Graphics g = Graphics.FromImage(Canvas.Image))
				{
					g.DrawImage(originalImage, 0, 0);
				}
				scale = false;
				rotate = false;
			}
			TransitWidth(SplittingLayoutPanel);
			Canvas.Invalidate();

		}
		private void PrepareMagicalRectangle(Bitmap bmp, Rectangle rect, bool mirror = false)
		{
			int width, height;

			if (int.TryParse(ClippingRectangleWidth.Text, out width) && int.TryParse(ClippingRectangleHeight.Text, out height))
			{
				if (width > 0 && height > 0)
				{
					using (Graphics g = Graphics.FromImage(magicalRectangle.Bitmap))
					{
						g.DrawImage(bmp, 0, 0, rect, GraphicsUnit.Pixel);
					}

					if (mirror) filterManager.ChangeMirrorState();
					filterManager.NewBitmap(magicalRectangle);

					magicalRectangle = filterManager.GetModyfiedBitmap();
					magicalRectangle.EqualizeHistogram(EqualizeHistogramRed.Checked, EqualizeHistogramGreen.Checked, EqualizeHistogramBlue.Checked);
					switch (HistogramTabControl.SelectedTab.Text)
					{
						case "R": RHistogramPictureBox.Image = magicalRectangle.GetRHistogram(); break;
						case "G": GHistogramPictureBox.Image = magicalRectangle.GetGHistogram(); break;
						case "B": BHistogramPictureBox.Image = magicalRectangle.GetBHistogram(); break;
					}
					using (Graphics g = Graphics.FromImage(magicalRectangle.Bitmap))
					{
						g.DrawRectangle(new Pen(Color.Blue), 0, 0, width - 1, height - 1);
					}
					paintingManager.InitializeClippingRectanglePainter(magicalRectangle.Bitmap, paintingManager.selectedArea.X, paintingManager.selectedArea.Y, paintingManager.selectedArea.Width, paintingManager.selectedArea.Height);
				}
			}
		}
		#endregion

		#region - Clicking Buttons -
		private void CheckBox7x7_CheckedChanged(object sender, EventArgs e)
		{
			if (CheckBox7x7.Checked)
			{
				CheckBox7x7.Text = "OFF";
				filterManager.EnableMatrixFilter(GridView7x7);
			}
			else
			{
				CheckBox7x7.Text = "ON";
				filterManager.DisableMatrixFilter();
			}
		}
		private void CheckBox5x5_CheckedChanged(object sender, EventArgs e)
		{
			if (CheckBox5x5.Checked)
			{
				CheckBox5x5.Text = "OFF";
				filterManager.EnableMatrixFilter(GridView5x5);
			}
			else
			{
				CheckBox5x5.Text = "ON";
				filterManager.DisableMatrixFilter();
			}
		}
		private void CheckBox3x3_CheckedChanged(object sender, EventArgs e)
		{
			if (CheckBox3x3.Checked)
			{
				CheckBox3x3.Text = "OFF";
				filterManager.EnableMatrixFilter(GridView3x3);
			}
			else
			{
				CheckBox3x3.Text = "ON";
				filterManager.DisableMatrixFilter();
			}
		}
		private void RShowFilterWindowButton_Click(object sender, EventArgs e)
		{
			TransitHeight(RTabLayotPanel);
			if (RShowFilterWindowButton.Text == "Show function filter editor")
			{
				RShowFilterWindowButton.Text = "Show histogram";
			}
			else
			{
				RShowFilterWindowButton.Text = "Show function filter editor";
			}
		}
		private void GShowFilterWindowButton_Click(object sender, EventArgs e)
		{
			TransitHeight(GTabLayotPanel);
			if (GShowFilterWindowButton.Text == "Show function filter editor")
			{
				GShowFilterWindowButton.Text = "Show histogram";
			}
			else
			{
				GShowFilterWindowButton.Text = "Show function filter editor";
			}
		}
		private void BShowFilterWindowButton_Click(object sender, EventArgs e)
		{
			TransitHeight(BTabLayotPanel);
			if (BShowFilterWindowButton.Text == "Show function filter editor")
			{
				BShowFilterWindowButton.Text = "Show histogram";
			}
			else
			{
				BShowFilterWindowButton.Text = "Show function filter editor";
			}
		}
		private void Gaussian5x5Button_Click(object sender, EventArgs e)
		{
			SetGaussian5x5();
		}

		private void Laplacian5x5Button_Click(object sender, EventArgs e)
		{
			SetLaplacian5x5();
		}

		private void Gaussian3x3Button_Click(object sender, EventArgs e)
		{
			SetGaussian3x3();
		}

		private void Laplacian3x3Button_Click(object sender, EventArgs e)
		{
			SetLaplacian3x3();
		}

		private void Gaussian7x7Button_Click(object sender, EventArgs e)
		{
			SetGaussian7x7();
		}

		private void Laplacian7x7Button_Click(object sender, EventArgs e)
		{
			SetLaplacian7x7();
		}
		#endregion

		#region - Function Filter stuff -
		private void RFilterVisualizingMainPB_MouseClick(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				filterFunctionSetter.AddRemoveRPoint(e.X, e.Y);
			}
		}
		private void GFilterVisualizingMainPB_MouseClick(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				filterFunctionSetter.AddRemoveGPoint(e.X, e.Y);
			}
		}
		private void BFilterVisualizingMainPB_MouseClick(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				filterFunctionSetter.AddRemoveBPoint(e.X, e.Y);
			}
		}
		private void RFilterVisualizingMainPB_MouseDown(object sender, MouseEventArgs e)
		{
			RApplyFilterCheckBox.Checked = false;
			if (e.Button == MouseButtons.Left)
			{
				filterFunctionSetter.SetRPointToMove(e.X, e.Y);
				isMouseDown = true;
			}
		}
		private void GFilterVisualizingMainPB_MouseDown(object sender, MouseEventArgs e)
		{
			GApplyFilterCheckBox.Checked = false;
			if (e.Button == MouseButtons.Left)
			{
				filterFunctionSetter.SetGPointToMove(e.X, e.Y);
				isMouseDown = true;
			}
		}
		private void BFilterVisualizingMainPB_MouseDown(object sender, MouseEventArgs e)
		{
			BApplyFilterCheckBox.Checked = false;
			if (e.Button == MouseButtons.Left)
			{
				filterFunctionSetter.SetBPointToMove(e.X, e.Y);
				isMouseDown = true;
			}
		}
		private void RFilterVisualizingMainPB_MouseMove(object sender, MouseEventArgs e)
		{
			if (isMouseDown && filterFunctionSetter.move)
			{
				filterFunctionSetter.MoveRPoint(e.Y);
			}
		}
		private void GFilterVisualizingMainPB_MouseMove(object sender, MouseEventArgs e)
		{
			if (isMouseDown && filterFunctionSetter.move)
			{
				filterFunctionSetter.MoveGPoint(e.Y);
			}
		}
		private void BFilterVisualizingMainPB_MouseMove(object sender, MouseEventArgs e)
		{
			if (isMouseDown && filterFunctionSetter.move)
			{
				filterFunctionSetter.MoveBPoint(e.Y);
			}
		}
		private void RFilterVisualizingMainPB_MouseUp(object sender, MouseEventArgs e)
		{
			if (isMouseDown && filterFunctionSetter.move)
			{
				isMouseDown = false;
				filterFunctionSetter.move = false;
			}
		}
		private void GFilterVisualizingMainPB_MouseUp(object sender, MouseEventArgs e)
		{
			if (isMouseDown && filterFunctionSetter.move)
			{
				isMouseDown = false;
				filterFunctionSetter.move = false;
			}
		}
		private void BFilterVisualizingMainPB_MouseUp(object sender, MouseEventArgs e)
		{
			if (isMouseDown && filterFunctionSetter.move)
			{
				isMouseDown = false;
				filterFunctionSetter.move = false;
			}
		}

		private void RApplyFilterCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (RApplyFilterCheckBox.Checked) filterManager.EnableRedFunctionFilter(filterFunctionSetter.GetRFunction());
			else filterManager.DisableRedFunctionFilter();
		}
		private void GApplyFilterCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (GApplyFilterCheckBox.Checked) filterManager.EnableGreenFunctionFilter(filterFunctionSetter.GetGFunction());
			else filterManager.DisableGreenFunctionFilter();
		}
		private void BApplyFilterCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (BApplyFilterCheckBox.Checked) filterManager.EnableBlueFunctionFilter(filterFunctionSetter.GetBFunction());
			else filterManager.DisableBlueFunctionFilter();
		}
		#endregion

		#region - Other - 
		private void TransitHeight(TableLayoutPanel p)
		{
			if (p.RowStyles[0].Height > 0)
			{
				p.RowStyles[1].Height = p.RowStyles[0].Height;
				p.RowStyles[0].Height = 0;
			}
			else
			{
				p.RowStyles[0].Height = p.RowStyles[1].Height;
				p.RowStyles[1].Height = 0;
			}
		}
		private void TransitWidth(TableLayoutPanel p)
		{
			if (p.ColumnStyles[0].Width > 0)
			{
				p.ColumnStyles[1].Width = p.ColumnStyles[0].Width;
				p.ColumnStyles[0].Width = 0;
			}
			else
			{
				p.ColumnStyles[0].Width = p.ColumnStyles[1].Width;
				p.ColumnStyles[1].Width = 0;
			}
		}
		private void SetLaplacian3x3()
		{
			double[,] values3x3 = new double[,] {{0, -1, 0 },
										  { -1, 5, -1 },
										  { 0, -1, 0 }};
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
					GridView3x3.Rows[i].Cells[j].Value = values3x3[i, j];
			}
		}
		private void SetLaplacian5x5()
		{
			double[,] values5x5 = new double[,] { { -4, -1, 0, -1, -4 },
												{ -1, 2, 3, 2, -1 },
												{ 0, 3, 5, 3, 0 },
												{ -1, 2, 3, 2, -1 },
												{ -4,-1, 0, -1, -4}};
			for (int i = 0; i < 5; i++)
			{
				for (int j = 0; j < 5; j++)
					GridView5x5.Rows[i].Cells[j].Value = values5x5[i, j];
			}
		}
		private void SetLaplacian7x7()
		{
			double[,] values7x7 = new double[,] { { -10, -5, -2, -1, -2, -5, -10 },
												{ -5, 0, 3, 4, 3, 0, -5 },
												{ -2, 3, 6, 7, 6, 3, -2 },
												{ -1, 4, 7, 9, 7, 4, -1 },
												{ -2, 3, 6, 7, 6, 3, -2 },
												{ -5, 0, 3, 4, 3, 0, -5 },
												{ -10, -5, -2, -1, -2, -5, -10}};
			for (int i = 0; i < 7; i++)
			{
				for (int j = 0; j < 7; j++)
					GridView7x7.Rows[i].Cells[j].Value = values7x7[i, j];
			}
		}
		private void SetGaussian3x3()
		{
			double[,] values3x3 = new double[,] {{0, 1, 0 },
												 { 1, 4, 1 },
												 { 0, 1, 0 }};
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
					GridView3x3.Rows[i].Cells[j].Value = values3x3[i, j];
			}
		}
		private void SetGaussian5x5()
		{
			double[,] values5x5 = new double[,] { { 0, 1, 2, 1, 0 },
												{ 1, 4, 8, 4, 1 },
												{ 2, 8, 16, 8, 2 },
												{ 1, 4, 8, 4, 1 },
												{ 0, 1, 2, 1, 0 }};
			for (int i = 0; i < 5; i++)
			{
				for (int j = 0; j < 5; j++)
					GridView5x5.Rows[i].Cells[j].Value = values5x5[i, j];
			}
		}
		private void SetGaussian7x7()
		{
			double[,] values7x7 = new double[,] { { 1, 4, 7, 10, 7, 4, 1 },
												{ 4, 12, 26, 33, 26, 12, 4 },
												{ 7, 26, 55, 71, 55, 26, 7 },
												{ 10, 33, 71, 91, 71, 33, 10},
												{ 7, 26, 55, 71, 55, 26, 7 },
												{ 4, 12, 26, 33, 26, 12, 4 },
												{ 1, 4, 7, 10, 7, 4, 1 }};
			for (int i = 0; i < 7; i++)
			{
				for (int j = 0; j < 7; j++)
					GridView7x7.Rows[i].Cells[j].Value = values7x7[i, j];
			}
		}
		private void MatrixFiltersTabControl_SelectedIndexChanged(object sender, EventArgs e)
		{
			CheckBox3x3.Checked = false;
			CheckBox5x5.Checked = false;
			CheckBox7x7.Checked = false;
		}
		private void SetXYAxes()
		{
			Bitmap RY = new Bitmap(17, 126);
			Bitmap GY = new Bitmap(17, 126);
			Bitmap BY = new Bitmap(17, 126);
			Bitmap RX = new Bitmap(164, 17);
			Bitmap GX = new Bitmap(164, 17);
			Bitmap BX = new Bitmap(164, 17);

			for (int i = 0; i < 126; i++)
			{
				Color r = Color.FromArgb((int)((255.0 / 126) * i), (int)((255.0 / 126) * i), 0, 0);
				Color g = Color.FromArgb((int)((255.0 / 126) * i), 0, (int)((255.0 / 126) * i), 0);
				Color b = Color.FromArgb((int)((255.0 / 126) * i), 0, 0, (int)((255.0 / 126) * i));

				using (Graphics gr = Graphics.FromImage(RY))
				{
					gr.DrawLine(new Pen(r), new Point(0, i), new Point(16, i));
				}
				using (Graphics gg = Graphics.FromImage(GY))
				{
					gg.DrawLine(new Pen(g), new Point(0, i), new Point(16, i));
				}
				using (Graphics gb = Graphics.FromImage(BY))
				{
					gb.DrawLine(new Pen(b), new Point(0, i), new Point(16, i));
				}
			}
			for (int i = 0; i < 164; i++)
			{
				Color r = Color.FromArgb((int)((255.0 / 164) * i), (int)((255.0 / 164) * i), 0, 0);
				Color g = Color.FromArgb((int)((255.0 / 164) * i), 0, (int)((255.0 / 164) * i), 0);
				Color b = Color.FromArgb((int)((255.0 / 164) * i), 0, 0, (int)((255.0 / 164) * i));

				using (Graphics gr = Graphics.FromImage(RX))
				{
					gr.DrawLine(new Pen(r), new Point((164 - i), 0), new Point((164 - i), 16));
				}
				using (Graphics gg = Graphics.FromImage(GX))
				{
					gg.DrawLine(new Pen(g), new Point((164 - i), 0), new Point((164 - i), 16));
				}
				using (Graphics gb = Graphics.FromImage(BX))
				{
					gb.DrawLine(new Pen(b), new Point((164 - i), 0), new Point((164 - i), 16));
				}
			}
			RYAxisForFilterVisPB.Image = RY;
			GYAxisForFilterVisPB.Image = GY;
			BYAxisForFilterVisPB.Image = BY;
			RXAxisForFilterVisPB.Image = RX;
			GXAxisForFilterVisPB.Image = GX;
			BXAxisForFilterVisPB.Image = BX;
		}
		private void GammaEnableCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (GammaEnableCheckBox.Checked == true)
			{
				float gamma;
				if (float.TryParse(GammaTextBox.Text, out gamma))
				{
					if (gamma > 0)
					{
						filterManager.EnableGamma(gamma);
					}
				}
			}
			else
			{
				filterManager.DisableGamma();
			}
		}

		#endregion

		#endregion

		#region - Zad 4 GUI stuff -
		private void Button3D_Click(object sender, EventArgs e)
		{
			MainTable.RowStyles[0].Height = 0;
			paintingManager.Initialize();
			Canvas.Invalidate();
			float a = SplittingLayoutPanel.ColumnStyles[0].Width;
			float b = SplittingLayoutPanel.ColumnStyles[1].Width;
			SplittingLayoutPanel.ColumnStyles[2].Width = a > b ? a : b;
			SplittingLayoutPanel.ColumnStyles[0].Width = 0;
			SplittingLayoutPanel.ColumnStyles[1].Width = 0;
			buttons[cbi] = false;
			filtersOn = false;
		}
		private void Import3DModelButton_Click(object sender, EventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();

			openFileDialog.InitialDirectory = @"C:\Users\MarcinZ\Documents\Visual Studio 2017\Projects\GK1\GK1\3DModels";
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				string path = openFileDialog.FileName;
				string[] p = path.Split('.');
				if (p.Length <= 1 || p[1] != "off") return;

				mode3dON = true;
				_3DManager = new _3dObjectManager(path, Viewing3DTabControl.SelectedIndex,StandardPhongCheckBox.Checked,
					EnableFogCheckBox.Checked,EnableTransparencyCheckBox.Checked,LoadTextureCheckBox.Checked,
					phongManager,FogIntensitySlider.Value/10.0,TransparentCountSlider.Value*10,TransparencyLevelSlider.Value/10.0,
					StandardColorButton.BackColor,FogColorButton.BackColor,BackfaceCullingCheckBox.Checked);
				_3DManager.SetCanvasSize(Canvas.Width, Canvas.Height);
				LoadedTextureLabel.Text = "";
				Canvas.Invalidate();
				LITextBox.Text = _3DManager.GetLightIntensity().ToString();
				GridLITextBox.Text = _3DManager.GetLightIntensity().ToString();
				RandomLITextBox.Text = _3DManager.GetLightIntensity().ToString();
				char s;
				char.TryParse(@"\", out s);
				string[] name = path.Split(s);
				FileNameLabel.Text = name[name.Length - 1];
				GridFileNameLabel.Text = name[name.Length - 1];
				RandomFileNameLabel.Text = name[name.Length - 1];
				ScaleTextBox.Text = 100.ToString();
				GridScaleTextBox.Text = 100.ToString();
				RandomScaleTextBox.Text = 100.ToString();
			}
		}

		#region - PictureBoxes -
		private void StandardPhongPictureBox_MouseClick(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				phongLightningSetter.AddRemoveSLight(e.X, e.Y);
				StandardPhongPictureBox.Invalidate();
			}
		}
		private void RandomPhongPictureBox_MouseClick(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Right)
			{
				phongLightningSetter.AddRemoveRLight(e.X, e.Y);
				StandardPhongPictureBox.Invalidate();
			}
		}
		private void StandardPhongPictureBox_MouseMove(object sender, MouseEventArgs e)
		{
			if (isMouseDown && phongLightningSetter.move)
			{
				phongLightningSetter.MoveSPoint(e.X,e.Y);
				StandardPhongPictureBox.Invalidate();
			}
		}
		private void RandomPhongPictureBox_MouseMove(object sender, MouseEventArgs e)
		{
			if (isMouseDown && phongLightningSetter.move)
			{
				phongLightningSetter.MoveRPoint(e.X, e.Y);
				RandomPhongPictureBox.Invalidate();
			}
		}
		private void StandardPhongPictureBox_Paint(object sender, PaintEventArgs e)
		{
			phongLightningSetter.DrawSLightSource(e.Graphics);
		}
		private void RandomPhongPictureBox_Paint(object sender, PaintEventArgs e)
		{
			phongLightningSetter.DrawRLightSource(e.Graphics);
		}
		private void StandardPhongPictureBox_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				phongLightningSetter.SetSPointToMove(e.X, e.Y);
				isMouseDown = true;
			}
		}
		private void RandomPhongPictureBox_MouseDown(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				phongLightningSetter.SetRPointToMove(e.X, e.Y);
				isMouseDown = true;
			}
		}
		private void StandardPhongPictureBox_MouseUp(object sender, MouseEventArgs e)
		{
			if (isMouseDown && phongLightningSetter.move)
			{
				isMouseDown = false;
				phongLightningSetter.move = false;
			}
		}
		private void RandomPhongPictureBox_MouseUp(object sender, MouseEventArgs e)
		{
			if (isMouseDown && phongLightningSetter.move)
			{
				isMouseDown = false;
				phongLightningSetter.move = false;
			}
		}


		#endregion

		#region - Buttons -
		private void EditStandardPhongButton_Click(object sender, EventArgs e)
		{
			TransitWidth(StandardViewSplittingPanel);
		}
		private void RandomEditPhongButton_Click(object sender, EventArgs e)
		{
			TransitWidth(RandomViewSplittingPanel);
		}
		private void StandardPhongApplyButton_Click(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				double a, s, d, alfa;
				if (double.TryParse(StandardAmbienttextBox.Text, out a) && 
					double.TryParse(StandardDiffuseTextBox.Text, out d) && 
					double.TryParse(StandardSpecularTextBox.Text, out s) &&
					double.TryParse(StandardSpecularExponentTextBox.Text, out alfa))
				{
					List<Point> lights = new List<Point>(phongLightningSetter.StandardLights.Count);
					foreach (Point l in phongLightningSetter.StandardLights)
					{
						lights.Add(new Point(((Canvas.Width * l.X) / StandardPhongPictureBox.Width), ((Canvas.Height * l.Y) / StandardPhongPictureBox.Height)));
					}
					phongManager = new PhongManager(a, s, d, alfa, lights, Canvas.Width / 2, Canvas.Height / 2, StandardAmbientColorButton.BackColor, StandardSpecularColorButton.BackColor, StandardDiffuseColorButton.BackColor);
				}
				_3DManager.EffectManager.pm = phongManager;
				Canvas.Invalidate();
			}
			TransitWidth(StandardViewSplittingPanel);
		}
		private void RandomPhongApplyButton_Click(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				double a, s, d, alfa;
				if (double.TryParse(StandardAmbienttextBox.Text, out a) &&
					double.TryParse(StandardDiffuseTextBox.Text, out d) &&
					double.TryParse(StandardSpecularTextBox.Text, out s) &&
					double.TryParse(StandardSpecularExponentTextBox.Text, out alfa))
				{
					List<Point> lights = new List<Point>(phongLightningSetter.RandomLights.Count);
					foreach (Point l in phongLightningSetter.RandomLights)
					{
						lights.Add(new Point((Canvas.Width * l.X / RandomPhongPictureBox.Width), (Canvas.Height * l.Y / RandomPhongPictureBox.Height)));
					}
					phongManager = new PhongManager(a, s, d,alfa, lights, Canvas.Width / 2, Canvas.Height / 2, RandomAmbientColorButton.BackColor, RandomSpecularColorButton.BackColor, RandomDiffuseColorButton.BackColor);
				}
				_3DManager.EffectManager.pm = phongManager;
				Canvas.Invalidate();
			}
			TransitWidth(RandomViewSplittingPanel);
		}
		private void StandardColorButton_Click(object sender, EventArgs e)
		{
			ChangeColor(StandardColorButton);
			Grid3DColorButton.BackColor = StandardColorButton.BackColor;
		}
		private void Grid3DColorButton_Click(object sender, EventArgs e)
		{
			ChangeColor(Grid3DColorButton);
			StandardColorButton.BackColor = Grid3DColorButton.BackColor;
		}
		private void LoadTextureButton_Click(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				OpenFileDialog openFileDialog = new OpenFileDialog();

				openFileDialog.Filter = "All supported graphics |*.jpg; *.jpeg; *.jpe; *.jfif; *.png; *.bmp; *.gif; *.tif | JPEG (*.jpg;*.jpeg) | *.jpg;*.jpeg |Portable Network Graphics (*.png) | *.png";
				openFileDialog.InitialDirectory = @"C:\Users\MarcinZ\Documents\Visual Studio 2017\Projects\GK1\GK1\Images";

				if (openFileDialog.ShowDialog() == DialogResult.OK)
				{
					string path = openFileDialog.FileName;
					Bitmap bmp = new Bitmap(path);
					_3DManager.LoadTexture(bmp);
					_3DManager.EffectManager.texture = LoadTextureCheckBox.Checked;
					char s;
					char.TryParse(@"\", out s);
					string[] name = path.Split(s);
					LoadedTextureLabel.Text = name[name.Length - 1];
					Canvas.Invalidate();
				}
			}
		}
		private void FogColorButton_Click(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				ColorDialog colorDialog = new ColorDialog();
				if (colorDialog.ShowDialog() == DialogResult.OK && mode3dON)
				{
					_3DManager.EffectManager.fogC = colorDialog.Color;
					FogColorButton.BackColor = colorDialog.Color;
					if (EnableFogCheckBox.Checked) Canvas.Invalidate();
				}
			}
		}
		private void StandardDiffuseColorButton_Click(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				ColorDialog colorDialog = new ColorDialog();
				if (colorDialog.ShowDialog() == DialogResult.OK)
				{
					StandardDiffuseColorButton.BackColor = colorDialog.Color;
					RandomDiffuseColorButton.BackColor = colorDialog.Color;
				}
			}
		}
		private void StandardSpecularColorButton_Click(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				ColorDialog colorDialog = new ColorDialog();
				if (colorDialog.ShowDialog() == DialogResult.OK)
				{
					StandardSpecularColorButton.BackColor = colorDialog.Color;
					RandomSpecularColorButton.BackColor = colorDialog.Color;
				}
			}
		}
		private void StandardAmbientColorButton_Click(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				ColorDialog colorDialog = new ColorDialog();
				if (colorDialog.ShowDialog() == DialogResult.OK)
				{
					StandardAmbientColorButton.BackColor = colorDialog.Color;
					RandomAmbientColorButton.BackColor = colorDialog.Color;
				}
			}
		}
		private void RandomDiffuseColorButton_Click(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				ColorDialog colorDialog = new ColorDialog();
				if (colorDialog.ShowDialog() == DialogResult.OK)
				{
					StandardDiffuseColorButton.BackColor = colorDialog.Color;
					RandomDiffuseColorButton.BackColor = colorDialog.Color;
				}
			}
		}
		private void RandomSpecularColorButton_Click(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				ColorDialog colorDialog = new ColorDialog();
				if (colorDialog.ShowDialog() == DialogResult.OK)
				{
					StandardSpecularColorButton.BackColor = colorDialog.Color;
					RandomSpecularColorButton.BackColor = colorDialog.Color;
				}
			}
		}
		private void RandomAmbientColorButton_Click(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				ColorDialog colorDialog = new ColorDialog();
				if (colorDialog.ShowDialog() == DialogResult.OK)
				{
					StandardAmbientColorButton.BackColor = colorDialog.Color;
					RandomAmbientColorButton.BackColor = colorDialog.Color;
				}
			}
		}
		#endregion

		#region - CheckBoxes -
		private void StandardPhongCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				_3DManager.EffectManager.phong = StandardPhongCheckBox.Checked;
				RandomPhongCheckBox.Checked = StandardPhongCheckBox.Checked;
				Canvas.Invalidate();
			}
		}
		private void RandomPhongCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				_3DManager.EffectManager.phong = RandomPhongCheckBox.Checked;
				StandardPhongCheckBox.Checked = RandomPhongCheckBox.Checked;
				Canvas.Invalidate();
			}
		}
		private void LoadTextureCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				_3DManager.EffectManager.texture = LoadTextureCheckBox.Checked;
				Canvas.Invalidate();
			}
		}
		private void EnableFogCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				_3DManager.SetFog(EnableFogCheckBox.Checked, FogColorButton.BackColor, FogIntensitySlider.Value / 10.0);
				Canvas.Invalidate();
			}
		}
		private void EnableTransparencyCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				_3DManager.EffectManager.transparency = EnableTransparencyCheckBox.Checked;
				Canvas.Invalidate();
			}
		}
		private void BackfaceCullingCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				_3DManager.bfc = BackfaceCullingCheckBox.Checked;
				Canvas.Invalidate();
			}
		}
		#endregion

		#region - TextBoxes - 
		private void LITextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
			{
				LIChanged(LITextBox);
				RandomLITextBox.Text = LITextBox.Text;
				GridLITextBox.Text = LITextBox.Text;
			}
		}
		private void GridLITextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
			{
				LIChanged(GridLITextBox);
				LITextBox.Text = GridLITextBox.Text;
				RandomLITextBox.Text = GridLITextBox.Text;
			}
		}
		private void RandomLITextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
			{
				LIChanged(RandomLITextBox);
				LITextBox.Text = RandomLITextBox.Text;
				GridLITextBox.Text = RandomLITextBox.Text;
			}
		}
		private void ScaleTextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
			{
				ScaleChanged(ScaleTextBox);
			}
		}
		private void GridScaleTextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
			{
				ScaleChanged(GridScaleTextBox);
			}
		}
		private void RandomScaleTextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Enter)
			{
				ScaleChanged(RandomScaleTextBox);
			}
		}

		#endregion

		#region - Other -
		private void Canvas_MouseHover(object sender, EventArgs e)
		{
			Canvas.Focus();
		}
		private void Canvas_MouseWheel(object sender, MouseEventArgs e)
		{
			if (mode3dON)
			{
				int s = _3DManager.WheelScale(e.Delta);
				ScaleTextBox.Text = s.ToString();
				GridScaleTextBox.Text = s.ToString();
				RandomScaleTextBox.Text = s.ToString();
				Canvas.Invalidate();
			}
		}
		private void LIChanged(TextBox tb)
		{
			if (mode3dON)
			{
				int li;
				if (int.TryParse(tb.Text, out li))
				{
					if (li >= 0 && li <= 100)
					{
						_3DManager.SetLightIntensity(li);
						using (Graphics g = Graphics.FromImage(Canvas.Image))
						{
							_3DManager.Paint(Canvas.Width, Canvas.Height, g);
						}
						Canvas.Invalidate();
					}
				}
			}
		}
		private void ScaleChanged(TextBox tb)
		{
			if (mode3dON)
			{
				float s;
				if (float.TryParse(tb.Text, out s))
				{
					if (s > 0 && s <= 1000)
					{
						_3DManager.Scale(s / 100);
						Canvas.Invalidate();
					}
				}
			}
		}
		private void ChangeColor(Button b)
		{
			ColorDialog colorDialog = new ColorDialog();
			if (colorDialog.ShowDialog() == DialogResult.OK && mode3dON)
			{
				b.BackColor = colorDialog.Color;
				if (mode3dON)
				{
					_3DManager.EffectManager.color = colorDialog.Color;
					Canvas.Invalidate();
				}
			}
		}
		private void Viewing3DTabControl_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				_3DManager.EffectManager.tabIndex = Viewing3DTabControl.SelectedIndex;
				Canvas.Invalidate();
			}
		}
		#endregion

		#region - Sliders -
		private void FogIntensitySlider_ValueChanged(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				_3DManager.EffectManager.fogI = FogIntensitySlider.Value / 10.0;
				if (EnableFogCheckBox.Checked) Canvas.Invalidate();
			}
		}
		private void TransparencyLevelSlider_ValueChanged(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				_3DManager.EffectManager.transI = TransparencyLevelSlider.Value / 10.0;
				if (EnableTransparencyCheckBox.Checked) Canvas.Invalidate();
			}
		}
		private void TransparentCountSlider_ValueChanged(object sender, EventArgs e)
		{
			if (mode3dON)
			{
				_3DManager.EffectManager.transC = TransparentCountSlider.Value * 10;
				if (EnableTransparencyCheckBox.Checked) Canvas.Invalidate();
			}
		}
		#endregion

		private void BackTo2DButton_Click(object sender, EventArgs e)
		{
			MainTable.RowStyles[0].Height = 30;
			paintingManager.Initialize();
			float a = SplittingLayoutPanel.ColumnStyles[2].Width;
			SplittingLayoutPanel.ColumnStyles[2].Width = 0;
			SplittingLayoutPanel.ColumnStyles[0].Width = a;
			SplittingLayoutPanel.ColumnStyles[1].Width = 0;
			mode3dON = false;
			buttons[cbi] = true;
		}
	}

	#endregion
}
