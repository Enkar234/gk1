﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace GK1
{
	abstract class Painter
	{
		public int startX, startY, endX, endY;
		public Color color;
		public bool filled { get; set; }
		public bool aa;
		public Painter(int sx, int sy, int ex, int ey, Color c, bool a = false, bool f = false)
		{
			startX = sx;
			startY = sy;
			endX = ex;
			endY = ey;
			color = c;
			aa = a;
			filled = f;
		}
		public void Paint(Graphics g, Bitmap bmp = null)
		{
			if (aa) AAPaint(g);
			else SimplePaint(g);
		}
		public void PutPixel(int x, int y, float c, Graphics graphics, Color? clr = null)
		{
			if (c < 0 || c > 1) return;
			Color newColor = Color.FromArgb((int)((1 - c) * 255), color.R, color.G, color.B);
			if (clr.HasValue)
			{
				newColor = clr.Value;
			}
			SolidBrush brush = new SolidBrush(newColor);
			graphics.FillRectangle(brush, x, y, 1, 1);
		}
		public abstract void Update(Color c, bool a, Bitmap bmp = null);
		public abstract void Move(int dx, int dy);
		public abstract void Drag(int sx, int sy, int ex, int ey);
		public abstract void SimplePaint(Graphics g);
		public abstract void AAPaint(Graphics g);
	}
	class LineDrawer : Painter
	{
		public LineDrawer(int sx, int sy, int ex, int ey, bool aa, Color c) : base(sx, sy, ex, ey, c, aa) { }
		public override void SimplePaint(Graphics g)
		{
			int dx = Math.Abs(endX - startX);
			int dy = Math.Abs(endY - startY);
			int kx = endX > startX ? 1 : -1;
			int ky = endY > startY ? 1 : -1;
			int d = dx > dy ? 2 * dy - dx : 2 * dx - dy;
			int incrE = dx > dy ? 2 * dy : 2 * dx;
			int incrNE = dx > dy ? 2 * (dy - dx) : 2 * (dx - dy);

			int x = startX;
			int y = startY;
			PutPixel(x, y, 0, g);

			while (x != endX || y != endY)
			{
				if (d < 0)
				{
					d += incrE;
					if (dx > dy) x += kx;
					else y += ky;
				}
				else
				{
					int b = Math.Abs(incrE - incrNE);
					PutPixel(x, y, 0, g);
					d += incrNE;
					x += kx;
					y += ky;
				}
				PutPixel(x, y, 0, g);
			}
		}
		public override void AAPaint(Graphics g)
		{
			int kx = endX > startX ? 1 : -1;
			int ky = endY > startY ? 1 : -1;

			if (Math.Abs(endX - startX) > Math.Abs(endY - startY))
			{
				float y = startY;
				float m = (float)Math.Abs(endY - startY) / Math.Abs(endX - startX);
				for (int x = startX; x != endX; x += kx)
				{
					float c = y - (float)Math.Floor(y);
					if (startY > endY)
					{
						if (c != 0) PutPixel(x, (int)y, 1 - c, g);
						PutPixel(x, (int)y + ky, c, g);
					}
					else
					{
						PutPixel(x, (int)y, c, g);
						if (c != 0) PutPixel(x, (int)y + ky, 1 - c, g);
					}
					y += m * ky;
				}
			}
			else
			{
				float x = startX;
				float m = (float)Math.Abs(endX - startX) / Math.Abs(endY - startY);
				for (int y = startY; y != endY; y += ky)
				{
					float c = x - (int)x;
					if (startX > endX)
					{
						if (c != 0) PutPixel((int)x, y, 1 - c, g);
						PutPixel((int)x + kx, y, c, g);
					}
					else
					{
						PutPixel((int)x, y, c, g);
						if (c != 0) PutPixel((int)x + kx, y, 1 - c, g);
					}
					x += m * kx;
				}
			}
		}
		public override void Move(int dx, int dy)
		{
			startX += dx;
			startY += dy;
			endX += dx;
			endY += dy;
		}
		public override void Drag(int sx, int sy, int ex, int ey)
		{
			startX = sx;
			startY = sy;
			endX = ex;
			endY = ey;
		}
		public override void Update(Color c, bool a, Bitmap bmp = null)
		{
			color = c;
			aa = a;
		}
	}
	class CircleDrawer : Painter
	{
		public CircleDrawer(int sx, int sy, int ex, int ey, Color c, bool aa, bool f) : base(sx, sy, ex, ey, c, aa, f) { }
		public override void SimplePaint(Graphics g)
		{
			if (filled)
			{
				g.FillEllipse(new SolidBrush(color), new Rectangle(startX, startY, Math.Abs(startX - endX), Math.Abs(startX - endX)));
				return;
			}

			//g.DrawEllipse(new Pen(color), new Rectangle(startX, startY, Math.Abs(startX - endX), Math.Abs(startX - endX)));

			int R = Math.Abs(startX - endX) / 2;
			int deltaE = 3;
			int deltaSE = 5 - 2 * R;
			int d = 1 - R;
			int x = 0;
			int y = R;
			int x0 = startX + R;
			int y0 = startY + R;

			PutPixel(x0 + R, y0, 0, g);
			PutPixel(x0, y0 - R, 0, g);
			PutPixel(x0 - R, y0, 0, g);
			PutPixel(x0, y0 + R, 0, g);
			while (y >= x)
			{
				if (d < 0)
				{
					d += deltaE;
					deltaE += 2;
					deltaSE += 2;
				}
				else
				{
					d += deltaSE;
					deltaE += 2;
					deltaSE += 4;
					y--;
				}
				x++;
				PutPixel(x0 + x, y0 + y, 0, g);
				PutPixel(x0 + y, y0 + x, 0, g);
				PutPixel(x0 - y, y0 + x, 0, g);
				PutPixel(x0 - x, y0 + y, 0, g);
				PutPixel(x0 - x, y0 - y, 0, g);
				PutPixel(x0 - y, y0 - x, 0, g);
				PutPixel(x0 + y, y0 - x, 0, g);
				PutPixel(x0 + x, y0 - y, 0, g);
			}
		}
		public override void AAPaint(Graphics g)
		{
			if (filled)
			{
				g.FillEllipse(new SolidBrush(color), startX, startY, Math.Abs(endX - startX), Math.Abs(endX - startX));
			}

			int R = Math.Abs(startX - endX) / 2;
			int x = R;
			int y = 0;
			float T = 0;
			int x0 = startX + R;
			int y0 = startY + R;

			PutPixel(x0 + R, y0, 0, g);
			PutPixel(x0, y0 - R, 0, g);
			PutPixel(x0 - R, y0, 0, g);
			PutPixel(x0, y0 + R, 0, g);

			while (x > y)
			{
				y++;
				float D = (float)Math.Ceiling(Math.Sqrt(R * R - y * y)) - (float)Math.Sqrt(R * R - y * y);
				if (D < T)
					x--;
				PutPixel(x0 + x - 1, y0 + y, 1 - D, g);
				PutPixel(x0 + x, y0 + y, D, g);

				PutPixel(x0 + y, y0 + x - 1, 1 - D, g);
				PutPixel(x0 + y, y0 + x, D, g);

				PutPixel(x0 - y, y0 + x - 1, 1 - D, g);
				PutPixel(x0 - y, y0 + x, D, g);

				PutPixel(x0 - x + 1, y0 + y, 1 - D, g);
				PutPixel(x0 - x, y0 + y, D, g);

				PutPixel(x0 - x, y0 - y, D, g);
				PutPixel(x0 - x + 1, y0 - y, 1 - D, g);

				PutPixel(x0 - y, y0 - x, D, g);
				PutPixel(x0 - y, y0 - x + 1, 1 - D, g);

				PutPixel(x0 + y, y0 - x, D, g);
				PutPixel(x0 + y, y0 - x + 1, 1 - D, g);

				PutPixel(x0 + x, y0 - y, D, g);
				PutPixel(x0 + x - 1, y0 - y, 1 - D, g);
				T = D;
			}
		}
		public override void Move(int dx, int dy)
		{
			startX += dx;
			startY += dy;
			endX += dx;
			endY += dy;
		}
		public override void Drag(int sx, int sy, int ex, int ey)
		{
			startX = sx;
			startY = sy;
			endX = ex;
			endY = ey;
		}
		public override void Update(Color c, bool a, Bitmap bmp = null)
		{
			color = c;
			aa = a;
		}
	}
	class PointDrawer : Painter
	{
		public PointDrawer(int sx, int sy, int ex, int ey, Color c) : base(sx, sy, ex, ey, c) { }
		public override void SimplePaint(Graphics g)
		{
			SolidBrush brush = new SolidBrush(color);
			using (Graphics gr = g)
			{
				gr.FillRectangle(brush, startX, startY, 1, 1);
			}
		}
		public override void AAPaint(Graphics g)
		{
			SimplePaint(g);
		}
		public override void Move(int dx, int dy)
		{
			startX += dx;
			startY += dy;
			endX += dx;
			endY += dy;
		}
		public override void Drag(int sx, int sy, int ex, int ey)
		{
			startX = sx;
			startY = sy;
			endX = ex;
			endY = ey;
		}
		public override void Update(Color c, bool a, Bitmap bmp = null)
		{
			color = c;
			aa = a;
		}
	}
	class PolygonDrawer : Painter
	{
		List<Point> points;
		public List<Painter> lineDrawers;
		Bitmap bitmap;
		EdgeList ET;
		EdgeList ETcopy;
		Pen pen;
		int miniX;
		int miniY;
		int height;
		public PolygonDrawer(List<Point> ppoints, Color c, bool f) : base(ppoints.First().X, ppoints.First().Y, ppoints.Last().X, ppoints.Last().Y, c, false, f)
		{
			int count = ppoints.Count;
			if (count > 1)
			{
				lineDrawers = new List<Painter>(count);
				points = new List<Point>(count);
				pen = new Pen(c);
				for (int i = 0; i < count; i++)
				{
					lineDrawers.Add(new LineDrawer(ppoints[i].X, ppoints[i].Y, ppoints[(i + 1) % count].X, ppoints[(i + 1) % count].Y, aa, c));
					points.Add(ppoints[i]);
				}
				if (f)
				{
					PrepareBitmap();
				}
			}
		}
		public override void SimplePaint(Graphics g)
		{
			if (filled)
			{
				g.DrawImage(bitmap as Image, new PointF(miniX, miniY));
			}
			else
			{
				foreach (Painter painter in lineDrawers)
				{
					painter.Paint(g);
				}
			}
		}
		public override void AAPaint(Graphics g)
		{
			foreach (Painter painter in lineDrawers)
			{
				painter.Paint(g);
			}
		}
		public override void Move(int dx, int dy)
		{
			foreach (Painter painter in lineDrawers)
			{
				painter.Move(dx, dy);
			}
			for (int i = 0; i < points.Count; i++)
			{
				points[i] = new Point(points[i].X + dx, points[i].Y + dy);
			}
			miniX += dx;
			miniY += dy;
		}
		public override void Drag(int sx, int sy, int ex, int ey)
		{

		}
		public override void Update(Color c, bool a, Bitmap bmp = null)
		{
			color = c;
			pen = new Pen(color);
			filled = a;
			if (!filled)
			{
				foreach (Painter painter in lineDrawers)
				{
					painter.Update(c, aa);
				}
			}
			if (a)
			{
				PrepareBitmap();
			}
		}
		public void LoadTexture(Bitmap bmp)
		{
			PrepareBitmap(bmp);
		}
		public void ReduceColors(int k)
		{
			List<List<(int from, int to)>> lines = GetLines();
			Octree tree = OctreeBuilder.PrepareOctree(lines, bitmap);
			tree = OctreeBuilder.ReduceToK(tree, k);
			PaintWithReducedPalette(lines, tree);
		}
		private void PaintWithReducedPalette(List<List<(int from, int to)>> lines, Octree tree)
		{
			for (int y = 0; y < lines.Count; y++)
			{
				for (int p = 0; p < lines[y].Count; p++)
				{
					for (int j = lines[y][p].from; j <= lines[y][p].to; j++)
					{
						Color c = bitmap.GetPixel(j, y);
						Color rc = OctreeBuilder.ReducedColor(c, tree, 7);
						using (Graphics gr = Graphics.FromImage(bitmap))
						{
							gr.FillRectangle(new SolidBrush(rc), j, y, 1, 1);
						}
					}
				}
			}
		}
		private void PrepareBitmap(Bitmap bmp = null)
		{
			int count = points.Count;
			int minX = points.Min(e => e.X);
			int maxX = points.Max(e => e.X);
			int minY = points.Min(e => e.Y);
			int maxY = points.Max(e => e.Y);
			height = Math.Abs(maxY - minY) + 1;
			miniX = minX;
			miniY = minY;
			bitmap = new Bitmap(Math.Abs(maxX - minX) + 1, Math.Abs(maxY - minY) + 1);

			ET = new EdgeList(height);

			for (int i = 0; i < count; i++)
			{
				int Ymax = Math.Max(points[i].Y, points[(i + 1) % count].Y) - miniY;
				int X;
				if (points[i].Y < points[(i + 1) % count].Y)
				{
					X = points[i].X;
				}
				else if (points[i].Y > points[(i + 1) % count].Y)
				{
					X = points[(i + 1) % count].X;
				}
				else X = points[i].X < points[(i + 1) % count].X ? points[i].X : points[(i + 1) % count].X;
				X -= miniX;
				float cc;
				if (points[(i + 1) % count].Y - points[i].Y == 0)
					cc = float.MaxValue;
				else
					cc = (float)(points[(i + 1) % count].X - points[i].X) / (points[(i + 1) % count].Y - points[i].Y);
				Edge newEdge = new Edge(Ymax, X, cc);

				int Ymin = Math.Min(points[i].Y, points[(i + 1) % count].Y) - miniY;

				if (ET[Ymin] == null) ET[Ymin] = new List<Edge>();
				ET[Ymin].Add(newEdge);
				ET[Ymin].Sort((a1, a2) => a1.Ymax - a2.Ymax);
			}
			ETcopy = new EdgeList(ET);
			FillPolygon(bmp);
			FillEdges(bmp);
		}
		private List<List<(int from, int to)>> GetLines()
		{
			List<List<(int from, int to)>> lines = new List<List<(int from, int to)>>();
			int y = 0;
			List<Edge> AET = new List<Edge>();
			ET = new EdgeList(ETcopy);
			while (ET.Count > 0 || AET.Count > 0)
			{
				lines.Add(new List<(int from, int to)>());
				if (ET[y] != null)
				{
					AET.AddRange(ET[y]);
					ET[y] = null;
				}
				for (int i = 0; i < AET.Count; i++)
				{
					if (AET[i].Ymax == y)
					{
						AET.RemoveAt(i);
						i--;
					}
				}
				AET.Sort((a1, a2) => (int)(a1.X - a2.X));

				for (int i = 0; i < AET.Count; i += 2)
				{
					int X1 = AET[i].X - (int)AET[i].X >= 0.5 ? (int)AET[i].X + 1 : (int)AET[i].X;
					int X2 = AET[(i + 1) % AET.Count].X - (int)AET[(i + 1) % AET.Count].X >= 0.5 ? (int)AET[(i + 1) % AET.Count].X + 1 : (int)AET[(i + 1) % AET.Count].X;
					lines[y].Add((X1, X2));
				}

				for (int i = 0; i < AET.Count; i++)
				{
					AET[i].X += AET[i].c;
				}
				y++;
			}
			return lines;
		}
		private void FillEdges(Bitmap bmp)
		{
			int n = points.Count;
			for (int i = 0; i < n; i++)
			{
				int startX = points[i].X - miniX;
				int startY = points[i].Y - miniY;
				int endX = points[(i + 1) % n].X - miniX;
				int endY = points[(i + 1) % n].Y - miniY;

				int dx = Math.Abs(endX - startX);
				int dy = Math.Abs(endY - startY);
				int kx = endX > startX ? 1 : -1;
				int ky = endY > startY ? 1 : -1;
				int d = dx > dy ? 2 * dy - dx : 2 * dx - dy;
				int incrE = dx > dy ? 2 * dy : 2 * dx;
				int incrNE = dx > dy ? 2 * (dy - dx) : 2 * (dx - dy);

				int x = startX;
				int y = startY;
				Color c = color;
				SolidBrush brush;
				if (bmp != null && x < bmp.Width && y < bmp.Height)
				{
					c = bmp.GetPixel(x, y);
					brush = new SolidBrush(c);

				}
				else
				{
					brush = new SolidBrush(color);
				}
				using (Graphics gr = Graphics.FromImage(bitmap))
				{
					gr.FillRectangle(brush, x, y, 1, 1);
				}

				while (x != endX || y != endY)
				{
					if (d < 0)
					{
						d += incrE;
						if (dx > dy) x += kx;
						else y += ky;
					}
					else
					{
						int b = Math.Abs(incrE - incrNE);
						if (bmp != null && x < bmp.Width && y < bmp.Height)
						{
							c = bmp.GetPixel(x, y);
							brush = new SolidBrush(c);

						}
						else
						{
							brush = new SolidBrush(color);
						}
						using (Graphics gr = Graphics.FromImage(bitmap))
						{
							gr.FillRectangle(brush, x, y, 1, 1);
						}
						d += incrNE;
						x += kx;
						y += ky;
					}
					if (bmp != null && x < bmp.Width && y < bmp.Height)
					{
						c = bmp.GetPixel(x, y);
						brush = new SolidBrush(c);

					}
					else
					{
						brush = new SolidBrush(color);
					}
					using (Graphics gr = Graphics.FromImage(bitmap))
					{
						gr.FillRectangle(brush, x, y, 1, 1);
					}
				}
			}
		}
		private void FillPolygon(Bitmap bmp = null)
		{
			List<List<(int from, int to)>> lines = GetLines();

			for (int y = 0; y < lines.Count; y++)
			{
				using (Graphics gr = Graphics.FromImage(bitmap))
				{

					if (bmp == null)
					{
						for (int p = 0; p < lines[y].Count; p++)
						{
							gr.DrawLine(pen, new Point(lines[y][p].from, y), new Point(lines[y][p].to, y));
						}
					}
					else
					{
						for (int p = 0; p < lines[y].Count; p++)
						{
							int j = lines[y][p].from;
							for (; j <= lines[y][p].to; j++)
							{
								if (bmp.Width > j && bmp.Height > y)
								{
									Color c = bmp.GetPixel(j, y);
									SolidBrush textureBrush = new SolidBrush(c);

									gr.FillRectangle(textureBrush, j, y, 1, 1);
								}
								else break;
							}
							if (j < lines[y][p].to) gr.DrawLine(pen, new Point(j, y), new Point(lines[y][p].to, y));
						}
					}

				}
			}
		}
	}
	class ClippingRectangle : Painter
	{
		public Bitmap Image;
		public ClippingRectangle(Bitmap bmp, int sx, int sy, int width, int height) : base(sx, sy, sx + width, sy + height, Color.Transparent, false)
		{
			Image = bmp;
		}
		public override void AAPaint(Graphics g)
		{
			throw new NotImplementedException();
		}
		public override void Drag(int sx, int sy, int ex, int ey)
		{

		}
		public override void Move(int dx, int dy)
		{
			startX += dx;
			startY += dy;
			endX += dx;
			endY += dy;
		}
		public override void SimplePaint(Graphics g)
		{
			g.DrawImage(Image, startX, startY);
		}
		public override void Update(Color c, bool a, Bitmap bmp = null)
		{
			Image = bmp;
		}
	}
}
