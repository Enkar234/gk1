﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GK1
{
	class FilterFunctionSetter
	{
		List<Point> RPoints;
		List<Point> GPoints;
		List<Point> BPoints;
		PictureBox RPB;
		PictureBox GPB;
		PictureBox BPB;
		int RpointToMove;
		int GpointToMove;
		int BpointToMove;
		public bool move=false;
		public FilterFunctionSetter(PictureBox r, PictureBox g, PictureBox b)
		{
			RPoints = new List<Point>();
			GPoints = new List<Point>();
			BPoints = new List<Point>();
			Point s = new Point(0, 123);
			Point e = new Point(160, 0);
			RPoints.Add(s);
			RPoints.Add(e);
			GPoints.Add(s);
			GPoints.Add(e);
			BPoints.Add(s);
			BPoints.Add(e);
			RPB = r;
			GPB = g;
			BPB = b;
			RPB.Image = new Bitmap(164, 126); 
			GPB.Image = new Bitmap(164, 126); 
			BPB.Image = new Bitmap(164, 126);
			DrawRPolyline();
			DrawGPolyline();
			DrawBPolyline();
		}
		private void DrawRPolyline()
		{
			Clear(RPB);
			using (Graphics g = Graphics.FromImage(RPB.Image))
			{
				Pen p = new Pen(Color.Black);
				SolidBrush sb = new SolidBrush(Color.Black);
				for (int i = 0; i < RPoints.Count - 1; i++)
				{
					g.FillEllipse(sb, RPoints[i].X-1, RPoints[i].Y-1, 3,3);
					g.FillEllipse(sb, RPoints[i+1].X-1, RPoints[i+1].Y-1, 3, 3);
					g.DrawLine(p, RPoints[i], RPoints[i + 1]);
				}
			}
			RPB.Invalidate();
		}
		private void DrawGPolyline()
		{
			Clear(GPB);
			using (Graphics g = Graphics.FromImage(GPB.Image))
			{
				Pen p = new Pen(Color.Black);
				SolidBrush sb = new SolidBrush(Color.Black);
				for (int i = 0; i < GPoints.Count - 1; i++)
				{
					g.FillEllipse(sb, GPoints[i].X-1, GPoints[i].Y-1, 3, 3);
					g.FillEllipse(sb, GPoints[i + 1].X-1, GPoints[i + 1].Y-1, 3, 3);
					g.DrawLine(p, GPoints[i], GPoints[i + 1]);
				}
			}
			GPB.Invalidate();
		}
		private void DrawBPolyline()
		{
			Clear(BPB);
			using (Graphics g = Graphics.FromImage(BPB.Image))
			{
				Pen p = new Pen(Color.Black);
				SolidBrush sb = new SolidBrush(Color.Black);
				for (int i = 0; i < BPoints.Count - 1; i++)
				{
					g.FillEllipse(sb, BPoints[i].X-1, BPoints[i].Y-1, 3, 3);
					g.FillEllipse(sb, BPoints[i + 1].X-1, BPoints[i + 1].Y-1, 3, 3);
					g.DrawLine(p, BPoints[i], BPoints[i + 1]);
				}
			}
			BPB.Invalidate();
		}
		private void Clear(PictureBox pb)
		{
			using (Graphics g = Graphics.FromImage(pb.Image))
			{
				g.FillRectangle(new SolidBrush(Color.White), 0, 0, 164, 126);
			}
		}
		public void AddRemoveRPoint(int x, int y)
		{
			(bool? remove, int indx) = ToRemove(RPoints, x, y);
			if (!remove.HasValue)
			{
				RPoints[indx] = new Point(x, y);
			}
			else
			{
				if (remove.Value)
				{
					RPoints.RemoveAt(indx);
				}
				if (!remove.Value)
				{
					int index = FindIndexToInsert(RPoints, x);
					RPoints.Insert(index, new Point(x, y));
				}
			}
			DrawRPolyline();
		}
		public void AddRemoveGPoint(int x, int y)
		{
			(bool? remove, int indx) = ToRemove(GPoints, x, y);
			if (!remove.HasValue)
			{
				GPoints[indx] = new Point(x, y);
			}
			else
			{
				if (remove.Value)
				{
					GPoints.RemoveAt(indx);
				}
				if (!remove.Value)
				{
					int index = FindIndexToInsert(GPoints, x);
					GPoints.Insert(index, new Point(x, y));
				}
			}
			DrawGPolyline();
		}
		public void AddRemoveBPoint(int x, int y)
		{
			(bool? remove, int indx) = ToRemove(BPoints, x, y);
			if (!remove.HasValue)
			{
				BPoints[indx] = new Point(x, y);
			}
			else
			{
				if (remove.Value)
				{
					BPoints.RemoveAt(indx);
				}
				if (!remove.Value)
				{
					int index = FindIndexToInsert(BPoints, x);
					BPoints.Insert(index, new Point(x, y));
				}
			}
			DrawBPolyline();
		}
		private int FindIndexToInsert(List<Point> points, int x)
		{
			for(int i= points.Count-1; i>=0;i--)
			{
				if (x > points[i].X) return i+1;
			}
			return points.Count - 1;
		}
		private (bool?, int) ToRemove(List<Point> points, int x, int y)
		{
			int i = 0;
			foreach(Point p in points)
			{
				if (x == p.X && (y>p.Y+7 || y<p.Y-7))
					return (null, i);
				if (x > p.X - 7 && x < p.X + 7 && y > p.Y - 7 && y < p.Y + 7) return (true, i);
				i++;
			}
			return (false,0);
		}
		public void SetRPointToMove(int x, int y)
		{
			int i = 0;
			foreach (Point p in RPoints)
			{
				if (x > p.X - 7 && x < p.X + 7 && y > p.Y - 7 && y < p.Y + 7)
				{
					RpointToMove = i;
					move = true;
					return;
				}
				i++;
			}
			move = false;
		}
		public void SetGPointToMove(int x, int y)
		{
			int i = 0;
			foreach (Point p in GPoints)
			{
				if (x > p.X - 7 && x < p.X + 7 && y > p.Y - 7 && y < p.Y + 7)
				{
					GpointToMove = i;
					move = true;
					return;
				}
				i++;
			}
			move = false;
		}
		public void SetBPointToMove(int x, int y)
		{
			int i = 0;
			foreach (Point p in BPoints)
			{
				if (x > p.X - 7 && x < p.X + 7 && y > p.Y - 7 && y < p.Y + 7)
				{
					BpointToMove = i;
					move = true;
					return;
				}
				i++;
			}
			move = false;
		}
		public void MoveRPoint(int y)
		{
			if (y >= 0 && y < 124)
			{
				RPoints[RpointToMove] = new Point(RPoints[RpointToMove].X, y);
				DrawRPolyline();
			}
		}
		public void MoveGPoint(int y)
		{
			if (y >= 0 && y < 124)
			{
				GPoints[GpointToMove] = new Point(GPoints[GpointToMove].X, y);
				DrawGPolyline();
			}
		}
		public void MoveBPoint(int y)
		{
			if (y >= 0 && y < 124)
			{
				BPoints[BpointToMove] = new Point(BPoints[BpointToMove].X, y);
				DrawBPolyline();
			}
		}
		public int[] GetRFunction()
		{
			int[] R = new int[256];
			List<(PointF e, int x)> equations = new List<(PointF e, int x)>();
			for (int i = 0; i < RPoints.Count - 1; i++)
			{
				float a = (float)(RPoints[i + 1].Y - RPoints[i].Y) / (RPoints[i + 1].X - RPoints[i].X);
				float b = RPoints[i].Y - a * RPoints[i].X;
				equations.Add((new PointF(a, b), RPoints[i + 1].X));
			}
			int k = 0;
			for (int i = 0; i < 256; i++)
			{
				if (i*(160.0/255) > equations[k].x) k++;
				float a = equations[k].e.X;
				float b = equations[k].e.Y;
				R[255-i] = (int)((255.0 / 123) * ((160.0 / 255) * a * i + b));
			}
			return R;
		}
		public int[] GetGFunction()
		{
			int[] G = new int[256];
			List<(PointF e, int x)> equations = new List<(PointF e, int x)>();
			for (int i = 0; i < GPoints.Count - 1; i++)
			{
				float a = (float)(GPoints[i + 1].Y - GPoints[i].Y) / (GPoints[i + 1].X - GPoints[i].X);
				float b = GPoints[i].Y - a * GPoints[i].X;
				equations.Add((new PointF(a, b), GPoints[i + 1].X));
			}
			int k = 0;
			for (int i = 0; i < 256; i++)
			{
				if (i * (160.0 / 255) > equations[k].x) k++;
				float a = equations[k].e.X;
				float b = equations[k].e.Y;
				G[255-i] = (int)((255.0 / 123) * ((160.0 / 255) * a * i + b));
			}
			return G;
		}
		public int[] GetBFunction()
		{
			int[] B = new int[256];
			List<(PointF e, int x)> equations = new List<(PointF e, int x)>();
			for (int i = 0; i < BPoints.Count - 1; i++)
			{
				float a = (float)(BPoints[i + 1].Y - BPoints[i].Y) / (BPoints[i + 1].X - BPoints[i].X);
				float b = BPoints[i].Y - a * BPoints[i].X;
				equations.Add((new PointF(a, b), BPoints[i + 1].X));
			}
			int k = 0;
			for (int i = 0; i < 256; i++)
			{
				if (i * (160.0 / 255) > equations[k].x) k++;
				float a = equations[k].e.X;
				float b = equations[k].e.Y;
				B[255-i] = (int)((255.0 / 123) * ((160.0 / 255) * a * i + b));
			}
			return B;
		}
	}
}
