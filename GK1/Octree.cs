﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GK1
{
	class Octree
	{
		public uint Count;
		public uint R,G,B;
		public bool IsLeaf = false;
		Octree[] child;
		public Octree()
		{
			Count = 0;
			child = new Octree[8];
		}
		public Octree this[int key]
		{
			get
			{
				if (key >= 0 && key <= 7)
					return child[key];
				else return null;
			}
			set
			{
				if (key >= 0 && key <= 7)
					child[key] = value;
			}
		}
	}
	static class OctreeBuilder
	{
		private static List<(Octree tree, uint sum)> NodesToReduce;
		private static uint smallestSum = uint.MaxValue;
		public static void Insert(Octree t, Color c, int l)
		{
			if(l==0)
			{
				t.Count++;
				t.R += c.R;
				t.G += c.G;
				t.B += c.B;
				t.IsLeaf = true;
				return;
			}
			int Rbit = (c.R & (1 << l)) == 0 ? 0 : 1;
			int Gbit = (c.G & (1 << l)) == 0 ? 0 : 1;
			int Bbit = (c.B & (1 << l)) == 0 ? 0 : 1;
			int index = 4*Rbit + 2*Gbit + Bbit;
			if (t[index] == null) t[index] = new Octree();
			Insert(t[index], c, l - 1);
		}
		public static Color ReducedColor(Color c, Octree tree, int l)
		{
			int Rbit = (c.R & (1 << l)) == 0 ? 0 : 1;
			int Gbit = (c.G & (1 << l)) == 0 ? 0 : 1;
			int Bbit = (c.B & (1 << l)) == 0 ? 0 : 1;
			int index = 4 * Rbit + 2 * Gbit + Bbit;
			if (tree[index] != null)
			{
				return ReducedColor(c, tree[index],l-1);
			}
			else return Color.FromArgb((int)tree.R, (int)tree.G, (int)tree.B);
		}
		public static Octree PrepareOctree(List<List<(int from, int to)>> lines, Bitmap bitmap)
		{
			Octree tree = new Octree();
			for (int y = 0; y < lines.Count; y++)
			{
				for (int p = 0; p < lines[y].Count; p++)
				{
					for (int j = lines[y][p].from; j <= lines[y][p].to; j++)
					{
						Color c = bitmap.GetPixel(j, y);
						Insert(tree, c, 7);
					}
				}
			}
			return tree;
		}
		public static Octree ReduceToK(Octree tree, int k)
		{
			int leaves = CountLeaves(tree);
			while(leaves>k)
			{
				NodesToReduce = new List<(Octree tree, uint sum)>();
				FindPotentialNodesToReduce(tree);
				smallestSum = uint.MaxValue;
				Octree ToReduce=null;
				uint min = uint.MaxValue;
				foreach(var t in NodesToReduce)
				{
					if(t.sum<min)
					{
						ToReduce = t.tree;
						min = t.sum;
					}
				}
				leaves -= CountReducedLeaves(ToReduce);
				ReduceNode(ToReduce);
			}
			RebuildPalette(tree);
			return tree;
		}
		private static void RebuildPalette(Octree tree)
		{
			if (tree == null) return;
			if (tree.Count > 0)
			{
				tree.R /= tree.Count;
				tree.G /= tree.Count;
				tree.B /= tree.Count;
			}
			for(int i=0;i<8;i++)
			{
				if(tree[i]!=null)
				{
					RebuildPalette(tree[i]);
				}
			}
		}
		private static void FindPotentialNodesToReduce(Octree tree)
		{
			bool hasLeaves = false;
			uint sum = 0;
			for (int i = 0; i < 8; i++)
			{
				if (tree[i] != null)
				{
					if (tree[i].IsLeaf)
					{
						hasLeaves = true;
						sum += tree[i].Count;
					}
				}
			}
			if (hasLeaves && sum > 0 && sum < smallestSum)
			{
				smallestSum = sum;
				NodesToReduce.Add((tree,sum));
				if (smallestSum == 1) return;
			}
			for (int i = 0; i < 8; i++)
			{
				if (tree[i] != null)
				{
					FindPotentialNodesToReduce(tree[i]);
				}
			}
		}
		private static void ReduceNode(Octree tree)
		{
			if (tree == null) return;
			for (int i = 0; i < 8; i++)
			{
				if (tree[i]!=null)
				{
					if (tree[i].IsLeaf)
					{
						tree.Count += tree[i].Count;
						tree.R += tree[i].R;
						tree.G += tree[i].G;
						tree.B += tree[i].B;
						tree[i] = null;
					}
				}
			}
			bool leaf = true;
			for (int i = 0; i < 8; i++)
			{
				if (tree[i] != null)
				{
					leaf = false;
				}
			}
			tree.IsLeaf = leaf;
		}
		private static int CountLeaves(Octree tree)
		{
			int sum = 0;
			if (tree == null) return 0;

			bool leaf=true;
			for(int i=0;i<8;i++)
			{
				if (tree[i] != null)
				{
					leaf = false;
					sum += CountLeaves(tree[i]);
				}
			}
			if (leaf) sum++;
			return sum;
		}
		private static int CountReducedLeaves(Octree tree)
		{
			int sum = 0;
			bool leavesOnly=true;
			for(int i=0;i<8;i++)
			{
				if(tree[i]!=null)
				{
					if(tree[i].IsLeaf)
					{
						sum++;
					}
					else
					{
						leavesOnly = false;
					}
				}
			}
			if (leavesOnly)
				sum--;
			return sum;
		}
	}
}
